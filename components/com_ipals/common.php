<?php

/**
 * Some common functions used in multiple controllers
 */
class Common
{
	public function loadAmazonS3($settings)
	{
		$CI =& get_instance();

		if($settings->amazonS3)
		{
			$S3_ACCESS_KEY = 'AKIAIKTP2QVWVL2YNYHA';
			$S3_SECRET_KEY = '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';

			if(!isset($CI->S3)) $CI->load->library('S3');
			return new S3($S3_ACCESS_KEY, $S3_SECRET_KEY, false, $settings->amazonEndPoint);
		}

		return false;
	}

	public function loadSettings()
	{
		$CI =& get_instance();
		
		$sts = $CI->db->get('ipals_settings')->result();

		$settings = new stdClass;
		foreach($sts as $st)
		{
			$settings->{$st->key} = $st->value;
		}

		// need to expand some variables
		$settings->propertyClasses    = @json_decode($settings->propertyClasses, true);
		$settings->openHouseClasses   = @json_decode($settings->openHouseClasses, true);
		$settings->cleanupClasses     = @json_decode($settings->cleanupClasses, true);
		$settings->statusDeleteValues = @json_decode($settings->statusDeleteValues, true);
		$settings->statusAcceptValues = @json_decode($settings->statusAcceptValues, true);
		$settings->cleanupTypes       = @json_decode($settings->cleanupTypes, true);

		return $settings;
	}

	public function verifyLicense($settings)
	{
		$results = $this->ipals20CheckLicense($settings->licenseKey, $settings->local_key);

		// Check for errors, if we weren't successful, DO NOT 
		// continue the script. Die here.
		switch ($results['status']) 
		{
			case 'Active':
				// get new local key and save it somewhere?
				// NYI: do we need it for iPALS?
				$localkeydata = $results['localkey'];
				break;
			case 'Invalid':
				die('License key is Invalid');
			case 'Expired':
				die('License key is Expired');
			case 'Suspended':
				die('License key is Suspended');
			default:
				die('Invalid Response');
		}	
	}

	public function humanize($str)
	{
		$j = 0;
		$return = array();
		for($i = 0; $i < strlen($str); $i++) 
		{ 
			$char = $str[$i]; 
			if(preg_match("/[A-Z]/", $char))
			{ 
				@$return[++$j] .= $char; 
			} 
			else 
			{ 
				@$return[$j] .= $char; 
			} 
		} 
		
		return implode(' ', array_map('ucwords', $return));
	}

	function ipals20CheckLicense($licensekey, $localkey) 
	{
		// Enter the url to your WHMCS installation here
		$whmcsurl = 'https://portal.mostwantedwebhosting.com/';
		// Must match what is specified in the MD5 Hash Verification field
		// of the licensing product that will be used with this check.
		$licensing_secret_key = 'Jordan#2014$';
		// The number of days to wait between performing remote license checks
		$localkeydays = 15;
		// The number of days to allow failover for after local key expiry
		$allowcheckfaildays = 5;

		// -----------------------------------
		//  -- Do not edit below this line --
		// -----------------------------------

		$check_token = time() . md5(mt_rand(1000000000, 9999999999) . $licensekey);
		$checkdate = date("Ymd");
		$domain = $_SERVER['SERVER_NAME'];
		$usersip = isset($_SERVER['SERVER_ADDR']) ? $_SERVER['SERVER_ADDR'] : $_SERVER['LOCAL_ADDR'];
		$dirpath = dirname(__FILE__);
		$verifyfilepath = 'modules/servers/licensing/verify.php';
		$localkeyvalid = false;
		if ($localkey) {
			$localkey = str_replace("\n", '', $localkey); # Remove the line breaks
			$localdata = substr($localkey, 0, strlen($localkey) - 32); # Extract License Data
			$md5hash = substr($localkey, strlen($localkey) - 32); # Extract MD5 Hash
			if ($md5hash == md5($localdata . $licensing_secret_key)) {
				$localdata = strrev($localdata); # Reverse the string
				$md5hash = substr($localdata, 0, 32); # Extract MD5 Hash
				$localdata = substr($localdata, 32); # Extract License Data
				$localdata = base64_decode($localdata);
				$localkeyresults = unserialize($localdata);
				$originalcheckdate = $localkeyresults['checkdate'];
				if ($md5hash == md5($originalcheckdate . $licensing_secret_key)) {
					$localexpiry = date("Ymd", mktime(0, 0, 0, date("m"), date("d") - $localkeydays, date("Y")));
					if ($originalcheckdate > $localexpiry) {
						$localkeyvalid = true;
						$results = $localkeyresults;
						$validdomains = explode(',', $results['validdomain']);
						if (!in_array($_SERVER['SERVER_NAME'], $validdomains)) {
							$localkeyvalid = false;
							$localkeyresults['status'] = "Invalid";
							$results = array();
						}
						$validips = explode(',', $results['validip']);
						if (!in_array($usersip, $validips)) {
							$localkeyvalid = false;
							$localkeyresults['status'] = "Invalid";
							$results = array();
						}
						$validdirs = explode(',', $results['validdirectory']);
						if (!in_array($dirpath, $validdirs)) {
							$localkeyvalid = false;
							$localkeyresults['status'] = "Invalid";
							$results = array();
						}
					}
				}
			}
		}
		if (!$localkeyvalid) {
			$postfields = array(
				'licensekey' => $licensekey,
				'domain' => $domain,
				'ip' => $usersip,
				'dir' => $dirpath,
			);
			if ($check_token) $postfields['check_token'] = $check_token;
			$query_string = '';
			foreach ($postfields AS $k=>$v) {
				$query_string .= $k.'='.urlencode($v).'&';
			}
			if (function_exists('curl_exec')) {
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $whmcsurl . $verifyfilepath);
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $query_string);
				curl_setopt($ch, CURLOPT_TIMEOUT, 30);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				$data = curl_exec($ch);
				curl_close($ch);
			} else {
				$fp = fsockopen($whmcsurl, 80, $errno, $errstr, 5);
				if ($fp) {
					$newlinefeed = "\r\n";
					$header = "POST ".$whmcsurl . $verifyfilepath . " HTTP/1.0" . $newlinefeed;
					$header .= "Host: ".$whmcsurl . $newlinefeed;
					$header .= "Content-type: application/x-www-form-urlencoded" . $newlinefeed;
					$header .= "Content-length: ".@strlen($query_string) . $newlinefeed;
					$header .= "Connection: close" . $newlinefeed . $newlinefeed;
					$header .= $query_string;
					$data = '';
					@stream_set_timeout($fp, 20);
					@fputs($fp, $header);
					$status = @socket_get_status($fp);
					while (!@feof($fp)&&$status) {
						$data .= @fgets($fp, 1024);
						$status = @socket_get_status($fp);
					}
					@fclose ($fp);
				}
			}
			if (!$data) {
				$localexpiry = date("Ymd", mktime(0, 0, 0, date("m"), date("d") - ($localkeydays + $allowcheckfaildays), date("Y")));
				if ($originalcheckdate > $localexpiry) {
					$results = $localkeyresults;
				} else {
					$results = array();
					$results['status'] = "Invalid";
					$results['description'] = "Remote Check Failed";
					return $results;
				}
			} else {
				preg_match_all('/<(.*?)>([^<]+)<\/\\1>/i', $data, $matches);
				$results = array();
				foreach ($matches[1] AS $k=>$v) {
					$results[$v] = $matches[2][$k];
				}
			}
			if (!is_array($results)) {
				die("Invalid License Server Response");
			}
			if ($results['md5hash']) {
				if ($results['md5hash'] != md5($licensing_secret_key . $check_token)) {
					$results['status'] = "Invalid";
					$results['description'] = "MD5 Checksum Verification Failed";
					return $results;
				}
			}
			if ($results['status'] == "Active") {
				$results['checkdate'] = $checkdate;
				$data_encoded = serialize($results);
				$data_encoded = base64_encode($data_encoded);
				$data_encoded = md5($checkdate . $licensing_secret_key) . $data_encoded;
				$data_encoded = strrev($data_encoded);
				$data_encoded = $data_encoded . md5($data_encoded . $licensing_secret_key);
				$data_encoded = wordwrap($data_encoded, 80, "\n", true);
				$results['localkey'] = $data_encoded;
			}
			$results['remotecheck'] = true;
		}
		unset($postfields,$data,$matches,$whmcsurl,$licensing_secret_key,$checkdate,$usersip,$localkeydays,$allowcheckfaildays,$md5hash);
		return $results;
	}
}