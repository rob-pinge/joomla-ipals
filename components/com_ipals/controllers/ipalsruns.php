<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		3.0.0
	@build			21st September, 2017
	@created		16th August, 2017
	@package		iPALS
	@subpackage		settings.php
	@author			Most Wanted Web Services, Inc. <http://retsforealestate.com>	
	@copyright		Copyright (C) 2015. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
  ____  _____  _____  __  __  __      __       ___  _____  __  __  ____  _____  _  _  ____  _  _  ____ 
 (_  _)(  _  )(  _  )(  \/  )(  )    /__\     / __)(  _  )(  \/  )(  _ \(  _  )( \( )( ___)( \( )(_  _)
.-_)(   )(_)(  )(_)(  )    (  )(__  /(__)\   ( (__  )(_)(  )    (  )___/ )(_)(  )  (  )__)  )  (   )(  
\____) (_____)(_____)(_/\/\_)(____)(__)(__)   \___)(_____)(_/\/\_)(__)  (_____)(_)\_)(____)(_)\_) (__) 

/------------------------------------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla controlleradmin library
jimport('joomla.application.component.controlleradmin');

/**
 * Settings Controller
 */
class IpalsControllerIpalsruns extends IpalsController
{
	protected $text_prefix = 'COM_IPALS_SETTINGS';
	/**
	 * Proxy for getModel.
	 * @since	2.5
	 */
	public function getModel($name = 'Setting', $prefix = 'IpalsModel', $config = array())
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));
		
		return $model;
	}

	public function exportData()
	{
		// [Interpretation 7719] Check for request forgeries
		JSession::checkToken() or die(JText::_('JINVALID_TOKEN'));
		// [Interpretation 7721] check if export is allowed for this user.
		$user = JFactory::getUser();
		if ($user->authorise('setting.export', 'com_ipals') && $user->authorise('core.export', 'com_ipals'))
		{
			// [Interpretation 7725] Get the input
			$input = JFactory::getApplication()->input;
			$pks = $input->post->get('cid', array(), 'array');
			// [Interpretation 7728] Sanitize the input
			JArrayHelper::toInteger($pks);
			// [Interpretation 7730] Get the model
			$model = $this->getModel('Settings');
			// [Interpretation 7732] get the data to export
			$data = $model->getExportData($pks);
			if (IpalsHelper::checkArray($data))
			{
				// [Interpretation 7736] now set the data to the spreadsheet
				$date = JFactory::getDate();
				IpalsHelper::xls($data,'Settings_'.$date->format('jS_F_Y'),'Settings exported ('.$date->format('jS F, Y').')','settings');
			}
		}
		// [Interpretation 7741] Redirect to the list screen with error.
		$message = JText::_('COM_IPALS_EXPORT_FAILED');
		$this->setRedirect(JRoute::_('index.php?option=com_ipals&view=settings', false), $message, 'error');
		return;
	}


	public function importData()
	{
		// [Interpretation 7750] Check for request forgeries
		JSession::checkToken() or die(JText::_('JINVALID_TOKEN'));
		// [Interpretation 7752] check if import is allowed for this user.
		$user = JFactory::getUser();
		if ($user->authorise('setting.import', 'com_ipals') && $user->authorise('core.import', 'com_ipals'))
		{
			// [Interpretation 7756] Get the import model
			$model = $this->getModel('Settings');
			// [Interpretation 7758] get the headers to import
			$headers = $model->getExImPortHeaders();
			if (IpalsHelper::checkObject($headers))
			{
				// [Interpretation 7762] Load headers to session.
				$session = JFactory::getSession();
				$headers = json_encode($headers);
				$session->set('setting_VDM_IMPORTHEADERS', $headers);
				$session->set('backto_VDM_IMPORT', 'settings');
				$session->set('dataType_VDM_IMPORTINTO', 'setting');
				// [Interpretation 7768] Redirect to import view.
				$message = JText::_('COM_IPALS_IMPORT_SELECT_FILE_FOR_SETTINGS');
				$this->setRedirect(JRoute::_('index.php?option=com_ipals&view=import', false), $message);
				return;
			}
		}
		// [Interpretation 7788] Redirect to the list screen with error.
		$message = JText::_('COM_IPALS_IMPORT_FAILED');
		$this->setRedirect(JRoute::_('index.php?option=com_ipals&view=settings', false), $message, 'error');
		return;
	}  
}
