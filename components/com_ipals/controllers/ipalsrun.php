<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		3.0.0
	@build			21st September, 2017
	@created		16th August, 2017
	@package		iPALS
	@subpackage		setting.php
	@author			Most Wanted Web Services, Inc. <http://retsforealestate.com>	
	@copyright		Copyright (C) 2015. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
  ____  _____  _____  __  __  __      __       ___  _____  __  __  ____  _____  _  _  ____  _  _  ____ 
 (_  _)(  _  )(  _  )(  \/  )(  )    /__\     / __)(  _  )(  \/  )(  _ \(  _  )( \( )( ___)( \( )(_  _)
.-_)(   )(_)(  )(_)(  )    (  )(__  /(__)\   ( (__  )(_)(  )    (  )___/ )(_)(  )  (  )__)  )  (   )(  
\____) (_____)(_____)(_/\/\_)(____)(__)(__)   \___)(_____)(_/\/\_)(__)  (_____)(_)\_)(____)(_)\_) (__) 

/------------------------------------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla controllerform library
jimport('joomla.application.component.controllerform');

/**
 * Setting Controller
 */
class IpalsControllerIpalsrun extends IpalsController
{
	/**
	 * Current or most recently performed task.
	 *
	 * @var    string
	 * @since  12.2
	 * @note   Replaces _task.
	 */
	protected $task;

	public function __construct($config = array())
	{
		$this->view_list = 'Ipalsrun'; // safeguard for setting the return view listing to the main view.
		parent::__construct($config);
	}

        /**
	 * Method override to check if you can add a new record.
	 *
	 * @param   array  $data  An array of input data.
	 *
	 * @return  boolean
	 *
	 * @since   1.6
	 */
	protected function allowAdd($data = array())
	{
		// [Interpretation 9795] Access check.
		$access = JFactory::getUser()->authorise('setting.access', 'com_ipals');
		if (!$access)
		{
			return false;
		}
		// [Interpretation 9812] In the absense of better information, revert to the component permissions.
		return parent::allowAdd($data);
	}

	/**
	 * Method override to check if you can edit an existing record.
	 *
	 * @param   array   $data  An array of input data.
	 * @param   string  $key   The name of the key for the primary key.
	 *
	 * @return  boolean
	 *
	 * @since   1.6
	 */
	protected function allowEdit($data = array(), $key = 'id')
	{
		$user		= JFactory::getUser();
		$recordId	= (int) isset($data[$key]) ? $data[$key] : 0;

		if ($recordId)
		{
			$permission = $user->authorise('core.edit', 'com_ipals.ipalsrun.' . (int) $recordId);
			if (!$permission)
			{
				if ($user->authorise('core.edit.own', 'com_ipals.ipalsrun.' . $recordId))
				{
					$ownerId = (int) isset($data['created_by']) ? $data['created_by'] : 0;
					if (empty($ownerId))
					{
						$record = $this->getModel()->getItem($recordId);

						if (empty($record))
						{
							return false;
						}
						$ownerId = $record->created_by;
					}

					if ($ownerId == $user->id)
					{
						if ($user->authorise('core.edit.own', 'com_ipals'))
						{
							return true;
						}
					}
				}
				return false;
			}
		}

		return parent::allowEdit($data, $key);
	}

	/**
	 * Gets the URL arguments to append to an item redirect.
	 *
	 * @param   integer  $recordId  The primary key id for the item.
	 * @param   string   $urlVar    The name of the URL variable for the id.
	 *
	 * @return  string  The arguments to append to the redirect URL.
	 *
	 * @since   12.2
	 */
	protected function getRedirectToItemAppend($recordId = null, $urlVar = 'id')
	{
		$tmpl   = $this->input->get('tmpl');
		$layout = $this->input->get('layout', 'edit', 'string');

		$ref 	= $this->input->get('ref', 0, 'string');
		$refid 	= $this->input->get('refid', 0, 'int');

		// Setup redirect info.

		$append = '';

		if ($refid)
                {
			$append .= '&ref='.(string)$ref.'&refid='.(int)$refid;
		}
                elseif ($ref)
                {
			$append .= '&ref='.(string)$ref;
                }

		if ($tmpl)
		{
			$append .= '&tmpl=' . $tmpl;
		}

		if ($layout)
		{
			$append .= '&layout=' . $layout;
		}

		if ($recordId)
		{
			$append .= '&' . $urlVar . '=' . $recordId;
		}

		return $append;
	}

	/**
	 * Method to run batch operations.
	 *
	 * @param   object  $model  The model.
	 *
	 * @return  boolean   True if successful, false otherwise and internal error is set.
	 *
	 * @since   2.5
	 */
	public function batch($model = null)
	{
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		// Set the model
		$model = $this->getModel('Setting', '', array());

		// Preset the redirect
		$this->setRedirect(JRoute::_('index.php?option=com_ipals&view=settings' . $this->getRedirectToListAppend(), false));

		return parent::batch($model);
	}

	/**
	 * Method to cancel an edit.
	 *
	 * @param   string  $key  The name of the primary key of the URL variable.
	 *
	 * @return  boolean  True if access level checks pass, false otherwise.
	 *
	 * @since   12.2
	 */
	public function cancel($key = null)
	{
		// get the referal details
		$this->ref 		= $this->input->get('ref', 0, 'word');
		$this->refid 	= $this->input->get('refid', 0, 'int');

		$cancel = parent::cancel($key);

		if ($cancel)
		{
			if ($this->refid)
			{
				$redirect = '&view='.(string)$this->ref.'&layout=edit&id='.(int)$this->refid;

				// Redirect to the item screen.
				$this->setRedirect(
					JRoute::_(
						'index.php?option=' . $this->option . $redirect, false
					)
				);
			}
			elseif ($this->ref)
			{
				$redirect = '&view='.(string)$this->ref;

				// Redirect to the list screen.
				$this->setRedirect(
					JRoute::_(
						'index.php?option=' . $this->option . $redirect, false
					)
				);
			}
		}
		else
		{
			// Redirect to the items screen.
			$this->setRedirect(
				JRoute::_(
					'index.php?option=' . $this->option . '&view=' . $this->view_list, false
				)
			);
		}
		return $cancel;
	}

	/**
	 * Method to save a record.
	 *
	 * @param   string  $key     The name of the primary key of the URL variable.
	 * @param   string  $urlVar  The name of the URL variable if different from the primary key (sometimes required to avoid router collisions).
	 *
	 * @return  boolean  True if successful, false otherwise.
	 *
	 * @since   12.2
	 */
	public function save($key = null, $urlVar = null)
	{
		// get the referal details
		$this->ref 		= $this->input->get('ref', 0, 'word');
		$this->refid 	= $this->input->get('refid', 0, 'int');

                if ($this->ref || $this->refid)
                {
                        // to make sure the item is checkedin on redirect
                        $this->task = 'save';
                }

		$saved = parent::save($key, $urlVar);

		if ($this->refid && $saved)
		{
			$redirect = '&view='.(string)$this->ref.'&layout=edit&id='.(int)$this->refid;

			// Redirect to the item screen.
			$this->setRedirect(
				JRoute::_(
					'index.php?option=' . $this->option . $redirect, false
				)
			);
		}
		elseif ($this->ref && $saved)
		{
			$redirect = '&view='.(string)$this->ref;

			// Redirect to the list screen.
			$this->setRedirect(
				JRoute::_(
					'index.php?option=' . $this->option . $redirect, false
				)
			);
		}
		return $saved;
	}

	/**
	 * Function that allows child controller access to model data
	 * after the data has been saved.
	 *
	 * @param   JModel  &$model     The data model object.
	 * @param   array   $validData  The validated data.
	 *
	 * @return  void
	 *
	 * @since   11.1
	 */
	protected function postSaveHook(JModelLegacy $model, $validData = array())
	{
		return;
	}

	public function runManually()
	{
		global $settings, $queryFields;
		$this->load->library('common');
		$settings = $this->settings = $this->common->loadSettings();

		$this->load->helper('url');
		$this->load->library(array('core/resource'));

		$this->load->plugin_library(array('property_resource', 'agent_resource', 'office_resource', 'open_house_resource'), $this->settings->plugin);
		
		// this exits if license did not verify
		if($_SERVER['SERVER_NAME'] != 'ipals.cam')
			$this->common->verifyLicense($settings);

		// ==========================================
		// Looking for the Amazon S3 settings? 
		// They've been moved to libraries/common.php
		// ==========================================
		$this->S3 = $this->common->loadAmazonS3($settings);
		
		// TODO get these from DB
		$resources = array();
		if(!@$this->settings->idx)
		{
			$queryFields = $this->queryFields = $this->loadQueryFields();
			$this->phrets->login();
			
			// IDX does not yet implement anything other than property
			$resources = array('office', 'agent');
		}
			
		$this->load->helper('inflector');
		foreach($resources as $resource)
		{
			// underscore-ize our resource to access it via CI
			$_resource = underscore($this->common->humanize($resource));

			// if we haven't set a Resource in the settings, skip this
			if(empty($this->settings->{$resource . 'Resource'})) continue;
			
			$this->{$_resource . '_resource'}->init($this->settings->{$resource . 'Resource'}, $this->settings->{$resource . 'Class'});

			// we may still need office_resource, even thought we don't want it to process for EZ Realty
			if(empty($this->settings->{$resource . 'Resource'})) continue;
			$this->{$_resource . '_resource'}->process();
		}
				
		for($this->settings->batchLimit; $this->settings->batchLimit > 0; $this->settings->batchLimit--)
		{					
			// property's a special case
			foreach($this->settings->propertyClasses as $subType => $class)
			{
				if(empty($class)) continue;
				
				// subtype's always the same in offce & agent, but not in property
				$extra = array('subType' => $subType);
				$this->property_resource->init($this->settings->propertyResource, $class, $extra);
				$this->property_resource->process();			
			}
			
			// we want to process open houses after properties, but in the same timeframe (i.e. with batches)
			if(!empty($this->settings->openHouseClasses) && !empty($this->settings->openHouseResource))
			{
				foreach($this->settings->openHouseClasses as $subType => $class)
				{
					if(empty($class)) continue;

					$extra = array('subType' => $subType);
					$this->open_house_resource->init($this->settings->openHouseResource, $class, $extra);
					if(!empty($this->open_house_resource->table))
					{
						$this->open_house_resource->process();	
					}
				}
			}

			$this->updateLastRun();	
		}

		if(@$this->settings->cleanupScript)
		{
			// this is pretty hacky... probably should go into a lib?
			require_once(APPPATH . 'controllers/cleanup.php');
			$cleanup = new Cleanup();
			$cleanup->index();
		}
	}

}
