<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		3.0.0
	@build			21st September, 2017
	@created		16th August, 2017
	@package		iPALS
	@subpackage		ipalsrun.php
	@author			Most Wanted Web Services, Inc. <http://retsforealestate.com>	
	@copyright		Copyright (C) 2015. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
  ____  _____  _____  __  __  __      __       ___  _____  __  __  ____  _____  _  _  ____  _  _  ____ 
 (_  _)(  _  )(  _  )(  \/  )(  )    /__\     / __)(  _  )(  \/  )(  _ \(  _  )( \( )( ___)( \( )(_  _)
.-_)(   )(_)(  )(_)(  )    (  )(__  /(__)\   ( (__  )(_)(  )    (  )___/ )(_)(  )  (  )__)  )  (   )(  
\____) (_____)(_____)(_/\/\_)(____)(__)(__)   \___)(_____)(_/\/\_)(__)  (_____)(_)\_)(____)(_)\_) (__) 

/------------------------------------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import the Joomla modellist library
jimport('joomla.application.component.modellist');

/**
 * Ipals Model for Ipalsrun
 */
class IpalsModelIpalsrun extends JModelList
{
	/**
	 * Model user data.
	 *
	 * @var        strings
	 */
	protected $user;
	protected $userId;
	protected $guest;
	protected $groups;
	protected $levels;
	protected $app;
	protected $input;
	protected $uikitComp;

	/**
	 * Method to build an SQL query to load the list data.
	 *
	 * @return      string  An SQL query
	 */
	protected function getListQuery()
	{
		// Get the current user for authorisation checks
		$this->user		= JFactory::getUser();
		$this->userId		= $this->user->get('id');
		$this->guest		= $this->user->get('guest');
        $this->groups		= $this->user->get('groups');
        $this->authorisedGroups	= $this->user->getAuthorisedGroups();
		$this->levels		= $this->user->getAuthorisedViewLevels();
		$this->app		= JFactory::getApplication();
		$this->input		= $this->app->input;
		$this->initSet		= true; 
		// [Interpretation 2669] Get a db connection.
		$db = JFactory::getDbo();

		// [Interpretation 2678] Create a new query object.
		$query = $db->getQuery(true);

		// [Interpretation 1430] Get from #__ipals_field as a
		$query->select($db->quoteName(
			array('a.id','a.asset_id','a.settingsid','a.field','a.key','a.actionfields','a.fieldstype','a.description','a.published','a.created_by','a.modified_by','a.created','a.modified','a.version','a.hits','a.ordering'),
			array('id','asset_id','settingsid','field','key','actionfields','fieldstype','description','published','created_by','modified_by','created','modified','version','hits','ordering')));
		$query->from($db->quoteName('#__ipals_field', 'a'));
		// [Interpretation 1775] Check if JRequest::getInt('id') is a string or numeric value.
		$checkValue = JRequest::getInt('id');
		if (isset($checkValue) && IpalsHelper::checkString($checkValue))
		{
			$query->where('a.settingid = ' . $db->quote($checkValue));
		}
		elseif (is_numeric($checkValue))
		{
			$query->where('a.settingid = ' . $checkValue);
		}
		else
		{
			return false;
		}
		$query->where('a.access IN (' . implode(',', $this->levels) . ')');
		// [Interpretation 1923] Get where a.published is 1
		$query->where('a.published = 1');

		// [Interpretation 2693] return the query object
		return $query;
	}

	/**
	 * Method to get an array of data items.
	 *
	 * @return  mixed  An array of data items on success, false on failure.
	 */
	public function getItems()
	{
		$user = JFactory::getUser();
		// [Interpretation 2019] check if this user has permission to access item
		if (!$user->authorise('site.ipalsrun.access', 'com_ipals'))
		{
			$app = JFactory::getApplication();
			$app->enqueueMessage(JText::_('COM_IPALS_NOT_AUTHORISED_TO_VIEW_IPALSRUN'), 'error');
			// [Interpretation 2016] redirect away to the home page if no access allowed.
			$app->redirect(JURI::root());
			return false;
		}  
		// load parent items
		$items = parent::getItems();

		// Get the global params
		$globalParams = JComponentHelper::getParams('com_ipals', true);

		// [Interpretation 2714] Convert the parameter fields into objects.
		if (IpalsHelper::checkArray($items))
		{
			foreach ($items as $nr => &$item)
			{
				// [Interpretation 2719] Always create a slug for sef URL's
				$item->slug = (isset($item->alias) && isset($item->id)) ? $item->id.':'.$item->alias : $item->id;
				// [Interpretation 1680] set settingidSettingsidB to the $item object.
				$item->settingidSettingsidB = $this->getSettingidSettingsidAded_B($item->);
				// [Interpretation 1680] set settingidSettingsidC to the $item object.
				$item->settingidSettingsidC = $this->getSettingidSettingsidAded_C($item->);
				// [Interpretation 1680] set settingidSettingsidD to the $item object.
				$item->settingidSettingsidD = $this->getSettingidSettingsidAded_D($item->);
			}
		} 

		// return items
		return $items;
	} 

	/**
	* Method to get an array of  Objects.
	*
	* @return mixed  An array of  Objects on success, false on failure.
	*
	*/
	public function getSettingidSettingsidAded_B($settingid)
	{
		// [Interpretation 2445] Get a db connection.
		$db = JFactory::getDbo();

		// [Interpretation 2447] Create a new query object.
		$query = $db->getQuery(true);

		// [Interpretation 2449] Get from #__ipals_ as b
		$query->select($db->quoteName(
			array('b.id','b.asset_id','b.settingsid','b.key','b.value','b.group','b.auto','b.querytype','b.description','b.published','b.created_by','b.modified_by','b.created','b.modified','b.version','b.hits','b.ordering'),
			array('id','asset_id','settingsid','key','value','group','auto','querytype','description','published','created_by','modified_by','created','modified','version','hits','ordering')));
		$query->from($db->quoteName('#__ipals_', 'b'));
		$query->where('b.settingsid = ' . $db->quote($settingid));

		// [Interpretation 2503] Reset the query using our newly populated query object.
		$db->setQuery($query);
		$db->execute();

		// [Interpretation 2506] check if there was data returned
		if ($db->getNumRows())
		{
			return $db->loadObjectList();
		}
		return false;
	}

	/**
	* Method to get an array of  Objects.
	*
	* @return mixed  An array of  Objects on success, false on failure.
	*
	*/
	public function getSettingidSettingsidAded_C($settingid)
	{
		// [Interpretation 2445] Get a db connection.
		$db = JFactory::getDbo();

		// [Interpretation 2447] Create a new query object.
		$query = $db->getQuery(true);

		// [Interpretation 2449] Get from #__ipals_ as c
		$query->select($db->quoteName(
			array('c.id','c.asset_id','c.settingsid','c.key','c.conditional','c.published','c.created_by','c.modified_by','c.created','c.modified','c.version','c.hits','c.ordering'),
			array('id','asset_id','settingsid','key','conditional','published','created_by','modified_by','created','modified','version','hits','ordering')));
		$query->from($db->quoteName('#__ipals_', 'c'));
		$query->where('c.settingsid = ' . $db->quote($settingid));

		// [Interpretation 2503] Reset the query using our newly populated query object.
		$db->setQuery($query);
		$db->execute();

		// [Interpretation 2506] check if there was data returned
		if ($db->getNumRows())
		{
			return $db->loadObjectList();
		}
		return false;
	}

	/**
	* Method to get an array of  Objects.
	*
	* @return mixed  An array of  Objects on success, false on failure.
	*
	*/
	public function getSettingidSettingsidAded_D($settingid)
	{
		// [Interpretation 2445] Get a db connection.
		$db = JFactory::getDbo();

		// [Interpretation 2447] Create a new query object.
		$query = $db->getQuery(true);

		// [Interpretation 2449] Get from #__ipals_ as d
		$query->select($db->quoteName(
			array('d.id','d.asset_id','d.settingsid','d.key','d.table','d.source','d.destination','d.add_if_not_exists','d.published','d.created_by','d.modified_by','d.created','d.modified','d.version','d.hits','d.ordering'),
			array('id','asset_id','settingsid','key','table','source','destination','add_if_not_exists','published','created_by','modified_by','created','modified','version','hits','ordering')));
		$query->from($db->quoteName('#__ipals_', 'd'));
		$query->where('d.settingsid = ' . $db->quote($settingid));

		// [Interpretation 2503] Reset the query using our newly populated query object.
		$db->setQuery($query);
		$db->execute();

		// [Interpretation 2506] check if there was data returned
		if ($db->getNumRows())
		{
			return $db->loadObjectList();
		}
		return false;
	}


	/**
	* Custom Method
	*
	* @return mixed  item data object on success, false on failure.
	*
	*/
	public function getSettings()
	{

		if (!isset($this->initSet) || !$this->initSet)
		{
			$this->user		= JFactory::getUser();
			$this->userId		= $this->user->get('id');
			$this->guest		= $this->user->get('guest');
			$this->groups		= $this->user->get('groups');
			$this->authorisedGroups	= $this->user->getAuthorisedGroups();
			$this->levels		= $this->user->getAuthorisedViewLevels();
			$this->initSet		= true;
		}
		// [Interpretation 2055] Get a db connection.
		$db = JFactory::getDbo();

		// [Interpretation 2057] Create a new query object.
		$query = $db->getQuery(true);

		// [Interpretation 1430] Get from #__ipals_setting as a
		$query->select($db->quoteName(
			array('a.id','a.asset_id','a.retsserver','a.retslogin','a.retsusername','a.retspassword','a.rets_version','a.retsheaderversion','a.retsuseragent','a.name','a.alias','a.agentmidtable','a.agenttable','a.officetable','a.imagetable','a.propertymidtable','a.propertytable','a.settingstable','a.openhousetable','a.amenitiestable','a.dbrefreshrate','a.batchlimit','a.processlength','a.processlengthtype','a.limit','a.serverbreather','a.retsdebugmode','a.updateonduplicatekey','a.verboselevel','a.dateformat','a.agentresource','a.agentclass','a.officeresource','a.officeclass','a.statusaccept','a.statusacceptvalues','a.statusdelete','a.statusdeletevalues','a.agentid','a.propertyresource','a.residential','a.land','a.commercial','a.multifamily','a.customone','a.customtwo','a.customthree','a.customfour','a.customfive','a.customsix','a.retsphotoobjectname','a.getphotos','a.myradiovalue','a.thumbheight','a.thumbwidth','a.fullheight','a.fullwidth','a.thumbpath','a.thumbprefix','a.thumbpostfix','a.amazonsthree','a.amazonbucket','a.amazonendpoint','a.quickphoto','a.imagepath','a.siteroot','a.temppath','a.processgeocode','a.geocodeaddress','a.agentgeocodeaddress','a.progeocode','a.openhouseresource','a.ohresidential','a.ohland','a.ohcommercial','a.ohmultifamily','a.ohcustomone','a.ohcustomtwo','a.ohcustomthree','a.ohcustomfour','a.ohcustomfive','a.ohcustomsix','a.defaultagent','a.defaultoffice','a.lastrun','a.plugin','a.miscretsid','a.cleanupscript','a.cleanupresource','a.cleanupclasses','a.cleanupkey','a.cleanupretsquery','a.extkey','a.extphotos','a.imgkey','a.imgurl','a.mediaresource','a.mediaclass','a.published','a.created_by','a.modified_by','a.created','a.modified','a.version','a.hits','a.ordering'),
			array('id','asset_id','retsserver','retslogin','retsusername','retspassword','rets_version','retsheaderversion','retsuseragent','name','alias','agentmidtable','agenttable','officetable','imagetable','propertymidtable','propertytable','settingstable','openhousetable','amenitiestable','dbrefreshrate','batchlimit','processlength','processlengthtype','limit','serverbreather','retsdebugmode','updateonduplicatekey','verboselevel','dateformat','agentresource','agentclass','officeresource','officeclass','statusaccept','statusacceptvalues','statusdelete','statusdeletevalues','agentid','propertyresource','residential','land','commercial','multifamily','customone','customtwo','customthree','customfour','customfive','customsix','retsphotoobjectname','getphotos','myradiovalue','thumbheight','thumbwidth','fullheight','fullwidth','thumbpath','thumbprefix','thumbpostfix','amazonsthree','amazonbucket','amazonendpoint','quickphoto','imagepath','siteroot','temppath','processgeocode','geocodeaddress','agentgeocodeaddress','progeocode','openhouseresource','ohresidential','ohland','ohcommercial','ohmultifamily','ohcustomone','ohcustomtwo','ohcustomthree','ohcustomfour','ohcustomfive','ohcustomsix','defaultagent','defaultoffice','lastrun','plugin','miscretsid','cleanupscript','cleanupresource','cleanupclasses','cleanupkey','cleanupretsquery','extkey','extphotos','imgkey','imgurl','mediaresource','mediaclass','published','created_by','modified_by','created','modified','version','hits','ordering')));
		$query->from($db->quoteName('#__ipals_setting', 'a'));
		$query->where('a.access IN (' . implode(',', $this->levels) . ')');
		// [Interpretation 1775] Check if JRequest::getInt('id') is a string or numeric value.
		$checkValue = JRequest::getInt('id');
		if (isset($checkValue) && IpalsHelper::checkString($checkValue))
		{
			$query->where('a.id = ' . $db->quote($checkValue));
		}
		elseif (is_numeric($checkValue))
		{
			$query->where('a.id = ' . $checkValue);
		}
		else
		{
			return false;
		}
		// [Interpretation 1923] Get where a.published is 1
		$query->where('a.published = 1');
		$query->order('a.ordering ASC');

		// [Interpretation 2068] Reset the query using our newly populated query object.
		$db->setQuery($query);
		// [Interpretation 2070] Load the results as a stdClass object.
		$data = $db->loadObject();

		if (empty($data))
		{
			return false;
		}
		if (IpalsHelper::checkJson($data->statusacceptvalues))
		{
			// [Interpretation 1632] Decode statusacceptvalues
			$data->statusacceptvalues = json_decode($data->statusacceptvalues, true);
		}
		// [Interpretation 1647] Make sure the content prepare plugins fire on statusdeletevalues.
		$data->statusdeletevalues = JHtml::_('content.prepare',$data->statusdeletevalues);
		// [Interpretation 1649] Checking if statusdeletevalues has uikit components that must be loaded.
		$this->uikitComp = IpalsHelper::getUikitComp($data->statusdeletevalues,$this->uikitComp);
		// [Interpretation 1647] Make sure the content prepare plugins fire on cleanupclasses.
		$data->cleanupclasses = JHtml::_('content.prepare',$data->cleanupclasses);
		// [Interpretation 1649] Checking if cleanupclasses has uikit components that must be loaded.
		$this->uikitComp = IpalsHelper::getUikitComp($data->cleanupclasses,$this->uikitComp);

		// [Interpretation 2179] return data object.
		return $data;
	}


	/**
	* Get the uikit needed components
	*
	* @return mixed  An array of objects on success.
	*
	*/
	public function getUikitComp()
	{
		if (isset($this->uikitComp) && IpalsHelper::checkArray($this->uikitComp))
		{
			return $this->uikitComp;
		}
		return false;
	}  
}
