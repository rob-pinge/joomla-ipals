<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		3.0.0
	@build			21st September, 2017
	@created		16th August, 2017
	@package		iPALS
	@subpackage		ipalscleanup.php
	@author			Most Wanted Web Services, Inc. <http://retsforealestate.com>	
	@copyright		Copyright (C) 2015. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
  ____  _____  _____  __  __  __      __       ___  _____  __  __  ____  _____  _  _  ____  _  _  ____ 
 (_  _)(  _  )(  _  )(  \/  )(  )    /__\     / __)(  _  )(  \/  )(  _ \(  _  )( \( )( ___)( \( )(_  _)
.-_)(   )(_)(  )(_)(  )    (  )(__  /(__)\   ( (__  )(_)(  )    (  )___/ )(_)(  )  (  )__)  )  (   )(  
\____) (_____)(_____)(_/\/\_)(____)(__)(__)   \___)(_____)(_/\/\_)(__)  (_____)(_)\_)(____)(_)\_) (__) 

/------------------------------------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla modelitem library
jimport('joomla.application.component.modelitem');

/**
 * Ipals Ipalscleanup Model
 */
class IpalsModelIpalscleanup extends JModelItem
{
	/**
	 * Model context string.
	 *
	 * @var        string
	 */
	protected $_context = 'com_ipals.ipalscleanup';

	/**
	 * Model user data.
	 *
	 * @var        strings
	 */
	protected $user;
	protected $userId;
	protected $guest;
	protected $groups;
	protected $levels;
	protected $app;
	protected $input;
	protected $uikitComp;

	/**
	 * @var object item
	 */
	protected $item;

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @since   1.6
	 *
	 * @return void
	 */
	protected function populateState()
	{
		$this->app	= JFactory::getApplication();
		$this->input 	= $this->app->input;
		// Get the itme main id
		$id		= $this->input->getInt('id', null);
		$this->setState('ipalscleanup.id', $id);

		// Load the parameters.
		$params = $this->app->getParams();
		$this->setState('params', $params);
		parent::populateState();
	}

	/**
	 * Method to get article data.
	 *
	 * @param   integer  $pk  The id of the article.
	 *
	 * @return  mixed  Menu item data object on success, false on failure.
	 */
	public function getItem($pk = null)
	{
		$this->user		= JFactory::getUser();
		// [Interpretation 2019] check if this user has permission to access item
		if (!$this->user->authorise('site.ipalscleanup.access', 'com_ipals'))
		{
			$app = JFactory::getApplication();
			$app->enqueueMessage(JText::_('COM_IPALS_NOT_AUTHORISED_TO_VIEW_IPALSCLEANUP'), 'error');
			// [Interpretation 2016] redirect away to the home page if no access allowed.
			$app->redirect(JURI::root());
			return false;
		}
		$this->userId		= $this->user->get('id');
		$this->guest		= $this->user->get('guest');
                $this->groups		= $this->user->get('groups');
                $this->authorisedGroups	= $this->user->getAuthorisedGroups();
		$this->levels		= $this->user->getAuthorisedViewLevels();
		$this->initSet		= true;

		$pk = (!empty($pk)) ? $pk : (int) $this->getState('ipalscleanup.id');
		
		if ($this->_item === null)
		{
			$this->_item = array();
		}

		if (!isset($this->_item[$pk]))
		{
			try
			{
				// [Interpretation 2055] Get a db connection.
				$db = JFactory::getDbo();

				// [Interpretation 2057] Create a new query object.
				$query = $db->getQuery(true);

				// [Interpretation 1430] Get from #__ipals_setting as a
				$query->select($db->quoteName(
			array('a.id','a.asset_id','a.retsserver','a.retslogin','a.retsusername','a.retspassword','a.rets_version','a.retsheaderversion','a.retsuseragent','a.name','a.alias','a.agentmidtable','a.agenttable','a.officetable','a.imagetable','a.propertymidtable','a.propertytable','a.settingstable','a.openhousetable','a.amenitiestable','a.dbrefreshrate','a.batchlimit','a.plugin','a.limit','a.serverbreather','a.dateformat','a.agentresource','a.agentclass','a.officeresource','a.officeclass','a.propertyresource','a.residential','a.land','a.commercial','a.multifamily','a.customone','a.customtwo','a.customthree','a.customfour','a.customfive','a.customsix','a.statusaccept','a.statusacceptvalues','a.statusdelete','a.statusdeletevalues','a.agentid','a.retsdebugmode','a.updateonduplicatekey','a.verboselevel','a.retsphotoobjectname','a.getphotos','a.myradiovalue','a.thumbheight','a.thumbwidth','a.fullheight','a.fullwidth','a.thumbpath','a.thumbprefix','a.thumbpostfix','a.amazonsthree','a.amazonbucket','a.amazonendpoint','a.quickphoto','a.imagepath','a.siteroot','a.temppath','a.geocodeaddress','a.agentgeocodeaddress','a.processgeocode','a.progeocode','a.openhouseresource','a.ohresidential','a.ohland','a.ohcommercial','a.ohmultifamily','a.ohcustomone','a.ohcustomtwo','a.ohcustomfive','a.ohcustomthree','a.ohcustomfour','a.ohcustomsix','a.defaultagent','a.defaultoffice','a.lastrun','a.plugin','a.miscretsid','a.cleanupresource','a.cleanupclasses','a.cleanupkey','a.cleanupretsquery','a.cleanupscript','a.extkey','a.extphotos','a.imgkey','a.imgurl','a.mediaresource','a.mediaclass','a.published','a.created_by','a.modified_by','a.created','a.modified','a.version','a.hits','a.ordering'),
			array('id','asset_id','retsserver','retslogin','retsusername','retspassword','rets_version','retsheaderversion','retsuseragent','name','alias','agentmidtable','agenttable','officetable','imagetable','propertymidtable','propertytable','settingstable','openhousetable','amenitiestable','dbrefreshrate','batchlimit','plugin','limit','serverbreather','dateformat','agentresource','agentclass','officeresource','officeclass','propertyresource','residential','land','commercial','multifamily','customone','customtwo','customthree','customfour','customfive','customsix','statusaccept','statusacceptvalues','statusdelete','statusdeletevalues','agentid','retsdebugmode','updateonduplicatekey','verboselevel','retsphotoobjectname','getphotos','myradiovalue','thumbheight','thumbwidth','fullheight','fullwidth','thumbpath','thumbprefix','thumbpostfix','amazonsthree','amazonbucket','amazonendpoint','quickphoto','imagepath','siteroot','temppath','geocodeaddress','agentgeocodeaddress','processgeocode','progeocode','openhouseresource','ohresidential','ohland','ohcommercial','ohmultifamily','ohcustomone','ohcustomtwo','ohcustomfive','ohcustomthree','ohcustomfour','ohcustomsix','defaultagent','defaultoffice','lastrun','plugin','miscretsid','cleanupresource','cleanupclasses','cleanupkey','cleanupretsquery','cleanupscript','extkey','extphotos','imgkey','imgurl','mediaresource','mediaclass','published','created_by','modified_by','created','modified','version','hits','ordering')));
				$query->from($db->quoteName('#__ipals_setting', 'a'));

				// [Interpretation 2068] Reset the query using our newly populated query object.
				$db->setQuery($query);
				// [Interpretation 2070] Load the results as a stdClass object.
				$data = $db->loadObject();

				if (empty($data))
				{
					$app = JFactory::getApplication();
					// [Interpretation 2082] If no data is found redirect to default page and show warning.
					$app->enqueueMessage(JText::_('COM_IPALS_NOT_FOUND_OR_ACCESS_DENIED'), 'warning');
					$app->redirect(JURI::root());
					return false;
				}
				if (IpalsHelper::checkJson($data->statusacceptvalues))
				{
					// [Interpretation 1632] Decode statusacceptvalues
					$data->statusacceptvalues = json_decode($data->statusacceptvalues, true);
				}
				// [Interpretation 1647] Make sure the content prepare plugins fire on statusdeletevalues.
				$data->statusdeletevalues = JHtml::_('content.prepare',$data->statusdeletevalues);
				// [Interpretation 1649] Checking if statusdeletevalues has uikit components that must be loaded.
				$this->uikitComp = IpalsHelper::getUikitComp($data->statusdeletevalues,$this->uikitComp);
				// [Interpretation 1647] Make sure the content prepare plugins fire on cleanupclasses.
				$data->cleanupclasses = JHtml::_('content.prepare',$data->cleanupclasses);
				// [Interpretation 1649] Checking if cleanupclasses has uikit components that must be loaded.
				$this->uikitComp = IpalsHelper::getUikitComp($data->cleanupclasses,$this->uikitComp);
				// [Interpretation 1680] set idSettingsidB to the $data object.
				$data->idSettingsidB = $this->getIdSettingsidDeac_B($data->id);
				// [Interpretation 1680] set idSettingsidC to the $data object.
				$data->idSettingsidC = $this->getIdSettingsidDeac_C($data->id);
				// [Interpretation 1680] set idSettingsidD to the $data object.
				$data->idSettingsidD = $this->getIdSettingsidDeac_D($data->id);
				// [Interpretation 1680] set idSettingsidE to the $data object.
				$data->idSettingsidE = $this->getIdSettingsidDeac_E($data->id);

				// [Interpretation 2185] set data object to item.
				$this->_item[$pk] = $data;
			}
			catch (Exception $e)
			{
				if ($e->getCode() == 404)
				{
					// Need to go thru the error handler to allow Redirect to work.
					JError::raiseWaring(404, $e->getMessage());
				}
				else
				{
					$this->setError($e);
					$this->_item[$pk] = false;
				}
			}
		}

		return $this->_item[$pk];
	} 

	/**
	* Method to get an array of  Objects.
	*
	* @return mixed  An array of  Objects on success, false on failure.
	*
	*/
	public function getIdSettingsidDeac_B($id)
	{
		// [Interpretation 2445] Get a db connection.
		$db = JFactory::getDbo();

		// [Interpretation 2447] Create a new query object.
		$query = $db->getQuery(true);

		// [Interpretation 2449] Get from #__ipals_ as b
		$query->select($db->quoteName(
			array('b.id','b.asset_id','b.settingsid','b.field','b.key','b.mylist','b.mylist','b.description','b.published','b.created_by','b.modified_by','b.created','b.modified','b.version','b.hits','b.ordering'),
			array('id','asset_id','settingsid','field','key','mylist','mylist','description','published','created_by','modified_by','created','modified','version','hits','ordering')));
		$query->from($db->quoteName('#__ipals_', 'b'));
		$query->where('b.settingsid = ' . $db->quote($id));

		// [Interpretation 2503] Reset the query using our newly populated query object.
		$db->setQuery($query);
		$db->execute();

		// [Interpretation 2506] check if there was data returned
		if ($db->getNumRows())
		{
			return $db->loadObjectList();
		}
		return false;
	}

	/**
	* Method to get an array of  Objects.
	*
	* @return mixed  An array of  Objects on success, false on failure.
	*
	*/
	public function getIdSettingsidDeac_C($id)
	{
		// [Interpretation 2445] Get a db connection.
		$db = JFactory::getDbo();

		// [Interpretation 2447] Create a new query object.
		$query = $db->getQuery(true);

		// [Interpretation 2449] Get from #__ipals_ as c
		$query->select($db->quoteName(
			array('c.id','c.asset_id','c.settingsid','c.key','c.value','c.group','c.auto','c.mylist','c.description','c.published','c.created_by','c.modified_by','c.created','c.modified','c.version','c.hits','c.ordering'),
			array('id','asset_id','settingsid','key','value','group','auto','mylist','description','published','created_by','modified_by','created','modified','version','hits','ordering')));
		$query->from($db->quoteName('#__ipals_', 'c'));
		$query->where('c.settingsid = ' . $db->quote($id));

		// [Interpretation 2503] Reset the query using our newly populated query object.
		$db->setQuery($query);
		$db->execute();

		// [Interpretation 2506] check if there was data returned
		if ($db->getNumRows())
		{
			return $db->loadObjectList();
		}
		return false;
	}

	/**
	* Method to get an array of  Objects.
	*
	* @return mixed  An array of  Objects on success, false on failure.
	*
	*/
	public function getIdSettingsidDeac_D($id)
	{
		// [Interpretation 2445] Get a db connection.
		$db = JFactory::getDbo();

		// [Interpretation 2447] Create a new query object.
		$query = $db->getQuery(true);

		// [Interpretation 2449] Get from #__ipals_ as d
		$query->select($db->quoteName(
			array('d.id','d.asset_id','d.settingsid','d.key','d.conditional','d.published','d.created_by','d.modified_by','d.created','d.modified','d.version','d.hits','d.ordering'),
			array('id','asset_id','settingsid','key','conditional','published','created_by','modified_by','created','modified','version','hits','ordering')));
		$query->from($db->quoteName('#__ipals_', 'd'));
		$query->where('d.settingsid = ' . $db->quote($id));

		// [Interpretation 2503] Reset the query using our newly populated query object.
		$db->setQuery($query);
		$db->execute();

		// [Interpretation 2506] check if there was data returned
		if ($db->getNumRows())
		{
			return $db->loadObjectList();
		}
		return false;
	}

	/**
	* Method to get an array of  Objects.
	*
	* @return mixed  An array of  Objects on success, false on failure.
	*
	*/
	public function getIdSettingsidDeac_E($id)
	{
		// [Interpretation 2445] Get a db connection.
		$db = JFactory::getDbo();

		// [Interpretation 2447] Create a new query object.
		$query = $db->getQuery(true);

		// [Interpretation 2449] Get from #__ipals_ as e
		$query->select($db->quoteName(
			array('e.id','e.asset_id','e.settingsid','e.key','e.table','e.source','e.destination','e.add_if_not_exists','e.published','e.created_by','e.modified_by','e.created','e.modified','e.version','e.hits','e.ordering'),
			array('id','asset_id','settingsid','key','table','source','destination','add_if_not_exists','published','created_by','modified_by','created','modified','version','hits','ordering')));
		$query->from($db->quoteName('#__ipals_', 'e'));
		$query->where('e.settingsid = ' . $db->quote($id));

		// [Interpretation 2503] Reset the query using our newly populated query object.
		$db->setQuery($query);
		$db->execute();

		// [Interpretation 2506] check if there was data returned
		if ($db->getNumRows())
		{
			return $db->loadObjectList();
		}
		return false;
	}


	/**
	* Get the uikit needed components
	*
	* @return mixed  An array of objects on success.
	*
	*/
	public function getUikitComp()
	{
		if (isset($this->uikitComp) && IpalsHelper::checkArray($this->uikitComp))
		{
			return $this->uikitComp;
		}
		return false;
	}  
}
