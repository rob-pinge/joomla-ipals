<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		3.0.0
	@build			21st September, 2017
	@created		16th August, 2017
	@package		iPALS
	@subpackage		view.html.php
	@author			Most Wanted Web Services, Inc. <http://retsforealestate.com>	
	@copyright		Copyright (C) 2015. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
  ____  _____  _____  __  __  __      __       ___  _____  __  __  ____  _____  _  _  ____  _  _  ____ 
 (_  _)(  _  )(  _  )(  \/  )(  )    /__\     / __)(  _  )(  \/  )(  _ \(  _  )( \( )( ___)( \( )(_  _)
.-_)(   )(_)(  )(_)(  )    (  )(__  /(__)\   ( (__  )(_)(  )    (  )___/ )(_)(  )  (  )__)  )  (   )(  
\____) (_____)(_____)(_/\/\_)(____)(__)(__)   \___)(_____)(_/\/\_)(__)  (_____)(_)\_)(____)(_)\_) (__) 

/------------------------------------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla view library
jimport('joomla.application.component.view');

/**
 * Ipals View class for the Ipalsrun
 */
class IpalsViewIpalsrun extends JViewLegacy
{
	// Overwriting JView display method
	function display($tpl = null)
	{
		// get combined params of both component and menu
		$this->app = JFactory::getApplication();
		$this->params = $this->app->getParams();
		$this->menu = $this->app->getMenu()->getActive();
		// get the user object
		$this->user = JFactory::getUser();
		// [Interpretation 2815] Initialise variables.
		$this->items	= $this->get('Items');
		$this->pagination	= $this->get('Pagination');
		$this->settings	= $this->get('Settings');

		// [Interpretation 2844] Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode(PHP_EOL, $errors));
			return false;
		}

		// [Interpretation 2862] Set the toolbar
		$this->addToolBar();

		// [Interpretation 2864] set the document
		$this->_prepareDocument();

		parent::display($tpl);
	}

        /**
	 * Prepares the document
	 */
	protected function _prepareDocument()
	{

		// [Interpretation 3391] always make sure jquery is loaded.
		JHtml::_('jquery.framework');
		// [Interpretation 3393] Load the header checker class.
		require_once( JPATH_COMPONENT_SITE.'/helpers/headercheck.php' );
		// [Interpretation 3402] Initialize the header checker.
		$HeaderCheck = new ipalsHeaderCheck;

		// [Interpretation 3407] Load uikit options.
		$uikit = $this->params->get('uikit_load');
		// [Interpretation 3409] Set script size.
		$size = $this->params->get('uikit_min');
		// [Interpretation 3411] Set css style.
		$style = $this->params->get('uikit_style');

		// [Interpretation 3414] The uikit css.
		if ((!$HeaderCheck->css_loaded('uikit.min') || $uikit == 1) && $uikit != 2 && $uikit != 3)
		{
			$this->document->addStyleSheet(JURI::root(true) .'/media/com_ipals/uikit/css/uikit'.$style.$size.'.css');
		}
		// [Interpretation 3419] The uikit js.
		if ((!$HeaderCheck->js_loaded('uikit.min') || $uikit == 1) && $uikit != 2 && $uikit != 3)
		{
			$this->document->addScript(JURI::root(true) .'/media/com_ipals/uikit/js/uikit'.$size.'.js');
		}

		// [Interpretation 3484] Load the needed uikit components in this view.
		$uikitComp = $this->get('UikitComp');
		if ($uikit != 2 && isset($uikitComp) && IpalsHelper::checkArray($uikitComp))
		{
			// [Interpretation 3488] load just in case.
			jimport('joomla.filesystem.file');
			// [Interpretation 3490] loading...
			foreach ($uikitComp as $class)
			{
				foreach (IpalsHelper::$uk_components[$class] as $name)
				{
					// [Interpretation 3495] check if the CSS file exists.
					if (JFile::exists(JPATH_ROOT.'/media/com_ipals/uikit/css/components/'.$name.$style.$size.'.css'))
					{
						// [Interpretation 3498] load the css.
						$this->document->addStyleSheet(JURI::root(true) .'/media/com_ipals/uikit/css/components/'.$name.$style.$size.'.css');
					}
					// [Interpretation 3501] check if the JavaScript file exists.
					if (JFile::exists(JPATH_ROOT.'/media/com_ipals/uikit/js/components/'.$name.$size.'.js'))
					{
						// [Interpretation 3504] load the js.
						$this->document->addScript(JURI::root(true) .'/media/com_ipals/uikit/js/components/'.$name.$size.'.js', 'text/javascript', true);
					}
				}
			}
		}   
		// [Interpretation 3349] load the meta description
		if ($this->params->get('menu-meta_description'))
		{
			$this->document->setDescription($this->params->get('menu-meta_description'));
		}
		// [Interpretation 3354] load the key words if set
		if ($this->params->get('menu-meta_keywords'))
		{
			$this->document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
		}
		// [Interpretation 3359] check the robot params
		if ($this->params->get('robots'))
		{
			$this->document->setMetadata('robots', $this->params->get('robots'));
		} 
		// add the document default css file
		$this->document->addStyleSheet(JURI::root(true) .'/components/com_ipals/assets/css/ipalsrun.css'); 
        }

	/**
	 * Setting the toolbar
	 */
	protected function addToolBar()
	{
		// adding the joomla toolbar to the front
		JLoader::register('JToolbarHelper', JPATH_ADMINISTRATOR.'/includes/toolbar.php');
		
		// set help url for this view if found
		$help_url = IpalsHelper::getHelpUrl('ipalsrun');
		if (IpalsHelper::checkString($help_url))
		{
			JToolbarHelper::help('COM_IPALS_HELP_MANAGER', false, $help_url);
		}
		// now initiate the toolbar
		$this->toolbar = JToolbar::getInstance();
	}

        /**
	 * Escapes a value for output in a view script.
	 *
	 * @param   mixed  $var  The output to escape.
	 *
	 * @return  mixed  The escaped value.
	 */
	public function escape($var, $sorten = false, $length = 40)
	{
                // use the helper htmlEscape method instead.
		return IpalsHelper::htmlEscape($var, $this->_charset, $sorten, $length);
	}
}
