<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		2.0.0
	@build			22nd September, 2017
	@created		1st May, 2016
	@package		Most Wanted Real Estate
	@subpackage		favorites.php
	@author			Most Wanted Web Services, Inc. <http://mostwantedrealestatesites.com>	
	@copyright		Copyright (C) 2015-2017. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
	
	Most Wanted Real Estate Component
	
/------------------------------------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import the Joomla modellist library
jimport('joomla.application.component.modellist');

/**
 * Mostwantedrealestate Model for Favorites
 */
class MostwantedrealestateModelFavorites extends JModelList
{
	/**
	 * Model user data.
	 *
	 * @var        strings
	 */
	protected $user;
	protected $userId;
	protected $guest;
	protected $groups;
	protected $levels;
	protected $app;
	protected $input;
	protected $uikitComp;

	/**
	 * Method to build an SQL query to load the list data.
	 *
	 * @return      string  An SQL query
	 */
	protected function getListQuery()
	{
		// Get the current user for authorisation checks
		$this->user		= JFactory::getUser();
		$this->userId		= $this->user->get('id');
		$this->guest		= $this->user->get('guest');
                $this->groups		= $this->user->get('groups');
                $this->authorisedGroups	= $this->user->getAuthorisedGroups();
		$this->levels		= $this->user->getAuthorisedViewLevels();
		$this->app		= JFactory::getApplication();
		$this->input		= $this->app->input;
		$this->initSet		= true; 
		// [Interpretation 2669] Get a db connection.
		$db = JFactory::getDbo();

		// [Interpretation 2678] Create a new query object.
		$query = $db->getQuery(true);

		// [Interpretation 1430] Get from #__mostwantedrealestate_favorite_listing as a
		$query->select($db->quoteName(
			array('a.id','a.asset_id','a.uid','a.propertyid','a.published','a.created_by','a.modified_by','a.created','a.modified','a.version','a.hits','a.ordering'),
			array('id','asset_id','uid','propertyid','published','created_by','modified_by','created','modified','version','hits','ordering')));
		$query->from($db->quoteName('#__mostwantedrealestate_favorite_listing', 'a'));

		// [Interpretation 1430] Get from #__mostwantedrealestate_ as b
		$query->select($db->quoteName(
			array('b.id','b.asset_id','b.street','b.streettwo','b.countryid','b.cityid','b.stateid','b.postcode','b.county','b.mls_image','b.viewad','b.showprice','b.price','b.closeprice','b.priceview','b.mls_disclaimer','b.name','b.alias','b.mls_id','b.rets_source','b.listingtype','b.catid','b.featured','b.soleagency','b.listoffice','b.agent','b.colistagent','b.mkt_stats','b.trans_type','b.owncoords','b.latitude','b.longitude','b.bedrooms','b.bathrooms','b.fullbaths','b.thqtrbaths','b.halfbaths','b.qtrbaths','b.squarefeet','b.sqftlower','b.sqftmainlevel','b.sqftupper','b.style','b.year','b.yearremodeled','b.exteriorfinish','b.roof','b.flooring','b.porchpatio','b.frontage','b.waterfront','b.waterfronttype','b.welldepth','b.subdivision','b.landareasqft','b.acrestotal','b.lotdimensions','b.totalrooms','b.otherrooms','b.livingarea','b.ensuite','b.garagetype','b.parkingcarport','b.stories','b.basementandfoundation','b.basementsize','b.basementpctfinished','b.heating','b.cooling','b.appliances','b.indoorfeatures','b.houseconstruction','b.outdoorfeatures','b.fencing','b.communityfeatures','b.otherfeatures','b.phoneavailableyn','b.garbagedisposalyn','b.familyroompresent','b.laundryroompresent','b.kitchenpresent','b.livingroompresent','b.parkingspaceyn','b.customone','b.customtwo','b.customthree','b.customfour','b.customfive','b.customsix','b.customseven','b.customeight','b.storage','b.waterresources','b.sewer','b.zoning','b.propdesc','b.bldg_name','b.buildingfeatures','b.takings','b.returns','b.netprofit','b.bustype','b.bussubtype','b.percentoffice','b.percentwarehouse','b.loadingfac','b.currentuse','b.carryingcap','b.parkinggarage','b.bldgsqft','b.totalrents','b.numunits','b.unitdetails','b.tenancytype','b.tentantpdutilities','b.commonareas','b.landtype','b.stock','b.fixtures','b.fittings','b.rainfall','b.soiltype','b.grazing','b.cropping','b.irrigation','b.rent_type','b.offpeak','b.freq','b.deposit','b.sleeps','b.mediaurl','b.mediatype','b.pdfinfoone','b.pdfinfotwo','b.flplone','b.flpltwo','b.covenantsyn','b.openhouse','b.openhouseinfo','b.hofees','b.annualinsurance','b.taxannual','b.taxyear','b.utilities','b.electricservice','b.averageutilelec','b.averageutilgas','b.pm_price_override','b.pmstartdate','b.pmenddate','b.propmgt_price','b.propmgt_description','b.viewbooking','b.availdate','b.private','b.ctown','b.ctport','b.schooldist','b.elementary','b.midschool','b.highschool','b.university','b.published','b.created_by','b.modified_by','b.created','b.modified','b.version','b.hits','b.ordering'),
			array('_property_id','_property_asset_id','_property_street','_property_streettwo','_property_countryid','_property_cityid','_property_stateid','_property_postcode','_property_county','_property_mls_image','_property_viewad','_property_showprice','_property_price','_property_closeprice','_property_priceview','_property_mls_disclaimer','_property_name','_property_alias','_property_mls_id','_property_rets_source','_property_listingtype','_property_catid','_property_featured','_property_soleagency','_property_listoffice','_property_agent','_property_colistagent','_property_mkt_stats','_property_trans_type','_property_owncoords','_property_latitude','_property_longitude','_property_bedrooms','_property_bathrooms','_property_fullbaths','_property_thqtrbaths','_property_halfbaths','_property_qtrbaths','_property_squarefeet','_property_sqftlower','_property_sqftmainlevel','_property_sqftupper','_property_style','_property_year','_property_yearremodeled','_property_exteriorfinish','_property_roof','_property_flooring','_property_porchpatio','_property_frontage','_property_waterfront','_property_waterfronttype','_property_welldepth','_property_subdivision','_property_landareasqft','_property_acrestotal','_property_lotdimensions','_property_totalrooms','_property_otherrooms','_property_livingarea','_property_ensuite','_property_garagetype','_property_parkingcarport','_property_stories','_property_basementandfoundation','_property_basementsize','_property_basementpctfinished','_property_heating','_property_cooling','_property_appliances','_property_indoorfeatures','_property_houseconstruction','_property_outdoorfeatures','_property_fencing','_property_communityfeatures','_property_otherfeatures','_property_phoneavailableyn','_property_garbagedisposalyn','_property_familyroompresent','_property_laundryroompresent','_property_kitchenpresent','_property_livingroompresent','_property_parkingspaceyn','_property_customone','_property_customtwo','_property_customthree','_property_customfour','_property_customfive','_property_customsix','_property_customseven','_property_customeight','_property_storage','_property_waterresources','_property_sewer','_property_zoning','_property_propdesc','_property_bldg_name','_property_buildingfeatures','_property_takings','_property_returns','_property_netprofit','_property_bustype','_property_bussubtype','_property_percentoffice','_property_percentwarehouse','_property_loadingfac','_property_currentuse','_property_carryingcap','_property_parkinggarage','_property_bldgsqft','_property_totalrents','_property_numunits','_property_unitdetails','_property_tenancytype','_property_tentantpdutilities','_property_commonareas','_property_landtype','_property_stock','_property_fixtures','_property_fittings','_property_rainfall','_property_soiltype','_property_grazing','_property_cropping','_property_irrigation','_property_rent_type','_property_offpeak','_property_freq','_property_deposit','_property_sleeps','_property_mediaurl','_property_mediatype','_property_pdfinfoone','_property_pdfinfotwo','_property_flplone','_property_flpltwo','_property_covenantsyn','_property_openhouse','_property_openhouseinfo','_property_hofees','_property_annualinsurance','_property_taxannual','_property_taxyear','_property_utilities','_property_electricservice','_property_averageutilelec','_property_averageutilgas','_property_pm_price_override','_property_pmstartdate','_property_pmenddate','_property_propmgt_price','_property_propmgt_description','_property_viewbooking','_property_availdate','_property_private','_property_ctown','_property_ctport','_property_schooldist','_property_elementary','_property_midschool','_property_highschool','_property_university','_property_published','_property_created_by','_property_modified_by','_property_created','_property_modified','_property_version','_property_hits','_property_ordering')));
		$query->join('LEFT', ($db->quoteName('#__mostwantedrealestate_', 'b')) . ' ON (' . $db->quoteName('a.propertyid') . ' = ' . $db->quoteName('b.id') . ')');

		// [Interpretation 1430] Get from #__mostwantedrealestate_ as c
		$query->select($db->quoteName(
			array('c.id','c.asset_id','c.image','c.description','c.name','c.stateid','c.alias','c.owncoords','c.latitude','c.longitude','c.published','c.created_by','c.modified_by','c.created','c.modified','c.version','c.hits','c.ordering'),
			array('_city_id','_city_asset_id','_city_image','_city_description','_city_name','_city_stateid','_city_alias','_city_owncoords','_city_latitude','_city_longitude','_city_published','_city_created_by','_city_modified_by','_city_created','_city_modified','_city_version','_city_hits','_city_ordering')));
		$query->join('LEFT', ($db->quoteName('#__mostwantedrealestate_', 'c')) . ' ON (' . $db->quoteName('b.cityid') . ' = ' . $db->quoteName('c.id') . ')');

		// [Interpretation 1430] Get from #__mostwantedrealestate_ as d
		$query->select($db->quoteName(
			array('d.id','d.asset_id','d.image','d.description','d.name','d.alias','d.countryid','d.owncoords','d.latitude','d.longitude','d.published','d.created_by','d.modified_by','d.created','d.modified','d.version','d.hits','d.ordering'),
			array('_state_id','_state_asset_id','_state_image','_state_description','_state_name','_state_alias','_state_countryid','_state_owncoords','_state_latitude','_state_longitude','_state_published','_state_created_by','_state_modified_by','_state_created','_state_modified','_state_version','_state_hits','_state_ordering')));
		$query->join('LEFT', ($db->quoteName('#__mostwantedrealestate_', 'd')) . ' ON (' . $db->quoteName('b.stateid') . ' = ' . $db->quoteName('d.id') . ')');
		$query->where('a.access IN (' . implode(',', $this->levels) . ')');
		$query->where('a.uid = ' . (int) $this->userId);
		// [Interpretation 1923] Get where a.published is 1
		$query->where('a.published = 1');
		$query->order('a.ordering ASC');

		// [Interpretation 2693] return the query object
		return $query;
	}

	/**
	 * Method to get an array of data items.
	 *
	 * @return  mixed  An array of data items on success, false on failure.
	 */
	public function getItems()
	{
		$user = JFactory::getUser();
		// [Interpretation 2019] check if this user has permission to access item
		if (!$user->authorise('site.favorites.access', 'com_mostwantedrealestate'))
		{
			$app = JFactory::getApplication();
			$app->enqueueMessage(JText::_('COM_MOSTWANTEDREALESTATE_NOT_AUTHORISED_TO_VIEW_FAVORITES'), 'error');
			// [Interpretation 2011] redirect away to the default view if no access allowed.
			$app->redirect(JRoute::_('index.php?option=com_mostwantedrealestate&view=properties'));
			return false;
		}  
		// load parent items
		$items = parent::getItems();

		// Get the global params
		$globalParams = JComponentHelper::getParams('com_mostwantedrealestate', true);

		// [Interpretation 2714] Convert the parameter fields into objects.
		if (MostwantedrealestateHelper::checkArray($items))
		{
			foreach ($items as $nr => &$item)
			{
				// [Interpretation 2719] Always create a slug for sef URL's
				$item->slug = (isset($item->alias) && isset($item->id)) ? $item->id.':'.$item->alias : $item->id;
				// [Interpretation 1680] set idPropidH to the $item object.
				$item->idPropidH = $this->getIdPropidFcfc_H($item->_property_id);
			}
		} 

		// return items
		return $items;
	} 

	/**
	* Method to get an array of  Objects.
	*
	* @return mixed  An array of  Objects on success, false on failure.
	*
	*/
	public function getIdPropidFcfc_H($id)
	{
		// [Interpretation 2445] Get a db connection.
		$db = JFactory::getDbo();

		// [Interpretation 2447] Create a new query object.
		$query = $db->getQuery(true);

		// [Interpretation 2449] Get from #__mostwantedrealestate_ as h
		$query->select($db->quoteName(
			array('h.id','h.asset_id','h.propid','h.path','h.filename','h.type','h.title','h.description','h.rets_source','h.published','h.created_by','h.modified_by','h.created','h.modified','h.version','h.hits','h.ordering'),
			array('id','asset_id','propid','path','filename','type','title','description','rets_source','published','created_by','modified_by','created','modified','version','hits','ordering')));
		$query->from($db->quoteName('#__mostwantedrealestate_', 'h'));
		$query->where('h.propid = ' . $db->quote($id));

		// [Interpretation 2503] Reset the query using our newly populated query object.
		$db->setQuery($query);
		$db->execute();

		// [Interpretation 2506] check if there was data returned
		if ($db->getNumRows())
		{
			return $db->loadObjectList();
		}
		return false;
	}


	/**
	* Get the uikit needed components
	*
	* @return mixed  An array of objects on success.
	*
	*/
	public function getUikitComp()
	{
		if (isset($this->uikitComp) && MostwantedrealestateHelper::checkArray($this->uikitComp))
		{
			return $this->uikitComp;
		}
		return false;
	}  
}
