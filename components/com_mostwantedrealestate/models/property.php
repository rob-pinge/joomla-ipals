<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		2.0.0
	@build			22nd September, 2017
	@created		1st May, 2016
	@package		Most Wanted Real Estate
	@subpackage		property.php
	@author			Most Wanted Web Services, Inc. <http://mostwantedrealestatesites.com>	
	@copyright		Copyright (C) 2015-2017. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
	
	Most Wanted Real Estate Component
	
/------------------------------------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla modelitem library
jimport('joomla.application.component.modelitem');

/**
 * Mostwantedrealestate Property Model
 */
class MostwantedrealestateModelProperty extends JModelItem
{
	/**
	 * Model context string.
	 *
	 * @var        string
	 */
	protected $_context = 'com_mostwantedrealestate.property';

	/**
	 * Model user data.
	 *
	 * @var        strings
	 */
	protected $user;
	protected $userId;
	protected $guest;
	protected $groups;
	protected $levels;
	protected $app;
	protected $input;
	protected $uikitComp;

	/**
	 * @var object item
	 */
	protected $item;

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @since   1.6
	 *
	 * @return void
	 */
	protected function populateState()
	{
		$this->app	= JFactory::getApplication();
		$this->input 	= $this->app->input;
		// Get the itme main id
		$id		= $this->input->getInt('id', null);
		$this->setState('property.id', $id);

		// Load the parameters.
		$params = $this->app->getParams();
		$this->setState('params', $params);
		parent::populateState();
	}

	/**
	 * Method to get article data.
	 *
	 * @param   integer  $pk  The id of the article.
	 *
	 * @return  mixed  Menu item data object on success, false on failure.
	 */
	public function getItem($pk = null)
	{
		$this->user		= JFactory::getUser();
		// [Interpretation 2019] check if this user has permission to access item
		if (!$this->user->authorise('site.property.access', 'com_mostwantedrealestate'))
		{
			$app = JFactory::getApplication();
			$app->enqueueMessage(JText::_('COM_MOSTWANTEDREALESTATE_NOT_AUTHORISED_TO_VIEW_PROPERTY'), 'error');
			// [Interpretation 2011] redirect away to the default view if no access allowed.
			$app->redirect(JRoute::_('index.php?option=com_mostwantedrealestate&view=properties'));
			return false;
		}
		$this->userId		= $this->user->get('id');
		$this->guest		= $this->user->get('guest');
                $this->groups		= $this->user->get('groups');
                $this->authorisedGroups	= $this->user->getAuthorisedGroups();
		$this->levels		= $this->user->getAuthorisedViewLevels();
		$this->initSet		= true;

		$pk = (!empty($pk)) ? $pk : (int) $this->getState('property.id');
		
		if ($this->_item === null)
		{
			$this->_item = array();
		}

		if (!isset($this->_item[$pk]))
		{
			try
			{
				// [Interpretation 2055] Get a db connection.
				$db = JFactory::getDbo();

				// [Interpretation 2057] Create a new query object.
				$query = $db->getQuery(true);

				// [Interpretation 1430] Get from #__mostwantedrealestate_property as a
				$query->select($db->quoteName(
			array('a.id','a.asset_id','a.street','a.streettwo','a.countryid','a.cityid','a.stateid','a.postcode','a.county','a.mls_image','a.viewad','a.showprice','a.price','a.closeprice','a.closedate','a.priceview','a.mls_disclaimer','a.name','a.alias','a.mls_id','a.rets_source','a.listingtype','a.catid','a.featured','a.soleagency','a.listoffice','a.agent','a.colistagent','a.mkt_stats','a.trans_type','a.owncoords','a.latitude','a.longitude','a.bedrooms','a.bathrooms','a.fullbaths','a.thqtrbaths','a.halfbaths','a.qtrbaths','a.squarefeet','a.sqftlower','a.sqftmainlevel','a.sqftupper','a.style','a.year','a.yearremodeled','a.exteriorfinish','a.roof','a.flooring','a.porchpatio','a.frontage','a.waterfront','a.waterfronttype','a.welldepth','a.subdivision','a.landareasqft','a.acrestotal','a.lotdimensions','a.totalrooms','a.otherrooms','a.livingarea','a.ensuite','a.garagetype','a.parkingcarport','a.stories','a.basementandfoundation','a.basementsize','a.basementpctfinished','a.heating','a.cooling','a.houseconstruction','a.fencing','a.phoneavailableyn','a.garbagedisposalyn','a.familyroompresent','a.laundryroompresent','a.kitchenpresent','a.livingroompresent','a.parkingspaceyn','a.customone','a.customtwo','a.customthree','a.customfour','a.customfive','a.customsix','a.customseven','a.customeight','a.storage','a.waterresources','a.sewer','a.zoning','a.propdesc','a.appliances','a.otherfeatures','a.outdoorfeatures','a.indoorfeatures','a.communityfeatures','a.bldg_name','a.takings','a.returns','a.netprofit','a.bustype','a.bussubtype','a.buildingfeatures','a.percentoffice','a.percentwarehouse','a.loadingfac','a.currentuse','a.carryingcap','a.parkinggarage','a.bldgsqft','a.totalrents','a.numunits','a.tenancytype','a.tentantpdutilities','a.commonareas','a.unitdetails','a.landtype','a.stock','a.fixtures','a.fittings','a.rainfall','a.soiltype','a.grazing','a.cropping','a.irrigation','a.rent_type','a.offpeak','a.freq','a.deposit','a.sleeps','a.mediaurl','a.mediatype','a.pdfinfoone','a.pdfinfotwo','a.flplone','a.flpltwo','a.covenantsyn','a.openhouse','a.openhouseinfo','a.hofees','a.annualinsurance','a.taxannual','a.taxyear','a.utilities','a.electricservice','a.averageutilelec','a.averageutilgas','a.terms','a.pm_price_override','a.pmstartdate','a.pmenddate','a.propmgt_price','a.propmgt_description','a.viewbooking','a.availdate','a.private','a.ctown','a.ctport','a.schooldist','a.elementary','a.midschool','a.highschool','a.university','a.published','a.created_by','a.modified_by','a.created','a.modified','a.version','a.hits','a.ordering'),
			array('id','asset_id','street','streettwo','countryid','cityid','stateid','postcode','county','mls_image','viewad','showprice','price','closeprice','closedate','priceview','mls_disclaimer','name','alias','mls_id','rets_source','listingtype','catid','featured','soleagency','listoffice','agent','colistagent','mkt_stats','trans_type','owncoords','latitude','longitude','bedrooms','bathrooms','fullbaths','thqtrbaths','halfbaths','qtrbaths','squarefeet','sqftlower','sqftmainlevel','sqftupper','style','year','yearremodeled','exteriorfinish','roof','flooring','porchpatio','frontage','waterfront','waterfronttype','welldepth','subdivision','landareasqft','acrestotal','lotdimensions','totalrooms','otherrooms','livingarea','ensuite','garagetype','parkingcarport','stories','basementandfoundation','basementsize','basementpctfinished','heating','cooling','houseconstruction','fencing','phoneavailableyn','garbagedisposalyn','familyroompresent','laundryroompresent','kitchenpresent','livingroompresent','parkingspaceyn','customone','customtwo','customthree','customfour','customfive','customsix','customseven','customeight','storage','waterresources','sewer','zoning','propdesc','appliances','otherfeatures','outdoorfeatures','indoorfeatures','communityfeatures','bldg_name','takings','returns','netprofit','bustype','bussubtype','buildingfeatures','percentoffice','percentwarehouse','loadingfac','currentuse','carryingcap','parkinggarage','bldgsqft','totalrents','numunits','tenancytype','tentantpdutilities','commonareas','unitdetails','landtype','stock','fixtures','fittings','rainfall','soiltype','grazing','cropping','irrigation','rent_type','offpeak','freq','deposit','sleeps','mediaurl','mediatype','pdfinfoone','pdfinfotwo','flplone','flpltwo','covenantsyn','openhouse','openhouseinfo','hofees','annualinsurance','taxannual','taxyear','utilities','electricservice','averageutilelec','averageutilgas','terms','pm_price_override','pmstartdate','pmenddate','propmgt_price','propmgt_description','viewbooking','availdate','private','ctown','ctport','schooldist','elementary','midschool','highschool','university','published','created_by','modified_by','created','modified','version','hits','ordering')));
				$query->from($db->quoteName('#__mostwantedrealestate_property', 'a'));

				// [Interpretation 1430] Get from #__mostwantedrealestate_ as c
				$query->select($db->quoteName(
			array('c.name'),
			array('_country_name')));
				$query->join('LEFT', ($db->quoteName('#__mostwantedrealestate_', 'c')) . ' ON (' . $db->quoteName('a.countryid') . ' = ' . $db->quoteName('c.id') . ')');

				// [Interpretation 1430] Get from #__mostwantedrealestate_ as d
				$query->select($db->quoteName(
			array('d.name'),
			array('_state_name')));
				$query->join('LEFT', ($db->quoteName('#__mostwantedrealestate_', 'd')) . ' ON (' . $db->quoteName('a.stateid') . ' = ' . $db->quoteName('d.id') . ')');

				// [Interpretation 1430] Get from #__mostwantedrealestate_ as e
				$query->select($db->quoteName(
			array('e.name'),
			array('_city_name')));
				$query->join('LEFT', ($db->quoteName('#__mostwantedrealestate_', 'e')) . ' ON (' . $db->quoteName('a.cityid') . ' = ' . $db->quoteName('e.id') . ')');

				// [Interpretation 1430] Get from #__mostwantedrealestate_ as f
				$query->select($db->quoteName(
			array('f.name'),
			array('_agent_name')));
				$query->join('LEFT', ($db->quoteName('#__mostwantedrealestate_', 'f')) . ' ON (' . $db->quoteName('a.agent') . ' = ' . $db->quoteName('f.id') . ')');

				// [Interpretation 1430] Get from #__mostwantedrealestate_ as g
				$query->select($db->quoteName(
			array('g.name'),
			array('_agency_name')));
				$query->join('LEFT', ($db->quoteName('#__mostwantedrealestate_', 'g')) . ' ON (' . $db->quoteName('a.listoffice') . ' = ' . $db->quoteName('g.id') . ')');

				// [Interpretation 1430] Get from #__mostwantedrealestate_ as h
				$query->select($db->quoteName(
			array('h.name'),
			array('_market_status_name')));
				$query->join('LEFT', ($db->quoteName('#__mostwantedrealestate_', 'h')) . ' ON (' . $db->quoteName('a.mkt_stats') . ' = ' . $db->quoteName('h.id') . ')');

				// [Interpretation 1430] Get from #__mostwantedrealestate_ as i
				$query->select($db->quoteName(
			array('i.name'),
			array('_rent_type_name')));
				$query->join('LEFT', ($db->quoteName('#__mostwantedrealestate_', 'i')) . ' ON (' . $db->quoteName('a.rent_type') . ' = ' . $db->quoteName('i.id') . ')');

				// [Interpretation 1430] Get from #__mostwantedrealestate_ as j
				$query->select($db->quoteName(
			array('j.name'),
			array('_rental_frequency_name')));
				$query->join('LEFT', ($db->quoteName('#__mostwantedrealestate_', 'j')) . ' ON (' . $db->quoteName('a.freq') . ' = ' . $db->quoteName('j.id') . ')');

				// [Interpretation 1430] Get from #__mostwantedrealestate_ as k
				$query->select($db->quoteName(
			array('k.name'),
			array('_transaction_type_name')));
				$query->join('LEFT', ($db->quoteName('#__mostwantedrealestate_', 'k')) . ' ON (' . $db->quoteName('a.trans_type') . ' = ' . $db->quoteName('k.id') . ')');

				// [Interpretation 1430] Get from #__mostwantedrealestate_ as l
				$query->select($db->quoteName(
			array('l.featurename'),
			array('_feature_type_basement')));
				$query->join('LEFT', ($db->quoteName('#__mostwantedrealestate_', 'l')) . ' ON (' . $db->quoteName('a.basementandfoundation') . ' = ' . $db->quoteName('l.id') . ')');

				// [Interpretation 1430] Get from #__mostwantedrealestate_ as m
				$query->select($db->quoteName(
			array('m.featurename'),
			array('_feature_type_fencing')));
				$query->join('LEFT', ($db->quoteName('#__mostwantedrealestate_', 'm')) . ' ON (' . $db->quoteName('a.fencing') . ' = ' . $db->quoteName('m.id') . ')');

				// [Interpretation 1430] Get from #__mostwantedrealestate_ as n
				$query->select($db->quoteName(
			array('n.featurename'),
			array('_feature_type_garagetype')));
				$query->join('LEFT', ($db->quoteName('#__mostwantedrealestate_', 'n')) . ' ON (' . $db->quoteName('a.garagetype') . ' = ' . $db->quoteName('n.id') . ')');

				// [Interpretation 1430] Get from #__mostwantedrealestate_ as o
				$query->select($db->quoteName(
			array('o.featurename'),
			array('_feature_type_porchpatio')));
				$query->join('LEFT', ($db->quoteName('#__mostwantedrealestate_', 'o')) . ' ON (' . $db->quoteName('a.porchpatio') . ' = ' . $db->quoteName('o.id') . ')');

				// [Interpretation 1430] Get from #__mostwantedrealestate_ as p
				$query->select($db->quoteName(
			array('p.featurename'),
			array('_feature_type_frontage')));
				$query->join('LEFT', ($db->quoteName('#__mostwantedrealestate_', 'p')) . ' ON (' . $db->quoteName('a.frontage') . ' = ' . $db->quoteName('p.id') . ')');

				// [Interpretation 1430] Get from #__mostwantedrealestate_ as q
				$query->select($db->quoteName(
			array('q.featurename'),
			array('_feature_type_roof')));
				$query->join('LEFT', ($db->quoteName('#__mostwantedrealestate_', 'q')) . ' ON (' . $db->quoteName('a.roof') . ' = ' . $db->quoteName('q.id') . ')');

				// [Interpretation 1430] Get from #__mostwantedrealestate_ as r
				$query->select($db->quoteName(
			array('r.featurename'),
			array('_feature_type_exteriorfinish')));
				$query->join('LEFT', ($db->quoteName('#__mostwantedrealestate_', 'r')) . ' ON (' . $db->quoteName('a.exteriorfinish') . ' = ' . $db->quoteName('r.id') . ')');

				// [Interpretation 1430] Get from #__mostwantedrealestate_ as s
				$query->select($db->quoteName(
			array('s.featurename'),
			array('_feature_type_style')));
				$query->join('LEFT', ($db->quoteName('#__mostwantedrealestate_', 's')) . ' ON (' . $db->quoteName('a.style') . ' = ' . $db->quoteName('s.id') . ')');

				// [Interpretation 1430] Get from #__mostwantedrealestate_ as t
				$query->select($db->quoteName(
			array('t.featurename'),
			array('_feature_type_waterresources')));
				$query->join('LEFT', ($db->quoteName('#__mostwantedrealestate_', 't')) . ' ON (' . $db->quoteName('a.waterresources') . ' = ' . $db->quoteName('t.id') . ')');

				// [Interpretation 1430] Get from #__mostwantedrealestate_ as u
				$query->select($db->quoteName(
			array('u.featurename'),
			array('_feature_type_heating')));
				$query->join('LEFT', ($db->quoteName('#__mostwantedrealestate_', 'u')) . ' ON (' . $db->quoteName('a.heating') . ' = ' . $db->quoteName('u.id') . ')');

				// [Interpretation 1430] Get from #__mostwantedrealestate_ as v
				$query->select($db->quoteName(
			array('v.featurename'),
			array('_feature_type_cooling')));
				$query->join('LEFT', ($db->quoteName('#__mostwantedrealestate_', 'v')) . ' ON (' . $db->quoteName('a.cooling') . ' = ' . $db->quoteName('v.id') . ')');

				// [Interpretation 1430] Get from #__mostwantedrealestate_ as w
				$query->select($db->quoteName(
			array('w.featurename'),
			array('_feature_type_sewer')));
				$query->join('LEFT', ($db->quoteName('#__mostwantedrealestate_', 'w')) . ' ON (' . $db->quoteName('a.sewer') . ' = ' . $db->quoteName('w.id') . ')');

				// [Interpretation 1430] Get from #__mostwantedrealestate_ as x
				$query->select($db->quoteName(
			array('x.featurename'),
			array('_feature_type_zoning')));
				$query->join('LEFT', ($db->quoteName('#__mostwantedrealestate_', 'x')) . ' ON (' . $db->quoteName('a.zoning') . ' = ' . $db->quoteName('x.id') . ')');

				// [Interpretation 1430] Get from #__mostwantedrealestate_ as y
				$query->select($db->quoteName(
			array('y.featurename'),
			array('_feature_type_terms')));
				$query->join('LEFT', ($db->quoteName('#__mostwantedrealestate_', 'y')) . ' ON (' . $db->quoteName('a.terms') . ' = ' . $db->quoteName('y.id') . ')');

				// [Interpretation 1430] Get from #__categories as b
				$query->select($db->quoteName(
			array('b.title'),
			array('title')));
				$query->join('LEFT', ($db->quoteName('#__categories', 'b')) . ' ON (' . $db->quoteName('a.catid') . ' = ' . $db->quoteName('b.id') . ')');
				// [Interpretation 1775] Check if JRequest::getInt('id') is a string or numeric value.
				$checkValue = JRequest::getInt('id');
				if (isset($checkValue) && MostwantedrealestateHelper::checkString($checkValue))
				{
					$query->where('a.id = ' . $db->quote($checkValue));
				}
				elseif (is_numeric($checkValue))
				{
					$query->where('a.id = ' . $checkValue);
				}
				else
				{
					return false;
				}
				$query->where('a.access IN (' . implode(',', $this->levels) . ')');

				// [Interpretation 2068] Reset the query using our newly populated query object.
				$db->setQuery($query);
				// [Interpretation 2070] Load the results as a stdClass object.
				$data = $db->loadObject();

				if (empty($data))
				{
					$app = JFactory::getApplication();
					// [Interpretation 2082] If no data is found redirect to default page and show warning.
					$app->enqueueMessage(JText::_('COM_MOSTWANTEDREALESTATE_NOT_FOUND_OR_ACCESS_DENIED'), 'warning');
					$app->redirect(JRoute::_('index.php?option=com_mostwantedrealestate&view=properties'));
					return false;
				}
				if (MostwantedrealestateHelper::checkJson($data->commonareas))
				{
					// [Interpretation 1632] Decode commonareas
					$data->commonareas = json_decode($data->commonareas, true);
				}
				if (MostwantedrealestateHelper::checkJson($data->indoorfeatures))
				{
					// [Interpretation 1632] Decode indoorfeatures
					$data->indoorfeatures = json_decode($data->indoorfeatures, true);
				}
				if (MostwantedrealestateHelper::checkJson($data->buildingfeatures))
				{
					// [Interpretation 1632] Decode buildingfeatures
					$data->buildingfeatures = json_decode($data->buildingfeatures, true);
				}
				if (MostwantedrealestateHelper::checkJson($data->otherfeatures))
				{
					// [Interpretation 1632] Decode otherfeatures
					$data->otherfeatures = json_decode($data->otherfeatures, true);
				}
				if (MostwantedrealestateHelper::checkJson($data->colistagent))
				{
					// [Interpretation 1632] Decode colistagent
					$data->colistagent = json_decode($data->colistagent, true);
				}
				if (MostwantedrealestateHelper::checkJson($data->terms))
				{
					// [Interpretation 1632] Decode terms
					$data->terms = json_decode($data->terms, true);
				}
				if (MostwantedrealestateHelper::checkJson($data->waterresources))
				{
					// [Interpretation 1632] Decode waterresources
					$data->waterresources = json_decode($data->waterresources, true);
				}
				if (MostwantedrealestateHelper::checkJson($data->appliances))
				{
					// [Interpretation 1632] Decode appliances
					$data->appliances = json_decode($data->appliances, true);
				}
				if (MostwantedrealestateHelper::checkJson($data->outdoorfeatures))
				{
					// [Interpretation 1632] Decode outdoorfeatures
					$data->outdoorfeatures = json_decode($data->outdoorfeatures, true);
				}
				if (MostwantedrealestateHelper::checkJson($data->communityfeatures))
				{
					// [Interpretation 1632] Decode communityfeatures
					$data->communityfeatures = json_decode($data->communityfeatures, true);
				}
				if (MostwantedrealestateHelper::checkJson($data->porchpatio))
				{
					// [Interpretation 1632] Decode porchpatio
					$data->porchpatio = json_decode($data->porchpatio, true);
				}
				if (MostwantedrealestateHelper::checkJson($data->unitdetails))
				{
					// [Interpretation 1632] Decode unitdetails
					$data->unitdetails = json_decode($data->unitdetails, true);
				}
				if (MostwantedrealestateHelper::checkJson($data->garagetype))
				{
					// [Interpretation 1632] Decode garagetype
					$data->garagetype = json_decode($data->garagetype, true);
				}
				if (MostwantedrealestateHelper::checkJson($data->heating))
				{
					// [Interpretation 1632] Decode heating
					$data->heating = json_decode($data->heating, true);
				}
				if (MostwantedrealestateHelper::checkJson($data->cooling))
				{
					// [Interpretation 1632] Decode cooling
					$data->cooling = json_decode($data->cooling, true);
				}
				if (MostwantedrealestateHelper::checkJson($data->openhouseinfo))
				{
					// [Interpretation 1632] Decode openhouseinfo
					$data->openhouseinfo = json_decode($data->openhouseinfo, true);
				}
				// [Interpretation 1647] Make sure the content prepare plugins fire on mls_disclaimer.
				$data->mls_disclaimer = JHtml::_('content.prepare',$data->mls_disclaimer);
				// [Interpretation 1649] Checking if mls_disclaimer has uikit components that must be loaded.
				$this->uikitComp = MostwantedrealestateHelper::getUikitComp($data->mls_disclaimer,$this->uikitComp);
				// [Interpretation 1647] Make sure the content prepare plugins fire on propdesc.
				$data->propdesc = JHtml::_('content.prepare',$data->propdesc);
				// [Interpretation 1649] Checking if propdesc has uikit components that must be loaded.
				$this->uikitComp = MostwantedrealestateHelper::getUikitComp($data->propdesc,$this->uikitComp);
				// [Interpretation 1647] Make sure the content prepare plugins fire on private.
				$data->private = JHtml::_('content.prepare',$data->private);
				// [Interpretation 1649] Checking if private has uikit components that must be loaded.
				$this->uikitComp = MostwantedrealestateHelper::getUikitComp($data->private,$this->uikitComp);

				// [Interpretation 2185] set data object to item.
				$this->_item[$pk] = $data;
			}
			catch (Exception $e)
			{
				if ($e->getCode() == 404)
				{
					// Need to go thru the error handler to allow Redirect to work.
					JError::raiseWaring(404, $e->getMessage());
				}
				else
				{
					$this->setError($e);
					$this->_item[$pk] = false;
				}
			}
		}

		return $this->_item[$pk];
	} 


	/**
	* Custom Method
	*
	* @return mixed  An array of objects on success, false on failure.
	*
	*/
	public function getAllImages()
	{

		if (!isset($this->initSet) || !$this->initSet)
		{
			$this->user		= JFactory::getUser();
			$this->userId		= $this->user->get('id');
			$this->guest		= $this->user->get('guest');
			$this->groups		= $this->user->get('groups');
			$this->authorisedGroups	= $this->user->getAuthorisedGroups();
			$this->levels		= $this->user->getAuthorisedViewLevels();
			$this->initSet		= true;
		}

		// [Interpretation 2235] Get the global params
		$globalParams = JComponentHelper::getParams('com_mostwantedrealestate', true);
		// [Interpretation 2669] Get a db connection.
		$db = JFactory::getDbo();

		// [Interpretation 2678] Create a new query object.
		$query = $db->getQuery(true);

		// [Interpretation 1430] Get from #__mostwantedrealestate_image as a
		$query->select($db->quoteName(
			array('a.propid','a.path','a.filename','a.type','a.title','a.description','a.rets_source'),
			array('propid','path','filename','type','title','description','rets_source')));
		$query->from($db->quoteName('#__mostwantedrealestate_image', 'a'));
		// [Interpretation 1775] Check if JRequest::getInt('id') is a string or numeric value.
		$checkValue = JRequest::getInt('id');
		if (isset($checkValue) && MostwantedrealestateHelper::checkString($checkValue))
		{
			$query->where('a.propid = ' . $db->quote($checkValue));
		}
		elseif (is_numeric($checkValue))
		{
			$query->where('a.propid = ' . $checkValue);
		}
		else
		{
			return false;
		}
		// [Interpretation 1923] Get where a.published is 1
		$query->where('a.published = 1');

		// [Interpretation 2240] Reset the query using our newly populated query object.
		$db->setQuery($query);
		$items = $db->loadObjectList();

		if (empty($items))
		{
			return false;
		}

		// [Interpretation 2714] Convert the parameter fields into objects.
		if (MostwantedrealestateHelper::checkArray($items))
		{
			foreach ($items as $nr => &$item)
			{
				// [Interpretation 2719] Always create a slug for sef URL's
				$item->slug = (isset($item->alias) && isset($item->id)) ? $item->id.':'.$item->alias : $item->id;
				// [Interpretation 1647] Make sure the content prepare plugins fire on description.
				$item->description = JHtml::_('content.prepare',$item->description);
				// [Interpretation 1649] Checking if description has uikit components that must be loaded.
				$this->uikitComp = MostwantedrealestateHelper::getUikitComp($item->description,$this->uikitComp);
			}
		}
		// [Interpretation 2249] return items
		return $items;
	}


	/**
	* Custom Method
	*
	* @return mixed  An array of objects on success, false on failure.
	*
	*/
	public function similarProperties()
	{

		if (!isset($this->initSet) || !$this->initSet)
		{
			$this->user		= JFactory::getUser();
			$this->userId		= $this->user->get('id');
			$this->guest		= $this->user->get('guest');
			$this->groups		= $this->user->get('groups');
			$this->authorisedGroups	= $this->user->getAuthorisedGroups();
			$this->levels		= $this->user->getAuthorisedViewLevels();
			$this->initSet		= true;
		}

		// [Interpretation 2235] Get the global params
		$globalParams = JComponentHelper::getParams('com_mostwantedrealestate', true);
		// [Interpretation 2669] Get a db connection.
		$db = JFactory::getDbo();

		// [Interpretation 2678] Create a new query object.
		$query = $db->getQuery(true);

		// [Interpretation 1430] Get from #__mostwantedrealestate_property as a
		$query->select($db->quoteName(
			array('a.id','a.asset_id','a.street','a.streettwo','a.countryid','a.cityid','a.stateid','a.postcode','a.county','a.mls_image','a.viewad','a.showprice','a.price','a.closeprice','a.priceview','a.mls_disclaimer','a.name','a.alias','a.mls_id','a.rets_source','a.listingtype','a.catid','a.featured','a.soleagency','a.listoffice','a.agent','a.colistagent','a.mkt_stats','a.trans_type','a.owncoords','a.latitude','a.longitude','a.bedrooms','a.bathrooms','a.fullbaths','a.thqtrbaths','a.halfbaths','a.qtrbaths','a.squarefeet','a.sqftlower','a.sqftmainlevel','a.sqftupper','a.style','a.year','a.yearremodeled','a.exteriorfinish','a.roof','a.flooring','a.porchpatio','a.frontage','a.waterfront','a.waterfronttype','a.welldepth','a.subdivision','a.landareasqft','a.acrestotal','a.lotdimensions','a.totalrooms','a.otherrooms','a.livingarea','a.ensuite','a.garagetype','a.parkingcarport','a.stories','a.basementandfoundation','a.basementsize','a.basementpctfinished','a.heating','a.cooling','a.appliances','a.indoorfeatures','a.houseconstruction','a.outdoorfeatures','a.fencing','a.communityfeatures','a.otherfeatures','a.phoneavailableyn','a.garbagedisposalyn','a.familyroompresent','a.laundryroompresent','a.kitchenpresent','a.livingroompresent','a.parkingspaceyn','a.customone','a.customtwo','a.customthree','a.customfour','a.customfive','a.customsix','a.customseven','a.customeight','a.storage','a.waterresources','a.sewer','a.zoning','a.propdesc','a.bldg_name','a.buildingfeatures','a.takings','a.returns','a.netprofit','a.bustype','a.bussubtype','a.percentoffice','a.percentwarehouse','a.loadingfac','a.currentuse','a.carryingcap','a.parkinggarage','a.bldgsqft','a.totalrents','a.numunits','a.unitdetails','a.tenancytype','a.tentantpdutilities','a.commonareas','a.landtype','a.stock','a.fixtures','a.fittings','a.rainfall','a.soiltype','a.grazing','a.cropping','a.irrigation','a.rent_type','a.offpeak','a.freq','a.deposit','a.sleeps','a.mediaurl','a.mediatype','a.pdfinfoone','a.pdfinfotwo','a.flplone','a.flpltwo','a.covenantsyn','a.openhouse','a.openhouseinfo','a.hofees','a.annualinsurance','a.taxannual','a.taxyear','a.utilities','a.electricservice','a.averageutilelec','a.averageutilgas','a.pm_price_override','a.pmstartdate','a.pmenddate','a.propmgt_price','a.propmgt_description','a.viewbooking','a.availdate','a.private','a.ctown','a.ctport','a.schooldist','a.elementary','a.midschool','a.highschool','a.university','a.published','a.created_by','a.modified_by','a.created','a.modified','a.version','a.hits','a.ordering'),
			array('id','asset_id','street','streettwo','countryid','cityid','stateid','postcode','county','mls_image','viewad','showprice','price','closeprice','priceview','mls_disclaimer','name','alias','mls_id','rets_source','listingtype','catid','featured','soleagency','listoffice','agent','colistagent','mkt_stats','trans_type','owncoords','latitude','longitude','bedrooms','bathrooms','fullbaths','thqtrbaths','halfbaths','qtrbaths','squarefeet','sqftlower','sqftmainlevel','sqftupper','style','year','yearremodeled','exteriorfinish','roof','flooring','porchpatio','frontage','waterfront','waterfronttype','welldepth','subdivision','landareasqft','acrestotal','lotdimensions','totalrooms','otherrooms','livingarea','ensuite','garagetype','parkingcarport','stories','basementandfoundation','basementsize','basementpctfinished','heating','cooling','appliances','indoorfeatures','houseconstruction','outdoorfeatures','fencing','communityfeatures','otherfeatures','phoneavailableyn','garbagedisposalyn','familyroompresent','laundryroompresent','kitchenpresent','livingroompresent','parkingspaceyn','customone','customtwo','customthree','customfour','customfive','customsix','customseven','customeight','storage','waterresources','sewer','zoning','propdesc','bldg_name','buildingfeatures','takings','returns','netprofit','bustype','bussubtype','percentoffice','percentwarehouse','loadingfac','currentuse','carryingcap','parkinggarage','bldgsqft','totalrents','numunits','unitdetails','tenancytype','tentantpdutilities','commonareas','landtype','stock','fixtures','fittings','rainfall','soiltype','grazing','cropping','irrigation','rent_type','offpeak','freq','deposit','sleeps','mediaurl','mediatype','pdfinfoone','pdfinfotwo','flplone','flpltwo','covenantsyn','openhouse','openhouseinfo','hofees','annualinsurance','taxannual','taxyear','utilities','electricservice','averageutilelec','averageutilgas','pm_price_override','pmstartdate','pmenddate','propmgt_price','propmgt_description','viewbooking','availdate','private','ctown','ctport','schooldist','elementary','midschool','highschool','university','published','created_by','modified_by','created','modified','version','hits','ordering')));
		$query->from($db->quoteName('#__mostwantedrealestate_property', 'a'));

		// [Interpretation 1430] Get from #__mostwantedrealestate_ as b
		$query->select($db->quoteName(
			array('b.name'),
			array('_state_name')));
		$query->join('LEFT', ($db->quoteName('#__mostwantedrealestate_', 'b')) . ' ON (' . $db->quoteName('a.stateid') . ' = ' . $db->quoteName('b.id') . ')');

		// [Interpretation 1430] Get from #__mostwantedrealestate_ as c
		$query->select($db->quoteName(
			array('c.name'),
			array('_city_name')));
		$query->join('LEFT', ($db->quoteName('#__mostwantedrealestate_', 'c')) . ' ON (' . $db->quoteName('a.cityid') . ' = ' . $db->quoteName('c.id') . ')');
		// [Interpretation 1923] Get where a.id is $id
		$query->where('a.id != ' . $db->quote($id));
		// [Interpretation 1923] Get where a.cityid is $cityid
		$query->where('a.cityid = ' . $db->quote($cityid));
		// [Interpretation 1923] Get where a.bedrooms is $bedrooms
		$query->where('a.bedrooms = ' . $db->quote($bedrooms));
		// [Interpretation 1923] Get where a.published is 1
		$query->where('a.published = 1');
		$query->order('a.id ASC');

		// [Interpretation 2240] Reset the query using our newly populated query object.
		$db->setQuery($query);
		$items = $db->loadObjectList();

		if (empty($items))
		{
			return false;
		}

		// [Interpretation 2714] Convert the parameter fields into objects.
		if (MostwantedrealestateHelper::checkArray($items))
		{
			foreach ($items as $nr => &$item)
			{
				// [Interpretation 2719] Always create a slug for sef URL's
				$item->slug = (isset($item->alias) && isset($item->id)) ? $item->id.':'.$item->alias : $item->id;
				if (MostwantedrealestateHelper::checkJson($item->commonareas))
				{
					// [Interpretation 1632] Decode commonareas
					$item->commonareas = json_decode($item->commonareas, true);
				}
				if (MostwantedrealestateHelper::checkJson($item->indoorfeatures))
				{
					// [Interpretation 1632] Decode indoorfeatures
					$item->indoorfeatures = json_decode($item->indoorfeatures, true);
				}
				if (MostwantedrealestateHelper::checkJson($item->buildingfeatures))
				{
					// [Interpretation 1632] Decode buildingfeatures
					$item->buildingfeatures = json_decode($item->buildingfeatures, true);
				}
				if (MostwantedrealestateHelper::checkJson($item->otherfeatures))
				{
					// [Interpretation 1632] Decode otherfeatures
					$item->otherfeatures = json_decode($item->otherfeatures, true);
				}
				if (MostwantedrealestateHelper::checkJson($item->colistagent))
				{
					// [Interpretation 1632] Decode colistagent
					$item->colistagent = json_decode($item->colistagent, true);
				}
				if (MostwantedrealestateHelper::checkJson($item->waterresources))
				{
					// [Interpretation 1632] Decode waterresources
					$item->waterresources = json_decode($item->waterresources, true);
				}
				if (MostwantedrealestateHelper::checkJson($item->appliances))
				{
					// [Interpretation 1632] Decode appliances
					$item->appliances = json_decode($item->appliances, true);
				}
				if (MostwantedrealestateHelper::checkJson($item->outdoorfeatures))
				{
					// [Interpretation 1632] Decode outdoorfeatures
					$item->outdoorfeatures = json_decode($item->outdoorfeatures, true);
				}
				if (MostwantedrealestateHelper::checkJson($item->communityfeatures))
				{
					// [Interpretation 1632] Decode communityfeatures
					$item->communityfeatures = json_decode($item->communityfeatures, true);
				}
				if (MostwantedrealestateHelper::checkJson($item->porchpatio))
				{
					// [Interpretation 1632] Decode porchpatio
					$item->porchpatio = json_decode($item->porchpatio, true);
				}
				if (MostwantedrealestateHelper::checkJson($item->unitdetails))
				{
					// [Interpretation 1632] Decode unitdetails
					$item->unitdetails = json_decode($item->unitdetails, true);
				}
				if (MostwantedrealestateHelper::checkJson($item->garagetype))
				{
					// [Interpretation 1632] Decode garagetype
					$item->garagetype = json_decode($item->garagetype, true);
				}
				if (MostwantedrealestateHelper::checkJson($item->heating))
				{
					// [Interpretation 1632] Decode heating
					$item->heating = json_decode($item->heating, true);
				}
				if (MostwantedrealestateHelper::checkJson($item->cooling))
				{
					// [Interpretation 1632] Decode cooling
					$item->cooling = json_decode($item->cooling, true);
				}
				if (MostwantedrealestateHelper::checkJson($item->openhouseinfo))
				{
					// [Interpretation 1632] Decode openhouseinfo
					$item->openhouseinfo = json_decode($item->openhouseinfo, true);
				}
				// [Interpretation 1647] Make sure the content prepare plugins fire on mls_disclaimer.
				$item->mls_disclaimer = JHtml::_('content.prepare',$item->mls_disclaimer);
				// [Interpretation 1649] Checking if mls_disclaimer has uikit components that must be loaded.
				$this->uikitComp = MostwantedrealestateHelper::getUikitComp($item->mls_disclaimer,$this->uikitComp);
				// [Interpretation 1647] Make sure the content prepare plugins fire on propdesc.
				$item->propdesc = JHtml::_('content.prepare',$item->propdesc);
				// [Interpretation 1649] Checking if propdesc has uikit components that must be loaded.
				$this->uikitComp = MostwantedrealestateHelper::getUikitComp($item->propdesc,$this->uikitComp);
				// [Interpretation 1647] Make sure the content prepare plugins fire on private.
				$item->private = JHtml::_('content.prepare',$item->private);
				// [Interpretation 1649] Checking if private has uikit components that must be loaded.
				$this->uikitComp = MostwantedrealestateHelper::getUikitComp($item->private,$this->uikitComp);
				// [Interpretation 1965] set the global propid value.
				$this->a_propid = $item->id;
			}
		}
		// [Interpretation 2249] return items
		return $items;
	}


	/**
	* Get the uikit needed components
	*
	* @return mixed  An array of objects on success.
	*
	*/
	public function getUikitComp()
	{
		if (isset($this->uikitComp) && MostwantedrealestateHelper::checkArray($this->uikitComp))
		{
			return $this->uikitComp;
		}
		return false;
	}  
}
