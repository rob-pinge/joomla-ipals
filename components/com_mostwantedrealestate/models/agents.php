<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		2.0.0
	@build			22nd September, 2017
	@created		1st May, 2016
	@package		Most Wanted Real Estate
	@subpackage		agents.php
	@author			Most Wanted Web Services, Inc. <http://mostwantedrealestatesites.com>	
	@copyright		Copyright (C) 2015-2017. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
	
	Most Wanted Real Estate Component
	
/------------------------------------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import the Joomla modellist library
jimport('joomla.application.component.modellist');

/**
 * Mostwantedrealestate Model for Agents
 */
class MostwantedrealestateModelAgents extends JModelList
{
	/**
	 * Model user data.
	 *
	 * @var        strings
	 */
	protected $user;
	protected $userId;
	protected $guest;
	protected $groups;
	protected $levels;
	protected $app;
	protected $input;
	protected $uikitComp;

	/**
	 * Method to build an SQL query to load the list data.
	 *
	 * @return      string  An SQL query
	 */
	protected function getListQuery()
	{
		// Get the current user for authorisation checks
		$this->user		= JFactory::getUser();
		$this->userId		= $this->user->get('id');
		$this->guest		= $this->user->get('guest');
                $this->groups		= $this->user->get('groups');
                $this->authorisedGroups	= $this->user->getAuthorisedGroups();
		$this->levels		= $this->user->getAuthorisedViewLevels();
		$this->app		= JFactory::getApplication();
		$this->input		= $this->app->input;
		$this->initSet		= true; 
		// [Interpretation 2669] Get a db connection.
		$db = JFactory::getDbo();

		// [Interpretation 2678] Create a new query object.
		$query = $db->getQuery(true);

		// [Interpretation 1430] Get from #__mostwantedrealestate_agent as a
		$query->select($db->quoteName(
			array('a.id','a.asset_id','a.street','a.streettwo','a.countryid','a.cityid','a.stateid','a.postcode','a.uid','a.email','a.phone','a.mobile','a.fax','a.image','a.bio','a.name','a.alias','a.catid','a.agencyid','a.featured','a.default_agent_yn','a.viewad','a.rets_source','a.owncoords','a.latitude','a.longitude','a.fbook','a.twitter','a.pinterest','a.linkedin','a.youtube','a.gplus','a.skype','a.instagram','a.website','a.blog','a.published','a.created_by','a.modified_by','a.created','a.modified','a.version','a.hits','a.ordering'),
			array('id','asset_id','street','streettwo','countryid','cityid','stateid','postcode','uid','email','phone','mobile','fax','image','bio','name','alias','catid','agencyid','featured','default_agent_yn','viewad','rets_source','owncoords','latitude','longitude','fbook','twitter','pinterest','linkedin','youtube','gplus','skype','instagram','website','blog','published','created_by','modified_by','created','modified','version','hits','ordering')));
		$query->from($db->quoteName('#__mostwantedrealestate_agent', 'a'));

		// [Interpretation 1430] Get from #__mostwantedrealestate_agency as b
		$query->select($db->quoteName(
			array('b.name'),
			array('agency_name')));
		$query->join('LEFT', ($db->quoteName('#__mostwantedrealestate_agency', 'b')) . ' ON (' . $db->quoteName('a.agencyid') . ' = ' . $db->quoteName('b.id') . ')');

		// [Interpretation 1430] Get from #__mostwantedrealestate_country as c
		$query->select($db->quoteName(
			array('c.name'),
			array('country_name')));
		$query->join('LEFT', ($db->quoteName('#__mostwantedrealestate_country', 'c')) . ' ON (' . $db->quoteName('a.countryid') . ' = ' . $db->quoteName('c.id') . ')');

		// [Interpretation 1430] Get from #__mostwantedrealestate_state as d
		$query->select($db->quoteName(
			array('d.name'),
			array('state_name')));
		$query->join('LEFT', ($db->quoteName('#__mostwantedrealestate_state', 'd')) . ' ON (' . $db->quoteName('a.stateid') . ' = ' . $db->quoteName('d.id') . ')');

		// [Interpretation 1430] Get from #__mostwantedrealestate_city as e
		$query->select($db->quoteName(
			array('e.name'),
			array('city_name')));
		$query->join('LEFT', ($db->quoteName('#__mostwantedrealestate_city', 'e')) . ' ON (' . $db->quoteName('a.cityid') . ' = ' . $db->quoteName('e.id') . ')');

		// [Interpretation 1430] Get from #__categories as f
		$query->select($db->quoteName(
			array('f.title'),
			array('title')));
		$query->join('LEFT', ($db->quoteName('#__categories', 'f')) . ' ON (' . $db->quoteName('a.catid') . ' = ' . $db->quoteName('f.id') . ')');
		// [Interpretation 1923] Get where a.published is 1
		$query->where('a.published = 1');
		// [Interpretation 1923] Get where b.published is 1
		$query->where('b.published = 1');
		$query->order('a.name ASC');

		// [Interpretation 2693] return the query object
		return $query;
	}

	/**
	 * Method to get an array of data items.
	 *
	 * @return  mixed  An array of data items on success, false on failure.
	 */
	public function getItems()
	{
		$user = JFactory::getUser();
		// [Interpretation 2019] check if this user has permission to access item
		if (!$user->authorise('site.agents.access', 'com_mostwantedrealestate'))
		{
			$app = JFactory::getApplication();
			$app->enqueueMessage(JText::_('COM_MOSTWANTEDREALESTATE_NOT_AUTHORISED_TO_VIEW_AGENTS'), 'error');
			// [Interpretation 2016] redirect away to the home page if no access allowed.
			$app->redirect(JURI::root());
			return false;
		}  
		// load parent items
		$items = parent::getItems();

		// Get the global params
		$globalParams = JComponentHelper::getParams('com_mostwantedrealestate', true);

		// [Interpretation 2714] Convert the parameter fields into objects.
		if (MostwantedrealestateHelper::checkArray($items))
		{
			foreach ($items as $nr => &$item)
			{
				// [Interpretation 2719] Always create a slug for sef URL's
				$item->slug = (isset($item->alias) && isset($item->id)) ? $item->id.':'.$item->alias : $item->id;
				// [Interpretation 1647] Make sure the content prepare plugins fire on bio.
				$item->bio = JHtml::_('content.prepare',$item->bio);
				// [Interpretation 1649] Checking if bio has uikit components that must be loaded.
				$this->uikitComp = MostwantedrealestateHelper::getUikitComp($item->bio,$this->uikitComp);
			}
		} 


// do a quick build of all edit links
if (isset($items) && $items)
{
	foreach ($items as $nr => &$item)
	{
		$canDo = MostwantedrealestateHelper::getActions('agent',$item,'agents');
		if ($canDo->get('agent.edit'))
		{
			$item->editLink = '<br /><br /><a class="uk-button uk-button-primary uk-width-1-1" href="';
			$item->editLink .= JRoute::_('index.php?option=com_mostwantedrealestate&view=agent&task=agent.edit&id=' . $item->id);
			$item->editLink .= '"><i class="uk-icon-pencil"></i><span class="uk-hidden-small">';
			$item->editLink .= JText::_('COM_MOSTWANTEDREALESTATE_EDIT_AGENT');
			$item->editLink .= '</span></a>';
		}
		else
		{
			$item->editLink = '';
		}
	}
}

		// return items
		return $items;
	} 


	/**
	* Get the uikit needed components
	*
	* @return mixed  An array of objects on success.
	*
	*/
	public function getUikitComp()
	{
		if (isset($this->uikitComp) && MostwantedrealestateHelper::checkArray($this->uikitComp))
		{
			return $this->uikitComp;
		}
		return false;
	}  
}
