<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		2.0.0
	@build			22nd September, 2017
	@created		1st May, 2016
	@package		Most Wanted Real Estate
	@subpackage		agency.php
	@author			Most Wanted Web Services, Inc. <http://mostwantedrealestatesites.com>	
	@copyright		Copyright (C) 2015-2017. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
	
	Most Wanted Real Estate Component
	
/------------------------------------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import the Joomla modellist library
jimport('joomla.application.component.modellist');

/**
 * Mostwantedrealestate Model for Agency
 */
class MostwantedrealestateModelAgency extends JModelList
{
	/**
	 * Model user data.
	 *
	 * @var        strings
	 */
	protected $user;
	protected $userId;
	protected $guest;
	protected $groups;
	protected $levels;
	protected $app;
	protected $input;
	protected $uikitComp;

	/**
	 * Method to build an SQL query to load the list data.
	 *
	 * @return      string  An SQL query
	 */
	protected function getListQuery()
	{
		// Get the current user for authorisation checks
		$this->user		= JFactory::getUser();
		$this->userId		= $this->user->get('id');
		$this->guest		= $this->user->get('guest');
                $this->groups		= $this->user->get('groups');
                $this->authorisedGroups	= $this->user->getAuthorisedGroups();
		$this->levels		= $this->user->getAuthorisedViewLevels();
		$this->app		= JFactory::getApplication();
		$this->input		= $this->app->input;
		$this->initSet		= true; 
		// [Interpretation 2669] Get a db connection.
		$db = JFactory::getDbo();

		// [Interpretation 2678] Create a new query object.
		$query = $db->getQuery(true);

		// [Interpretation 1430] Get from #__mostwantedrealestate_property as a
		$query->select($db->quoteName(
			array('a.id','a.asset_id','a.street','a.streettwo','a.countryid','a.cityid','a.stateid','a.postcode','a.county','a.mls_image','a.viewad','a.showprice','a.price','a.closeprice','a.priceview','a.mls_disclaimer','a.name','a.alias','a.mls_id','a.rets_source','a.listingtype','a.catid','a.featured','a.soleagency','a.listoffice','a.agent','a.colistagent','a.mkt_stats','a.trans_type','a.owncoords','a.latitude','a.longitude','a.bedrooms','a.bathrooms','a.fullbaths','a.thqtrbaths','a.halfbaths','a.qtrbaths','a.squarefeet','a.sqftlower','a.sqftmainlevel','a.sqftupper','a.style','a.year','a.yearremodeled','a.exteriorfinish','a.roof','a.flooring','a.porchpatio','a.frontage','a.waterfront','a.waterfronttype','a.welldepth','a.subdivision','a.landareasqft','a.acrestotal','a.lotdimensions','a.totalrooms','a.otherrooms','a.livingarea','a.ensuite','a.garagetype','a.parkingcarport','a.stories','a.basementandfoundation','a.basementsize','a.basementpctfinished','a.heating','a.cooling','a.appliances','a.indoorfeatures','a.houseconstruction','a.outdoorfeatures','a.fencing','a.communityfeatures','a.otherfeatures','a.phoneavailableyn','a.garbagedisposalyn','a.familyroompresent','a.laundryroompresent','a.kitchenpresent','a.livingroompresent','a.parkingspaceyn','a.customone','a.customtwo','a.customthree','a.customfour','a.customfive','a.customsix','a.customseven','a.customeight','a.storage','a.waterresources','a.sewer','a.zoning','a.propdesc','a.bldg_name','a.buildingfeatures','a.takings','a.returns','a.netprofit','a.bustype','a.bussubtype','a.percentoffice','a.percentwarehouse','a.loadingfac','a.currentuse','a.carryingcap','a.parkinggarage','a.bldgsqft','a.totalrents','a.numunits','a.unitdetails','a.tenancytype','a.tentantpdutilities','a.commonareas','a.landtype','a.stock','a.fixtures','a.fittings','a.rainfall','a.soiltype','a.grazing','a.cropping','a.irrigation','a.rent_type','a.offpeak','a.freq','a.deposit','a.sleeps','a.mediaurl','a.mediatype','a.pdfinfoone','a.pdfinfotwo','a.flplone','a.flpltwo','a.covenantsyn','a.openhouse','a.openhouseinfo','a.hofees','a.annualinsurance','a.taxannual','a.taxyear','a.utilities','a.electricservice','a.averageutilelec','a.averageutilgas','a.pm_price_override','a.pmstartdate','a.pmenddate','a.propmgt_price','a.propmgt_description','a.viewbooking','a.availdate','a.private','a.ctown','a.ctport','a.schooldist','a.elementary','a.midschool','a.highschool','a.university','a.published','a.created_by','a.modified_by','a.created','a.modified','a.version','a.hits','a.ordering'),
			array('id','asset_id','street','streettwo','countryid','cityid','stateid','postcode','county','mls_image','viewad','showprice','price','closeprice','priceview','mls_disclaimer','name','alias','mls_id','rets_source','listingtype','catid','featured','soleagency','listoffice','agent','colistagent','mkt_stats','trans_type','owncoords','latitude','longitude','bedrooms','bathrooms','fullbaths','thqtrbaths','halfbaths','qtrbaths','squarefeet','sqftlower','sqftmainlevel','sqftupper','style','year','yearremodeled','exteriorfinish','roof','flooring','porchpatio','frontage','waterfront','waterfronttype','welldepth','subdivision','landareasqft','acrestotal','lotdimensions','totalrooms','otherrooms','livingarea','ensuite','garagetype','parkingcarport','stories','basementandfoundation','basementsize','basementpctfinished','heating','cooling','appliances','indoorfeatures','houseconstruction','outdoorfeatures','fencing','communityfeatures','otherfeatures','phoneavailableyn','garbagedisposalyn','familyroompresent','laundryroompresent','kitchenpresent','livingroompresent','parkingspaceyn','customone','customtwo','customthree','customfour','customfive','customsix','customseven','customeight','storage','waterresources','sewer','zoning','propdesc','bldg_name','buildingfeatures','takings','returns','netprofit','bustype','bussubtype','percentoffice','percentwarehouse','loadingfac','currentuse','carryingcap','parkinggarage','bldgsqft','totalrents','numunits','unitdetails','tenancytype','tentantpdutilities','commonareas','landtype','stock','fixtures','fittings','rainfall','soiltype','grazing','cropping','irrigation','rent_type','offpeak','freq','deposit','sleeps','mediaurl','mediatype','pdfinfoone','pdfinfotwo','flplone','flpltwo','covenantsyn','openhouse','openhouseinfo','hofees','annualinsurance','taxannual','taxyear','utilities','electricservice','averageutilelec','averageutilgas','pm_price_override','pmstartdate','pmenddate','propmgt_price','propmgt_description','viewbooking','availdate','private','ctown','ctport','schooldist','elementary','midschool','highschool','university','published','created_by','modified_by','created','modified','version','hits','ordering')));
		$query->from($db->quoteName('#__mostwantedrealestate_property', 'a'));

		// [Interpretation 1430] Get from #__mostwantedrealestate_ as c
		$query->select($db->quoteName(
			array('c.name'),
			array('_agency_name')));
		$query->join('LEFT', ($db->quoteName('#__mostwantedrealestate_', 'c')) . ' ON (' . $db->quoteName('a.listoffice') . ' = ' . $db->quoteName('c.id') . ')');

		// [Interpretation 1430] Get from #__mostwantedrealestate_ as d
		$query->select($db->quoteName(
			array('d.name'),
			array('_agent_name')));
		$query->join('LEFT', ($db->quoteName('#__mostwantedrealestate_', 'd')) . ' ON (' . $db->quoteName('a.agent') . ' = ' . $db->quoteName('d.id') . ')');

		// [Interpretation 1430] Get from #__mostwantedrealestate_ as e
		$query->select($db->quoteName(
			array('e.name'),
			array('_country_name')));
		$query->join('LEFT', ($db->quoteName('#__mostwantedrealestate_', 'e')) . ' ON (' . $db->quoteName('a.countryid') . ' = ' . $db->quoteName('e.id') . ')');

		// [Interpretation 1430] Get from #__mostwantedrealestate_ as f
		$query->select($db->quoteName(
			array('f.name'),
			array('_state_name')));
		$query->join('LEFT', ($db->quoteName('#__mostwantedrealestate_', 'f')) . ' ON (' . $db->quoteName('a.stateid') . ' = ' . $db->quoteName('f.id') . ')');

		// [Interpretation 1430] Get from #__mostwantedrealestate_ as g
		$query->select($db->quoteName(
			array('g.name'),
			array('_city_name')));
		$query->join('LEFT', ($db->quoteName('#__mostwantedrealestate_', 'g')) . ' ON (' . $db->quoteName('a.cityid') . ' = ' . $db->quoteName('g.id') . ')');

		// [Interpretation 1430] Get from #__categories as b
		$query->select($db->quoteName(
			array('b.title','b.alias'),
			array('title','alias')));
		$query->join('LEFT', ($db->quoteName('#__categories', 'b')) . ' ON (' . $db->quoteName('a.catid') . ' = ' . $db->quoteName('b.id') . ')');
		// [Interpretation 1775] Check if JRequest::getInt('id') is a string or numeric value.
		$checkValue = JRequest::getInt('id');
		if (isset($checkValue) && MostwantedrealestateHelper::checkString($checkValue))
		{
			$query->where('a.listoffice = ' . $db->quote($checkValue));
		}
		elseif (is_numeric($checkValue))
		{
			$query->where('a.listoffice = ' . $checkValue);
		}
		else
		{
			return false;
		}
		$query->where('a.access IN (' . implode(',', $this->levels) . ')');
		// [Interpretation 1923] Get where a.published is 1
		$query->where('a.published = 1');
		$query->order('a.ordering ASC');

		// [Interpretation 2693] return the query object
		return $query;
	}

	/**
	 * Method to get an array of data items.
	 *
	 * @return  mixed  An array of data items on success, false on failure.
	 */
	public function getItems()
	{
		$user = JFactory::getUser();
		// [Interpretation 2019] check if this user has permission to access item
		if (!$user->authorise('site.agency.access', 'com_mostwantedrealestate'))
		{
			$app = JFactory::getApplication();
			$app->enqueueMessage(JText::_('COM_MOSTWANTEDREALESTATE_NOT_AUTHORISED_TO_VIEW_AGENCY'), 'error');
			// [Interpretation 2011] redirect away to the default view if no access allowed.
			$app->redirect(JRoute::_('index.php?option=com_mostwantedrealestate&view=properties'));
			return false;
		}  
		// load parent items
		$items = parent::getItems();

		// Get the global params
		$globalParams = JComponentHelper::getParams('com_mostwantedrealestate', true);

		// [Interpretation 2714] Convert the parameter fields into objects.
		if (MostwantedrealestateHelper::checkArray($items))
		{
			foreach ($items as $nr => &$item)
			{
				// [Interpretation 2719] Always create a slug for sef URL's
				$item->slug = (isset($item->alias) && isset($item->id)) ? $item->id.':'.$item->alias : $item->id;
				if (MostwantedrealestateHelper::checkJson($item->commonareas))
				{
					// [Interpretation 1632] Decode commonareas
					$item->commonareas = json_decode($item->commonareas, true);
				}
				if (MostwantedrealestateHelper::checkJson($item->indoorfeatures))
				{
					// [Interpretation 1632] Decode indoorfeatures
					$item->indoorfeatures = json_decode($item->indoorfeatures, true);
				}
				if (MostwantedrealestateHelper::checkJson($item->buildingfeatures))
				{
					// [Interpretation 1632] Decode buildingfeatures
					$item->buildingfeatures = json_decode($item->buildingfeatures, true);
				}
				if (MostwantedrealestateHelper::checkJson($item->otherfeatures))
				{
					// [Interpretation 1632] Decode otherfeatures
					$item->otherfeatures = json_decode($item->otherfeatures, true);
				}
				if (MostwantedrealestateHelper::checkJson($item->colistagent))
				{
					// [Interpretation 1632] Decode colistagent
					$item->colistagent = json_decode($item->colistagent, true);
				}
				if (MostwantedrealestateHelper::checkJson($item->waterresources))
				{
					// [Interpretation 1632] Decode waterresources
					$item->waterresources = json_decode($item->waterresources, true);
				}
				if (MostwantedrealestateHelper::checkJson($item->appliances))
				{
					// [Interpretation 1632] Decode appliances
					$item->appliances = json_decode($item->appliances, true);
				}
				if (MostwantedrealestateHelper::checkJson($item->outdoorfeatures))
				{
					// [Interpretation 1632] Decode outdoorfeatures
					$item->outdoorfeatures = json_decode($item->outdoorfeatures, true);
				}
				if (MostwantedrealestateHelper::checkJson($item->communityfeatures))
				{
					// [Interpretation 1632] Decode communityfeatures
					$item->communityfeatures = json_decode($item->communityfeatures, true);
				}
				if (MostwantedrealestateHelper::checkJson($item->porchpatio))
				{
					// [Interpretation 1632] Decode porchpatio
					$item->porchpatio = json_decode($item->porchpatio, true);
				}
				if (MostwantedrealestateHelper::checkJson($item->unitdetails))
				{
					// [Interpretation 1632] Decode unitdetails
					$item->unitdetails = json_decode($item->unitdetails, true);
				}
				if (MostwantedrealestateHelper::checkJson($item->garagetype))
				{
					// [Interpretation 1632] Decode garagetype
					$item->garagetype = json_decode($item->garagetype, true);
				}
				if (MostwantedrealestateHelper::checkJson($item->heating))
				{
					// [Interpretation 1632] Decode heating
					$item->heating = json_decode($item->heating, true);
				}
				if (MostwantedrealestateHelper::checkJson($item->cooling))
				{
					// [Interpretation 1632] Decode cooling
					$item->cooling = json_decode($item->cooling, true);
				}
				if (MostwantedrealestateHelper::checkJson($item->openhouseinfo))
				{
					// [Interpretation 1632] Decode openhouseinfo
					$item->openhouseinfo = json_decode($item->openhouseinfo, true);
				}
				// [Interpretation 1647] Make sure the content prepare plugins fire on mls_disclaimer.
				$item->mls_disclaimer = JHtml::_('content.prepare',$item->mls_disclaimer);
				// [Interpretation 1649] Checking if mls_disclaimer has uikit components that must be loaded.
				$this->uikitComp = MostwantedrealestateHelper::getUikitComp($item->mls_disclaimer,$this->uikitComp);
				// [Interpretation 1647] Make sure the content prepare plugins fire on propdesc.
				$item->propdesc = JHtml::_('content.prepare',$item->propdesc);
				// [Interpretation 1649] Checking if propdesc has uikit components that must be loaded.
				$this->uikitComp = MostwantedrealestateHelper::getUikitComp($item->propdesc,$this->uikitComp);
				// [Interpretation 1647] Make sure the content prepare plugins fire on private.
				$item->private = JHtml::_('content.prepare',$item->private);
				// [Interpretation 1649] Checking if private has uikit components that must be loaded.
				$this->uikitComp = MostwantedrealestateHelper::getUikitComp($item->private,$this->uikitComp);
				// [Interpretation 1680] set idPropidH to the $item object.
				$item->idPropidH = $this->getIdPropidBeae_H($item->id);
			}
		} 

		// return items
		return $items;
	} 

	/**
	* Method to get an array of  Objects.
	*
	* @return mixed  An array of  Objects on success, false on failure.
	*
	*/
	public function getIdPropidBeae_H($id)
	{
		// [Interpretation 2445] Get a db connection.
		$db = JFactory::getDbo();

		// [Interpretation 2447] Create a new query object.
		$query = $db->getQuery(true);

		// [Interpretation 2449] Get from #__mostwantedrealestate_ as h
		$query->select($db->quoteName(
			array('h.path','h.filename','h.type','h.title','h.description'),
			array('path','filename','type','title','description')));
		$query->from($db->quoteName('#__mostwantedrealestate_', 'h'));
		$query->where('h.propid = ' . $db->quote($id));

		// [Interpretation 2503] Reset the query using our newly populated query object.
		$db->setQuery($query);
		$db->execute();

		// [Interpretation 2506] check if there was data returned
		if ($db->getNumRows())
		{
			return $db->loadObjectList();
		}
		return false;
	}


	/**
	* Custom Method
	*
	* @return mixed  item data object on success, false on failure.
	*
	*/
	public function getAgency()
	{

		if (!isset($this->initSet) || !$this->initSet)
		{
			$this->user		= JFactory::getUser();
			$this->userId		= $this->user->get('id');
			$this->guest		= $this->user->get('guest');
			$this->groups		= $this->user->get('groups');
			$this->authorisedGroups	= $this->user->getAuthorisedGroups();
			$this->levels		= $this->user->getAuthorisedViewLevels();
			$this->initSet		= true;
		}
		// [Interpretation 2055] Get a db connection.
		$db = JFactory::getDbo();

		// [Interpretation 2057] Create a new query object.
		$query = $db->getQuery(true);

		// [Interpretation 1430] Get from #__mostwantedrealestate_agency as a
		$query->select($db->quoteName(
			array('a.street','a.streettwo','a.cityid','a.stateid','a.postcode','a.countryid','a.email','a.website','a.phone','a.license','a.fax','a.description','a.name','a.owncoords','a.latitude','a.longitude','a.image'),
			array('street','streettwo','cityid','stateid','postcode','countryid','email','website','phone','license','fax','description','name','owncoords','latitude','longitude','image')));
		$query->from($db->quoteName('#__mostwantedrealestate_agency', 'a'));

		// [Interpretation 1430] Get from #__mostwantedrealestate_ as b
		$query->select($db->quoteName(
			array('b.name'),
			array('_state_name')));
		$query->join('LEFT', ($db->quoteName('#__mostwantedrealestate_', 'b')) . ' ON (' . $db->quoteName('a.stateid') . ' = ' . $db->quoteName('b.id') . ')');

		// [Interpretation 1430] Get from #__mostwantedrealestate_ as c
		$query->select($db->quoteName(
			array('c.name'),
			array('_city_name')));
		$query->join('LEFT', ($db->quoteName('#__mostwantedrealestate_', 'c')) . ' ON (' . $db->quoteName('a.cityid') . ' = ' . $db->quoteName('c.id') . ')');

		// [Interpretation 1430] Get from #__mostwantedrealestate_ as d
		$query->select($db->quoteName(
			array('d.name'),
			array('_country_name')));
		$query->join('LEFT', ($db->quoteName('#__mostwantedrealestate_', 'd')) . ' ON (' . $db->quoteName('a.countryid') . ' = ' . $db->quoteName('d.id') . ')');
		$query->where('a.access IN (' . implode(',', $this->levels) . ')');
		// [Interpretation 1775] Check if JRequest::getInt('id') is a string or numeric value.
		$checkValue = JRequest::getInt('id');
		if (isset($checkValue) && MostwantedrealestateHelper::checkString($checkValue))
		{
			$query->where('a.id = ' . $db->quote($checkValue));
		}
		elseif (is_numeric($checkValue))
		{
			$query->where('a.id = ' . $checkValue);
		}
		else
		{
			return false;
		}
		// [Interpretation 1923] Get where a.published is 1
		$query->where('a.published = 1');
		$query->order('a.ordering ASC');

		// [Interpretation 2068] Reset the query using our newly populated query object.
		$db->setQuery($query);
		// [Interpretation 2070] Load the results as a stdClass object.
		$data = $db->loadObject();

		if (empty($data))
		{
			return false;
		}
		// [Interpretation 1647] Make sure the content prepare plugins fire on description.
		$data->description = JHtml::_('content.prepare',$data->description);
		// [Interpretation 1649] Checking if description has uikit components that must be loaded.
		$this->uikitComp = MostwantedrealestateHelper::getUikitComp($data->description,$this->uikitComp);

		// [Interpretation 2179] return data object.
		return $data;
	}


	/**
	* Get the uikit needed components
	*
	* @return mixed  An array of objects on success.
	*
	*/
	public function getUikitComp()
	{
		if (isset($this->uikitComp) && MostwantedrealestateHelper::checkArray($this->uikitComp))
		{
			return $this->uikitComp;
		}
		return false;
	}  
}
