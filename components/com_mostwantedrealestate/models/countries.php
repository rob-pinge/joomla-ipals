<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		2.0.0
	@build			22nd September, 2017
	@created		1st May, 2016
	@package		Most Wanted Real Estate
	@subpackage		countries.php
	@author			Most Wanted Web Services, Inc. <http://mostwantedrealestatesites.com>	
	@copyright		Copyright (C) 2015-2017. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
	
	Most Wanted Real Estate Component
	
/------------------------------------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import the Joomla modellist library
jimport('joomla.application.component.modellist');

/**
 * Mostwantedrealestate Model for Countries
 */
class MostwantedrealestateModelCountries extends JModelList
{
	/**
	 * Model user data.
	 *
	 * @var        strings
	 */
	protected $user;
	protected $userId;
	protected $guest;
	protected $groups;
	protected $levels;
	protected $app;
	protected $input;
	protected $uikitComp;

	/**
	 * Method to build an SQL query to load the list data.
	 *
	 * @return      string  An SQL query
	 */
	protected function getListQuery()
	{
		// Get the current user for authorisation checks
		$this->user		= JFactory::getUser();
		$this->userId		= $this->user->get('id');
		$this->guest		= $this->user->get('guest');
                $this->groups		= $this->user->get('groups');
                $this->authorisedGroups	= $this->user->getAuthorisedGroups();
		$this->levels		= $this->user->getAuthorisedViewLevels();
		$this->app		= JFactory::getApplication();
		$this->input		= $this->app->input;
		$this->initSet		= true; 
		// [Interpretation 2669] Get a db connection.
		$db = JFactory::getDbo();

		// [Interpretation 2678] Create a new query object.
		$query = $db->getQuery(true);

		// [Interpretation 1430] Get from #__mostwantedrealestate_country as a
		$query->select($db->quoteName(
			array('a.id','a.asset_id','a.name','a.alias','a.image','a.description','a.owncoords','a.latitude','a.longitude','a.published','a.created_by','a.modified_by','a.created','a.modified','a.version','a.hits','a.ordering'),
			array('id','asset_id','name','alias','image','description','owncoords','latitude','longitude','published','created_by','modified_by','created','modified','version','hits','ordering')));
		$query->from($db->quoteName('#__mostwantedrealestate_country', 'a'));
		$query->where('a.access IN (' . implode(',', $this->levels) . ')');
		// [Interpretation 1923] Get where a.published is 1
		$query->where('a.published = 1');
		$query->order('a.name ASC');

		// [Interpretation 2693] return the query object
		return $query;
	}

	/**
	 * Method to get an array of data items.
	 *
	 * @return  mixed  An array of data items on success, false on failure.
	 */
	public function getItems()
	{
		$user = JFactory::getUser();
		// [Interpretation 2019] check if this user has permission to access item
		if (!$user->authorise('site.countries.access', 'com_mostwantedrealestate'))
		{
			$app = JFactory::getApplication();
			$app->enqueueMessage(JText::_('COM_MOSTWANTEDREALESTATE_NOT_AUTHORISED_TO_VIEW_COUNTRIES'), 'error');
			// [Interpretation 2016] redirect away to the home page if no access allowed.
			$app->redirect(JURI::root());
			return false;
		}  
		// load parent items
		$items = parent::getItems();

		// Get the global params
		$globalParams = JComponentHelper::getParams('com_mostwantedrealestate', true);

		// [Interpretation 2714] Convert the parameter fields into objects.
		if (MostwantedrealestateHelper::checkArray($items))
		{
			foreach ($items as $nr => &$item)
			{
				// [Interpretation 2719] Always create a slug for sef URL's
				$item->slug = (isset($item->alias) && isset($item->id)) ? $item->id.':'.$item->alias : $item->id;
				// [Interpretation 1647] Make sure the content prepare plugins fire on description.
				$item->description = JHtml::_('content.prepare',$item->description);
				// [Interpretation 1649] Checking if description has uikit components that must be loaded.
				$this->uikitComp = MostwantedrealestateHelper::getUikitComp($item->description,$this->uikitComp);
			}
		} 

		// return items
		return $items;
	} 


	/**
	* Get the uikit needed components
	*
	* @return mixed  An array of objects on success.
	*
	*/
	public function getUikitComp()
	{
		if (isset($this->uikitComp) && MostwantedrealestateHelper::checkArray($this->uikitComp))
		{
			return $this->uikitComp;
		}
		return false;
	}  
}
