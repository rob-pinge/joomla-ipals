<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		2.0.0
	@build			22nd September, 2017
	@created		1st May, 2016
	@package		Most Wanted Real Estate
	@subpackage		default_agent-properties-grid.php
	@author			Most Wanted Web Services, Inc. <http://mostwantedrealestatesites.com>	
	@copyright		Copyright (C) 2015-2017. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
	
	Most Wanted Real Estate Component
	
/------------------------------------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access'); 

// Load JQuery Framework
JHtml::_('jquery.framework');

// Set Global Parameters
$globalParams = JComponentHelper::getParams('com_mostwantedrealestate');


?>
<!-- GRID VIEW-->
<div >
<?php echo JLayoutHelper::render('agentaboutlayout', $this->agent); ?>
  <h3><?php echo JText::_('COM_MOSTWANTEDREALESTATE_WHERE_AM_I'); ?></h3>

<!-- Google/Bing Maps -->
  <div id="map-area">
    <div id="map"></div>
  </div>
        <?php if($globalParams->get('map_provider') == '1'):?>
            <input type="button" value="Toggle Street View" onclick="toggleStreetView();"></input>
        <?php endif; ?>
<!-- End Google/Bing Maps -->

<!-- Property listing view-->
  <h3><?php echo JText::_('COM_MOSTWANTEDREALESTATE_BROWSE_MY_LISTINGS'); ?></h3>
  <div>
    <div class='uk-form-row'>
      <div><?php echo $this->loadTemplate('agent-filters'); ?></div>
      <div> </div>
    </div>
<?php echo $this->loadTemplate('agentpropertylistings'); ?>
  </div>
    <!-- End Property listing view--> 
<hr/>
<div>
<!-- Contact Form -->
<?php echo JLayoutHelper::render('agentcontact', $this->items); ?>
<!-- End Contact Form -->
</div>
</div>

<script type="text/javascript">
    jQuery(function(){
        initMap();
    })
</script>


