<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		2.0.0
	@build			22nd September, 2017
	@created		1st May, 2016
	@package		Most Wanted Real Estate
	@subpackage		default_openhousesgmap.php
	@author			Most Wanted Web Services, Inc. <http://mostwantedrealestatesites.com>	
	@copyright		Copyright (C) 2015-2017. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
	
	Most Wanted Real Estate Component
	
/------------------------------------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access'); 

?>
<div id="map_wrapper">
    <div id="map_canvas" class="mapping"></div>
</div>

<!-- Google Maps javascript -->
<script>
    var mapOptions = {
        apiKey: '<?php echo $this->params->get('googlemapsapi'); ?>',
        mapElement: 'map_canvas',
        initial_zoom: <?php echo $this->params->get('zoom'); ?>,
        center: {
            lat: <?php echo $this->params->get('latitude'); ?> ,
            lng: <?php echo $this->params->get('longitude'); ?>
        }
    };
    
jQuery(function($) {
    // Asynchronously Load the map API 
    var script = document.createElement('script');
    script.src = "//maps.googleapis.com/maps/api/js?callback=initMap&key=<?php echo $this->params->get('gmapsapi'); ?>";
    document.body.appendChild(script);
});
</script>
<!-- Google Maps CSS for Pin -->
<style>
#map_wrapper {
    height: 600px;
}

#map_canvas {
    width: 100%;
    height: 100%;
}
</style>

<?php
    $document = JFactory::getDocument();
    $document->addScript(JUri::base() . 'components/com_mostwantedrealestate/assets/js/googlemap_filtered.js');
?>
