<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		2.0.0
	@build			22nd September, 2017
	@created		1st May, 2016
	@package		Most Wanted Real Estate
	@subpackage		default_openhousesbingmap.php
	@author			Most Wanted Web Services, Inc. <http://mostwantedrealestatesites.com>	
	@copyright		Copyright (C) 2015-2017. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
	
	Most Wanted Real Estate Component
	
/------------------------------------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access'); 

?>
<!--<script type="text/javascript" src="http://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=8.0&callback=initMap" async defer></script>-->
<div id="map_wrapper">
    <div id="map_canvas"></div>
</div>

<script>
    
    var mapOptions = {
        apiKey: '<?php echo $this->params->get('bingmapsapi'); ?>',
        mapElement: 'map_canvas',
        initial_zoom: <?php echo $this->params->get('zoom'); ?>,
        center: {
            lat: <?php echo $this->params->get('latitude'); ?> ,
            lng: <?php echo $this->params->get('longitude'); ?>
        }
    };
    
    jQuery(function(){

        jQuery('<script>')
           .attr('type', 'text/javascript')
           .attr('src', 'https://www.bing.com/api/maps/mapcontrol?callback=initMap')
           //.attr('async', 'true')
           //.attr('defer', 'true')
           .appendTo('body');
        ////initMap(mapOptions);
    });
    
  </script>
    <style>
        #map_wrapper {
            height: 600px;
            position:relative;
        }
        
    /*#map_canvas { position: absolute; top: 20; left: 10;  height: 500px; }*/
        #map_canvas {
            width: 100%;
            height: 100%;
            position: absolute;
        }
        /*
</style>
<?php
    $document = JFactory::getDocument();
    $document->addScript(JUri::base() . 'components/com_mostwantedrealestate/assets/js/bingmap_filtered.js');
?>
