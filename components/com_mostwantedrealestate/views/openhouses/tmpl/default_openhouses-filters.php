<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		2.0.0
	@build			22nd September, 2017
	@created		1st May, 2016
	@package		Most Wanted Real Estate
	@subpackage		default_openhouses-filters.php
	@author			Most Wanted Web Services, Inc. <http://mostwantedrealestatesites.com>	
	@copyright		Copyright (C) 2015-2017. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
	
	Most Wanted Real Estate Component
	
/------------------------------------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access'); 

?>
<div id="filter_container" class="uk-form">
  <fieldset data-uk-margin>
    <?php if ($this->params->get('oh_keyword_filter') == 1) : ?>
        <input type="text" id="keyword" placeholder="Keyword">
    <?php endif; ?>
    <?php if ($this->params->get('oh_category_filter') == 1) : ?>
        <div  class="uk-button uk-form-select" data-uk-form-select>
        <span></span>
        <i class="uk-icon-caret-down"></i>
        <select id="categoryDd">
          <option value="">All Property Categories:</option>
          <?php foreach ($this->categorylist as $item) { echo "<option value=" . $item->id . ">" . $item->title . "</option>"; } ?>
        </select>
        </div>
	<?php endif; ?>
	<?php if ($this->params->get('oh_transtype_filter') == 1) : ?>
        <div  class="uk-button uk-form-select" data-uk-form-select>
        <span></span>
        <i class="uk-icon-caret-down"></i>
        <select id="transtypeDd">
          <option value="">All Transaction Types:</option>
          <?php foreach ($this->transactiontypeslist as $item) { echo "<option value=" . $item->id . ">" . $item->name . "</option>"; } ?>
        </select>
        </div>
	<?php endif; ?>
    <?php if ($this->params->get('oh_mktstatus_filter') == 1) : ?>
        <div  class="uk-button uk-form-select" data-uk-form-select>
        <span></span>
        <i class="uk-icon-caret-down"></i>
        <select id="marketstatusDd">
          <option value="">All Market Statuses:</option>
          <?php foreach ($this->marketstatuslist as $item) { echo "<option value=" . $item->id . ">" . $item->name . "</option>"; } ?>
        </select>
        </div>
	<?php endif; ?>
    <?php if ($this->params->get('oh_agent_filter') == 1) : ?>
        <div  class="uk-button uk-form-select" data-uk-form-select>
        <span></span>
        <i class="uk-icon-caret-down"></i>
        <select id="agentDd">
          <option value="">All Agents:</option>
          <?php foreach ($this->agentlist as $item) { echo "<option value=" . $item->id . ">" . $item->name . "</option>"; } ?>
        </select>
        </div>
	<?php endif; ?>
    <?php if ($this->params->get('oh_state_filter') == 1) : ?>
        <div  class="uk-button uk-form-select" data-uk-form-select>
        <span></span>
        <i class="uk-icon-caret-down"></i>
        <select id="stateDd">
          <option value="">All States:</option>
          <?php foreach ($this->statelist as $item) { echo "<option value=" . $item->id . ">" . $item->name . "</option>"; } ?>
        </select>
        </div>
	<?php endif; ?>
    <?php if ($this->params->get('oh_city_filter') == 1) : ?>
        <div  class="uk-button uk-form-select" data-uk-form-select>
        <span></span>
        <i class="uk-icon-caret-down"></i>
        <select id="cityDd">
          <option value="">All Cities:</option>
          <?php foreach ($this->citylist as $item) { echo "<option value=" . $item->id . ">" . $item->name . "</option>"; } ?>
        </select>
        </div>
	<?php endif; ?>
    <?php if ($this->params->get('oh_beds_filter') == 1) : ?>
        <div  class="uk-button uk-form-select" data-uk-form-select>
        <span></span>
        <i class="uk-icon-caret-down"></i>
        <select name="minbedsDd" id='minbedsDd'>
            <option value="">Min beds:</option>
            <?php
                $minbeds = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
                foreach ($minbeds as $minbed) {
                    echo "<option value=" . $minbed . ">" . $minbed . "</option>";
                }
            ?>
        </select>
        </div>
	<?php endif; ?>
    <?php if ($this->params->get('oh_baths_filter') == 1) : ?>
        <div  class="uk-button uk-form-select" data-uk-form-select>
        <span></span>
        <i class="uk-icon-caret-down"></i>
        <select name="minbathDd" id='minbathDd'>
          <option value="">Min baths:</option>
          <?php
                        $minbaths = ['1 or more', '2 or more', '3 or more', '4 or more', '5 or more'];
                        foreach ($minbaths as $key => $val) {
                            echo "<option value=" . $key . ">" . $val . "</option>";
                        }
                        ?>
        </select>
        </div>
	<?php endif; ?>
    <?php if ($this->params->get('oh_area_filter') == 1) : ?>
        <div  class="uk-button uk-form-select" data-uk-form-select>
        <span></span>
        <i class="uk-icon-caret-down"></i>
        <select name="min_area" id='min_area'>
          <option value="">Min area:</option>
          <?php
                        $areas = [0, 500, 1000, 1500, 2000, 2500, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000, 15000, 20000];
                        foreach ($areas as $area) {
                            echo "<option value=" . $area . ">" . $area . "</option>";
                        }
                        ?>
        </select>
        </div>
        <div  class="uk-button uk-form-select" data-uk-form-select>
        <span></span>
        <i class="uk-icon-caret-down"></i>
        <select name="max_area" id='max_area'>
          <option value="">Max area:</option>
            <?php
                $areas = [25000, 20000, 15000, 10000, 9000, 8000, 7000, 6000, 5000, 4000, 3000, 2500, 2000, 1500, 1000, 500, 0];
                foreach ($areas as $area) {
                    echo "<option value=" . $area . ">" . $area . "</option>";
                }
            ?>
        </select>
        </div>
	<?php endif; ?>
    <?php if ($this->params->get('oh_price_filter') == 1) : ?>
        <fieldset data-uk-margin>
            <input type="number" id="min_price" placeholder="Min Price">
            <input type="number" id="max_price" placeholder="Max Price">
        </fieldset>
	<?php endif; ?>
    <?php if ($this->params->get('oh_land_filter') == 1) : ?>
        <input type="number" name='min_land' id='min_land' placeholder="Min Land">
        <input type="number" name='max_land' id='max_land' placeholder="Max Land">
	<?php endif; ?>
  </fieldset>
</div>
    
    <?php
        $document = JFactory::getDocument();
        $document->addScript(JUri::base() . 'components/com_mostwantedrealestate/assets/js/propertyfilter.js');
        $document->addScript(JUri::base() . 'components/com_mostwantedrealestate/assets/js/sortBy.js');
    ?>
<script type="text/javascript">

    var properties = <?php echo json_encode($this->items);?>;
    var properties_filters_basepath = '<?php echo JURI::root(); ?>';
    
    jQuery(function(){
        jQuery(document).ready(function () {
        
            <?php if ( isset( $_REQUEST['categoryDd'] ) &&  ( $_REQUEST['categoryDd'] != NULL ) ) { ?>
            jQuery('.categoryDd').val("<?php echo $_REQUEST['categoryDd']; ?>");
            <?php } ?>
            <?php if ( isset( $_REQUEST['transtypeDd'] ) &&  ( $_REQUEST['transtypeDd'] != NULL ) ) { ?>
            jQuery('.transtypeDd').val("<?php echo $_REQUEST['transtypeDd']; ?>");
            <?php } ?>
            <?php if ( isset( $_REQUEST['marketstatusDd'] ) &&  ( $_REQUEST['marketstatusDd'] != NULL ) ) { ?>
            jQuery('.marketstatusDd').val("<?php echo $_REQUEST['marketstatusDd']; ?>");
            <?php } ?>
            <?php if ( isset( $_REQUEST['agentDd'] ) &&  ( $_REQUEST['agentDd'] != NULL ) ) { ?>
            jQuery('.agentDd').val("<?php echo $_REQUEST['agentDd']; ?>");
            <?php } ?>
            <?php if ( isset( $_REQUEST['stateDd'] ) &&  ( $_REQUEST['stateDd'] != NULL ) ) { ?>
            jQuery('.stateDd').val("<?php echo $_REQUEST['stateDd']; ?>");
            <?php } ?>
            <?php if ( isset( $_REQUEST['cityDd'] ) &&  ( $_REQUEST['cityDd'] != NULL ) ) { ?>
            jQuery('.cityDd').val("<?php echo $_REQUEST['cityDd']; ?>");
            <?php } ?>
            <?php if ( isset( $_REQUEST['minbedsDd'] ) &&  ( $_REQUEST['minbedsDd'] != NULL ) ) { ?>
            jQuery('.minbedsDd').val("<?php echo $_REQUEST['minbedsDd']; ?>");
            <?php } ?>
            <?php if ( isset( $_REQUEST['minbathDd'] ) &&  ( $_REQUEST['minbathDd'] != NULL ) ) { ?>
            jQuery('.minbathDd').val("<?php echo $_REQUEST['minbathDd']; ?>");
            <?php } ?>
            <?php if ( isset( $_REQUEST['min_area'] ) &&  ( $_REQUEST['min_area'] != NULL ) ) { ?>
            jQuery('.min_area').val("<?php echo $_REQUEST['min_area']; ?>");
            <?php } ?>
            <?php if (isset($_REQUEST['max_area']) &&  ($_REQUEST['max_area'] != NULL) ) { ?>
            jQuery('.max_area').val("<?php echo $_REQUEST['max_area']; ?>");
            <?php } ?>

        });
        
        initFilter();
    });
</script>
<?php
    $document = JFactory::getDocument();
    $document->addScript(JUri::base() . 'components/com_mostwantedrealestate/assets/js/js.cooky.js');
?>


