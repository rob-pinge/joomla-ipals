<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		2.0.0
	@build			22nd September, 2017
	@created		1st May, 2016
	@package		Most Wanted Real Estate
	@subpackage		default_property-grid.php
	@author			Most Wanted Web Services, Inc. <http://mostwantedrealestatesites.com>	
	@copyright		Copyright (C) 2015-2017. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
	
	Most Wanted Real Estate Component
	
/------------------------------------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access'); 

?>
<?php echo JLayoutHelper::render('propertyviewheading', $this->item); ?>

<!-- Slideshow-->
<div class="uk-width-1-1 uk-margin-bottom">
<?php
if ($this->params->get('property_slideshow') == 1):
  echo $this->loadTemplate('propertyslideshow');
endif;
?>
</div>
<!-- End Slideshow -->

<!-- GRID VIEW-->

<div class="uk-margin-bottom">
  <h3><?php echo $this->item->street.' '.$this->item->streettwo.' '.$this->item->city_name.' '.$this->item->state_name.' '.$this->item->country_name; ?></h3>
  <div></div>
<!-- Google/Bing Maps -->
  <div id="map-area">
    <div id="map"></div>
  </div>
        <?php if($this->params->get('map_provider') == '1'):?>
            <input type="button" value="Toggle Street View" onclick="toggleStreetView();"></input>
        <?php endif; ?>
<!-- End Google/Bing Maps -->

</div>
<!-- Key Details -->
<?php echo JLayoutHelper::render('propertyquickdetails', $this->item); ?>
<!-- End Key Details -->

<script type="text/javascript">
    jQuery(function(){
        initMap();
    })
</script>



