<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		2.0.0
	@build			22nd September, 2017
	@created		1st May, 2016
	@package		Most Wanted Real Estate
	@subpackage		default.php
	@author			Most Wanted Web Services, Inc. <http://mostwantedrealestatesites.com>	
	@copyright		Copyright (C) 2015-2017. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
	
	Most Wanted Real Estate Component
	
/------------------------------------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access'); 

/* JHTML::_('behavior.modal'); */
require_once( JPATH_BASE.'/components/com_mailto/helpers/mailto.php' );


?>
<?php echo $this->toolbar->render(); ?> 
<div class="uk-container">
<?php if ($this->params->get('property_layout') == 2) : ?>
<?php echo $this->loadTemplate('propertylayoutflat'); ?>
<?php elseif ($this->params->get('property_layout') == 3) : ?>
<?php echo $this->loadTemplate('property-tabbed'); ?>
<?php elseif ($this->params->get('property_layout') == 4) : ?>
<?php echo $this->loadTemplate('property-accordion'); ?>
<?php else: ?>
<?php echo $this->loadTemplate('property-grid'); ?>
<?php endif; ?>
</div>

<?php if($this->params->get('map_provider') == '1'){?>
<script>
    var map;
    
    function initMap() {
        if(typeof google == "undefined"){
            setTimeout(function(){
                initMap();
                return;
            },1000)
        }
        var place = {
            lat: <?php echo ( $this->item->latitude != '') ? $this->item->latitude : '47.6149942';?>,
            lng: <?php echo ($this->item->longitude != '') ? $this->item->longitude : '-122.4759886'; ?>
        };

        var zoom = <?php echo ($this->params->get('zoom') !='' ) ? $this->params->get('zoom') : '10';?>;
        
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: zoom,
            center: place
        });
        
        var marker = new google.maps.Marker({
            position: place,
            map: map
        });

        // We get the map's default panorama and set up some defaults.
        // Note that we don't yet set it visible.
        panorama = map.getStreetView();
        panorama.setPosition(place);
        panorama.setPov(/** @type {google.maps.StreetViewPov} */({
            heading: 265,
            pitch: 0
        }));
    }
    function toggleStreetView() {
        var toggle = panorama.getVisible();
        if (toggle == false) {
            panorama.setVisible(true);
        } else {
            panorama.setVisible(false);
        }
    }
</script> 
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=<?php echo $this->params->get('gmapsapi'); ?>">
</script>
<?php } 
if($this->params->get('map_provider') == '2'){
	$doc = JFactory::getDocument();
    //$doc->addScript(JURI::root()."components/com_mostwantedrealestate/assets/js/bingmap.min.js");
    $doc->addScript("https://www.bing.com/api/maps/mapcontrol");
  ?>
<!--<script type="text/javascript" src="http://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=7.0"  charset="UTF-8"></script>-->

<script>
    //jQuery(function(){
    //    jQuery('<script>')
    //        .attr('type', 'text/javascript')
    //        .attr('src', 'https://www.bing.com/api/maps/mapcontrol')
    //        //.attr('async', 'true')
    //        //.attr('defer', 'true')
    //        .appendTo('body');
    //
    //    ////initMap(mapOptions);
    //});
    
    
    var map;
    
    function initMap()
    {
        if(typeof Microsoft.Maps.Location != "function"){
            setTimeout(function(){
                initMap();
                return;
            },1000)
        }
        var lat = <?php echo ( $this->item->latitude != '') ? $this->item->latitude : '47.6149942';?>;
        var long = <?php echo ($this->item->longitude != '') ? $this->item->longitude : '-122.4759886'; ?>;
        var center = new Microsoft.Maps.Location(lat, long);
        
        // Initialize the map
        map = new Microsoft.Maps.Map(document.getElementById("map"),{
            credentials: "<?php echo $this->params->get('bingmapsapi'); ?>",
            center: center,
            mapTypeId: Microsoft.Maps.MapTypeId.road,//birdseye can be repalced with any one of arial,  auto, collinsBart, mercator, ordnanceSurvey, road
            zoom: <?php if($this->params->get('property_map_zoom') !='' )echo $this->params->get('property_map_zoom'); else{ echo '10';}?>
        });
    
        // Retrieve the location of the map center
    
    
        // Add a pin to the center of the map
        var pin = new Microsoft.Maps.Pushpin( center,
            {
                //icon:"components/com_mostwantedrealestate/assets/images/BluePushpin.png",
                icon: 'https://ecn.dev.virtualearth.net/mapcontrol/v7.0/7.0.20150902134620.61/i/poi_search.png',
                height:50,
                width:50,
                anchor: new Microsoft.Maps.Point(0,50),
                draggable: false
            });
        map.entities.push(pin);
    }
    
</script>
<?php  
}
?>
<!-- Single item view end --> 
<script>
jQuery(document).ready(function(){
    <?php if(isset($_REQUEST['sortTable']) && ( $_REQUEST['sortTable'] != NULL ) ) { ?>
    jQuery('.sortTable').val("<?php echo $_REQUEST['sortTable']; ?>");
    <?php } ?>
});
</script> 

<?php 
$print = JRequest::getVar('print'); 
if($print == 1){?>
<?php } ?>
  
