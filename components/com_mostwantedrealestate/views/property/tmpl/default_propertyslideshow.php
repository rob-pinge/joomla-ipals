<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		2.0.0
	@build			22nd September, 2017
	@created		1st May, 2016
	@package		Most Wanted Real Estate
	@subpackage		default_propertyslideshow.php
	@author			Most Wanted Web Services, Inc. <http://mostwantedrealestatesites.com>	
	@copyright		Copyright (C) 2015-2017. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
	
	Most Wanted Real Estate Component
	
/------------------------------------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access'); 

?>
                                    <div class="uk-slidenav-position" data-uk-slideshow="{animation: '<?php echo $this->params->get('slideshow_transtype'); ?>', autoplay:true,autoplayInterval: <?php echo $this->params->get('slider_autoplay_duration'); ?>}">
                                        <ul class="uk-slideshow">
<?php foreach ($this->allimages as $item): ?>
                                            <li><img src="<?php echo JURI::root().$item->path.$item->filename; ?>" alt=""></li>
<?php endforeach; ?>
                                        </ul>
                                        <a href="#" class="uk-slidenav uk-slidenav-contrast uk-slidenav-previous" data-uk-slideshow-item="previous" style="color: rgba(255,255,255,0.4)"></a>
                                        <a href="#" class="uk-slidenav uk-slidenav-contrast uk-slidenav-next" data-uk-slideshow-item="next" style="color: rgba(255,255,255,0.4)"></a>
                                    </div>

