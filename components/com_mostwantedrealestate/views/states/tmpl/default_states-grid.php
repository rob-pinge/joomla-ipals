<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		2.0.0
	@build			22nd September, 2017
	@created		1st May, 2016
	@package		Most Wanted Real Estate
	@subpackage		default_states-grid.php
	@author			Most Wanted Web Services, Inc. <http://mostwantedrealestatesites.com>	
	@copyright		Copyright (C) 2015-2017. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
	
	Most Wanted Real Estate Component
	
/------------------------------------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access'); 

?>
  <div class="uk-form-row">
    <div><?php echo $this->loadTemplate('states-filters'); ?></div>
    <div> </div>
  </div>
  <!-- Layout -->
  <div id="container">
  <ul class="uk-list">
    <?php foreach ($this->items as $item): ?>
    <li>
      <div class="uk-grid uk-panel uk-panel-box">
        <div class="uk-width-medium-1-3">
          <?php if(empty($item->image)){ ?>
          <div> <a href="<?php echo 'index.php?option=com_mostwantedrealestate&view=state&id='.$item->id;?>" title="<?php echo $item->name;?>" rel=""> <img class="uk-thumbnail uk-thumbnail-medium" src="<?php echo JURI::root().'/media/com_mostwantedrealestate/images/No_image_available.png'; ?>"> </a> </div>
          <?php } else { ?>
          <div> <a href="<?php echo 'index.php?option=com_mostwantedrealestate&view=state&id='.$item->id;?>" title="<?php echo $item->name;?>" rel=""> <img class="uk-thumbnail uk-thumbnail-medium"  src="<?php echo $item->image; ?>"> </a> </div>
          <?php } ?>
        </div>
        <a href="<?php echo 'index.php?option=com_mostwantedrealestate&view=state&id='.$item->id;?>" title="<?php echo $item->name;?>" rel="" >
        <h3><?php echo $item->name; ?></h3>
        </a>
        <div class="uk-width-medium-2-3">
          <div><?php echo substr($item->description,0,390); if(strlen($item->description) > 390)echo '...'; ?></div>
        </div>
      </div>
    </li>
    <?php endforeach; ?>
  </ul>
</div>

