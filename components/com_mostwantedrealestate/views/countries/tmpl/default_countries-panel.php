<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		2.0.0
	@build			22nd September, 2017
	@created		1st May, 2016
	@package		Most Wanted Real Estate
	@subpackage		default_countries-panel.php
	@author			Most Wanted Web Services, Inc. <http://mostwantedrealestatesites.com>	
	@copyright		Copyright (C) 2015-2017. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
	
	Most Wanted Real Estate Component
	
/------------------------------------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access'); 

?>
<form name="adminForm" method="post">
  <?php echo $this->loadTemplate('countries-filters'); ?> 
  <!-- Layout -->
  <div id="container">
  <div data-uk-grid-margin="" class="uk-grid">
  <?php foreach ($this->items as $item): ?>
  <div class="uk-width-medium-1-2">
    <div class="uk-panel uk-panel-box">
      <div class="img-padding">
        <?php if(empty($item->image)){ ?>
        <div> <a href="<?php echo 'index.php?option=com_mostwantedrealestate&view=country&id='.$item->id;?>" title="<?php echo $item->name;?>" rel="" class="g-logo g-logo-alt"> <img class='uk-thumbnail' src="<?php echo JURI::root().'/media/com_mostwantedrealestate/images/No_image_available.png'; ?>"> </a> </div>
        <?php } else { ?>
        <div> <a href="<?php echo 'index.php?option=com_mostwantedrealestate&view=country&id='.$item->id;?>" title="<?php echo $item->name;?>" rel="" class="g-logo g-logo-alt"> <img class='uk-thumbnail' src="<?php echo $item->image; ?>"> </a> </div>
        <?php } ?>
      </div>
      <spacer>
      <h1 class="uk-panel-title"><a href="<?php echo 'index.php?option=com_mostwantedrealestate&view=country&id='.$item->id;?>"><?php echo $item->name;?></a><br/>
      <spacer>
      <p><?php echo substr($item->description,0,390); if(strlen($item->description) > 390)echo '...'; ?></p>
    </div>
  </div>
</form>
<?php endforeach; ?>
</div>
</div>

