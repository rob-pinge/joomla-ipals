<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		2.0.0
	@build			22nd September, 2017
	@created		1st May, 2016
	@package		Most Wanted Real Estate
	@subpackage		default_properties-sort-filters.php
	@author			Most Wanted Web Services, Inc. <http://mostwantedrealestatesites.com>	
	@copyright		Copyright (C) 2015-2017. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
	
	Most Wanted Real Estate Component
	
/------------------------------------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access'); 

?>
<label for="sortTable" class="element-invisible">Sort Results By:</label>
<select name="sortTable" class="sortTable" id="sortTable" >
    <option value="">Sort Results By:</option>
    <option value='name_asc'>Name Ascending</option>
    <option value='name_desc'>Name Descending</option>
    <option value='price_asc'>Price Ascending</option>
    <option value='price_desc'>Price Descending</option>
</select>
