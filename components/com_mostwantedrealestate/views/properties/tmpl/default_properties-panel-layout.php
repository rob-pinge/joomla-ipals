<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		2.0.0
	@build			22nd September, 2017
	@created		1st May, 2016
	@package		Most Wanted Real Estate
	@subpackage		default_properties-panel-layout.php
	@author			Most Wanted Web Services, Inc. <http://mostwantedrealestatesites.com>	
	@copyright		Copyright (C) 2015-2017. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
	
	Most Wanted Real Estate Component
	
/------------------------------------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access'); 

?>
<!-- All Properties Code Goes Here -->
<form name="adminForm" method="post">
    <div class='uk-grid'>
        <div style="position:relative;" class="uk-width-large-2-3 uk-width-medium-1-1">
<?php if ($this->items): ?>
	<?php if ($this->params->get('map_provider') == 2) : ?>
		<?php echo $this->loadTemplate('allpropertiesbingmap'); ?>
	<?php elseif ($this->params->get('map_type') == 2) : ?>
		<?php echo $this->loadTemplate('allpropertiesgmapcluster'); ?>
	<?php else: ?>
		<?php echo $this->loadTemplate('allpropertiesgmap'); ?>
	<?php endif; ?>
<?php endif; ?>
        </div>
        <div class="uk-width-large-1-3 uk-width-medium-1-1">
            <?php echo $this->loadTemplate('properties-filters'); ?>
        </div>
    </div>

    <div id="propertylist_panel">
        <!-- Property listing view-->
        <h3><?php echo JText::_('COM_MOSTWANTEDREALESTATE_BROWSE_LISTINGS'); ?></h3>
        <form name="adminForm" method="post">
            <div class='uk-form-row'>
                <div><?php echo $this->loadTemplate('properties-sort-filters'); ?></div>
                <div> </div>
            </div>
        </form>
        <!-- Start Property listing view-->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<div id="container">
  <ul id="item_list_container" class="uk-list">
<?php echo JLayoutHelper::render('allpropertypanellistings', $this->items); ?>
</ul>
</div>
        <!-- End Property listing view-->
    </div>
</form>
