<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		2.0.0
	@build			22nd September, 2017
	@created		1st May, 2016
	@package		Most Wanted Real Estate
	@subpackage		default.php
	@author			Most Wanted Web Services, Inc. <http://mostwantedrealestatesites.com>	
	@copyright		Copyright (C) 2015-2017. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
	
	Most Wanted Real Estate Component
	
/------------------------------------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access'); 

// Set the heading of the page
$heading = ($this->params->get('page_heading')) ? $this->params->get('page_heading'):(isset($this->menu->title)) ? $this->menu->title:'';

?>
<?php echo $this->toolbar->render(); ?> <?php if ($this->params->get('show_page_heading')): ?>
	<h1 class="uk-text-primary"><?php echo $heading; ?></h1>
<?php endif; ?>
// properties_display setting. 1=grid, 2=panel, 3=landing page.
<?php if ($this->items): ?>
	<?php if ($this->params->get('properties_display') == 2) : ?>
		<?php echo $this->loadTemplate('properties-panel-layout'); ?>
	<?php elseif ($this->params->get('properties_display') == 3) : ?>
		<?php echo $this->loadTemplate('properties-landing-page'); ?>
	<?php else: ?>
		<?php echo $this->loadTemplate('properties-grid-layout'); ?>
	<?php endif; ?>
<?php else: ?>
	<div class="uk-alert uk-alert-warning" data-uk-alert>
		<a href="" class="uk-alert-close uk-close"></a>
		<p><?php echo JText::_('COM_MOSTWANTEDREALESTATE_NO_PROPERTIES_WERE_FOUND'); ?></p>
	</div>
<?php endif; ?>


<?php if (isset($this->items) && mostwantedrealestateHelper::checkArray($this->items) && count($this->items) > 4): ?>
<form name="adminForm" method="post">
	<div class="pagination">
		<?php if ($this->params->def('show_pagination_results', 1)) : ?>
			<p class="counter pull-right"> <?php echo $this->pagination->getPagesCounter(); ?> <?php echo $this->pagination->getLimitBox(); ?></p>
		<?php endif; ?>
		<?php echo $this->pagination->getPagesLinks(); ?>
	</div>
</form>
<?php endif; ?> 
