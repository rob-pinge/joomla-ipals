<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		2.0.0
	@build			22nd September, 2017
	@created		1st May, 2016
	@package		Most Wanted Real Estate
	@subpackage		default_state-properties-tab.php
	@author			Most Wanted Web Services, Inc. <http://mostwantedrealestatesites.com>	
	@copyright		Copyright (C) 2015-2017. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
	
	Most Wanted Real Estate Component
	
/------------------------------------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access'); 

?>
<!-- TABBED VIEW-->

<div>
  <ul class="uk-tab" data-uk-tab="{connect:'#state-id'}">
    <li><a href=""><?php echo JText::_('COM_MOSTWANTEDREALESTATE_ABOUT'); ?></a></li>
    <li><a href="" id="mapTab"><?php echo JText::_('COM_MOSTWANTEDREALESTATE_MAP'); ?></a></li>
    <li><a href=""><?php echo JText::_('COM_MOSTWANTEDREALESTATE_BROWSE_LISTINGS'); ?></a></li>
  </ul>
  <ul id="agt-id" class="uk-switcher uk-margin">
    <li class="uk-active"> <?php echo JLayoutHelper::render('stateaboutlayout', $this->agent); ?></li>
    <li>
  <div id="map-area">
    <div id="map"></div>
  </div>
        <?php if($this->params->get('map_provider') == '1'):?>
            <input type="button" value="Toggle Street View" onclick="toggleStreetView();"></input>
        <?php endif; ?>
    </li>
    <li> 
      <!-- Property listing view-->
      <h3><?php echo JText::_('COM_MOSTWANTEDREALESTATE_BROWSE_LISTINGS'); ?></h3>
      <div>

        <div class='uk-form-row'>
          <div><?php echo $this->loadTemplate('state-filters'); ?></div>
          <div> </div>
        </div>
<?php echo $this->loadTemplate('statepropertylistings'); ?>
      </div>


      <!-- End Property listing view--> 
    </li>
  </ul>
</div>

<script type="text/javascript">
    jQuery(function(){
        jQuery('#mapTab').on('click',function(){
            console.log(map);
            if(typeof map == 'undefined'){
                initMap();
            }
            
        })
    })
</script>
