<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		2.0.0
	@build			22nd September, 2017
	@created		1st May, 2016
	@package		Most Wanted Real Estate
	@subpackage		default_statepropertylistings.php
	@author			Most Wanted Web Services, Inc. <http://mostwantedrealestatesites.com>	
	@copyright		Copyright (C) 2015-2017. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
	
	Most Wanted Real Estate Component
	
/------------------------------------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access'); 

?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

	<div class="uk-grid uk-flex-middle" data-uk-grid-margin>
    <?php foreach ($this->items as $item): ?>
		<div class="uk-width-medium-1-5">
		<a href="<?php echo 'index.php?option=com_mostwantedrealestate&view=property&id='.$item->id;?>">
  <?php if ($this->params->get('state_thumb_type') == 1) : ?>
            <div class="uk-slidenav-position" data-uk-slideshow="{animation: 'scroll', autoplay:true,autoplayInterval: '3000'}">
                <ul class="uk-slideshow">
                 <?php
                     if( is_array($item->idPropidImageH) && ( count($item->idPropidImageH) > 0 ) ):
                        foreach ($item->idPropidImageH as $idPropidImageH):
                ?>
                    <li>
                        <img class="uk-thumbnail uk-thumbnail-medium"
                             src="<?php echo JURI::root().$idPropidImageH->path.$idPropidImageH->filename.'_th.'.$idPropidImageH->type; ?>"
                             alt="">
                    </li>
                <?php
                        endforeach;
                    else:
                ?>
                        <div> <a href="<?php echo 'index.php?option=com_mostwantedrealestate&view=property&id='.$item->id;?>" title="<?php echo $item->name;?>" rel=""> <img class="uk-thumbnail uk-thumbnail-medium" src="<?php echo JURI::root().'/media/com_mostwantedrealestate/images/No_image_available.png'; ?>"> </a> </div>
                <?php
                     endif;
                ?>

                </ul>
            </div>
				  <?php else: ?>
            <div class="uk-panel">
                <?php
                    if( is_array($item->idPropidImageH) && ( count($item->idPropidImageH) > 0 ) ):
                        $idPropidImageH = array_shift($item->idPropidImageH)
                        ?>
                        <div>
                            <img class="uk-thumbnail uk-thumbnail-medium"
                                   src="<?php echo JURI::root().$idPropidImageH->path.$idPropidImageH->filename; ?>"
                                   alt="">
						</div>
                        <?php
                    else:
                        ?>
                        <div>
                            <a href="<?php echo 'index.php?option=com_mostwantedrealestate&view=property&id='.$item->id;?>" title="<?php echo $item->name;?>" rel="">
                                <img class="uk-thumbnail uk-thumbnail-medium" src="<?php echo JURI::root().'/media/com_mostwantedrealestate/images/No_image_available.png'; ?>">
                            </a>
                        </div>
                        <?php
                    endif;
                ?>
            </div>
  <?php endif; ?>
			</a>
		  <?php if ($item->featured == 1): ?>
          <div class="uk-badge uk-badge-success">Featured</div>
          <?php endif; ?>
          <?php if ($item->openhouse == 1): ?>
          <div class="uk-badge">Open House</div>
          <?php endif; ?>
		</div>
		<div class="uk-width-medium-3-5 uk-flex-middle">
		<a href="<?php echo 'index.php?option=com_mostwantedrealestate&view=property&id='.$item->id;?>" title="<?php echo $item->name;?>" rel="" >
          <h3><?php echo $item->name; ?></h3>
          </a>
          <h4 class="uk-clearfix"><?php echo $item->street; ?><br/>
            <?php echo $item->city_name.', '.$item->state_name.' '.$item->postcode; ?></h4>
          <div><i class="uk-icon-bed" aria-hidden="true"></i><?php echo ' '.(int)$item->bedrooms.'  '; ?><i class="fa fa-bath" aria-hidden="true"></i><?php echo ' '.(int)$item->bathrooms.'  '; ?><i class="uk-icon-building" aria-hidden="true"></i><?php echo $item->squarefeet; ?>
 <?php if ($this->params->get('sqft_type') == 1) : ?>
ft<sup>2</sup>
<?php else : ?>
m<sup>2</sup>
<?php endif; ?>
</div>
          <div><?php echo substr($item->propdesc,0,390); if(strlen($item->propdesc) > 390)echo '...'; ?></div>
		</div>
		<div class="uk-width-medium-1-5">
          <?php if($item->pm_price_override == 1 && (int)$item->propmgt_price != 0 && date("Y-m-d") <= $item->pmenddate){?>
          <span style='color:red;text-decoration:line-through'> <span style='color:black'> <?php echo '$' . number_format($item->price); ?> </span> </span> <span class="uk-text-large"> <?php echo '$' . number_format($item->propmgt_price); ?> </span>
          <p class="uk-badge"><?php echo $item->propmgt_special; ?></p>
          <?php } else {?>
          <span class="uk-text-large"><?php echo '$' . number_format($item->price); ?></span>
          <?php } ?>
		</div>
    <?php endforeach; ?>
	</div>
