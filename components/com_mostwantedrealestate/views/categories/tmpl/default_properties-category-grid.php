<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		2.0.0
	@build			22nd September, 2017
	@created		1st May, 2016
	@package		Most Wanted Real Estate
	@subpackage		default_properties-category-grid.php
	@author			Most Wanted Web Services, Inc. <http://mostwantedrealestatesites.com>	
	@copyright		Copyright (C) 2015-2017. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
	
	Most Wanted Real Estate Component
	
/------------------------------------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access'); 

?>
<div data-uk-grid-margin="" class="tm-grid-truncate uk-grid uk-grid-divider uk-text-center">
<?php foreach ($this->items as $item): ?>
    <div class="uk-width-medium-1-3">
    	<div class="uk-panel uk-panel-box">
            <a href="<?php echo JRoute::_('index.php?option=com_mostwantedrealestate&view=category&id='.$item->id); ?>" title="<?php echo $item->title;?>" rel="" class="g-logo g-logo-alt">
                <img class='uk-thumbnail uk-thumbnail-mini' src="<?php echo $item->image; ?>">               
            </a>
        </div>
    </div>
    <div class="uk-width-medium-1-2">
    	<div class="uk-panel uk-panel-box">
		<a href="<?php echo JRoute::_('index.php?option=com_mostwantedrealestate&view=category&id='.$item->id); ?>" title="<?php echo $item->title;?>" rel="" ><h3 class="g-title" ><?php echo $item->title; ?></h3></a>
		<div><?php echo $item->description; ?></div>
        </div>
    </div>
<?php endforeach; ?>
</div>
