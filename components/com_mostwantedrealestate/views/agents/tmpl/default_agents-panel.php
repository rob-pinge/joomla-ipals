<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		2.0.0
	@build			22nd September, 2017
	@created		1st May, 2016
	@package		Most Wanted Real Estate
	@subpackage		default_agents-panel.php
	@author			Most Wanted Web Services, Inc. <http://mostwantedrealestatesites.com>	
	@copyright		Copyright (C) 2015-2017. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
	
	Most Wanted Real Estate Component
	
/------------------------------------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access'); 

// Load JQuery Framework
JHtml::_('jquery.framework');

// Set Global Parameters
$globalParams = JComponentHelper::getParams('com_mostwantedrealestate');


?>
<form name="adminForm" method="post">
  <?php echo $this->loadTemplate('agents-filters'); ?> 
  <!-- Property Layout -->
  <div id="container">
  <div data-uk-grid-margin="" class="uk-grid">
  <?php foreach ($this->items as $item): ?>
  <div class="uk-width-medium-1-2">
    <div class="uk-panel uk-panel-box">
      <div class="img-padding">
        <?php if(empty($item->image)){ ?>
        <div> <a href="<?php echo 'index.php?option=com_mostwantedrealestate&view=agentview&id='.$item->id;?>" title="<?php echo $item->name;?>" rel="" class="g-logo g-logo-alt"> <img class='uk-thumbnail' src="<?php echo JURI::root().'/media/com_mostwantedrealestate/images/No_image_available.png'; ?>"> </a> </div>
        <?php } else { ?>
        <div> <a href="<?php echo 'index.php?option=com_mostwantedrealestate&view=agentview&id='.$item->id;?>" title="<?php echo $item->name;?>" rel="" class="g-logo g-logo-alt"> <img class='uk-thumbnail' src="<?php echo $item->image; ?>"> </a> </div>
        <?php } ?>
      </div>
      <spacer>
      <h1 class="uk-panel-title"><a href="<?php echo 'index.php?option=com_mostwantedrealestate&view=agentview&id='.$item->id;?>"><?php echo $item->name;?></a><br/>
        <span class="uk-text-small"><?php echo $item->title; ?></span></h1>
      <h4><?php echo $item->street; ?></h4>
      <h4><?php echo $item->city_name.', '.$item->state_name.' '.$item->postcode; ?></h4>
      <h4><?php echo $item->phone; ?></h4>
      <spacer>
      <p><?php echo $item->bio; ?></p>
      <p><?php echo $item->editLink; ?></p>
    </div>
  </div>
</form>
<?php endforeach; ?>
</div>
</div>

