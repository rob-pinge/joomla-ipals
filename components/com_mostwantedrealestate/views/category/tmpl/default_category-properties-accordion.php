<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		2.0.0
	@build			22nd September, 2017
	@created		1st May, 2016
	@package		Most Wanted Real Estate
	@subpackage		default_category-properties-accordion.php
	@author			Most Wanted Web Services, Inc. <http://mostwantedrealestatesites.com>	
	@copyright		Copyright (C) 2015-2017. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
	
	Most Wanted Real Estate Component
	
/------------------------------------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access'); 

?>
<!-- ACCORDIAN  VIEW-->

<div class="uk-accordion" data-uk-accordion>
  <h3 class="uk-accordion-title"><?php echo JText::_('COM_MOSTWANTEDREALESTATE_DESCRIPTION'); ?></h3>
  <div class="uk-accordion-content">
	<h3 class="uk-article-title"><?php echo $this->category->title; ?></h3>
	<div><?php echo $this->category->description; ?></div>
  </div>
  <h3 class="uk-accordion-title"><?php echo JText::_('COM_MOSTWANTEDREALESTATE_BROWSE_CATEGORY_LISTINGS'); ?></h3>
  <div class="uk-accordion-content"> 
  <!-- Property listing view-->
  <h3><?php echo JText::_('COM_MOSTWANTEDREALESTATE_BROWSE_CATEGORYLISTINGS'); ?></h3>
    <div class='uk-form-row'>
      <div><?php echo $this->loadTemplate('categoryfilters'); ?></div>
      <div> </div>
    </div>
<?php echo $this->loadTemplate('categorypropertylistings'); ?>
    <!-- End Property listing view--> 
  </div>
</div>

