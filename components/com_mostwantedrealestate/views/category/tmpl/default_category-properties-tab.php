<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		2.0.0
	@build			22nd September, 2017
	@created		1st May, 2016
	@package		Most Wanted Real Estate
	@subpackage		default_category-properties-tab.php
	@author			Most Wanted Web Services, Inc. <http://mostwantedrealestatesites.com>	
	@copyright		Copyright (C) 2015-2017. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
	
	Most Wanted Real Estate Component
	
/------------------------------------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access'); 

?>
<!-- TABBED VIEW-->

<div>
  <ul class="uk-tab" data-uk-tab="{connect:'#company-id'}">
    <li><a href=""><?php echo JText::_('COM_MOSTWANTEDREALESTATE_DESCRIPTION'); ?></a></li>
    <li><a href=""><?php echo JText::_('COM_MOSTWANTEDREALESTATE_BROWSE_CATEGORY_LISTINGS'); ?></a></li>
  </ul>
  <ul id="company-id" class="uk-switcher uk-margin">
    <li class="uk-active">
	<h3 class="uk-article-title"><?php echo $this->category->title; ?></h3>
	<div><?php echo $this->category->description; ?></div>
	</li>
    <li> 
      <!-- Property listing view-->
      <h3><?php echo JText::_('COM_MOSTWANTEDREALESTATE_BROWSE_CATEGORY_LISTINGS'); ?></h3>
        <div class='uk-form-row'>
          <div><?php echo $this->loadTemplate('categoryfilters'); ?></div>
          <div> </div>
        </div>
<?php echo $this->loadTemplate('categorypropertylistings'); ?>
      <!-- End Property listing view--> 
    </li>
  </ul>
</div>

