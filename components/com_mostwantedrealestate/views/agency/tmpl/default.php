<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		2.0.0
	@build			22nd September, 2017
	@created		1st May, 2016
	@package		Most Wanted Real Estate
	@subpackage		default.php
	@author			Most Wanted Web Services, Inc. <http://mostwantedrealestatesites.com>	
	@copyright		Copyright (C) 2015-2017. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
	
	Most Wanted Real Estate Component
	
/------------------------------------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access'); 

// Set the heading of the page
$heading = ($this->params->get('page_heading')) ? $this->params->get('page_heading'):(isset($this->menu->title)) ? $this->menu->title:'';

if (isset($_COOKIE['filtered_items'])) {
	unset($_COOKIE['filtered_items']);
	setcookie('filtered_items', '', time() - 3600, '/');
}


?>
<?php echo $this->toolbar->render(); ?> <!-- Single agency view start -->

<div class="uk-container">
  <div class="uk-block">
    <div>
      <h3 class="uk-article-title"><?php echo $this->agency->name; ?></h3>
    </div>
  </div>
  <div class="uk-grid">
    <div class="uk-width-1-3">
      <?php if(empty($this->agency->image)){ ?>
      <div> <img class='uk-thumbnail' src="<?php echo JURI::root().'/media/com_mostwantedrealestate/images/No_image_available.png'; ?>"> </div>
      <?php } else { ?>
      <div> <img class='uk-thumbnail' src="<?php echo $this->agency->image; ?>" alt="<?php echo $this->agency->name; ?>"> </div>
      <?php } ?>
    </div>
    <div class="uk-width-2-3">
      <?php if(!empty($this->agency->phone)){?>
      <div><i class="uk-icon-justify uk-icon-phone"></i> <?php echo JText::_('COM_MOSTWANTEDREALESTATE_PHONE'); ?><span><?php echo ' '.$this->agency->phone; ?></span></div>
      <?php } if(!empty($this->agency->fax)){?>
      <div><i class="uk-icon-justify uk-icon-fax"></i> <?php echo JText::_('COM_MOSTWANTEDREALESTATE_FAX'); ?><span><?php echo ' '.$this->agency->fax; ?></span></div>
      <?php }if(!empty($this->agency->license)){?>
      <div><i class="uk-icon-justify fa fa-id-card-o"></i> <?php echo JText::_('COM_MOSTWANTEDREALESTATE_LICENSE'); ?><span><?php echo ' '.$this->agency->license; ?></span></div>
      <?php } ?>
    </div>
    <div></div>
  </div>
  <?php if ($this->items): ?>
  <?php if ($this->params->get('agency_properties_display') == 2) : ?>
  <?php echo $this->loadTemplate('agency-properties-tab'); ?>
  <?php elseif ($this->params->get('agency_properties_display') == 3) : ?>
  <?php echo $this->loadTemplate('agency-properties-accordion'); ?>
  <?php else: ?>
  <?php echo $this->loadTemplate('agency-properties-grid'); ?>
  <?php endif; ?>
  <?php else: ?>
  <div class="uk-alert uk-alert-warning" data-uk-alert> <a href="" class="uk-alert-close uk-close"></a>
    <p><?php echo JText::_('COM_MOSTWANTEDREALESTATE_NO_AGENCIES_WERE_FOUND'); ?></p>
  </div>
  <?php endif; ?>
</div>
<?php if($this->params->get('map_provider') == '1'){?>
<script>
    var map;
    
    function initMap() {
        if(typeof google == "undefined"){
            setTimeout(function(){
                initMap();
                return;
            },1000)
        }
        var place = {
            lat: <?php echo ( $this->agency->latitude != '') ? $this->agency->latitude : '47.6149942';?>,
            lng: <?php echo ($this->agency->longitude != '') ? $this->agency->longitude : '-122.4759886'; ?>
        };

        var zoom = <?php echo ($this->params->get('agency_map_zoom') !='' ) ? $this->params->get('agency_map_zoom') : '10';?>;
        
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: zoom,
            center: place
        });
        
        var marker = new google.maps.Marker({
            position: place,
            map: map
        });

        // We get the map's default panorama and set up some defaults.
        // Note that we don't yet set it visible.
        panorama = map.getStreetView();
        panorama.setPosition(place);
        panorama.setPov(/** @type {google.maps.StreetViewPov} */({
            heading: 265,
            pitch: 0
        }));
    }
    function toggleStreetView() {
        var toggle = panorama.getVisible();
        if (toggle == false) {
            panorama.setVisible(true);
        } else {
            panorama.setVisible(false);
        }
    }
</script> 
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=<?php echo $this->params->get('gmapsapi'); ?>">
</script>
<?php } 
if($this->params->get('map_provider') == '2'){
	$doc = JFactory::getDocument();
    //$doc->addScript(JURI::root()."components/com_mostwantedrealestate/assets/js/bingmap.min.js");
    $doc->addScript("https://www.bing.com/api/maps/mapcontrol");
  ?>
<!--<script type="text/javascript" src="http://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=7.0"  charset="UTF-8"></script>-->

<script>
    //jQuery(function(){
    //    jQuery('<script>')
    //        .attr('type', 'text/javascript')
    //        .attr('src', 'https://www.bing.com/api/maps/mapcontrol')
    //        //.attr('async', 'true')
    //        //.attr('defer', 'true')
    //        .appendTo('body');
    //
    //    ////initMap(mapOptions);
    //});
    
    
    var map;
    
    function initMap()
    {
        if(typeof Microsoft.Maps.Location != "function"){
            setTimeout(function(){
                initMap();
                return;
            },1000)
        }
        var lat = <?php echo ( $this->agency->latitude != '') ? $this->agency->latitude : '47.6149942';?>;
        var long = <?php echo ($this->agency->longitude != '') ? $this->agency->longitude : '-122.4759886'; ?>;
        var center = new Microsoft.Maps.Location(lat, long);
        
        // Initialize the map
        map = new Microsoft.Maps.Map(document.getElementById("map"),{
            credentials: "<?php echo $this->params->get('bingmapsapi'); ?>",
            center: center,
            mapTypeId: Microsoft.Maps.MapTypeId.road,//birdseye can be repalced with any one of arial,  auto, collinsBart, mercator, ordnanceSurvey, road
            zoom: <?php if($this->params->get('agency_map_zoom') !='' )echo $this->params->get('agency_map_zoom'); else{ echo '10';}?>
        });
    
        // Retrieve the location of the map center
    
    
        // Add a pin to the center of the map
        var pin = new Microsoft.Maps.Pushpin( center,
            {
                //icon:"components/com_mostwantedrealestate/assets/images/BluePushpin.png",
                icon: 'https://ecn.dev.virtualearth.net/mapcontrol/v7.0/7.0.20150902134620.61/i/poi_search.png',
                height:50,
                width:50,
                anchor: new Microsoft.Maps.Point(0,50),
                draggable: false
            });
        map.entities.push(pin);
    }
    
</script>
<?php  
}
?>
<!-- Single agency view end --> 
<script>
jQuery(document).ready(function(){
    <?php if(isset($_REQUEST['sortTable']) && ( $_REQUEST['sortTable'] != NULL ) ) { ?>
    jQuery('.sortTable').val("<?php echo $_REQUEST['sortTable']; ?>");
    <?php } ?>
});
</script> 

<?php if (isset($this->items) && mostwantedrealestateHelper::checkArray($this->items) && count($this->items) > 4): ?>
<form name="adminForm" method="post">
	<div class="pagination">
		<?php if ($this->params->def('show_pagination_results', 1)) : ?>
			<p class="counter pull-right"> <?php echo $this->pagination->getPagesCounter(); ?> <?php echo $this->pagination->getLimitBox(); ?></p>
		<?php endif; ?>
		<?php echo $this->pagination->getPagesLinks(); ?>
	</div>
</form>
<?php endif; ?> 
