<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		2.0.0
	@build			22nd September, 2017
	@created		1st May, 2016
	@package		Most Wanted Real Estate
	@subpackage		default_agency-properties-accordion.php
	@author			Most Wanted Web Services, Inc. <http://mostwantedrealestatesites.com>	
	@copyright		Copyright (C) 2015-2017. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
	
	Most Wanted Real Estate Component
	
/------------------------------------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access'); 

?>
<!-- ACCORDIAN  VIEW-->

<div class="uk-accordion" data-uk-accordion>
  <h3 class="uk-accordion-title"><?php echo JText::_('COM_MOSTWANTEDREALESTATE_ABOUT_US'); ?></h3>
  <div class="uk-accordion-content">
<?php echo JLayoutHelper::render('agencyaboutlayout', $this->agency); ?>
  </div>
  <h3 id="mapTab" class="uk-accordion-title"><?php echo JText::_('COM_MOSTWANTEDREALESTATE_WHERE_AM_WE'); ?></h3>
  <div class="uk-accordion-content">
  <div id="map-area">
    <div id="map"></div>
  </div>
    
      <?php if($this->params->get('map_provider') == '1'):?>
          <input type="button" value="Toggle Street View" onclick="toggleStreetView();"></input>
      <?php endif; ?>
  </div>
  <h3 class="uk-accordion-title"><?php echo JText::_('COM_MOSTWANTEDREALESTATE_BROWSE_OUR_LISTINGS'); ?></h3>

  <div class="uk-accordion-content"> 
  <!-- Property listing view-->
  <h3><?php echo JText::_('COM_MOSTWANTEDREALESTATE_BROWSE_OUR_LISTINGS'); ?></h3>
  <form name="adminForm" method="post">
    <div class='uk-form-row'>
      <div><?php echo $this->loadTemplate('agency-filters'); ?></div>
      <div> </div>
    </div>

    <?php echo JLayoutHelper::render('agencypropertylistings', $this->items); ?>
  </form>
    <!-- End Property listing view--> 
  </div>

  <h3 class="uk-accordion-title"><?php echo JText::_('COM_MOSTWANTEDREALESTATE_CONTACT_ME'); ?></h3>
  <div class="uk-accordion-content"> 
    <!-- Agent Contact Form --> 
<?php echo $this->loadTemplate('agencypropertylistings'); ?>
    <!-- End Agent Contact Form --> 
  </div>
</div>

<script type="text/javascript">
    jQuery(function(){
        jQuery('#mapTab').on('click',function(){
            console.log(map);
            if(typeof map == 'undefined'){
                initMap();
            }

        })
    })
</script>
