<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		2.0.0
	@build			22nd September, 2017
	@created		1st May, 2016
	@package		Most Wanted Real Estate
	@subpackage		default_transtypepropertieslandingpage.php
	@author			Most Wanted Web Services, Inc. <http://mostwantedrealestatesites.com>	
	@copyright		Copyright (C) 2015-2017. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
	
	Most Wanted Real Estate Component
	
/------------------------------------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access'); 

?>
<!-- All Properties Code Goes Here -->

<form name="adminForm" method="post">
  <div class='uk-grid'>
  <div class="uk-width-1-1"> <?php echo $this->loadTemplate('transtypepropertiesfilters'); ?> </div>
  <div style="position:relative;" class="uk-width-large-2-3 uk-width-medium-1-1">
    <?php if ($this->items): ?>
    <?php if ($this->params->get('map_provider') == 2) : ?>
    <?php echo $this->loadTemplate('transtypepropertiesbingmap'); ?>
    <?php elseif ($this->params->get('map_type') == 2) : ?>
    <?php echo $this->loadTemplate('transtypepropertiesgmapcluster'); ?>
    <?php else: ?>
    <?php echo $this->loadTemplate('transtypepropertiesgmap'); ?>
    <?php endif; ?>
    <?php endif; ?>
  </div>
  <div id="propertylist_panel" class="uk-width-large-1-3 uk-width-medium-1-1"> 
    <!-- Property listing view--> 
    <!-- Start Property listing view-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <div id="container">
      <div class="uk-panel uk-panel-scrollable uk-scrollable-text" style="height: 600px;">
        <ul id="item_list_container" class="uk-grid">
<?php echo JLayoutHelper::render('transtypepropertylandingpage', $this->items); ?>
        </ul>
      </div>
      <!-- End Property listing view--> 
    </div>
  </div>
</form>

