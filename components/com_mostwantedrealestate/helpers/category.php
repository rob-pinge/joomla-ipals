<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		2.0.0
	@build			22nd September, 2017
	@created		1st May, 2016
	@package		Most Wanted Real Estate
	@subpackage		category.php
	@author			Most Wanted Web Services, Inc. <http://mostwantedrealestatesites.com>	
	@copyright		Copyright (C) 2015-2017. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
	
	Most Wanted Real Estate Component
	
/------------------------------------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * Mostwantedrealestate Component Category Tree
 */

// [Interpretation 9697]Insure this view category file is loaded.
$classname = 'mostwantedrealestateAgentsCategories';
if (!class_exists($classname))
{
	$path = JPATH_SITE . '/components/com_mostwantedrealestate/helpers/categoryagents.php';
	if (is_file($path))
	{
		include_once $path;
	}
}
// [Interpretation 9697]Insure this view category file is loaded.
$classname = 'mostwantedrealestatePropertiesCategories';
if (!class_exists($classname))
{
	$path = JPATH_SITE . '/components/com_mostwantedrealestate/helpers/categoryproperties.php';
	if (is_file($path))
	{
		include_once $path;
	}
}
