<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		2.0.0-Alpha6
	@build			13th June, 2017
	@created		13th June, 2016
	@package		Most Wanted Real Estate
	@subpackage		allpropertyinfobox.php
	@author			Most Wanted Web Services, Inc. <http://mostwantedrealestatesites.com>	
	@copyright		Copyright (C) 2015-2017. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
	
	Most Wanted Real Estate Component
	
/------------------------------------------------------------------------------------------------------*/

// No direct access to this file

defined('JPATH_BASE') or die('Restricted access');



?>
<div class="uk-grid uk-flex-middle" data-uk-grid-margin>
    <div class="uk-width-medium-1-3">
        <a href="<?php echo 'index.php?option=com_mostwantedrealestate&view=property&id='.$displayData->id;?>">
            <div class="uk-panel">
                <?php
                    if( is_array($displayData->idPropidImageJ) && ( count($displayData->idPropidImageJ) > 0 ) ):
                        $idPropidImageJ = array_shift($displayData->idPropidImageJ)
                        ?>
                        <div>
                            <img class="uk-thumbnail uk-thumbnail-mini-box"
                                   src="<?php echo JURI::root().$idPropidImageJ->path.$idPropidImageJ->filename; ?>"
                                   alt=""> </div>
                        <?php
                    else:
                        ?>
                        <div>
                            <a href="<?php echo 'index.php?option=com_mostwantedrealestate&view=property&id='.$displayData->id;?>" title="<?php echo $displayData->name;?>" rel="">
                                <img class="uk-thumbnail uk-thumbnail-mini" src="<?php echo JURI::root().'/media/com_mostwantedrealestate/images/No_image_available.png'; ?>">
                            </a>
                        </div>
                        <?php
                    endif;
                ?>
            </div>
        </a>
        <?php if ($displayData->featured == 1): ?>
            <div class="uk-badge uk-badge-success">Featured</div>
        <?php endif; ?>
        <?php if ($displayData->openhouse == 1): ?>
            <div class="uk-badge">Open House</div>
        <?php endif; ?>
    </div>
    <div class="uk-width-medium-2-3 uk-flex-middle"> <a href="<?php echo 'index.php?option=com_mostwantedrealestate&view=property&id='.$displayData->id;?>" title="<?php echo $displayData->name;?>" rel="" >
            <h5><?php echo $displayData->name; ?></h5>
        </a>
        <h6 class="uk-clearfix"><?php echo $displayData->street; ?><br/>
            <?php echo $displayData->city_name.', '.$displayData->state_name.' '.$displayData->postcode; ?></h6>
        <div><i class="uk-icon-bed" aria-hidden="true"></i><?php echo ' '.(int)$displayData->bedrooms.'  '; ?><i class="fa fa-bath" aria-hidden="true"></i><?php echo ' '.(int)$displayData->bathrooms.'  '; ?><i class="uk-icon-building" aria-hidden="true"></i><?php echo $displayData->squarefeet; ?> ft<sup>2</sup></div>
        <div><?php echo substr($displayData->propdesc,0,390); if(strlen($displayData->propdesc) > 390)echo '...'; ?></div>
        <?php if($displayData->pm_price_override == 1 && (int)$displayData->propmgt_price != 0){?>
            <span style='color:red;text-decoration:line-through'> <span style='color:black'> <?php echo '$' . number_format($displayData->price); ?> </span> </span> <span class="uk-text-large"> <?php echo '$' . number_format($displayData->propmgt_price); ?> </span>
            <p class="uk-badge"><?php echo $displayData->propmgt_special; ?></p>
        <?php } else {?>
            <span class="uk-text-large"><?php echo '$' . number_format($displayData->price); ?></span>
        <?php } ?>
    </div>
</div>
</div>
<style>
    .uk-thumbnail-mini-box {
        width: 125px;
    }
</style>
