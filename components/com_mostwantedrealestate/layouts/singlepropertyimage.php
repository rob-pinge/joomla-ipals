<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		2.0.0
	@build			14th September, 2017
	@created		1st May, 2016
	@package		Most Wanted Real Estate
	@subpackage		singlepropertyimage.php
	@author			Most Wanted Web Services, Inc. <http://mostwantedrealestatesites.com>	
	@copyright		Copyright (C) 2015-2017. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
	
	Most Wanted Real Estate Component
	
/------------------------------------------------------------------------------------------------------*/

// No direct access to this file

defined('JPATH_BASE') or die('Restricted access');



?>
          <?php if(empty($displayData->path)){ ?>
          <div> <a href="<?php echo 'index.php?option=com_mostwantedrealestate&view=property&id='.$item->id;?>" title="<?php echo $item->name;?>" rel=""> <img class="uk-thumbnail uk-thumbnail-medium" src="<?php echo JURI::root().'media/com_mostwantedrealestate/images/No_image_available.png'; ?>"> </a> </div>
          <?php } else { ?>
          <div> <a href="<?php echo 'index.php?option=com_mostwantedrealestate&view=property&id='.$item->id;?>" title="<?php echo $item->name;?>" rel=""> <img class="uk-thumbnail uk-thumbnail-medium"  src="<?php echo JURI::root() . $displayData->path . $displayData->filename.'_fs'.$displayData->type; ?>"></a></div>
          <?php } ?>
		<?php if ($item->featured == 1): ?>
		<div class="uk-badge uk-badge-success">Featured</div>
		<?php endif; ?>
		<?php if ($item->openhouse == 1): ?>
		<div class="uk-badge">Open House</div>
		<?php endif; ?>

