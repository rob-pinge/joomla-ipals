<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		2.0.0
	@build			22nd September, 2017
	@created		1st May, 2016
	@package		Most Wanted Real Estate
	@subpackage		allpropertylandingpage.php
	@author			Most Wanted Web Services, Inc. <http://mostwantedrealestatesites.com>	
	@copyright		Copyright (C) 2015-2017. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
	
	Most Wanted Real Estate Component
	
/------------------------------------------------------------------------------------------------------*/

// No direct access to this file

defined('JPATH_BASE') or die('Restricted access');



?>
	<div class="uk-grid uk-flex-middle" data-uk-grid-margin>
		<div class="uk-width-medium-1-3">
		<a href="<?php echo 'index.php?option=com_mostwantedrealestate&view=property&id='.$displayData->id;?>">
  <?php if (JComponentHelper::getParams('com_mostwantedrealestate')->get('properties_thumb_type') == 1) : ?>
            <div class="uk-slidenav-position" data-uk-slideshow="{animation: 'scroll', autoplay:true,autoplayInterval: '3000'}">
                <ul class="uk-slideshow">
                 <?php
                     if( is_array($displayData->idPropidImageJ) && ( count($displayData->idPropidImageJ) > 0 ) ):
                        foreach ($displayData->idPropidImageJ as $idPropidImageJ):
                ?>
                    <li>
                        <img class="uk-thumbnail uk-thumbnail-mini-box"
                             src="<?php echo JURI::root().$idPropidImageJ->path.$idPropidImageJ->filename.'_th.'.$idPropidImageJ->type; ?>"
                             alt="">
                    </li>
                <?php
                        endforeach;
                    else:
                ?>
                        <div> <a href="<?php echo 'index.php?option=com_mostwantedrealestate&view=property&id='.$displayData->id;?>" title="<?php echo $displayData->name;?>" rel=""> <img class="uk-thumbnail uk-thumbnail-mini-box" src="<?php echo JURI::root().'/media/com_mostwantedrealestate/images/No_image_available.png'; ?>"> </a> </div>
                <?php
                     endif;
                ?>

                </ul>
            </div>
				  <?php else: ?>
            <div class="uk-panel">
                <?php
                    if( is_array($displayData->idPropidImageJ) && ( count($displayData->idPropidImageJ) > 0 ) ):
                        $idPropidImageJ = array_shift($displayData->idPropidImageJ)
                        ?>
                        <div>
                            <img class="uk-thumbnail uk-thumbnail-mini-box"
                                   src="<?php echo JURI::root().$idPropidImageJ->path.$idPropidImageJ->filename.'_th.'.$idPropidImageJ->type; ?>"
                                   alt="">
						</div>
                        <?php
                    else:
                        ?>
                        <div>
                            <a href="<?php echo 'index.php?option=com_mostwantedrealestate&view=property&id='.$displayData->id;?>" title="<?php echo $displayData->name;?>" rel="">
                                <img class="uk-thumbnail uk-thumbnail-mini-box" src="<?php echo JURI::root().'/media/com_mostwantedrealestate/images/No_image_available.png'; ?>">
                            </a>
                        </div>
                        <?php
                    endif;
                ?>
            </div>
  <?php endif; ?>
			</a>
		  <?php if ($displayData->featured == 1): ?>
          <div class="uk-badge uk-badge-success"><?php echo JText::_('COM_MOSTWANTEDREALESTATE_FEATURED'); ?></div>
          <?php endif; ?>
          <?php if ($displayData->openhouse == 1): ?>
          <div class="uk-badge"><?php echo JText::_('COM_MOSTWANTEDREALESTATE_OPEN_HOUSE'); ?></div>
          <?php endif; ?>

		</div>
		<div class="uk-width-medium-2-3 uk-flex-middle">
<a href="<?php echo 'index.php?option=com_mostwantedrealestate&view=property&id='.$displayData->id;?>" title="<?php echo $displayData->name;?>" rel="" >
            <div class="uk-clearfix\"><?php echo $displayData->street; ?><br/>
              <?php echo $displayData->city_name.', '.$displayData->state_name.' '.$displayData->postcode; ?></div>
			  </a>
            <div class="uk-text-small"><i class="uk-icon-bed" aria-hidden="true"></i><?php echo ' '.(int)$displayData->bedrooms.'  '; ?><i class="fa fa-bath" aria-hidden="true"></i><?php echo ' '.(int)$displayData->bathrooms.'  '; ?><i class="uk-icon-building" aria-hidden="true"></i><?php echo $displayData->squarefeet; ?>
 <?php if (JComponentHelper::getParams('com_mostwantedrealestate')->get('sqft_type') == 1) : ?>
ft<sup>2</sup>
<?php else : ?>
m<sup>2</sup>
<?php endif; ?>
</div>
            <div><?php echo substr($displayData->propdesc,0,390); if(strlen($displayData->propdesc) > 390)echo '...'; ?></div>
            <div class="uk-width-1-1\">
			<?php if($displayData->pm_price_override == 1 && (int)$displayData->propmgt_price != 0 && date("Y-m-d") <= $displayData->pmenddate){?>
            <span style="color:red;text-decoration:line-through uk-text-small">
				<span style='color:black'>
					<?php echo '$' . number_format($displayData->price); ?>
				</span>
			</span>
			<span class="uk-text-small">
				<?php echo '$' . number_format($displayData->propmgt_price); ?>
			</span>
			<p class="uk-badge"><?php echo $displayData->propmgt_special; ?></p>
            <?php } else {?>
            <span class="uk-text-small"><?php echo '$' . number_format($displayData->price); ?></span>
            <?php } ?>
			</div>
            <div class="uk-width-1-1 uk-text-small"><?php echo JText::_('COM_MOSTWANTEDREALESTATE_PRESENTED_BY'); ?></div>
			<div><img class="uk-thumbnail uk-thumbnail-mini-box" src="<?php echo JURI::root().$displayData->agency_image; ?>"</div>
		</div>
	</div>

<style>
    .uk-thumbnail-mini-box {
        width: 75px;
    }
</style>

