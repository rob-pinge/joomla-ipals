<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		2.0.0
	@build			22nd September, 2017
	@created		1st May, 2016
	@package		Most Wanted Real Estate
	@subpackage		propertyviewheading.php
	@author			Most Wanted Web Services, Inc. <http://mostwantedrealestatesites.com>	
	@copyright		Copyright (C) 2015-2017. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
	
	Most Wanted Real Estate Component
	
/------------------------------------------------------------------------------------------------------*/

// No direct access to this file

defined('JPATH_BASE') or die('Restricted access');

    $app =  JFactory::getApplication();
    $template = $app->getTemplate();
    $uri = JFactory::getURI();
    $url = $uri->toString();
    $link = MailtoHelper::addLink($url);


?>
<div>
  <div>
      <button class="uk-button uk-button-primary uk-button-small"  onclick='window.history.back();'><?php echo JText::_('COM_MOSTWANTEDREALESTATE_BACK'); ?></button>
    <?php if(isset($_COOKIE['filtered_items'])) : ?>
        <a id="itemPagingBack"
           class="uk-button uk-button-primary uk-button-small" href="index.php?option=com_mostwantedrealestate&view=properties"><?php echo JText::_('COM_MOSTWANTEDREALESTATE_RETURN_TO_SEARCH'); ?></a>
<a id="itemPagingNext" class="uk-button uk-button-primary uk-button-small"  disabled href='#'><?php echo JText::_('COM_MOSTWANTEDREALESTATE_NEXT'); ?></a>

<script>
    var nextItemUrlStub = 'index.php?option=com_mostwantedrealestate&view=property&id=';
    jQuery(function(){
        var next_id;
        var filtered_items = JSON.parse(Cookies.get('filtered_items'));
        var current_id = $_GET('id');
        if(filtered_items.indexOf(current_id) + 1 >= filtered_items.length){
            //go back to the first item
            next_id = filtered_items[0];
        }else{
            next_id = filtered_items[filtered_items.indexOf(current_id)+1];
        }
         
        jQuery('#itemPagingNext').attr('href', nextItemUrlStub + next_id).attr('disabled',false);
    });
</script>
    <?php endif; ?>
<div class="uk-float-right">
    <button onclick="addPropertyToFavorites(<?php echo $displayData->id ;?>);return false;" class="uk-button uk-button-primary uk-button-small uk-icon-bookmark"><?php echo JText::_('COM_MOSTWANTEDREALESTATE_ADD_TO_FAVORITES'); ?></button>
	<div class="uk-button-dropdown" data-uk-dropdown>
	<button class="uk-button uk-button-primary uk-button-small uk-icon-gear"><i class="uk-icon-caret-down"></i></button>
	<div class="uk-dropdown uk-dropdown-small">
		<ul class="uk-nav uk-nav-dropdown">
			<li><a class="uk-button uk-icon-print"
<?php if($print == 1): ?>
        onclick="window.print();"
<?php else: ?>
        onclick="window.open(this.href,'win2','width=400,height=600,menubar=no,resizable=yes'); return false;"
        href="<?php echo $url.'&tmpl=print';?>"
<?php endif; ?>
    ><?php echo JText::_('COM_MOSTWANTEDREALESTATE_PRINT'); ?></a></li>
			<li><a class="uk-button uk-icon-envelope"
       href="index.php?option=com_mailto&tmpl=component&template=<?echo $template;?>&link=<?php echo  $link; ?>"
       onclick="window.open(this.href,'win2','width=400,height=600,menubar=no,resizable=yes'); return false;" ><?php echo JText::_('COM_MOSTWANTEDREALESTATE_EMAIL'); ?></a></li>
	   </ul>
	   </div>
	   </div>
</div>
  </div>
</div>
<div>
  <div>
  </div>
</div>

<script type="text/javascript">
    function $_GET(q,s) {
        s = s ? s : window.location.search;
        var re = new RegExp('&'+q+'(?:=([^&]*))?(?=&|$)','i');
        return (s=s.replace(/^\?/,'&').match(re)) ? (typeof s[1] == 'undefined' ? '' : decodeURIComponent(s[1])) : undefined;
    }

    function addPropertyToFavorites(id){
        jQuery.ajax({
            url: "<?php echo JURI::root().'/index.php?option=com_mostwantedrealestate&task=property.addTofavorite&propertyId='; ?>"+id,
            success: function(data) {
                alert(data);
            }
        });

    }
</script>
    


<?php
    $document = JFactory::getDocument();
    $document->addScript(JUri::base() . 'components/com_mostwantedrealestate/assets/js/js.cooky.js');
?>
