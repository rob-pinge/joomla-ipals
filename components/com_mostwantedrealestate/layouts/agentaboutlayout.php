<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		2.0.0
	@build			22nd September, 2017
	@created		1st May, 2016
	@package		Most Wanted Real Estate
	@subpackage		agentaboutlayout.php
	@author			Most Wanted Web Services, Inc. <http://mostwantedrealestatesites.com>	
	@copyright		Copyright (C) 2015-2017. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
	
	Most Wanted Real Estate Component
	
/------------------------------------------------------------------------------------------------------*/

// No direct access to this file

defined('JPATH_BASE') or die('Restricted access');



?>
      <h4><?php echo $displayData->street; ?></h4>
      <h4><?php echo $displayData->city_name.', '.$displayData->state_name.' '.$displayData->postcode; ?></h4>
      <h4><?php echo $displayData->agency_name; ?></h4>
      <hr>
      <?php if(!empty($displayData->bio)){ ?>
      <h4><?php echo JText::_('COM_MOSTWANTEDREALESTATE_ABOUT_ME'); ?></h4>
      <p><?php echo $displayData->bio; ?></p>
      <?php } ?>

