<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		2.0.0
	@build			22nd September, 2017
	@created		1st May, 2016
	@package		Most Wanted Real Estate
	@subpackage		propertyfeatures.php
	@author			Most Wanted Web Services, Inc. <http://mostwantedrealestatesites.com>	
	@copyright		Copyright (C) 2015-2017. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
	
	Most Wanted Real Estate Component
	
/------------------------------------------------------------------------------------------------------*/

// No direct access to this file

defined('JPATH_BASE') or die('Restricted access');



?>
<div class="uk-container uk-container-center">
  <div class="uk-grid" data-uk-grid-margin>
<?php if($displayData->bldg_name !='' || $displayData->buildingfeatures !='' || $displayData->takings !='' || $displayData->returns !='' || $displayData->netprofit !='' || $displayData->bustype !='' || $displayData->bussubtype !='' || $displayData->percentoffice !='' || $displayData->percentwarehouse !='' || $displayData->loadingfac !='' || $displayData->currentuse !='' || $displayData->carryingcap !='' || $displayData->parkinggarage !=''): ?>
    <dl class="uk-description-list uk-description-list-horizontal uk-width-medium-1-2">
      <h3 class="tm-article-subtitle"><?php echo JText::_('COM_MOSTWANTEDREALESTATE_COMMERCIAL_FEATURES'); ?></h3>
	<?php if($displayData->bldg_name !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_BUILDING_NAME'); ?></dt>
      <dd><?php echo $displayData->bldg_name; ?></dd>
	  <?php endif; ?>
	<?php if($displayData->buildingfeatures !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_BUILDING_FEATURES'); ?></dt>
      <dd>
		<?php $bldgfeat = json_decode($displayData->buildingfeatures, true);
			for($i=0; $i<count($bldgfeat['bldgfeattype']); $i++) {
				echo $bldgfeat['bldgfeattype'][$i] . "<BR>";
			}		
		?>
      </dd>
	  <?php endif; ?>
	<?php if($displayData->takings !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_TAKINGS'); ?></dt>
      <dd><?php echo $displayData->takings; ?></dd>
	  <?php endif; ?>
	<?php if($displayData->returns !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_RETURNS'); ?></dt>
      <dd><?php echo $displayData->returns; ?></dd>
	  <?php endif; ?>
	<?php if($displayData->netprofit !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_NET_PROFIT'); ?></dt>
      <dd><?php echo $displayData->netprofit; ?></dd>
	  <?php endif; ?>
	<?php if($displayData->bustype !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_BUSINESS_TYPE'); ?></dt>
      <dd><?php echo $displayData->bustype; ?></dd>
	  <?php endif; ?>
	<?php if($displayData->bussubtype !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_BUSINESS_SUB_TYPE'); ?></dt>
      <dd><?php echo $displayData->bussubtype; ?></dd>
	  <?php endif; ?>
	<?php if($displayData->percentoffice !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_PERCENT_OFFICE'); ?></dt>
      <dd><?php echo $displayData->percentoffice; ?></dd>
	  <?php endif; ?>
	<?php if($displayData->percentwarehouse !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_PERCENT_WAREHOUSE'); ?></dt>
      <dd><?php echo $displayData->percentwarehouse; ?></dd>
	  <?php endif; ?>
	<?php if($displayData->loadingfac !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_LOADING_FACILITY'); ?></dt>
      <dd><?php echo $displayData->loadingfac; ?></dd>
	  <?php endif; ?>
	<?php if($displayData->currentuse !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_CURRENT_USE'); ?></dt>
      <dd><?php echo $displayData->currentuse; ?></dd>
	  <?php endif; ?>
	<?php if($displayData->carryingcap !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_CARRYING_CAP'); ?></dt>
      <dd><?php echo $displayData->carryingcap; ?></dd>
	  <?php endif; ?>
	<?php if($displayData->parkinggarage !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_PARKING_GARAGE'); ?></dt>
      <dd><?php echo $displayData->parkinggarage; ?></dd>
	<?php endif; ?>
    </dl>
<?php endif; ?>
<?php if($displayData->bldgsqft !='' || $displayData->numunits !='' || $displayData->unitfeatures !='' || $displayData->commonareas !='' || $displayData->tenancytype !='' || $displayData->tenantpdutilities !='' || $displayData->unitdetails !=''): ?>	
    <dl class="uk-description-list uk-description-list-horizontal uk-width-medium-1-2">
      <h3 class="tm-article-subtitle"><?php echo JText::_('COM_MOSTWANTEDREALESTATE_MULTIFAMILY_FEATURES'); ?></h3>
	<?php if($displayData->bldgsqft !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_BUILDING_SQFT'); ?></dt>
      <dd><?php echo $displayData->bldgsqft; ?></dd>
	  <?php endif; ?>
	<?php if($displayData->numunits !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_NUMBER_OF_UNITS'); ?></dt>
      <dd><?php echo $displayData->numunits; ?></dd>
	  <?php endif; ?>
<?php if($displayData->unitfeatures !=''): ?>
<dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_UNIT_FEATURES'); ?></dt>
<dd>
		<?php $unitfeat = json_decode($displayData->unitfeatures, true);
			for($i=0; $i<count($unitfeat['unitfeattype']); $i++) {
				echo $unitfeat['unitfeattype'][$i] . "<BR>";
			}		
		?>
</dd>
<?php endif; ?>
<?php if($displayData->commonareas !=''): ?>
<dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_COMMON_AREAS'); ?></dt>
<dd>
		<?php $commarea = json_decode($displayData->commonareas, true);
			for($i=0; $i<count($commarea['commonareastype']); $i++) {
				echo $commarea['commonareastype'][$i] . "<BR>";
			}		
		?>
</dd>
	<?php if($displayData->tenancytype !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_TYPE_OF_TENANCY'); ?></dt>
      <dd><?php echo $displayData->tenancytype; ?></dd>
	  <?php endif; ?>
	<?php if($displayData->tenantpdutilities !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_TENANT_PAID_UTILITIES') ?></dt>
      <dd><?php echo $displayData->tenantpdutilities;; ?></dd>
	  <?php endif; ?>
	<?php if($displayData->commonareas !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_COMMON_AREAS'); ?></dt>
      <dd><?php echo $displayData->commonareas; ?></dd>
	  <?php endif; ?>
	<?php if($displayData->unitdetails !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_UNIT_DETAILS'); ?></dt>
      <dd><?php echo $displayData->unitdetails; ?></dd>
	<?php endif; ?>
    </dl>
<?php if($displayData->unitdetails !=''): ?>
<div><?php echo JText::_('COM_MOSTWANTEDREALESTATE_UNIT_DETAILS'); ?></div>
		<?php $udetail = json_decode($displayData->unitdetails);
$count = count($udetail->unit_num);
if($count >=1){
			for($i=0; $i<count; $i++) {
		?>
                            <div class="uk-overflow-container uk-width-medium-1-1">
                                <table class="uk-table uk-table-striped">
                                    <thead>
                                        <tr>
                                            <th>Unit Number</th>
                                            <th>Unit Sq Ft</th>
                                            <th>Unit Rent</th>
                                            <th>Unit Beds</th>
                                            <th>Unit Baths</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><?php echo $udetail->unit_num[$i]; ?></td>
                                            <td><?php echo $udetail->unitsqft[$i]; ?></td>
                                            <td><?php echo $udetail->unitrent[$i]; ?></td>
                                            <td><?php echo $udetail->unitbeds[$i]; ?></td>
                                            <td><?php echo $udetail->unitbaths[$i]; ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
		<?php } } ?>
	<?php endif; ?>
<?php endif; ?>
<?php endif; ?>
<?php if($displayData->landtype !='' || $displayData->stock !='' || $displayData->fixtures !='' || $displayData->fittings !='' || $displayData->rainfall !='' || $displayData->soiltype !='' || $displayData->grazing !='' || $displayData->cropping !='' || $displayData->irrigation !=''): ?>
    <dl class="uk-description-list uk-description-list-horizontal uk-width-medium-1-2">
      <h3 class="tm-article-subtitle"><?php echo JText::_('COM_MOSTWANTEDREALESTATE_FARMRANCH_FEATURES'); ?></h3>
	<?php if($displayData->landtype !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_LAND_TYPE'); ?></dt>
      <dd><?php echo $displayData->landtype; ?></dd>
	  <?php endif; ?>
	<?php if($displayData->stock !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_STOCK'); ?></dt>
      <dd><?php echo $displayData->stock; ?></dd>
	  <?php endif; ?>
	<?php if($displayData->fixtures !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_FIXTURES'); ?></dt>
      <dd><?php echo $displayData->fixtures; ?></dd>
	  <?php endif; ?>
	<?php if($displayData->fittings !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_FITTINGS'); ?></dt>
      <dd><?php echo $displayData->fittings; ?></dd>
	  <?php endif; ?>
	<?php if($displayData->rainfall !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_RAINFALL'); ?></dt>
      <dd><?php echo $displayData->rainfall; ?></dd>
	  <?php endif; ?>
	<?php if($displayData->soiltype !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_SOIL_TYPE'); ?></dt>
      <dd><?php echo $displayData->soiltype; ?></dd>
	  <?php endif; ?>
	<?php if($displayData->grazing !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_GRAZING'); ?></dt>
      <dd><?php echo $displayData->grazing; ?></dd>
	  <?php endif; ?>
	<?php if($displayData->cropping !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_CROPPING'); ?></dt>
      <dd><?php echo $displayData->cropping; ?></dd>
	  <?php endif; ?>
	<?php if($displayData->irrigation !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_IRRIGATION'); ?></dt>
      <dd><?php echo $displayData->irrigation; ?></dd>
	<?php endif; ?>
    </dl>
<?php endif; ?>
	<?php if($displayData->rent_type !='' || $displayData->offpeak !='' || $displayData->freq !='' || $displayData->deposit !='' || $displayData->sleeps !=''): ?>
    <dl class="uk-description-list uk-description-list-horizontal uk-width-medium-1-2">
      <h3 class="tm-article-subtitle"><?php echo JText::_('COM_MOSTWANTEDREALESTATE_RENTAL_FEATURES'); ?></h3>
	<?php if($displayData->rent_type !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_RENT_TYPE'); ?></dt>
      <dd><?php echo $displayData->rent_type; ?></dd>
	  <?php endif; ?>
	<?php if($displayData->offpeak !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_OFF_PEAK'); ?></dt>
      <dd><?php echo $displayData->offpeak; ?></dd>
	  <?php endif; ?>
	<?php if($displayData->freq !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_FREQUENCY'); ?></dt>
      <dd><?php echo $displayData->freq; ?></dd>
	  <?php endif; ?>
	<?php if($displayData->deposit !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_DEPOSIT'); ?></dt>
      <dd><?php echo $displayData->deposit; ?></dd>
	  <?php endif; ?>
	<?php if($displayData->sleeps !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_SLEEPS'); ?></dt>
      <dd><?php echo $displayData->sleeps; ?></dd>
	<?php endif; ?>
    </dl>
<?php endif; ?>
  </div>
</div>

