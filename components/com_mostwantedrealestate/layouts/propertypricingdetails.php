<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		2.0.0
	@build			22nd September, 2017
	@created		1st May, 2016
	@package		Most Wanted Real Estate
	@subpackage		propertypricingdetails.php
	@author			Most Wanted Web Services, Inc. <http://mostwantedrealestatesites.com>	
	@copyright		Copyright (C) 2015-2017. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
	
	Most Wanted Real Estate Component
	
/------------------------------------------------------------------------------------------------------*/

// No direct access to this file

defined('JPATH_BASE') or die('Restricted access');



?>
<div class="uk-container uk-container-center">
  <div class="uk-grid" data-uk-grid-margin>
    <dl class="uk-description-list uk-description-list-horizontal uk-width-medium-1-1">
      <h3 class="tm-article-subtitle"><?php echo JText::_('COM_MOSTWANTEDREALESTATE_PRICING_DETAILS'); ?></h3>
	<?php if((int)$displayData->annualinsurance !='0.00'): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_ANNUAL_INSURANCE'); ?></dt>
      <dd><?php echo '$' . number_format($displayData->annualinsurance); ?></dd>
	  <?php endif; ?>
	<?php if((int)$displayData->taxannual !='0.00'): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_ANNUAL_TAX'); ?></dt>
      <dd><?php echo '$' . number_format($displayData->taxannual); ?></dd>
	  <?php endif; ?>
	<?php if($displayData->taxyear !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_TAX_YEAR'); ?></dt>
      <dd><?php echo $displayData->taxyear; ?></dd>
	  <?php endif; ?>
	<?php if($displayData->utilities !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_UTILITIES'); ?></dt>
      <dd><?php echo '$' . number_format($displayData->utilities); ?></dd>
	  <?php endif; ?>
	<?php if($displayData->electricservice !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_ELECTRIC_SERVICE'); ?></dt>
      <dd><?php echo '$' . number_format($displayData->electricservice); ?></dd>
	  <?php endif; ?>
	<?php if($displayData->averageelectric !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_AVERAGE_ELECTRIC'); ?></dt>
      <dd><?php echo '$' . number_format($displayData->averageelectric); ?></dd>
	  <?php endif; ?>
	<?php if($displayData->averagegas !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_AVERAGE_GAS'); ?></dt>
      <dd><?php echo '$' . number_format($displayData->averagegas); ?></dd>
	  <?php endif; ?>
	<?php if($displayData->terms !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_TERMS'); ?></dt>
      <dd><?php echo $displayData->terms; ?></dd>
	  <?php endif; ?>
      <?php if($displayData->pm_price_override !='0' && (int)$displayData->propmgt_price != 0 && date("Y-m-d") <= $displayData->pmenddate): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_OVERRIDE_START_DATE'); ?></dt>
      <dd><?php echo $displayData->pmstartdate; ?></dd>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_OVERRIDE_END_DATE'); ?></dt>
      <dd><?php echo $displayData->pmenddate; ?></dd>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_SPECIAL_PRICE'); ?></dt>
      <dd><?php echo '$' . number_format($displayData->propmgt_price); ?></dd>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_SPECIAL_DESCRIPTION'); ?></dt>
      <dd><?php echo $displayData->propmgt_description; ?></dd>
	  <?php endif; ?>
	<?php if($displayData->viewbooking =='1'): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_VIEW_BOOKING_AVAILABLE_DATE'); ?></dt>
      <dd><?php echo $displayData->availdate; ?></dd>
	  <?php endif; ?>
    </dl>
  </div>
</div>

