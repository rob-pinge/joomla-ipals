<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		2.0.0
	@build			22nd September, 2017
	@created		1st May, 2016
	@package		Most Wanted Real Estate
	@subpackage		propertyquickdetails.php
	@author			Most Wanted Web Services, Inc. <http://mostwantedrealestatesites.com>	
	@copyright		Copyright (C) 2015-2017. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
	
	Most Wanted Real Estate Component
	
/------------------------------------------------------------------------------------------------------*/

// No direct access to this file

defined('JPATH_BASE') or die('Restricted access');



?>
<div class="uk-container uk-container-center">
  <div class="uk-grid" data-uk-grid-margin>
    <dl class="uk-description-list uk-description-list-horizontal uk-width-medium-1-2">
      <h3><?php echo JText::_('COM_MOSTWANTEDREALESTATE_KEY_DETAILS'); ?></h3>
<?php if($displayData->mls_id != ''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_MLS_ID'); ?>:</dt>
      <dd><?php echo $displayData->mls_id; ?></dd>
<?php endif; ?>
      <?php if((int)$displayData->price != 0): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_PRICE'); ?>:</dt>
      <dd>
        <?php if($displayData->pm_price_override == 1 && (int)$displayData->propmgt_price != 0 && date("Y-m-d") <= $displayData->pmenddate): ?>
        <span style="color:red;text-decoration:line-through"> <span style="color:black"><?php echo '$' . number_format($displayData->price); ?></span> </span><span><?php echo '$' . number_format($displayData->propmgt_price); ?></span>
        <?php else: ?>
        <span><?php echo '$' . number_format($displayData->price); ?></span>
        <?php endif; ?>
      </dd>
      <?php endif; ?>
      <dt><?php echo $displayData->transaction_type_name; ?>:</dt>
      <dd><?php echo $displayData->title; ?></dd>
	  <?php if($displayData->garagetype !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_GARAGE_DESCRIPTION'); ?>:</dt>
      <dd><?php echo $displayData->feature_type_garagetype; ?></dd>
	  <?php endif; ?>
	  <?php if($displayData->mls_disclaimer !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_DISCLAIMER'); ?></dt>
      <dd><?php echo $displayData->mls_disclaimer;?></dd>
	  <?php endif; ?>
    </dl>
	<?php if((int)$displayData->bedrooms != 0 || (int)$displayData->bathrooms != 0): ?>
    <dl class="uk-description-list uk-description-list-horizontal uk-width-medium-1-2">
      <h3 class="tm-article-subtitle"><?php echo JText::_('COM_MOSTWANTEDREALESTATE_BASE_PROPERTY_DETAILS'); ?></h3>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_BEDROOMS'); ?>:</dt>
      <dd><?php echo (int)$displayData->bedrooms; ?></dd>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_BATHROOMS'); ?>:</dt>
      <dd><?php echo (int)$displayData->bathrooms; ?></dd>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_FULL_BATHS'); ?>:</dt>
      <dd><?php echo (int)$displayData->fullbaths; ?></dd>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_THREEFOUR_BATHS'); ?>:</dt>
      <dd><?php echo (int)$displayData->thqtrbaths; ?></dd>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_ONETWO_BATHS'); ?>:</dt>
      <dd><?php echo (int)$displayData->halfbaths; ?></dd>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_ONEFOUR_BATHS'); ?>:</dt>
      <dd><?php echo (int)$displayData->qtrbaths; ?></dd>
    </dl>
	<?php endif; ?>
    <dl class="uk-description-list uk-description-list-horizontal uk-width-medium-1-1">
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_LISTING_COURTESY_OF'); ?></dt>
      <dd><?php echo $displayData->agent_name.'</b> '.'with'.' <b>'.$displayData->agency_name.'</b>'; ?></dd>
    </dl>
	<?php if($displayData->basementandfoundation !='' || $displayData->acrestotal !='' || $displayData->landareasqft !='' || $displayData->zoning !=''): ?>
    <dl class="uk-description-list uk-description-list-horizontal uk-width-medium-1-2">
      <h3 class="tm-article-subtitle"><?php echo JText::_('COM_MOSTWANTEDREALESTATE_BASEMENT_AND_FOUNDATION'); ?></h3>
	  <?php if($displayData->basementandfoundation !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_BASEMENT'); ?>:</dt>
      <dd><?php echo $displayData->feature_type_basement; ?></dd>
	  <?php endif; ?>
	  <?php if($displayData->acrestotal !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_ACRES'); ?>:</dt>
      <dd><?php echo $displayData->acrestotal; ?></dd>
	  <?php endif; ?>
	  <?php if($displayData->landareasqft !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_LAND_SQFT'); ?>:</dt>
      <dd><?php echo $displayData->landareasqft; ?></dd>
	  <?php endif; ?>
	  <?php if($displayData->zoning!=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_ZONING'); ?>:</dt>
      <dd><?php echo $displayData->feature_type_zoning; ?></dd>
	  <?php endif; ?>
    </dl>
	<?php endif; ?>
	<?php if($displayData->year !='' || $displayData->porchpatio !='' || $displayData->squarefeet !=''): ?>
    <dl class="uk-description-list uk-description-list-horizontal uk-width-medium-1-2">
      <h3 class="tm-article-subtitle"><?php echo JText::_('COM_MOSTWANTEDREALESTATE_BUILDING_INFORMATION'); ?></h3>
	  <?php if($displayData->year !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_YEAR_BUILT'); ?>:</dt>
      <dd><?php echo (int)$displayData->year; ?></dd>
	  <?php endif; ?>
	  <?php if($displayData->porchpatio !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_PORCHPATIO'); ?>:</dt>
      <dd><?php echo (int)$displayData->feature_type_porchpatio; ?></dd>
	  <?php endif; ?>
	  <?php if($displayData->squarefeet !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_FLOOR_AREA'); ?>:</dt>
      <dd><?php echo $displayData->squarefeet; ?>
 <?php if (JComponentHelper::getParams('com_mostwantedrealestate')->get('sqft_type') == 1) : ?>
ft<sup>2</sup>
<?php else : ?>
m<sup>2</sup>
<?php endif; ?>
</dd>
	  <?php endif; ?>
    </dl>
	<?php endif; ?>
	<?php if($displayData->openhouse != '0'): ?>
    <dl class="uk-description-list uk-description-list-horizontal uk-width-medium-1-1">
      <?php $openhouseinfo = json_decode($displayData->openhouseinfo); 
                            $count = count($openhouseinfo->oh_id);
                            if($count >=1){ 
                                for($i=0;$i<$count;$i++){
                                ?>
	  <?php 
  	  $d1 = date_parse(date("Y-m-d"));
	  $d2 = date_parse ($openhouseinfo->ohend[$i]);
if($d1 < $d2){
	echo '<h3 class="tm-article-subtitle">' . JText::_('COM_MOSTWANTEDREALESTATE_OPEN_HOUSE_DETAILS') . '</h3>';
    if((int)$openhouseinfo->oh_id[$i] != 0) {
      echo '<dt>' . JText::_('COM_MOSTWANTEDREALESTATE_OPEN_HOUSE_ID') . '</dt>';
      echo '<dd>' . (int)$openhouseinfo->oh_id[$i] . '</dd>';
	}
    if($openhouseinfo->ohstart[$i] != '') {
      echo '<dt>' . JText::_('COM_MOSTWANTEDREALESTATE_OPEN_HOUSE_START_DATE') . '</dt>';
      echo '<dd>' . $openhouseinfo->ohstart[$i] . '</dd>';
	}
    if($openhouseinfo->ohend[$i] != '') {
      echo '<dt>' . JText::_('COM_MOSTWANTEDREALESTATE_OPEN_HOUSE_END_DATE') . '</dt>';
      echo '<dd>' . $openhouseinfo->ohend[$i] . '</dd>';
    }
    if($openhouseinfo->ohouse_desc[$i] != '') {
      echo '<dt>' . JText::_('COM_MOSTWANTEDREALESTATE_OPEN_HOUSE_DESCRIPTION') . '</dt>';
      echo '<dd>' . $openhouseinfo->ohouse_desc[$i] . '</dd>';
	}
	}
	  ?>
    </dl>
    <?php } } ?>
	<?php endif; ?>
	<?php if($displayData->propdesc !=''): ?>
    <dl class="uk-description-list uk-description-list-horizontal uk-width-medium-1-1">
      <h3 class="tm-article-subtitle"><?php echo JText::_('COM_MOSTWANTEDREALESTATE_PROPERTY_DESCRIPTION'); ?></h3>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_ABOUT_THIS_LISTING'); ?></dt>
      <dd><?php echo $displayData->propdesc; ?></dd>
    </dl>
	<?php endif; ?>
  </div>
</div>

