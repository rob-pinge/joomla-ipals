<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		2.0.0
	@build			22nd September, 2017
	@created		1st May, 2016
	@package		Most Wanted Real Estate
	@subpackage		propertyoverview.php
	@author			Most Wanted Web Services, Inc. <http://mostwantedrealestatesites.com>	
	@copyright		Copyright (C) 2015-2017. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
	
	Most Wanted Real Estate Component
	
/------------------------------------------------------------------------------------------------------*/

// No direct access to this file

defined('JPATH_BASE') or die('Restricted access');



?>
<div class="uk-container uk-container-center">
  <div class="uk-grid" data-uk-grid-margin>
<?php if($displayData->yearremodeled !='' || $displayData->style !='' || $displayData->houseconstruction !='' || $displayData->exteriorfinish !='' || $displayData->roof !='' || $displayData->flooring !='' || $displayData->frontage !='' || (int)$displayData->waterfront !='0'): ?>
  <dl class="uk-description-list uk-description-list-horizontal uk-width-medium-1-2">
      <h3 class="tm-article-subtitle"><?php echo JText::_('COM_MOSTWANTEDREALESTATE_PROPERTY_FEATURES'); ?></h3>
	<?php if($displayData->yearremodeled !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_YEAR_REMODELED'); ?></dt>
      <dd><?php echo $displayData->yearremodeled; ?></dd>
	  <?php endif; ?>
	<?php if($displayData->style!=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_STYLE'); ?></dt>
      <dd><?php echo $displayData->feature_type_style; ?></dd>
	  <?php endif; ?>
	<?php if($displayData->houseconstruction !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_HOUSE_CONSTRUCTION'); ?></dt>
      <dd><?php echo $displayData->houseconstruction; ?></dd>
	  <?php endif; ?>
	<?php if($displayData->exteriorfinish !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_EXTERIOR_FINISH'); ?></dt>
      <dd><?php echo $displayData->feature_type_exteriorfinish; ?></dd>
	  <?php endif; ?>
	<?php if($displayData->roof !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_ROOF'); ?></dt>
      <dd><?php echo $displayData->feature_type_roof; ?></dd>
	  <?php endif; ?>
	<?php if($displayData->flooring !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_FLOORING'); ?></dt>
      <dd><?php echo $displayData->flooring; ?></dd>
	  <?php endif; ?>
	<?php if($displayData->frontage !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_ROAD_FRONTAGE'); ?></dt>
      <dd><?php echo $displayData->feature_type_frontage; ?></dd>
	  <?php endif; ?>
	<?php if((int)$displayData->waterfront !='0'): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_WATERFRONTLAKERIVER'); ?></dt>
      <dd><?php echo $displayData->waterfront; ?></dd>
	<?php endif; ?>
	  </dl>
	  <?php endif; ?>
	  <?php if($displayData->welldepth !='' || $displayData->subdivision !='' || $displayData->totalrooms !='' || $displayData->otherrooms !='' || $displayData->livingarea !='' || (int)$displayData->ensuite !='0' || $displayData->parkingcarport !='' || (int)$displayData->stories !='0'): ?>
    <dl class="uk-description-list uk-description-list-horizontal uk-width-medium-1-2">
      <h3 class="tm-article-subtitle"><?php echo JText::_('COM_MOSTWANTEDREALESTATE_PROPERTY_FEATURES'); ?></h3>
<?php if($displayData->welldepth !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_WELL_DEPTH'); ?></dt>
      <dd><?php echo $displayData->welldepth; ?></dd>
	  <?php endif; ?>
	<?php if($displayData->subdivision !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_SUBDIVISION'); ?></dt>
      <dd><?php echo $displayData->subdivision; ?></dd>
	  <?php endif; ?>
	<?php if($displayData->totalrooms !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_TOTAL_ROOMS'); ?></dt>
      <dd><?php echo $displayData->totalrooms; ?></dd>
	  <?php endif; ?>
	<?php if($displayData->otherrooms !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_OTHER_ROOMS'); ?></dt>
      <dd><?php echo $displayData->otherrooms; ?></dd>
	  <?php endif; ?>
	<?php if($displayData->livingarea !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_LIVING_AREA'); ?></dt>
      <dd><?php echo $displayData->livingarea; ?></dd>
	  <?php endif; ?>
	<?php if((int)$displayData->ensuite !='0'): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_ENSUITE'); ?></dt>
      <dd><?php echo $displayData->ensuite; ?></dd>
	  <?php endif; ?>
	<?php if($displayData->parkingcarport !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_CARPORT'); ?></dt>
      <dd><?php echo $displayData->parkingcarport; ?></dd>
	  <?php endif; ?>
	<?php if((int)$displayData->stories !='0'): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_STORIES'); ?></dt>
      <dd><?php echo $displayData->stories; ?></dd>
	  <?php endif; ?>
	  </dl>
	  <?php endif; ?>
	<?php if($displayData->appliances !='' || $displayData->indoorfeatures !='' || $displayData->outdoorfeatures !='' || $displayData->communityfeatures !='' || $displayData->otherfeatures !='' || (int)$displayData->phoneavailableyn !='0' || $displayData->garbagedisposal !='' || (int)$displayData->familyroompresent !='0' || (int)$displayData->laundryroompresent !='0' || (int)$displayData->kitchenpresent !='0' || (int)$displayData->livingroompresent !='0'): ?>
    <dl class="uk-description-list uk-description-list-horizontal uk-width-medium-1-2">
      <h3 class="tm-article-subtitle"><?php echo JText::_('COM_MOSTWANTEDREALESTATE_PROPERTY_FEATURES'); ?></h3>
	<?php if($displayData->appliances !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_APPLIANCES'); ?></dt>
		<dd>
	  		<?php $appliancetype = json_decode($displayData->appliances, true);
			for($i=0; $i<count($appliancetype['appliance_type']); $i++) {
				echo $appliancetype['appliance_type'][$i] . "<BR>";
			}		
			?>
		</dd>
	  <?php endif; ?>
	<?php if($displayData->indoorfeatures !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_INDOOR_FEATURES'); ?></dt>
      <dd>
		<?php $infeat = json_decode($displayData->indoorfeatures, true);
			for($i=0; $i<count($infeat['indoorfeattype']); $i++) {
				echo $infeat['indoorfeattype'][$i] . "<BR>";
			}		
		?>
        </dd>
	  <?php endif; ?>
	<?php if($displayData->outdoorfeatures !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_OUTDOOR_FEATURES'); ?></dt>
      <dd>
		<?php $outfeat = json_decode($displayData->outdoorfeatures, true);
			for($i=0; $i<count($outfeat['outdoorfeattype']); $i++) {
				echo $outfeat['outdoorfeattype'][$i] . "<BR>";
			}		
		?>
       </dd>
	  <?php endif; ?>
	<?php if($displayData->communityfeatures !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_COMMUNITY_FEATURES'); ?></dt>
      <dd>
		<?php $communityfeat = json_decode($displayData->communityfeatures, true);
			for($i=0; $i<count($communityfeat['commfeattype']); $i++) {
				echo $communityfeat['commfeattype'][$i] . "<BR>";
			}		
		?>
       </dd>
	  <?php endif; ?>
	<?php if($displayData->otherfeatures !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_OTHER_FEATURES'); ?></dt>
      <dd>
		<?php $otherfeat = json_decode($displayData->otherfeatures, true);
			for($i=0; $i<count($otherfeat['otherfeattype']); $i++) {
				echo $otherfeat['otherfeattype'][$i] . "<BR>";
			}		
		?>
      </dd>
	  <?php endif; ?>
	<?php if((int)$displayData->phoneavailableyn !='0'): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_PHONE_AVAILABLE'); ?></dt>
      <dd><?php echo $displayData->phoneavailableyn; ?></dd>
	  <?php endif; ?>
	<?php if($displayData->garbagedisposal !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_GARBAGE_DISPOSAL'); ?></dt>
      <dd><?php echo $displayData->garbagedisposal; ?></dd>
	  <?php endif; ?>
	<?php if((int)$displayData->familyroompresent !='0'): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_FAMILY_ROOM_PRESENT'); ?></dt>
      <dd><?php echo $displayData->familyroompresent; ?></dd>
	  <?php endif; ?>
	<?php if((int)$displayData->laundryroompresent !='0'): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_LAUNDRY_ROOM_PRESENT'); ?></dt>
      <dd><?php echo $displayData->laundryroompresent; ?></dd>
	  <?php endif; ?>
	<?php if((int)$displayData->kitchenpresent !='0'): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_KITCHEN_PRESENT'); ?></dt>
      <dd><?php echo $displayData->kitchenpresent; ?></dd>
	  <?php endif; ?>
	<?php if((int)$displayData->livingroompresent !='0'): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_LIVING_ROOM_PRESENT'); ?></dt>
      <dd><?php echo $displayData->livingroompresent; ?></dd>
	  <?php endif; ?>
    </dl>
<?php endif; ?>
	<?php if((int)$displayData->parkingspaceyn !='0' || 	$displayData->customone !='' || $displayData->customtwo !='' || $displayData->customthree !='' || $displayData->customfour !='' || $displayData->customfive !='' || $displayData->customsix !='' || $displayData->customseven !='' || $displayData->customeight !='' || $displayData->storage !='' || $displayData->waterresources !='' || $displayData->fencing !='' || $displayData->heating !='' || $displayData->cooling !='' || $displayData->sewer !=''): ?>
    <dl class="uk-description-list uk-description-list-horizontal uk-width-medium-1-2">
      <h3 class="tm-article-subtitle"><?php echo JText::_('COM_MOSTWANTEDREALESTATE_PROPERTY_FEATURES'); ?></h3>
	<?php if((int)$displayData->fencing !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_FENCING'); ?></dt>
      <dd><?php echo $displayData->feature_type_fencing; ?></dd>
	  <?php endif; ?>
	<?php if((int)$displayData->heating !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_HEATING'); ?></dt>
      <dd><?php echo $displayData->feature_type_heating; ?></dd>
	  <?php endif; ?>
	<?php if((int)$displayData->cooling !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_COOLING'); ?></dt>
      <dd><?php echo $displayData->feature_type_cooling; ?></dd>
	  <?php endif; ?>
	<?php if((int)$displayData->sewer !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_SEWER'); ?></dt>
      <dd><?php echo $displayData->feature_type_sewer; ?></dd>
	  <?php endif; ?>
	<?php if((int)$displayData->parkingspaceyn !='0'): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_PARKING_SPACE'); ?></dt>
      <dd><?php echo $displayData->parkingspaceyn; ?></dd>
	  <?php endif; ?>
	<?php if($displayData->customone !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_CUSTOM_ONE'); ?></dt>
      <dd><?php echo $displayData->customone; ?></dd>
	  <?php endif; ?>
	<?php if($displayData->customtwo !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_CUSTOM_TWO'); ?></dt>
      <dd><?php echo $displayData->customtwo; ?></dd>
	  <?php endif; ?>
	<?php if($displayData->customthree !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_CUSTOM_THREE'); ?></dt>
      <dd><?php echo $displayData->customthree; ?></dd>
	  <?php endif; ?>
	<?php if($displayData->customfour !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_CUSTOM_FOUR'); ?></dt>
      <dd><?php echo $displayData->customfour; ?></dd>
	  <?php endif; ?>
	<?php if($displayData->customfive !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_CUSTOM_FIVE'); ?></dt>
      <dd><?php echo $displayData->customfive; ?></dd>
	  <?php endif; ?>
	<?php if($displayData->customsix !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_CUSTOM_SIX'); ?></dt>
      <dd><?php echo $displayData->customsix; ?></dd>
	  <?php endif; ?>
	<?php if($displayData->customseven !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_CUSTOM_SEVEN'); ?></dt>
      <dd><?php echo $displayData->customseven; ?></dd>
	  <?php endif; ?>
	<?php if($displayData->customeight !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_CUSTOM_EIGHT'); ?></dt>
      <dd><?php echo $displayData->customeight; ?></dd>
	  <?php endif; ?>
	<?php if($displayData->storage !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_STORAGE'); ?></dt>
      <dd><?php echo $displayData->storage; ?></dd>
	  <?php endif; ?>
	<?php if($displayData->waterresources !=''): ?>
      <dt><?php echo JText::_('COM_MOSTWANTEDREALESTATE_WATER_RESOURCES'); ?></dt>
      <dd><?php echo $displayData->feature_type_waterresources; ?></dd>
	  <?php endif; ?>
    </dl>
<?php endif; ?>
  </div>
</div>

