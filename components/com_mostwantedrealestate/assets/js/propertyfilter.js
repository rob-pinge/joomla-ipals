/**
 * Created by cash america on 5/8/2017.
 */
var filtered_items;

var timeOut = null;
function initFilter(){
    jQuery("#filter_container select").each(function(){
        jQuery(this).on('change',function(){
            updateFilteredItems();
        })
    });
    jQuery("#filter_container input").each(function(){
        jQuery(this).on('keyup',function(){
            if(timeOut != null)
                clearTimeout(timeOut);

            timeOut = setTimeout(updateFilteredItems, 1000);
        })
    });

    jQuery("select#sortTable").on('change',function(e){
        sortItems(e.target);
    });

    updateFilteredItems();


}

function updateFilteredItems(){
    var catid           = ((catid           = jQuery('#filter_container #categoryDd').val()     ) && ( catid        != '' ) )     ? catid      : false;
    var trans_type      = ((trans_type      = jQuery('#filter_container #transtypeDd').val()    ) && ( trans_type   != '' ) )     ? trans_type : false;
    var mkt_stats       = ((mkt_stats       = jQuery('#filter_container #marketstatusDd').val() ) && ( mkt_stats   != '' ) )      ? mkt_stats : false;
    var agent           = ((agent           = jQuery('#filter_container #agentDd').val()        ) && ( agent   != '' ) )          ? agent : false;
    var stateid         = ((stateid         = jQuery('#filter_container #stateDd').val()        ) && ( stateid   != '' ) )        ? stateid : false;
    var cityid          = ((cityid          = jQuery('#filter_container #cityDd').val()         ) && ( cityid   != '' ) )         ? cityid : false;
    var bedrooms        = ((bedrooms        = jQuery('#filter_container #minbedsDd').val()      ) && ( bedrooms   != '' ) )       ? bedrooms : false;
    var bathrooms       = ((bathrooms       = jQuery('#filter_container #minbathDd').val()      ) && ( bathrooms   != '' ) )      ? bathrooms : false;
    var livingareaMin   = ((livingareaMin   = jQuery('#filter_container #min_area').val()       ) && ( livingareaMin   != '' ) )  ? livingareaMin : false;
    var livingareaMax   = ((livingareaMax   = jQuery('#filter_container #max_area').val()       ) && ( livingareaMax   != '' ) )  ? livingareaMax : false;
    var priceMin        = ((priceMin        = jQuery('#filter_container #min_price').val()      ) && ( priceMin   != '' ) )       ? priceMin : false;
    var priceMax        = ((priceMax        = jQuery('#filter_container #max_price').val()      ) && ( priceMax   != '' ) )       ? priceMax : false;
    var landMin         = ((landMin         = jQuery('#filter_container #min_land').val()       ) && ( landMin   != '' ) )        ? landMin : false;
    var landMax         = ((landMax         = jQuery('#filter_container #max_land').val()       ) && ( landMax   != '' ) )        ? landMax : false;
    var waterfront		= ((waterfront      = jQuery('#filter_container #waterfront').val()	    ) && ( waterfront   != '' ) )     ? waterfront : false;

    filtered_items = jQuery.grep(properties, function(v) {
        //console.log( 'catid: '           + 'select value: ' + catid + ' v value: ' + parseInt( v.catid ) );
        //console.log( 'comparison: ' + ( catid ? ( parseInt( v.catid ) == parseInt( jQuery('.categoryDd').val() ) ) : true ) );
        //console.log( 'trans_type: '      + 'select value: ' + catid + ' v value: ' + parseInt( v.trans_type ) );
        //console.log( 'mkt_stats: '       + 'select value: ' + catid + ' v value: ' + parseInt( v.mkt_stats ) );
        //console.log( 'agent: '           + 'select value: ' + catid + ' v value: ' + parseInt( v.agent ) );
        //console.log( 'stateid: '         + 'select value: ' + catid + ' v value: ' + parseInt( v.stateid ) );
        //console.log( 'cityid: '          + 'select value: ' + catid + ' v value: ' + parseInt( v.cityid ) );
        //console.log( 'bedrooms: '        + 'select value: ' + catid + ' v value: ' + parseInt( v.bedrooms ) );
        //console.log( 'bathrooms: '       + 'select value: ' + catid + ' v value: ' + parseInt( v.bathrooms ) );
        //console.log( 'livingareaMin: '   + 'select value: ' + livingareaMin + ' v value: ' + parseInt( v.squarefeet ) );
        //console.log( 'livingareaMax: '   + 'select value: ' + livingareaMax + ' v value: ' + parseInt( v.squarefeet ) );
        ////console.log( 'landMin: '         + 'select value: ' + landMin + ' v value: ' + parseInt( v.price ) );
        ////console.log( 'landMax: '         + 'select value: ' + landMax + ' v value: ' + parseInt( v.price ) );
        //console.log( 'priceMin: '        + 'select value: ' + priceMin + ' v value: ' + parseInt( v.price ) );
        //console.log( 'priceMax: '        + 'select value: ' + priceMax + ' v value: ' + parseInt( v.price ) );
        //console.log( ( parseInt( v.price ) <= jQuery('#max_price').val() ) );
        return  (
            ( catid           ? ( parseInt( v.catid )       ==  catid )     : true )
            &&
            ( trans_type      ? ( parseInt( v.trans_type )  ==  trans_type )    : true )
            &&
            ( mkt_stats       ? ( parseInt( v.mkt_stats )   ==  mkt_stats ) : true )
            &&
            ( agent           ? ( parseInt( v.agent )       ==  agent )     : true )
            &&
            ( stateid         ? ( parseInt( v.stateid )     ==  stateid )   : true )
            &&
            ( cityid          ? ( parseInt( v.cityid )      ==  cityid )    : true )
            &&
            ( bedrooms        ? ( parseInt( v.bedrooms )    >=   bedrooms ) : true )
            &&
            ( bathrooms       ? ( parseInt( v.bathrooms )   >=   bathrooms )    : true )
            &&
            ( livingareaMin   ? ( parseInt( v.squarefeet )  >=  livingareaMin ) : true )
            &&
            ( livingareaMax   ? ( parseInt( v.squarefeet )  <=  livingareaMax ) : true )
            &&
            ( waterfront      ? ( parseInt( v.waterfront )  	==   waterfront )   : true )
            &&
            ( priceMin
                    ? (
                        parseInt(v.price) !=0 || parseInt(v.pm_price_override) != 0
                            ? (
                            parseInt(v.pm_price_override) != 0
                                ? ( parseInt( v.propmgt_price ) >= priceMin )
                                : ( parseInt( v.price ) >= priceMin )
                        )
                            : true
                    )
                    : true
            )
            &&
            ( priceMax
                    ? (
                        parseInt(v.price) !=0  || parseInt(v.pm_price_override) != 0
                            ? (
                            parseInt(v.pm_price_override) != 0
                                ? ( parseInt( v.propmgt_price ) <= priceMax )
                                : ( parseInt( v.price ) <= priceMax )
                        )
                            : true
                    )
                    : true
            )
            &&
            ( landMin
                    ? (
                        ( v.landareasqft !='' && ( parseInt(v.landareasqft) !=0 ) )
                            ? ( parseInt( v.landareasqft ) > landMin )
                            : true
                    )
                    : true
            )
            &&
            ( landMax
                    ? (
                        ( v.landareasqft !='' && ( parseInt(v.landareasqft) !=0 ) )
                            ? ( parseInt( v.landareasqft ) <landMax )
                            : true
                    )
                    : true
            )
            &&
            filterKeywords(v)
        )
    });

    filtered_items_cookie_array = filtered_items.map( function(item,i){
        return item.id;
    });

    Cookies.set( 'filtered_items', JSON.stringify(filtered_items_cookie_array) );

    if(typeof map != 'undefined'){updateMapMarkers(filtered_items);}

    renderFilteredItemList();
}

function renderFilteredItemList(){
    jQuery('#item_list_container').html("");
    jQuery.each(filtered_items,function(key,value){
        //var html = getItemHtml(value);
        //var container = jQuery('<li></li>');
        //console.log(value.html);
        //jQuery(container).html(value.html);
        jQuery('#item_list_container').append(value.html);
    })
    jQuery.UIkit.init();
}

function filterKeywords(item){
    searchString = jQuery('#keyword').val();

    if(
        searchString == ''
        ||
        item.name.toString().toLowerCase().search(searchString) >= 0
        ||
        item.mls_id.toString(item.mls_id).toLowerCase().search(searchString) >= 0
        ||
        item.street.toString().toLowerCase().search(searchString) >= 0
        ||
        item.postcode.toString().toLowerCase().search(searchString) >= 0
    ){
        return true;
    }else{
        return false;
    }

}

function sortItems(element){
    var sortType = jQuery(element).val();

    if( sortType == 'name_asc' ){
        //a.name.localeCompare(b.name)
        sortBy(filtered_items, (o) =>  o.name);
        renderFilteredItemList();
    }
    if( sortType == 'name_desc' ){
        //b.name.localeCompare(a.name)
        sortBy(filtered_items, (o) => 'DESC:' + o.name);
        renderFilteredItemList();
    }
    if( sortType == 'price_asc' ){
        sortBy( filtered_items, (o) => parseInt(o.price) );
        renderFilteredItemList();
    }
    if( sortType == 'price_desc' ){
        sortBy(filtered_items, (o) => -parseInt(o.price) );
        renderFilteredItemList();
    }

    //filtered_items.sort(function(a, b) {
    //    return parseFloat(a.price) - parseFloat(b.price);
    //});
}

function getItemHtml(item){

    var
        html,
        image_html,
        price_html;

    var base_url = properties_filters_basepath;

    if( typeof item.image == 'undefined'){
        image_html =
            '<div>' +
            '<a href="index.php?option=com_mostwantedrealestate&view=property&id=' + item.id + '" title="'+item.name+'" rel="">' +
            '<img class="uk-thumbnail uk-thumbnail-medium" src="' + base_url + '/media/com_mostwantedrealestate/images/No_image_available.png">' +
            '</a>' +
            '</div>';
    } else {
        image_html =
            '<div>' +
            '<a href="index.php?option=com_mostwantedrealestate&view=property&id='+item.id+'" title="'+item.name+'" rel="">' +
            '<img class="uk-thumbnail uk-thumbnail-medium"  src="' + base_url + item.image.path+'"> ' +
            '</a> <' +
            '</div>'
    }

    if( (item.pm_price_override == 1) && (parseInt(item.propmgt_price) != 0) ){
        price_html =
            '<span style="color:red;text-decoration:line-through"><span style="color:black">$'+item.price+'</span></span>'+
            '<span class="uk-text-large">$'+item.propmgt_price+'</span>'+
            '<p class="uk-badge">'+item.propmgt_special+'</p>';
    } else {
        price_html =
            '<span class="uk-text-large">$'+item.price+'</span>';
    }

    html =

        '<div class="uk-grid uk-panel uk-panel-box">' +
        '<div class="uk-width-medium-1-3">' +
        image_html +
        '</div>' +
        '<div class="uk-width-medium-1-3 uk-float-left">' +
        '<a href="index.php?option=com_mostwantedrealestate&view=property&id='+item.id+'" title="'+item.name+'" rel="" >' +
        '<h3>' + item.name + '</h3>' +
        '</a>' +
        '</div>' +
        '<div class="uk-width-medium-1-3 uk-float-right uk-text-right">' +
        price_html +
        '</div>' +
        '<div class="uk-width-medium-2-3">' +
        '<h4 class="uk-clearfix">' +
        item.street+'<br/>' +
        item.city_name+', ' + item.state_name + ' ' + item.postcode +
        '</h4>' +
        '<div>' +
        '<i class="uk-icon-bed" aria-hidden="true"></i> ' + parseInt(item.bedrooms) + '  ' +
        '<i class="fa fa-bath" aria-hidden="true"></i> ' + parseInt(item.bathrooms) + '  ' +
        '<i class="uk-icon-building" aria-hidden="true"></i> ' + parseInt(item.squarefeet)+'ft<sup>2</sup>' +
        '</div>' +
        '<div>' +
        item.propdesc.substring(0,390) + (item.propdesc.length > 390 ? '...' : '') +
        '</div>' +
        '</div>' +
        '</div>'
    ;

    return html;
}