/**
 * Created by cash america on 5/8/2017.
 */

var
    config,
    map,
    locations,
    mapOptions,
    userLocation,
    activeLocation,
    markers,
    updatededMarkers,
    boundedMarkers;

function initMap() {

    var center =  new google.maps.LatLng( parseFloat(mapOptions.center.lat), parseFloat(mapOptions.center.lng) );

    var base_path = properties_filters_basepath;

    config = {
        initial_zoom : 6,
        cluster_max_zoom : 12,
        labels : 'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
        base_path : base_path,
        image_path : base_path +'/assets/img',
        css_path : base_path +'/assets/css',
        js_path : base_path +'/assets/js',
        map_marker_icon_file : '/blue-dot.png',
        map_culster_icon_uri : ''
    };

    mapOptions = {
        zoom: mapOptions.initial_zoom,
        center: center,
        mapElement: mapOptions.mapElement,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: false,
        panControl: true,
        panControlOptions: {
            position: google.maps.ControlPosition.LEFT_CENTER
        },
        zoomControl: true,
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.LARGE,
            position: google.maps.ControlPosition.LEFT_CENTER
        },
        scaleControl: true,
        streetViewControl: true,
        streetViewControlOptions: {
            position: google.maps.ControlPosition.LEFT_CENTER
        }
    };

    map = new google.maps.Map(
        document.getElementById(mapOptions.mapElement), mapOptions
    );
    map.setTilt(45);


    updateMapMarkers(filtered_items);
}

function updateMapMarkers() {
    var bounds = new google.maps.LatLngBounds();
    var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

    markers = filtered_items.map(function (item, i) {

        var position = new google.maps.LatLng( parseFloat(item.latitude), parseFloat(item.longitude) );

        bounds.extend(position);
        console.log(item);

        var contentString =item.infohtml;

        var infoWindow = new google.maps.InfoWindow({
            content: contentString
        });

        var marker = new google.maps.Marker({
            position: position,
            map: map,
            title: item.name
        });

        google.maps.event.addListener(marker, "click", function (e) {
            infoWindow.open(map, this);
        });

        return marker;
    });

    map.fitBounds(bounds);
    // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
        //this.setZoom(14);
        google.maps.event.removeListener(boundsListener);
    });

   //var markerCluster = new MarkerClusterer(map, markers,{
   //    imagePath: 'https://raw.githubusercontent.com/googlemaps/js-marker-clusterer/gh-pages/images/m',
   //    maxZoom: config.cluster_max_zoom});

    google.maps.event.addListener(map, 'bounds_changed', function () {
        //updateFilteredItems();
    });
}


function getItemInfoBoxHtml(item){

    var
        html,
        image_html,
        price_html;

    if( typeof item.image == 'undefined'){
        image_html =
            '<div>' +
            '<a href="' + properties_filters_basepath + 'index.php?option=com_mostwantedrealestate&view=property&id=' + item.id + '" title="'+item.name+'" rel="">' +
            '<img class="uk-thumbnail uk-thumbnail-mini" src="' + properties_filters_basepath + '/media/com_mostwantedrealestate/images/No_image_available.png">' +
            '</a>' +
            '</div>';
    } else {
        image_html =
            '<div>' +
            '<a href="' + properties_filters_basepath + 'index.php?option=com_mostwantedrealestate&view=property&id='+item.id+'" title="'+item.name+'" rel="">' +
            '<img class="uk-thumbnail uk-thumbnail-mini"  src="' + properties_filters_basepath + item.image.path+'"> ' +
            '</a> <' +
            '</div>'
    }

    if( (item.pm_price_override == 1) && (parseInt(item.propmgt_price) != 0) ){
        price_html =
            '<span style="color:red;text-decoration:line-through"><span style="color:black">$'+item.price+'</span></span>'+
            '<span class="uk-text-small">$'+item.propmgt_price+'</span>'+
            '<p class="uk-badge">'+item.propmgt_special+'</p>';
    } else {
        price_html =
            '<span class="uk-text-small">$'+item.price+'</span>';
    }

    html =

        '<div class="uk-grid uk-panel">' +
            '<div class="uk-width-medium-1-2">' +
                image_html +
            '</div>' +
            '<div class="uk-width-medium-1-2">' +
                '<a href="' + properties_filters_basepath + 'index.php?option=com_mostwantedrealestate&view=property&id='+item.id+'" title="'+item.name+'" rel="" >' +
                    '<h3>' + item.name + '</h3>' +
                '</a>' +
            '</div>' +
            '<div class="uk-width-medium-1-2">' +
                price_html +
            '</div>' +
            '<div class="uk-width-medium-1-2">' +
                '<h4 class="uk-clearfix">' +
                    item.street+'<br/>' +
                    item.city_name+', ' + item.state_name + ' ' + item.postcode +
                '</h4>' +
            '</div>' +
            '<div class="uk-width-medium-1-2">' +
                '<i class="uk-icon-bed" aria-hidden="true"></i> ' + parseInt(item.bedrooms) + '  ' +
                '<i class="fa fa-bath" aria-hidden="true"></i> ' + parseInt(item.bathrooms) + '  ' +
                '<i class="uk-icon-building" aria-hidden="true"></i> ' + parseInt(item.squarefeet) +'ft<sup>2</sup>' +
            '</div>' +
            '<div class="uk-width-medium-1-2">' +
                item.propdesc.substring(0,390) + (item.propdesc.length > 390 ? '...' : '') +
            '</div>' +
        '</div>'
    ;

    return html;
}


/**
 * Set map bounds to include all markers.
 */
function boundToMarkers() {

}

function getBoundedItemList(){
    var locationList = boundedMarkers.map(function(location, i) {
        var name = (location.store_name != "")
            ?
            '<div class="storeListCompanyName">' +
            '<a target="_blank" href="http://'+ location.store_website + '">' + location.store_name + '</a>' +
            '</div>'
            :'';

        var street1 = (location.store_street1 != "")
            ? '<div class="storeListStreet1">' + location.store_street1 + '</div>'
            : '';

        var street2 = (location.store_street2 != "")
            ? '<div class="storeListStreet2">' + location.store_street2 + '</div>'
            : '';

        var city = (location.store_city != "")
            ? '<div class="storeListCity">' + location.store_city + '</div>'
            : '';


        var phone = (location.store_phone != "")
            ? '<div class="storeListTel">' + 'Telefon: ' + location.store_phone + '</div>'
            : '';

        var website = (location.store_website != "")
            ? '<div class="storeListWebsite">' + 'URL: <a target="_blank" href="http://'+ location.store_website + '">' + location.store_website + '</a>'  + '</div>'
            : '';

        var info = (location.store_info != "")
            ? '<p>' + location.store_info + '</p>'
            : '';

        var distance = '<div class="storeListDistance">' + getDistance(
                new google.maps.LatLng({
                    lat: parseFloat(location.store_lat),
                    lng: parseFloat(location.store_lng)}),
                new google.maps.LatLng(activeLocation)
            ) + 'km</div>';

        var mapLinkContainer = jQuery('<div class="storeListMaplink"></div>');
        var mapLink = jQuery('<a class="storeMapLink" href="">Veibeskrivelse</a>');

        jQuery(mapLinkContainer).append(mapLink);

        var moreinfoContainer = jQuery('<div class="moreinfoContainer"></div>');

        var moreinfoLink = jQuery('<a class="moreinfoAction" href="">Mer Info</a>');
        var moreinfoBody = jQuery('<div class="moreinfoBody hidden"></div>');

        jQuery(moreinfoBody).html(
            phone +
            website +
            info
        );

        jQuery(moreinfoContainer).append(moreinfoLink);
        jQuery(moreinfoContainer).append(moreinfoBody);

        var locationListItemContainer = jQuery('<li class="locationListItemContainer"></li>');

        jQuery(locationListItemContainer).append(name);
        jQuery(locationListItemContainer).append(street1);
        jQuery(locationListItemContainer).append(street2);
        jQuery(locationListItemContainer).append(moreinfoContainer);
        jQuery(locationListItemContainer).append(distance);
        jQuery(locationListItemContainer).append(mapLinkContainer);

        jQuery(moreinfoLink).on('click',function(e){
            e.preventDefault();
            console.log('ping');
            jQuery(moreinfoBody).toggleClass('hidden');
        });

        jQuery(mapLink).on('click',function(e){
            e.preventDefault();
            console.log(location);
            map.panTo({
                lat: parseFloat(location.store_lat),
                lng: parseFloat(location.store_lng)
            });
            map.setZoom(14);
        });

        return locationListItemContainer;
    });

    jQuery.each(locationList,function(index,locationListItem){
        jQuery('#locationListContainer').append(locationListItem);
    })


}

function updateBoundedMarkers() {
    updatededMarkers = [];
    for (var i = 0; i < locations.length; i++) {
        if (map.getBounds().contains(markers[i].getPosition())) {
            store_location = locations[i];
            store_location.distance = getDistance(
                new google.maps.LatLng({
                    lat: parseFloat(store_location.store_lat),
                    lng: parseFloat(store_location.store_lng)
                }),
                new google.maps.LatLng(activeLocation)
            );
            updatededMarkers.push(store_location);
        }
        else {
            // markers[i] is not in visible bounds
        }
    }
}

function setBounds(data) {
    //var rawBounds = data.results[0]["geometry"]["bounds"];

    var bounds = new google.maps.LatLngBounds(
        new google.maps.LatLng(data.southwest),
        new google.maps.LatLng(data.northeast)
    );

    map.fitBounds(bounds);
}

function sortMarkersByDistance(){
    updatededMarkers.sort(function(a, b) {
        return parseFloat(a.distance) - parseFloat(b.distance);
    });
}

function updateStatus(msg) {
    document.getElementById("divStatus").innerHTML = msg;
}

function getDistance(latLngA, latLngB) {

    return parseFloat(Math.round(( google.maps.geometry.spherical.computeDistanceBetween(latLngA, latLngB) / 1000) * 100) / 100).toFixed(2);
}

function handleLocationError(error) {
    switch (error.code) {
        case 0:
            updateStatus("There was an error while retrieving your location: " + error.message);
            break;

        case 1:
            updateStatus("The user prevented this page from retrieving the location.");
            break;

        case 2:
            updateStatus("The browser was unable to determine your location: " + error.message);
            break;

        case 3:

            updateStatus("The browser timed out before retrieving the location.");
            break;
    }
}

function setUserLocation(location) {
    activeLocation = userLocation = location;
}

function updateUserLocation(callback) {
    if (navigator.geolocation) {
        setUserLocationHTML5(callback);
    }else{
        setUserLocationFromGoogleGeolocation(callback)
    }
    console.log('updating user location');
}

function setUserLocationHTML5(callback){
    console.log('ite');
    navigator.geolocation.getCurrentPosition(
        function(position){
            console.log(position);
            var location =  {
                lat:position.coords.latitude,
                lng:position.coords.longitude
            };

            setUserLocation(location);
            callback();
        },function(error){
            handleLocationError(error);
            setUserLocationFromGoogleGeolocation(renderLocationList);
        }
        ,
        {
            timeout: 50000
        }
    );
    //while(typeof userLocation == 'undefined'){}
}

function setUserLocationFromGoogleGeolocation(callback) {
    var callback = callback;
    var location;
    return $.ajax({
        url: "https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyDwtPRHah6A6spbIuu22f4uyvQo0wPAw94",
        processData: false,
        contentType: 'application/json',
        method: 'POST',
        dataType: 'json',
        data: JSON.stringify({
            "considerIp": "true"
        })
    }).done(function (data) {
        console.log(data);
        location = {
            lat: data.location.lat,
            lng: data.location.lng
        };
        console.log(activeLocation);
        setUserLocation(location);
        callback();
    });
}

function panToNearMe() {
    console.log(userLocation);
    map.panTo(userLocation);
    map.setZoom(10);
    renderLocationList();
}

function panToZip() {
    var postal_code = jQuery('#zipsearch_input').val();
    geoCodeRequest = $.ajax({
        url: "https://maps.googleapis.com/maps/api/geocode/json?address=" + postal_code + "&components=country:norway",
        processData: false
    });

    geoCodeRequest.done(function (data) {
        setBounds(data.results[0]["geometry"]["bounds"]);
        renderLocationList();
    });
}