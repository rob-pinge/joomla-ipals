<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		2.0.0-Alpha1
	@build			22nd December, 2016
	@created		1st November, 2016
	@package		Most Wanted Real Estate
	@subpackage		property.php
	@author			Most Wanted Web Services, Inc. <http://mostwantedrealestatesites.com>	
	@copyright		Copyright (C) 2015-2016. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
	
	Most Wanted Real Estate Component
	
/------------------------------------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla controllerform library
require_once JPATH_COMPONENT.'/controller.php';

/**
 * Property Controller
 */
class MostwantedrealestateControllerProperty extends MostwantedrealestateController
{
	public function &getModel($name = 'Property', $prefix = 'MostwantedrealestateModel', $config = array())
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));

		return $model;
	}
        
        /**
	 * Function that allows child controller access to model data
	 * after the data has been saved.
	 *
	 * @param   JModel  &$model     The data model object.
	 * @param   array   $validData  The validated data.
	 *
	 * @return  void
	 *
	 * @since   11.1
	 */
	public function addTofavorite()
	{
		$app = JFactory::getApplication();		
		$jinput = $app->input;
                
                //get all variables
		$propertyId =$jinput->get('propertyId', '', 'STR');
                $db = JFactory::getDbo();
                $userId = JFactory::getUser()->id;
                if($userId == 0){
                    echo "Please login to add to favorites";die;
                }else{
                    //check if exists
                    
                    // Create a new query object.
                    $query = $db->getQuery(true);

                    // Select all records from the user profile table where key begins with "custom.".
                    // Order it by the ordering field.
                    $query->select($db->quoteName(array('propertyid')));
                    $query->from($db->quoteName('#__mostwantedrealestate_favorite_listing'));
                    $query->where($db->quoteName('propertyid') . ' = '. $propertyId);
                    $query->where($db->quoteName('uid') . ' = '. $userId);
                    

                    // Reset the query using our newly populated query object.
                    $db->setQuery($query);
                    
                    if (count($db->loadObjectList()) == 0)
                        {
                                // Create a new query object.
                            $query = $db->getQuery(true);

                            // Insert columns.
                            $columns = array('propertyid', 'uid', 'created_by', 'created','access');

                            // Insert values.
                            $values = array($propertyId, $userId, $userId,'NOW()',2);

                            // Prepare the insert query.
                            $query
                                ->insert($db->quoteName('#__mostwantedrealestate_favorite_listing'))
                                ->columns($db->quoteName($columns))
                                ->values(implode(',', $values));
                            // Set the query using our newly populated query object and execute it.
                            $db->setQuery($query);
                            $response = $db->execute();
                            if($response){
                                echo 'This listing has been successfully added to your favorites';
                            }
                        }else{
                            echo 'You have already added this listing to your favorites.';
                        }

                die;
                }
	}
}
