<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		2.0.0-Alpha1
	@build			16th December, 2016
	@created		1st November, 2016
	@package		Most Wanted Real Estate
	@subpackage		property.php
	@author			Most Wanted Web Services, Inc. <http://mostwantedrealestatesites.com>	
	@copyright		Copyright (C) 2015-2016. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
	
	Most Wanted Real Estate Component
	
/------------------------------------------------------------------------------------------------------*/

// No direct access.
defined('_JEXEC') or die;
// import Joomla controllerform library
require_once JPATH_COMPONENT.'/controller.php';
/**
 * Coupons list controller class.
 *
 * @since  1.6
 */
class MostwantedrealestateControllerAgent extends MostwantedrealestateController
{
	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional
	 * @param   array   $config  Configuration array for model. Optional
	 *
	 * @return object	The model
	 *
	 * @since	1.6
	 */
	public function &getModel($name = 'Agent', $prefix = 'MostwantedrealestateModel', $config = array())
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));

		return $model;
	}
        /**
	 * Method to upload images
	 *
	 * @param   object  $model  The model.
	 *
	 * @return  boolean   True if successful, false otherwise and internal error is set.
	 *
	 * @since   2.5
	 */
	public function mailAgent()
	{ 
		$app = JFactory::getApplication();		
		$jinput = $app->input; 
		
                $mailer = JFactory::getMailer();
                // Get plugin 'my_plugin' of plugin type 'my_plugin_type'
                $plugin = JPluginHelper::getPlugin('captcha', 'recaptcha');

                // Check if plugin is enabled
                if ($plugin)
                {
                    // Get plugin params
                    $pluginParams = new JRegistry($plugin->params);

                    $private_key = $pluginParams->get('private_key'); 
                }
                
                //get all variables
		$name =$jinput->get('name', '', 'STR');
		$email =$jinput->get('email', '', 'STR');
		$recipient =$jinput->get('agent_email', '', 'STR');
		$message =$jinput->get('message', '', 'STR');
		$day_phone =$jinput->get('day_phone', '', 'STR');
		$evening_phone =$jinput->get('evening_phone', '', 'STR');
		$mobile_phone =$jinput->get('mobile_phone', '', 'STR');
		$pre_contact =$jinput->get('pre_contact', '', 'STR');
		$captcha_response =$jinput->get('response', '', 'STR');
                $response = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$private_key."&response=".$captcha_response."&remoteip=".$_SERVER['REMOTE_ADDR']), true);

                // if the captcha is cleared with google, send the mail and echo ok.
                if ($response['success'] != false) {
		
                        $sender = array( $email, $name );

                        $mailer->addRecipient($recipient);

                        $mailer->setSender($sender);

                        $subject =  ucfirst($name)." sent a message";

                            $html = '<table style="background-color: #dee0e2; padding: 20px 20px 100px 20px;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">';
                        $html .= '<tbody>';
                        $html .= '<tr><td valign="top" align="center">';
                        $html .= '<table style="padding: 10px; width: 600px; background-color: #ffffff" width="600" cellspacing="0" cellpadding="0" border="0">';
                        $html .= '<thead>';
                        $html .= '<tr><th style="background:#026cb7 none repeat scroll 0 0;color:white;height:50px;font-size:20px;">Most Wanted Real Estate</th></tr>';
                        $html .= '</thead>';
                        $html .= '<tbody>';
                        $html .= '<tr><td>&nbsp;</td></tr>';
                        $html .= '<tr><td style="padding-top:20px;padding-bottom:20px;font-size:15px;">Hello Agent,<br><br>' . ucfirst($name) . ' wants to contact you </td></tr>';
                        $html .= '<tr><td style="padding-top:20px;padding-bottom:20px;font-size:15px;"><strong>Message: </strong>' . $message . ' </td></tr>';
                        $html .= '<tr><td>';
                        $html .= '<table style="width:400px;padding:10px 0 20px 0;" align="left">';
                        $html .= '<caption style="font-size:15px;text-align:left;"><strong><u>Contact Information</u></strong></caption>';
                        $html .= '<tr style="padding-top:5px;padding-bottom:5px;"><th style="padding: 7px;text-align: left;font-size:14px;">Name</th><td style="font-size:13px;">'. ucfirst($name) .'</td></tr>';
                        $html .= '<tr style="padding-top:5px;padding-bottom:5px;font-size:16px;"><th style="padding: 7px;text-align: left;font-size:14px;">Email</td><td style="font-size:13px;">'.$email.'</td></tr>';
                        $html .= '<tr style="padding-top:5px;padding-bottom:5px;font-size:16px;"><th style="padding: 7px;text-align: left;font-size:14px;">Day Phone</td><td style="font-size:13px;">'.$day_phone.'</td></tr>';
                        $html .= '<tr style="padding-top:5px;padding-bottom:5px;font-size:16px;"><th style="padding: 7px;text-align: left;font-size:14px;">Evening Phone</td><td style="font-size:13px;">'.$evening_phone.'</td></tr>';
                        $html .= '<tr style="padding-top:5px;padding-bottom:5px;font-size:16px;"><th style="padding: 7px;text-align: left;font-size:14px;">Mobile Phone</td><td style="font-size:13px;">'.$mobile_phone.'</td></tr>';
                        $html .= '<tr style="padding-top:5px;padding-bottom:5px;font-size:16px;"><th style="padding: 7px;text-align: left;font-size:14px;">Preferred contact method</td><td style="font-size:13px;">'.$pre_contact.'</td></tr>';
                        $html .= '</table>';
                        $html .= '</td></tr>';
                        $html .= '</tbody>';
                        $html .= '</table>';
                        $html .= '</td></tr>';
                        $html .= '</tbody>';
                        $html .= '</table>';

                        $body = $html;
                        $mailer->isHTML(true);
                        $mailer->setSubject($subject);
                        $mailer->setBody($body);
                        $send = $mailer->Send();
                        if ( $send !== true ) {
                            echo 'Error sending email: ' . $send->__toString();
                        } else {
                            echo '1';
                        }

                }else{
                    echo "Captcha error";
                }
		die();		
	}
}
