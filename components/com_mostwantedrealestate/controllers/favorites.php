<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		2.0.0-Alpha1
	@build			22nd December, 2016
	@created		1st November, 2016
	@package		Most Wanted Real Estate
	@subpackage		property.php
	@author			Most Wanted Web Services, Inc. <http://mostwantedrealestatesites.com>	
	@copyright		Copyright (C) 2015-2016. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
	
	Most Wanted Real Estate Component
	
/------------------------------------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla controllerform library
require_once JPATH_COMPONENT.'/controller.php';

/**
 * Property Controller
 */
class MostwantedrealestateControllerFavorites extends MostwantedrealestateController
{
	public function &getModel($name = 'Favorites', $prefix = 'MostwantedrealestateModel', $config = array())
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));

		return $model;
	}
        
        /**
	 * Function that allows child controller access to model data
	 * after the data has been saved.
	 *
	 * @param   JModel  &$model     The data model object.
	 * @param   array   $validData  The validated data.
	 *
	 * @return  void
	 *
	 * @since   11.1
	 */
	public function deletefromfavorites()
	{
		$app = JFactory::getApplication();		
		$jinput = $app->input;
                
                //get all variables
		$favId =$jinput->get('id', '', 'STR');
                $db = JFactory::getDbo();
                $userId = JFactory::getUser()->id;
                if($userId != 0){
                    $db = JFactory::getDbo();
 
                    $query = $db->getQuery(true);

                    // delete all custom keys for user 1001.
                    $conditions = array(
                        $db->quoteName('id') . ' = ' . $db->quote($favId)
                    );

                    $query->delete($db->quoteName('#__mostwantedrealestate_favorite_listing'));
                    $query->where($conditions);

                    $db->setQuery($query);

                    echo $result = $db->execute();
                }
                    

                die;
        }
	
}
