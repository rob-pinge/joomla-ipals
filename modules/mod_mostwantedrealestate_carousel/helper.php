<?php
/**
 * @copyright	Copyright (c) 2017 Most Wanted Web Services Inc. (http://mwweb.host). All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

// no direct access
defined('_JEXEC') or die;

/**
 * Most Wanted Web Services Inc. - mostwantedrealestate_carousel Helper Class.
 *
 * @package		Joomla.Site
 * @subpakage	MostWantedRealEstate.mostwantedrealestate_carousel
 */
abstract class modmostwantedrealestate_carouselHelper {
 
	public static function getPropertyCarouselData($params) {
		// get database object
		$db = JFactory::getDbo();
		
		// get the database query object from the driver
		$query = $db->getQuery(true);
				
		// generate the SQL
		$query
			->select(array('a.id', 'a.name'))
			->from($db->quoteName('#__mostwantedrealestate_property', 'a'));
        
        //filter on featured
        if($params->get('transtype') !='')
            $query->where($db->quoteName('a.trans_type') . ' = ' . $params->get('transtype') );
        
        //check if custom field selected and add to query if it is.
        if($params->get('reflistcust1') && $params->get('listcust1') !='')
            $query->where($db->quoteName('a.customone').'='.$params->get('listcust1'));
        
        //check if category selected and add to query if it is.
        if($params->get('reflistcat') && $params->get('listcat') !='')
            $query->where($db->quoteName('a.catid').'='.$params->get('listcat'));
        
        if($params->get('reflistcompany') && $params->get('listbiz') !='')
            $query->where( $db->quoteName('a.companyid').'='.$params->get('listbiz'));
        
        if($params->get('reflistcity') && $params->get('listcity') !='')
            $query->where( $db->quoteName('a.cityid').'='.$params->get('listcity'));
        
        if($params->get('refliststate') && $params->get('liststate') !='')
            $query->where( $db->quoteName('a.stateid').'='.$params->get('liststate'));
        
        if($params->get('reflistcountry') && $params->get('listcountry') !='')
            $query->where( $db->quoteName('a.countryid').'='.$params->get('listcountry'));
        
        //limit the number of results
        $query->setLimit($params->get('count'), 0);
        
        $query->order(self::getListOrder($params->get('listingorder')));
        
		// copy the query to the driver
		$db->setQuery($query);
		
		// load the row
		$records = $db->loadObjectList();
        
        foreach($records as &$record){
            $record->images = self::getPropertyImages($record->id);
        }
        
		// return the record to the caller
		return $records;
		
		}
    
    /**
     * Function getListOrder
     * Returns the order keyword for the order key
     *
     * @param $orderKey
     * @return string
     */
    public static function getListOrder($orderKey){
        $orderKeys = [
            0=>['created desc'],
            1=>['RAND() desc'],
            2=>['price desc'],
            3=>['price asc']
        ];
        
        return $orderKeys[$orderKey];
    }
    
    public static function getPropertyImages($propertyId){
        // get database object
        $db = JFactory::getDbo();
        
        // get the database query object from the driver
        $query = $db->getQuery(true);
        
        // generate the SQL
        $query
            ->select(array('i.propid', 'i.path'))
            ->select($query->concatenate(['i.filename','i.type'],'_th.').' AS '. $db->quoteName('filename') )
            ->from($db->quoteName('#__mostwantedrealestate_image', 'i'))
            ->where($db->quoteName('i.propid').'='.$propertyId);
        
        // copy the query to the driver
        $db->setQuery($query);
        
        // load the row
        $records = $db->loadObjectList();
        
        return $records;
        
    }
	
}