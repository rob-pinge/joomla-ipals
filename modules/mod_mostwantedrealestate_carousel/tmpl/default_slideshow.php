<?php
    /**
     * @copyright	Copyright (c) 2017 Most Wanted Web Services Inc. (http://mwweb.host). All rights reserved.
     * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
     */

// no direct access
    defined('_JEXEC') or die;

	JFactory::getDocument()->addScript(JURI::root().'/media/com_mostwantedrealestate/uikit/js/components/slideshow.js');
	JFactory::getDocument()->addStyleSheet(JURI::root().'/media/com_mostwantedrealestate/uikit/css/components/slideshow.css');
?>
	<div class="uk-slidenav-position" data-uk-slideshow="{autoplay: '<?php echo $params->get('slidesetautoplay');?>',  autoplayInterval: <?php echo $params->get('autoplayinterval');?>, animation: '<?php echo $params->get('slidesetanimation');?>', duration: <?php echo $params->get('animationduration');?>, pauseOnHover: '<?php echo $params->get('pauseonhover');?>', slices: <?php echo $params->get('slideshowslices');?>}"
>

		<ul class="uk-slideshow">
			<?php foreach($carouselData as $carouselDatum):?>
				<?php foreach($carouselDatum->images as $image):?>
					<li>
						<a href="<?php echo JRoute::_('index.php?option=com_mostwantedrealestate&view=property&id='.$carouselDatum->id);?>">
						<img class="uk-thumbnail uk-thumbnail-expand" src="<?php echo JURI::root().$image->path.$image->filename; ?>" alt="" width='auto' height='auto'>
						<div class="uk-thumbnail-caption"><?php echo $carouselDatum->name; ?></div>
						</a>
					</li>
				<?php endforeach; ?>
			<?php endforeach;?>
		</ul>
		<a href="#" class="uk-slidenav uk-slidenav-previous" data-uk-slideshow-item="previous"></a>
		<a href="#" class="uk-slidenav uk-slidenav-next" data-uk-slideshow-item="next"></a>
	</div>
