<?php
    /**
     * @copyright	Copyright (c) 2017 Most Wanted Web Services Inc. (http://mwweb.host). All rights reserved.
     * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
     */

// no direct access
    defined('_JEXEC') or die;

	JHtml::_('jquery.framework');
	JFactory::getDocument()->addScript(JURI::root().'/media/com_mostwantedrealestate/uikit/js/uikit.js');
	JFactory::getDocument()->addStyleSheet(JURI::root().'/media/com_mostwantedrealestate/uikit/css/uikit.css');
	JFactory::getDocument()->addStyleSheet(JURI::root().'/media/com_mostwantedrealestate/uikit/css/components/slidenav.css');
	JFactory::getDocument()->addStyleSheet(JURI::root().'/media/com_mostwantedrealestate/uikit/css/components/dotnav.css');

	if($params->get('slidertype') == "0" )
        require(JModuleHelper::getLayoutPath('mod_mostwantedrealestate_carousel', '_:default_carousel'));
    
    if($params->get('slidertype') == "1" )
        require(JModuleHelper::getLayoutPath('mod_mostwantedrealestate_carousel', '_:default_slideset'));

    if($params->get('slidertype') == "2" )
        require(JModuleHelper::getLayoutPath('mod_mostwantedrealestate_carousel', '_:default_slideshow'));

?>
