<?php
    /**
     * @copyright	Copyright (c) 2017 Most Wanted Web Services Inc. (http://mwweb.host). All rights reserved.
     * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
     */

// no direct access
    defined('_JEXEC') or die;

	JFactory::getDocument()->addScript(JURI::root().'/media/com_mostwantedrealestate/uikit/js/components/slider.js');
	JFactory::getDocument()->addStyleSheet(JURI::root().'/media/com_mostwantedrealestate/uikit/css/components/slider.css');
?>
	<div class="uk-slidenav-position" data-uk-slider="{autoplay: '<?php echo $params->get('slidesetautoplay');?>', autoplayInterval: <?php echo $params->get('autoplayinterval');?>, pauseOnHover: '<?php echo $params->get('pauseonhover');?>', threshold: <?php echo $params->get('sliderthreshold');?>, center: <?php echo $params->get('centeritems');?>}">
		<div class="uk-slider-container">
			<ul class="uk-slider uk-grid-width-large-1-6 uk-grid-width-medium-1-3 uk-grid-width-small-1-2">
                    <?php foreach($carouselData as $carouselDatum):?>
                        <?php foreach($carouselDatum->images as $image):?>
                        <li>
                            <a href="<?php echo JRoute::_('index.php?option=com_mostwantedrealestate&view=property&id='.$carouselDatum->id);?>">
                                <img class="uk-thumbnail uk-thumbnail-small" src="<?php echo JURI::root().$image->path.$image->filename; ?>" alt="" style="width:auto;height:<?php echo $params->get('slideheight');?>px !important;" data-uk-tooltip title="<?php echo $carouselDatum->name; ?>">
                            </a>
                        </li>
                        <?php endforeach; ?>
                    <?php endforeach;?>
			</ul>
		</div>
				<a href="#" class="uk-slidenav uk-slidenav-contrast uk-slidenav-previous" data-uk-slider-item="previous"></a>
				<a href="#" class="uk-slidenav uk-slidenav-contrast uk-slidenav-next" data-uk-slider-item="next"></a>
	</div>
