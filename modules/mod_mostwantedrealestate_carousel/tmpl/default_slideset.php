<?php
    /**
     * @copyright	Copyright (c) 2017 Most Wanted Web Services Inc. (http://mwweb.host). All rights reserved.
     * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
     */

// no direct access
    defined('_JEXEC') or die;

	JFactory::getDocument()->addScript(JURI::root().'/media/com_mostwantedrealestate/uikit/js/components/slideset.js');
?>
	<div class="uk-margin" data-uk-slideset="{default: <?php echo $params->get('numslides');?>, autoplay: '<?php echo $params->get('slidesetautoplay');?>', animation: '<?php echo $params->get('slidesetanimation');?>', autoplayInterval: <?php echo $params->get('autoplayinterval');?>, duration: <?php echo $params->get('animationduration');?>, pauseOnHover: '<?php echo $params->get('pauseonhover');?>'}">
		<div class="uk-slidenav-position uk-margin">
			<ul class="uk-slideset uk-grid uk-flex-center">
                    <?php foreach($carouselData as $carouselDatum):?>
                        <?php foreach($carouselDatum->images as $image):?>
                        <li>
                            <a href="<?php echo JRoute::_('index.php?option=com_mostwantedrealestate&view=property&id='.$carouselDatum->id);?>">
                                <img class="uk-thumbnail uk-thumbnail-mini" src="<?php echo JURI::root().$image->path.$image->filename; ?>" alt="" style="width:auto;height:<?php echo $params->get('slideheight');?>px !important;" data-uk-tooltip title="<?php echo $carouselDatum->name; ?>">
                            </a>
                        </li>
                        <?php endforeach; ?>
                    <?php endforeach;?>
			</ul>
			<a href="#" class="uk-slidenav uk-slidenav-contrast uk-slidenav-previous" data-uk-slideset-item="previous"></a>
			<a href="#" class="uk-slidenav uk-slidenav-contrast uk-slidenav-next" data-uk-slideset-item="next"></a>
		</div>
			<ul class="uk-slideset-nav uk-dotnav uk-flex-center"></ul>
	</div>
