<?php
/**
 * @copyright	Copyright (c) 2017 Most Wanted Web Services Inc. (http://mwweb.host). All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

// no direct access
defined('_JEXEC') or die;

/**
 * Most Wanted Web Services Inc. - mostwantedrealestate_quicksearch Helper Class.
 *
 * @package		Joomla.Site
 * @subpakage	MostWantedRealEstate.mostwantedrealestate_quicksearch
 */
class modmostwantedrealestate_quicksearchHelper {
    
    public static function getData($params) {
        
        $data = new stdClass();
        
        // get database object
        $db = JFactory::getDbo();
        
        
        if ($params->get('showmod_categories_select') == 1) {
            // get the database query object from the driver
            $query = $db->getQuery(true);
    
            // generate the SQL
            $query
                ->select(array('c.id', 'c.title'))
                ->from($db->quoteName('#__categories', 'c'))
                ->where($db->quoteName( 'c.extension') . '=' . $db->quote('com_mostwantedrealestate.properties'));
            // copy the query to the driver
            $db->setQuery($query);
    
            $data->categorylist = $db->loadObjectList();
        }
        
        if ($params->get('showmod_transtype_select') == 1) {
            // get the database query object from the driver
            $query = $db->getQuery(true);
    
            // generate the SQL
            $query
                ->select(array('a.id', 'a.name'))
                ->from($db->quoteName('#__mostwantedrealestate_transaction_type', 'a'));
            // copy the query to the driver
            $db->setQuery($query);
            
            $data->transactiontypeslist =  $db->loadObjectList();
        }
    
        if ($params->get('showmod_marketstatus_select') == 1) {
            // get the database query object from the driver
            $query = $db->getQuery(true);
    
            // generate the SQL
            $query
                ->select(array('a.id', 'a.name'))
                ->from($db->quoteName('#__mostwantedrealestate_market_status', 'a'));
            // copy the query to the driver
            $db->setQuery($query);
            
            $data->marketstatuslist =  $db->loadObjectList();
        }
        
        if ($params->get('showmod_agent_select') == 1) {
    
            // get the database query object from the driver
            $query = $db->getQuery(true);
    
            // generate the SQL
            $query
                ->select(array('a.id', 'a.name'))
                ->from($db->quoteName('#__mostwantedrealestate_agent', 'a'));
            // copy the query to the driver
            $db->setQuery($query);
            
            $data->agentlist  =  $db->loadObjectList();
        }
    
        if ($params->get('showmod_state_select') == 1){
            
            // get the database query object from the driver
            $query = $db->getQuery(true);
    
            // generate the SQL
            $query
                ->select(array('a.id', 'a.name'))
                ->from($db->quoteName('#__mostwantedrealestate_state', 'a'));
            // copy the query to the driver
            $db->setQuery($query);
            
            $data->statelist  =  $db->loadObjectList();
        }
    
        if ($params->get('showmod_city_select') == 1){
    
            // get the database query object from the driver
            $query = $db->getQuery(true);
    
            // generate the SQL
            $query
                ->select(array('a.id', 'a.name'))
                ->from($db->quoteName('#__mostwantedrealestate_city', 'a'));
            // copy the query to the driver
            $db->setQuery($query);
            
            $data->citylist  =  $db->loadObjectList();
        }
        
        // return the record to the caller
    
        
        return $data;

    }
}