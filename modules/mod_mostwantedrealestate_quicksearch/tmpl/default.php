<?php
    /**
     * @copyright	Copyright (c) 2017 Most Wanted Web Services Inc. (http://mwweb.host). All rights reserved.
     * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
     */
    
    // no direct access
    defined('_JEXEC') or die;
	
	JFactory::getDocument()->addScript(JURI::root().'/media/com_mostwantedrealestate/uikit/js/uikit.js');
	JFactory::getDocument()->addStyleSheet(JURI::root().'/media/com_mostwantedrealestate/uikit/css/uikit.css');
	JFactory::getDocument()->addScript(JURI::root().'/media/com_mostwantedrealestate/uikit/js/components/form-select.js');
	JFactory::getDocument()->addStyleSheet(JURI::root().'/media/com_mostwantedrealestate/uikit/css/components/form-select.css');

?>
<div id="quicksearch_container" class="uk-form">
    <fieldset data-uk-margin>
	<div class="uk-form-row uk-margin-remove">
        <?php if ($params->get('showmod_keyword_select') == 1) : ?>
            <input type="text" id="keyword" placeholder="Keyword">
            <script>
                jQuery(function(){
                    if( value = QuicksearchGetCookie('quicksearch.keyword') ){
                        jQuery("#quicksearch_container #keyword").val(value);
                    }

                    jQuery("#quicksearch_container #keyword").on("keyup",
                        QuicksearchDebounce( function(e) {
                            console.log('debounce complete');
                            QuicksearchSetCookie('quicksearch.keyword',e.target.value);
                        }, 250, false));
                });
            </script>
        <?php endif; ?>
	</div>
	<div class="uk-form-row uk-margin-remove">
        <?php if ($params->get('showmod_categories_select') == 1) : ?>
            <div  class="uk-button uk-width-1-1 uk-form-select" data-uk-form-select>
                <span></span>
                <i class="uk-icon-caret-down"></i>
                <select id="categoryDd">
                    <option value="">All Property Categories:</option>
                    <?php foreach ($data->categorylist as $item) { echo "<option value=" . $item->id . ">" . $item->title . "</option>"; } ?>
                </select>
            </div>
            <script>
                jQuery(function(){
                    if( value = QuicksearchGetCookie('quicksearch.categoryDd')){
                        jQuery("#quicksearch_container #categoryDd").val(value);
                    }
                    jQuery("#quicksearch_container #categoryDd").on("change",
                        function(e) {
                            QuicksearchSetCookie('quicksearch.categoryDd',e.target.value);
                    });
                });
            </script>
        <?php endif; ?>
	</div>
	<div class="uk-form-row uk-margin-remove">
        <?php if ($params->get('showmod_transtype_select') == 1) : ?>
            <div  class="uk-button uk-width-1-1 uk-form-select" data-uk-form-select>
                <span></span>
                <i class="uk-icon-caret-down"></i>
                <select id="transtypeDd">
                    <option value="">All Transaction Types:</option>
                    <?php foreach ($data->transactiontypeslist as $item) { echo "<option value=" . $item->id . ">" . $item->name . "</option>"; } ?>
                </select>
            </div>
            <script>
                jQuery(function(){
                    if( value = QuicksearchGetCookie('quicksearch.transtypeDd')){
                        jQuery("#quicksearch_container #transtypeDd").val(value);
                    }
                    jQuery("#quicksearch_container #transtypeDd").on("change",
                        QuicksearchDebounce(function(e) {
                            QuicksearchSetCookie('quicksearch.transtypeDd',e.target.value)
                        }));
                });
            </script>
        <?php endif; ?>
	</div>
	<div class="uk-form-row uk-margin-remove">
        <?php if ($params->get('showmod_marketstatus_select') == 1) : ?>
            <div  class="uk-button uk-width-1-1 uk-form-select" data-uk-form-select>
                <span></span>
                <i class="uk-icon-caret-down"></i>
                <select id="marketstatusDd">
                    <option value="">All Market Statuses:</option>
                    <?php foreach ($data->marketstatuslist as $item) { echo "<option value=" . $item->id . ">" . $item->name . "</option>"; } ?>
                </select>
            </div>
            <script>
                jQuery(function(){
                    if( value = QuicksearchGetCookie('quicksearch.marketstatusDd')){
                        jQuery("#quicksearch_container #marketstatusDd").val(value);
                    }
                    jQuery("#quicksearch_container #marketstatusDd").on("change",function(e){
                            QuicksearchSetCookie('quicksearch.marketstatusDd',e.target.value);
                    });
                });
            </script>
        <?php endif; ?>
	</div>
	<div class="uk-form-row uk-margin-remove">
        <?php if ($params->get('showmod_agent_select') == 1) : ?>
            <div  class="uk-button uk-width-1-1 uk-form-select" data-uk-form-select>
                <span></span>
                <i class="uk-icon-caret-down"></i>
                <select id="agentDd">
                    <option value="">All Agents:</option>
                    <?php foreach ($data->agentlist as $item) { echo "<option value=" . $item->id . ">" . $item->name . "</option>"; } ?>
                </select>
            </div>
            <script>
                jQuery(function(){
                    if( value = QuicksearchGetCookie('quicksearch.agentDd'))
                        jQuery('#quicksearch_container #agentDd').val(value);
                    
                    jQuery('#quicksearch_container #agentDd').on("change",function(e){
                        QuicksearchSetCookie('quicksearch.agentDd',e.target.value);
                    });
                });
            </script>
            
        <?php endif; ?>
	</div>
	<div class="uk-form-row uk-margin-remove">
        <?php if ($params->get('showmod_state_select') == 1) : ?>
            <div  class="uk-button uk-width-1-1 uk-form-select" data-uk-form-select>
                <span></span>
                <i class="uk-icon-caret-down"></i>
                <select id="stateDd">
                    <option value="">All States:</option>
                    <?php foreach ($data->statelist as $item) { echo "<option value=" . $item->id . ">" . $item->name . "</option>"; } ?>
                </select>
            </div>
            <script>
                jQuery(function(){
                    if( value = QuicksearchGetCookie('quicksearch.stateDd'))
                        jQuery('#quicksearch_container #stateDd').val(value);
                    
                    jQuery('#quicksearch_container #stateDd').on("change",function(e){
                        QuicksearchSetCookie('quicksearch.stateDd',e.target.value);
                    });
                });
            </script>
        <?php endif; ?>
	</div>
	<div class="uk-form-row uk-margin-remove">
        <?php if ($params->get('showmod_city_select') == 1) : ?>
            <div  class="uk-button uk-width-1-1 uk-form-select" data-uk-form-select>
                <span></span>
                <i class="uk-icon-caret-down"></i>
                <select id="cityDd">
                    <option value="">All Cities:</option>
                    <?php foreach ($data->citylist as $item) { echo "<option value=" . $item->id . ">" . $item->name . "</option>"; } ?>
                </select>
            </div>
            <script>
                jQuery(function(){
                    if( value = QuicksearchGetCookie('quicksearch.cityDd')){
                        jQuery('#quicksearch_container #cityDd').val(value);
                    }
                    
                    jQuery('#quicksearch_container #cityDd').on("change",function(e){
                        QuicksearchSetCookie('quicksearch.cityDd',e.target.value);
                    });
                });
            </script>
        <?php endif; ?>
	</div>
	<div class="uk-form-row uk-margin-remove">
        <?php if ($params->get('showmod_waterfront_select') == 1): ?>
            <div  class="uk-button uk-width-1-1 uk-form-select" data-uk-form-select>
                <span></span>
                <i class="uk-icon-caret-down"></i>
                <select id="waterfront">
                    <option value="">Waterfront:</option>
                    <option value="1">Yes</option>
                    <option value="0">No</option>
                </select>
            </div>
            <script>
                jQuery(function(){
                    if( value = QuicksearchGetCookie('quicksearch.waterfront'))
                        jQuery('#quicksearch_container #waterfront').val(value);

                    jQuery('#quicksearch_container #waterfront').on("change",function(e){
                        QuicksearchSetCookie('quicksearch.waterfront',e.target.value);
                    });
                });
            </script>
        <?php endif; ?>
	</div>
	<div class="uk-form-row uk-margin-remove">
        <?php if ($params->get('showmod_minbedsbaths_select') == 1) : ?>
            <div  class="uk-button uk-width-1-1 uk-form-select" data-uk-form-select>
                <span></span>
                <i class="uk-icon-caret-down"></i>
                <select name="minbedsDd" id='minbedsDd'>
                    <option value="">Min beds:</option>
                    <?php
                        $minbeds = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
                        foreach ($minbeds as $minbed) {
                            echo "<option value=" . $minbed . ">" . $minbed . "</option>";
                        }
                    ?>
                </select>
            </div>
            <script>
                jQuery(function(){
                    if( value = QuicksearchGetCookie('quicksearch.minbedsDd')){
                        jQuery('#quicksearch_container #minbedsDd').val(value);
                    }

                    jQuery('#quicksearch_container #minbedsDd').on("change",function(e){
                        QuicksearchSetCookie('quicksearch.minbedsDd',e.target.value);
                    });
                });
            </script>
        <?php endif; ?>
	</div>
	<div class="uk-form-row uk-margin-remove">
        <?php if ($params->get('showmod_minbedsbaths_select') == 1) : ?>
            <div  class="uk-button uk-width-1-1 uk-form-select" data-uk-form-select>
                <span></span>
                <i class="uk-icon-caret-down"></i>
                <select name="minbathDd" id='minbathDd'>
                    <option value="">Min baths:</option>
                    <?php
                        $minbaths = ['1 or more', '2 or more', '3 or more', '4 or more', '5 or more'];
                        foreach ($minbaths as $key => $val) {
                            echo "<option value=" . $key . ">" . $val . "</option>";
                        }
                    ?>
                </select>
            </div>
            <script>
                jQuery(function(){
                    
                    if( value = QuicksearchGetCookie('quicksearch.minbathDd'))
                        jQuery('#quicksearch_container #minbathDd').val(value);


                    jQuery('#quicksearch_container #minbathDd').on("change",function(e){
                        QuicksearchSetCookie('quicksearch.minbathDd',e.target.value);
                    });
                    
                })
            </script>
        <?php endif; ?>
	</div>
	<div class="uk-form-row uk-margin-remove">
        <?php if ($params->get('showmod_minmaxarea_select') == 1) : ?>
            <div  class="uk-button uk-form-select" data-uk-form-select>
                <span></span>
                <i class="uk-icon-caret-down"></i>
                <select name="min_area" id='min_area'>
                    <option value="">Min area:</option>
                    <?php
                        $areas = [0, 500, 1000, 1500, 2000, 2500, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000, 15000, 20000];
                        foreach ($areas as $area) {
                            echo "<option value=" . $area . ">" . $area . "</option>";
                        }
                    ?>
                </select>
            </div>
            <script>
                jQuery(function(){
                    if( value = QuicksearchGetCookie('quicksearch.min_area'))
                        jQuery('#quicksearch_container #min_area').val(value);


                    jQuery('#quicksearch_container #min_area').on("change",function(e){
                        QuicksearchSetCookie('quicksearch.min_area',e.target.value);
                    });
                });
            </script>
            <div  class="uk-button uk-form-select" data-uk-form-select>
                <span></span>
                <i class="uk-icon-caret-down"></i>
                <select name="max_area" id='max_area'>
                    <option value="">Max area:</option>
                    <?php
                        $areas = [25000, 20000, 15000, 10000, 9000, 8000, 7000, 6000, 5000, 4000, 3000, 2500, 2000, 1500, 1000, 500, 0];
                        foreach ($areas as $area) {
                            echo "<option value=" . $area . ">" . $area . "</option>";
                        }
                    ?>
                </select>
            </div>
            <script>
                jQuery(function(){
                    
                    if( value = QuicksearchGetCookie('quicksearch.max_area'))
                        jQuery('#quicksearch_container #max_area').val(value);

                    jQuery('#quicksearch_container #max_area').on("change",function(e){
                        QuicksearchSetCookie('quicksearch.max_area',e.target.value);
                    });
                });
            </script>
        <?php endif; ?>
	</div>
	<div class="uk-form-row">
        <?php if ($params->get('showmod_minmaxprice_select') == 1) : ?>
            <fieldset data-uk-margin>
                <input type="number" id="min_price" placeholder="Min Price" class="uk-margin-remove">
                <input type="number" id="max_price" placeholder="Max Price">
            </fieldset>

            <script>
                jQuery(function(){
                    if( value = QuicksearchGetCookie('quicksearch.min_price')){
                        jQuery('#quicksearch_container #min_price').val(value);
                    }
                    jQuery('#quicksearch_container #min_price').on("keyup",
                        QuicksearchDebounce(function(e) {
                            QuicksearchSetCookie('quicksearch.min_price',e.target.value)
                        }, 250 , false));

                    if( value = QuicksearchGetCookie('quicksearch.max_price')){
                        jQuery('#quicksearch_container #max_price').val(value);
                    }
                    jQuery('#quicksearch_container #max_price').on("keyup",
                        QuicksearchDebounce(function(e) {
                            QuicksearchSetCookie('quicksearch.max_price',e.target.value)
                        }, 250 , false));
                });
            </script>
        <?php endif; ?>
	</div>
	<div class="uk-form-row">
        <?php if ($params->get('showmod_minmaxland_select') == 1) : ?>
            <input type="number" name='min_land' id='min_land' placeholder="Min Land" class="uk-margin-remove">
            <input type="number" name='max_land' id='max_land' placeholder="Max Land">
            <script>
                jQuery(function(){
                    if( value = QuicksearchGetCookie('quicksearch.min_land')){
                        jQuery('#quicksearch_container #min_land').val(value);
                    }
                    jQuery('#quicksearch_container #min_land').on("keyup",
                        QuicksearchDebounce(function(e) {
                            QuicksearchSetCookie('quicksearch.min_land',e.target.value);
                        }, 250 , false));

                    if( value = QuicksearchGetCookie('quicksearch.max_land')){
                        jQuery('#quicksearch_container #max_land').val(value);
                    }
                    jQuery('#quicksearch_container #max_land').on("keyup",
                        QuicksearchDebounce(function(e) {
                            QuicksearchSetCookie('quicksearch.max_land',e.target.value);
                        }, 250 , false));
                });
            </script>
        <?php endif; ?>
	</div>
	<div class="uk-form-row">
        <a class="btn btn-primary" href="<?php echo JRoute::_('index.php?option=com_mostwantedrealestate&view=properties'); ?>">Search</a>
	</div>
    </fieldset>
</div>
<script>
    jQuery(function(){
    
        
    });

    // Returns a function, that, as long as it continues to be invoked, will not
    // be triggered. The function will be called after it stops being called for
    // N milliseconds. If `immediate` is passed, trigger the function on the
    // leading edge, instead of the trailing.
    function QuicksearchDebounce(func, wait, immediate) {
        console.log('debouncing');
        var timeout;
        return function() {
            var context = this, args = arguments;
            var later = function() {
                timeout = null;
                if (!immediate) func.apply(context, args);
            };
            var callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) func.apply(context, args);
        };
    }

    function QuicksearchSetCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires="+d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }
    
    function QuicksearchGetCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for(var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return false;
    }
    
</script>
