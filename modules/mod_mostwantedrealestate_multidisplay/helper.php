<?php
/**
 * @copyright	Copyright (c) 2017 Most Wanted Web Services Inc. (http://mwweb.host). All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

// no direct access
defined('_JEXEC') or die;

/**
 * Most Wanted Web Services Inc. - mostwantedrealestate_multidisplay Helper Class.
 *
 * @package		Joomla.Site
 * @subpakage	MostWantedRealEstate.mostwantedrealestate_multidisplay
 */
class modmostwantedrealestate_multidisplayHelper {
    
    public static function getData($params) {
        // get database object
        $db = JFactory::getDbo();
        
        // get the database query object from the driver
        $query = $db->getQuery(true);
        
        // generate the SQL
        $query
            ->select(array('p.id', 'p.name', 'p.latitude','p.longitude'))
            ->from($db->quoteName('#__mostwantedrealestate_property', 'p'));
        
        
        //Get the city name if required.
        if($params->get('titlesource') && $params->get('titlesource') == '1'){
            $query->leftJoin('#__mostwantedrealestate_city AS c ON c.id = p.cityid');
            $query->select('c.name as name');
        }
    
        //filter on agent
        if($params->get('reflistagent') && $params->get('listagent') !='')
            $query->where($db->quoteName('p.agent') . ' = ' . $params->get('listagent') );
        
        //filter on transaction type
        if($params->get('listingstype') && $params->get('listingstype') !='')
            $query->where($db->quoteName('p.trans_type') . ' = ' . $params->get('listingstype') );
    
        //filter on market_status
        if($params->get('marketstatus') && $params->get('marketstatus') !='')
            $query->where($db->quoteName('p.mkt_stats') . ' = ' . $params->get('marketstatus') );
        
        //filter on featured
        if($params->get('displaystatus') && $params->get('displaystatus') !='')
            $query->where($db->quoteName('p.featured') . ' = ' . $params->get('displaystatus') );
        
        //check if custom field selected and add to query if it is.
        if($params->get('reflistcust1') && $params->get('listcust1') !='')
            $query->where($db->quoteName('p.customone').'='.$params->get('listcust1'));
        
        //check if category selected and add to query if it is.
        if($params->get('reflistcat') && $params->get('listcat') !='')
            $query->where($db->quoteName('p.catid').'='.$params->get('listcat'));
        
        if($params->get('reflistcompany') && $params->get('listbiz') !='')
            $query->where( $db->quoteName('p.companyid').'='.$params->get('listbiz'));
        
        if($params->get('reflistcity') && $params->get('listcity') !='')
            $query->where( $db->quoteName('p.cityid').'='.$params->get('listcity'));
        
        if($params->get('refliststate') && $params->get('liststate') !='')
            $query->where( $db->quoteName('p.stateid').'='.$params->get('liststate'));
        
        if($params->get('reflistcountry') && $params->get('listcountry') !='')
            $query->where( $db->quoteName('p.countryid').'='.$params->get('listcountry'));
        
        //limit the number of results
        $query->setLimit($params->get('count'), 0);
        
        //$query->order( 'a.'.self::getListOrder($params->get('listingorder')));
        
        // copy the query to the driver
        $db->setQuery($query);
        
        // load the row
        $records = $db->loadObjectList();
    
        foreach($records as &$record){
            $record->images = self::getPropertyImages($record->id);
        }
        
        // return the record to the caller
        return $records;
        
    }
    
    /**
     * Function getListOrder
     * Returns the order keyword for the order key
     *
     * @param $orderKey
     * @return string
     */
    public static function getListOrder($orderKey){
        $orderKeys = [
            0=>['a.created desc'],
            1=>['RAND() desc'],
            2=>['a.price desc'],
            3=>['a.price asc']
        ];
        
        return $orderKeys[$orderKey];
    }
    
    public static function getPropertyImages($propertyId){
        // get database object
        $db = JFactory::getDbo();
        
        // get the database query object from the driver
        $query = $db->getQuery(true);
        
        // generate the SQL
        $query
            ->select(array('i.propid', 'i.path'))
            ->select($query->concatenate(['i.filename','i.type'],'_th.').' AS '. $db->quoteName('filename') )
            ->from($db->quoteName('#__mostwantedrealestate_image', 'i'))
            ->where($db->quoteName('i.propid').'='.$propertyId);
        
        // copy the query to the driver
        $db->setQuery($query);
        
        // load the row
        $records = $db->loadObjectList();
        
        return $records;
        
    }
    
    
    
}