<?php
/**
 * @copyright	Copyright (c) 2017 Most Wanted Web Services Inc. (http://mwweb.host). All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

// no direct access
defined('_JEXEC') or die;

echo 'Most Wanted Real Estate Multi-Display of Listings Module';
?>
<div class="uk-grid" data-uk-grid-margin>
    <div class="uk-width-medium-1-1">
        <?php if(count($data) > 0 ):foreach($data as $datum):?>
            <div class="uk-panel uk-panel-box">
               <?php foreach($datum as $key=>$value):?>
                    <div><?php echo $key.':'.print_r($value);?></div><br/>
               <?php endforeach;?>
            </div>
        <?php endforeach;else: ?>
            <div class="uk-panel uk-panel-box">
                <h4 class="uk-panel-title">No Properties Found</h4>
            </div>
        <?php endif; ?>
    </div>
</div>