<?php
/**
 * @copyright	@copyright	Copyright (c) 2017 Most Wanted Web Services Inc. (http://mwweb.host). All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

// no direct access
defined('_JEXEC') or die;

// include the module helper functions only once
require_once __DIR__ . '/helper.php';

$agentData = modmostwantedrealestate_agentsHelper::getAgentsData($params);

$class_sfx = htmlspecialchars($params->get('class_sfx'));

require(JModuleHelper::getLayoutPath('mod_mostwantedrealestate_agents', $params->get('layout', 'default')));