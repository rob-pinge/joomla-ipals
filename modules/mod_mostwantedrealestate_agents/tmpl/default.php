<?php
/**
 * @copyright	Copyright (c) 2017 Most Wanted Web Services Inc. (http://mwweb.host). All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

// no direct access
defined('_JEXEC') or die;

	JHtml::_('jquery.framework');
	JFactory::getDocument()->addScript(JURI::root().'/media/com_mostwantedrealestate/uikit/js/uikit.js');
	JFactory::getDocument()->addStyleSheet(JURI::root().'/media/com_mostwantedrealestate/uikit/css/uikit.css');

?>
	<div class="uk-grid" data-uk-grid-margin>
		<div class="uk-width-medium-1-1">
            <?php if(count($agentData) > 0 ):foreach($agentData as $agentDatum):?>
			<div class="uk-panel uk-panel-box">
                
                <?php if( ( $params->get('titlesource') != "0") && ( $params->get('titlepos') =="0" )): ?>
                
                <?php if($params->get('titlesize') == '0'): ?><h4<?php else: ?><h3<?php endif; ?> class="uk-panel-title">
                    <a rel=""
                       href="<?php echo 'index.php?option=com_mostwantedrealestate&view=agentview&id='.$agentDatum->id;?>"
                       title="<?php echo ( $params->get('titlesource') == 2 ) ? $agentDatum->companyname.' '.$agentDatum->name  : $agentDatum->name ?>" >
                        <?php echo ( $params->get('titlesource') == 2 ) ? ($agentDatum->companyname.'<br/>'.$agentDatum->name) : ($agentDatum->name); ?>
                    </a>
                <?php if($params->get('titlesize') == '0'): ?></h4><?php else: ?></h3><?php endif; ?>
                
                <?php endif; ?>
                
				<div>
        <?php if(empty($agentDatum->image)){ ?>
        <div> <a href="<?php echo 'index.php?option=com_mostwantedrealestate&view=agentview&id='.$agentDatum->id;?>" title="<?php echo $agentDatum->name;?>" rel=""> <img class='uk-thumbnail uk-thumbnail-mini' src="<?php echo JURI::root().'/media/com_mostwantedrealestate/images/No_image_available.png'; ?>"> </a> </div>
        <?php } else { ?>
        <div> <a href="<?php echo 'index.php?option=com_mostwantedrealestate&view=agentview&id='.$agentDatum->id;?>" title="<?php echo $agentDatum->name;?>" rel=""> <img class='uk-thumbnail uk-thumbnail-mini' src="<?php echo $agentDatum->image; ?>"> </a> </div>
        <?php } ?>
				</div>
                
                <?php if( ( $params->get('titlesource') != "0") && ( $params->get('titlepos') =="1" )): ?>
                    
                <?php if($params->get('titlesize') == '0'): ?><h4<?php else: ?><h3<?php endif; ?> class="uk-panel-title">
                    <a rel=""
                       href="<?php echo 'index.php?option=com_mostwantedrealestate&view=agent&id='.$agentDatum->id;?>"
                       title="<?php echo ( $params->get('titlesource') == 2 ) ? $agentDatum->companyname.' '.$agentDatum->name  : $agentDatum->name ?>" >
                        <?php echo ( $params->get('titlesource') == 2 ) ? ($agentDatum->companyname.'<br/>'.$agentDatum->name) : ($agentDatum->name); ?>
                    </a>
                <?php if($params->get('titlesize') == '0'): ?></h4><?php else: ?></h3><?php endif; ?>
                
                <?php endif; ?>
				<h6><?php echo $agentDatum->street; ?><br/>
					<?php echo $agentDatum->city_name.', '.$agentDatum->state_name.' '.$agentDatum->postcode; ?><br/>
					<?php echo $agentDatum->phone; ?></h6>
				<spacer>
				<p><?php echo $agentDatum->bio; ?></p>
			</div>
            <?php endforeach;else: ?>
            <div class="uk-panel uk-panel-box">
                <h4 class="uk-panel-title">No Agents Found</h4>
            </div>
            <?php endif; ?>
            
		</div>
	</div>

<?php /*
    echo 'listingstype:'.$params->get("listingstype")."<br/>";
    echo 'listingorder:'.$params->get("listingorder")."<br/>";
    echo 'count:'.$params->get("count")."<br/>";
    echo 'reflistcat:'.$params->get("reflistcat")."<br/>";
    echo 'listcat:'.$params->get("listcat")."<br/>";
    echo 'reflistcompany:'.$params->get("reflistcompany")."<br/>";
    echo 'listbiz:'.$params->get("listbiz")."<br/>";
    echo 'reflistcity:'.$params->get("reflistcity")."<br/>";
    echo 'listcity:'.$params->get("listcity")."<br/>";
    echo 'refliststate:'.$params->get("refliststate")."<br/>";
    echo 'liststate:'.$params->get("liststate")."<br/>";
    echo 'reflistcountry:'.$params->get("reflistcountry")."<br/>";
    echo 'listcountry:'.$params->get("listcountry")."<br/>";
    echo 'titlepos:'.$params->get("titlepos")."<br/>";
    echo 'titlesize:'.$params->get("titlesize")."<br/>";
    echo 'titlesource:'.$params->get("titlesource")."<br/>";
    echo 'textsource:'.$params->get("textsource")."<br/>";
    echo 'layout:'.$params->get("layout")."<br/>";
    echo 'moduleclass_sfx:'.$params->get("moduleclass_sfx")."<br/>";
    echo 'cache:'.$params->get("cache")."<br/>";
    echo 'cache_time:'.$params->get("cache_time")."<br/>";
    echo 'cachemode:'.$params->get("cachemode")."<br/>";
    echo 'module_tag:'.$params->get("module_tag")."<br/>";
    echo 'bootstrap_size:'.$params->get("bootstrap_size")."<br/>";
    echo 'header_tag:'.$params->get("header_tag")."<br/>";
    echo 'header_class:'.$params->get("header_class")."<br/>";
    echo 'style:'.$params->get("style")."<br/>";
    echo 'initialized:'.$params->get("initialized")."<br/>";
    echo 'separator:'.$params->get("separator")."<br/>";*/?>