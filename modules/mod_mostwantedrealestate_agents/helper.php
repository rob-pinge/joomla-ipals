<?php
/**
 * @copyright	Copyright (c) 2017 Most Wanted Web Services Inc. (http://mwweb.host). All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

// no direct access
defined('_JEXEC') or die;

/**
 * Most Wanted Web Services Inc. - mostwantedrealestate_agents Helper Class.
 *
 * @package		Joomla.Site
 * @subpakage	MostWantedRealEstate.mostwantedrealestate_agents
 */
abstract class modmostwantedrealestate_agentsHelper {
        
	public static function getAgentsData($params) {
	 
	    //print_r($params);exit;
	    //var_dump($params);exit;
		// get database object
		$db = JFactory::getDbo();
		
		// get the database query object from the driver
        /** @var  JDatabaseQuery $query */
		$query = $db->getQuery(true);
		
		// generate the SQL
		$query
			->select(
				array(
					'a.id','a.catid', 'a.street','a.cityid',
					'a.stateid','a.postcode','a.countryid',
					'a.phone','a.mobile','a.fax','a.image',
					'a.bio','a.name','b.name as state_name',
					'c.name as city_name'
				)
			)
			->from($db->quoteName('#__mostwantedrealestate_agent', 'a'))
			->leftJoin('#__mostwantedrealestate_state AS b ON b.id = a.stateid')
			->leftJoin('#__mostwantedrealestate_city AS c ON c.id = a.cityid');
			/*->leftJoin('#__categories AS f ON f.id = a.catid');
			->leftJoin('#__mostwantedrealestate_country AS b ON b.id = a.countryid');*/
       
        //filter on featured
        if($params->get('listingtype') !='')
            $query->where($db->quoteName('a.featured') . ' = ' . $params->get('listingtype') );
        
		//check if category selected and add to query if it is.
        if($params->get('reflistcat'))
            $query->where($db->quoteName('a.catid').'='.$params->get('listcat'));
        
        if($params->get('reflistcompany'))
            $query->where( $db->quoteName('a.companyid').'='.$params->get('listbiz'));
        
        if($params->get('reflistcity'))
            $query->where( $db->quoteName('a.cityid').'='.$params->get('listcity'));
        
        if($params->get('refliststate'))
            $query->where( $db->quoteName('a.stateid').'='.$params->get('liststate'));
        
        if($params->get('reflistcountry'))
            $query->where( $db->quoteName('a.countryid').'='.$params->get('listcountry'));
        
        if($params->get('titlesource') == 2)
            $query
                ->select('a.companyid, c.name as companyname')
                ->leftJoin('#__mostwantedrealestate_company as f on f.id = a.companyid');
		
        //limit the number of results
		$query->setLimit($params->get('count'), 0);
		
		$query->order(self::getListOrder($params->get('listingorder')));
		
		// copy the query to the driver
		$db->setQuery($query);
		
		// load the row
		$record = $db->loadObjectList();
		
		// return the record to the caller
		return $record;		

		}
    
    /**
     * Function getListOrder
     * Returns the order keyword for the order key
     *
     * @param $orderKey
     * @return string
     */
    public static function getListOrder($orderKey){
	    $orderKeys = [
            //0=>['created desc'],
            1=>['RAND() desc'],
            2=>['ordering asc'],
            3=>['name desc'],
            4=>['featured desc']
        ];
	    
	    return $orderKeys[$orderKey];
    }
}