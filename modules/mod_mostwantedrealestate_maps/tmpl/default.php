<?php
    /**
     * @copyright	Copyright (c) 2017 Most Wanted Web Services Inc. (http://mwweb.host). All rights reserved.
     * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
     */
    
    // no direct access
    defined('_JEXEC') or die;
    
    foreach($data as &$datum){
        ob_start();
        require(JModuleHelper::getLayoutPath('mod_mostwantedrealestate_maps', '_:default_infobox'));
        $datum->infohtml = ob_get_contents();
        ob_end_clean();
    }
    
    if($params->get('usealtmap') == "0" )
        require(JModuleHelper::getLayoutPath('mod_mostwantedrealestate_maps', '_:default_google'));
    
    if($params->get('usealtmap') == "1" )
        require(JModuleHelper::getLayoutPath('mod_mostwantedrealestate_maps', '_:default_bing'));
    
    
    /*echo "transtype:".$params->get('transtype').'<br/>';
    echo "listingorder:".$params->get('listingorder').'<br/>';
    echo "count:".$params->get('count').'<br/>';
    echo "reflistcust1:".$params->get('reflistcust1').'<br/>';
    echo "listcust1:".$params->get('listcust1').'<br/>';
    echo "reflistcat:".$params->get('reflistcat').'<br/>';
    echo "listcat:".$params->get('listcat').'<br/>';
    echo "reflistcity:".$params->get('reflistcity').'<br/>';
    echo "listcity:".$params->get('listcity').'<br/>';
    echo "refliststate:".$params->get('refliststate').'<br/>';
    echo "liststate:".$params->get('liststate').'<br/>';
    echo "reflistcountry:".$params->get('reflistcountry').'<br/>';
    echo "listcountry:".$params->get('listcountry').'<br/>';
    echo "titlesource:".$params->get('titlesource').'<br/>';
    echo "usealtmap:".$params->get('usealtmap').'<br/>';
    echo "usestreetview:".$params->get('usestreetview').'<br/>';
    echo "control_size:".$params->get('control_size').'<br/>';
    echo "maplat:".$params->get('maplat').'<br/>';
    echo "maplong:".$params->get('maplong').'<br/>';
    echo "mapres:".$params->get('mapres').'<br/>';
    echo "mapheight:".$params->get('mapheight').'<br/>';*/
    
    
?>

<script>
    var filtered_items = <?php echo json_encode($data);?>;
</script>