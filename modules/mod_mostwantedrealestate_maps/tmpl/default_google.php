<?php
    // No direct access to this file
    defined('_JEXEC') or die('Restricted access');
    
    $component_params  = JComponentHelper::getParams( 'com_mostwantedrealestate' );
    JFactory::getDocument()->addScript(JUri::base() .'/modules/mod_mostwantedrealestate_maps/assets/js/googlemap.js')

?>
<div id="map_wrapper">
    <div id="map_canvas" class="mapping"></div>
</div>

<!-- Google Maps javascript -->
<script>
    var mapOptions = {
        apiKey: '<?php echo $component_params->get('gmapsapi'); ?>',
        mapElement: 'map_canvas',
        initial_zoom: <?php echo $params->get('mapres'); ?>,
        center: {
            lat: <?php echo $params->get('maplat'); ?> ,
            lng: <?php echo $params->get('maplong'); ?>
        }
    };

    jQuery(function($) {
        // Asynchronously Load the map API
        var script = document.createElement('script');
        script.src = "//maps.googleapis.com/maps/api/js?callback=initMap&key="+mapOptions.apiKey;
        document.body.appendChild(script);
    });
</script>
<!-- Google Maps CSS for Pin -->
<style>
    #map_wrapper {
        height: <?php echo $params->get('mapheight'); ?>px;
        position:relative;
    }

    #map_canvas {
        width: 100%;
        height: 100%;
    }
</style>