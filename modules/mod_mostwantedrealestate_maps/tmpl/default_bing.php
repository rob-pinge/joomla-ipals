<?php
    // No direct access to this file
    defined('_JEXEC') or die('Restricted access');
    
    $component_params  = JComponentHelper::getParams( 'com_mostwantedrealestate' );

    echo $params->get('mapres');
    JFactory::getDocument()->addScript(JUri::base() .'/modules/mod_mostwantedrealestate_maps/assets/js/bingmap.js')
?>

<div id="map_wrapper">
    <div id="map_canvas"></div>
</div>

<script>

    var mapOptions = {
        apiKey: '<?php echo $component_params->get('bingmapsapi'); ?>',
        mapElement: 'map_canvas',
        initial_zoom: <?php echo $params->get('mapres'); ?>,
        center: {
            lat: <?php echo $params->get('maplat'); ?> ,
            lng: <?php echo $params->get('maplong'); ?>
        }
    };

    jQuery(function(){

        jQuery('<script>')
            .attr('type', 'text/javascript')
            .attr('src', 'https://www.bing.com/api/maps/mapcontrol?callback=initMap')
            //.attr('async', 'true')
            //.attr('defer', 'true')
            .appendTo('body');
        //initMap(mapOptions);
    });

</script>
<style>
    #map_wrapper {
        height: <?php echo $params->get('mapheight'); ?>px;
        position:relative;
    }
    
    /*#map_canvas { position: absolute; top: 20; left: 10;  height: 500px; }*/
    #map_canvas {
        width: 100%;
        height: 100%;
        position: absolute;
    }
    /*
</style>

