/**
 * Created by cash america on 5/8/2017.
 */

var
    config,
    map,
    center,
    mapOptions,
    infobox,
    pinLayer,
    markers;

function initMap() {

    center = mapOptions.center
        ? new Microsoft.Maps.Location( mapOptions.center.lat, mapOptions.center.lng )
        : new Microsoft.Maps.Location( 47.629804069996275, -122.32080671523443 );


    defaultOptions = {
        labels : 'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
        base_path : location.href,
        image_path : location.href +'/assets/   img',
        css_path : location.href +'/assets/css',
        js_path : location.href +'/assets/js',
        map_marker_icon_file : '/blue-dot.png',
        map_culster_icon_uri : ''
    };

    mapOptions = {
        zoom: mapOptions.initial_zoom || 6,
        center: center ,
        cluster_max_zoom: mapOptions.cluster_max_zoom  || 12,
        mapElement: mapOptions.mapElement,
        apiKey: mapOptions.apiKey
    };


    map =    new Microsoft.Maps.Map(
        document.getElementById(mapOptions.mapElement),
        {
            credentials: mapOptions.apiKey,
            zoom: mapOptions.zoom,
            center: mapOptions.center
        }
    );

    //infoboxLayer = new Microsoft.Maps.EntityCollection();
    //pinLayer = new Microsoft.Maps.EntityCollection();

    //map.entities.push(pinLayer);
    //map.entities.push(infoboxLayer);

    //Create an infobox at the center of the map but don't show it.
    infobox = new Microsoft.Maps.Infobox(map.getCenter(), {
        visible: false,
        maxWidth: 400,
        maxHeight: 400
    });

    //Assign the infobox to a map instance.
    infobox.setMap(map);
    updateMapMarkers(filtered_items);
}

function updateMapMarkers(filtered_items) {
    map.entities.clear();
    filtered_items.map(function (item, i) {
        console.log(item);

        var position = new Microsoft.Maps.Location(parseFloat(item.latitude),parseFloat(item.longitude));

        var pushpin = new Microsoft.Maps.Pushpin(position, {
            text: i,
            title: item.name,
            icon: 'https://ecn.dev.virtualearth.net/mapcontrol/v7.0/7.0.20150902134620.61/i/poi_search.png',
            anchor: new Microsoft.Maps.Point(12,6)
        });

        var pinInfoBoxTitleString = item.name;
        /*var pinInfoBoxDescriptionString =
         '<div class="infoWindowContent">' +
         '<div class="infoWindowBodyContent">' +
         'location:' + item.id + ' content' +
         '</div>' +
         '</div>';*/

        var pinInfoBoxDescriptionString = item.infohtml;

        pushpin.metadata = {
            title: pinInfoBoxTitleString,
            description: pinInfoBoxDescriptionString
        };

        //var pinInfoBox = new Microsoft.Maps.Infobox(
        //    position,
        //    {
        //        title: pinInfoBoxTitleString,
        //        description: pinInfoBoxDescriptionString,
        //        visible: false
        //    });


        //Add a click event handler to the pushpin.
        Microsoft.Maps.Events.addHandler(pushpin, 'click', pushpinClicked);

        map.entities.push(pushpin);

        //pinInfoBox.setMap(map);

    });
}


function pushpinClicked(e) {
    //Make sure the infobox has metadata to display.
    if (e.target.metadata) {
        //Set the infobox options with the metadata of the pushpin.
        infobox.setOptions({
            location: e.target.getLocation(),
            title: e.target.metadata.title,
            description: e.target.metadata.description,
            visible: true
        });
    }
}


function getItemInfoBoxHtml(item){

    var
        html,
        image_html,
        price_html;

    var base_url = properties_filters_basepath;

    if( typeof item.image == 'undefined'){
        image_html =
            '<div>' +
            '<a href="' + properties_filters_basepath + 'index.php?option=com_mostwantedrealestate&view=property&id=' + item.id + '" title="'+item.name+'" rel="">' +
            '<img class="uk-thumbnail uk-thumbnail-mini" src="' + base_url + '/media/com_mostwantedrealestate/images/No_image_available.png">' +
            '</a>' +
            '</div>';
    } else {
        image_html =
            '<div>' +
            '<a href="' + properties_filters_basepath + 'index.php?option=com_mostwantedrealestate&view=property&id='+item.id+'" title="'+item.name+'" rel="">' +
            '<img class="uk-thumbnail uk-thumbnail-mini"  src="' + base_url + item.image.path+'"> ' +
            '</a> <' +
            '</div>'
    }

    if( (item.pm_price_override == 1) && (parseInt(item.propmgt_price) != 0) ){
        price_html =
            '<span style="color:red;text-decoration:line-through"><span style="color:black">$'+item.price+'</span></span>'+
            '<span class="uk-text-medium">$'+item.propmgt_price+'</span>'+
            '<p class="uk-badge">'+item.propmgt_special+'</p>';
    } else {
        price_html =
            '<span class="uk-text-medium">$'+item.price+'</span>';
    }

    html =

        '<div class="uk-grid uk-panel">' +
        '<div class="uk-width-medium-1-2">' +
        image_html +
        '</div>' +
        '<div class="uk-width-medium-1-2">' +
        '<a href="' + properties_filters_basepath + 'index.php?option=com_mostwantedrealestate&view=property&id='+item.id+'" title="'+item.name+'" rel="" >' +
        '<h3>' + item.name + '</h3>' +
        '</a>' +
        '</div>' +
        '<div class="uk-width-medium-1-2">' +
        price_html +
        '</div>' +
        '<div class="uk-width-medium-1-2">' +
        '<h4 class="uk-clearfix">' +
        item.street+'<br/>' +
        item.city_name+', ' + item.state_name + ' ' + item.postcode +
        '</h4>' +
        '</div>' +
        '<div class="uk-width-medium-1-2">' +
        '<i class="uk-icon-bed" aria-hidden="true"></i> ' + parseInt(item.bedrooms) + '  ' +
        '<i class="fa fa-bath" aria-hidden="true"></i> ' + parseInt(item.bathrooms) + '  ' +
        '<i class="uk-icon-building" aria-hidden="true"></i> ' + parseInt(item.squarefeet) +'ft<sup>2</sup>' +
        '</div>' +
        '<div class="uk-width-medium-1-2">' +
        item.propdesc.substring(0,390) + (item.propdesc.length > 390 ? '...' : '') +
        '</div>' +
        '</div>'
    ;

    return html;
}