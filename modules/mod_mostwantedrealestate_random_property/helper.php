<?php
/**
 * @copyright	Copyright (c) 2017 Most Wanted Web Services, Inc. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

// no direct access
defined('_JEXEC') or die;

/**
 * Most Wanted Real Estate - mostwantedrealestate_random_property Helper Class.
 *
 * @package		Joomla.Site
 * @subpakage	RandomProperty.mostwantedrealestate_random_property
 */
abstract class modmostwantedrealestate_random_propertyHelper {
	public static function getRandomPropertyData() {
		// get database object
		$db = JFactory::getDbo();
		
		// get the database query object from the driver
		$query = $db->getQuery(true);
		
		// create the query
		$query->select("COUNT(*)");
		$query->from("#__mostwantedrealestate_agent");
		
		// Store query back to the driver
		$db->setQuery($query);
		
		// Get the number of rows
		$count = $db->loadResult();
		
		// Get a random record from the table
		$recordNumber = rand(1, $count);
		
		// Get a clean Query object
		$query = $db->getQuery(true);
		
		// generate the SQL
		$query->select("name, phone")->from("#__mostwantedrealestate_agent");
		$query->where("id=($recordNumber)");
		
		// copy the query to the driver
		$db->setQuery($query);
		
		// load the row
		$record = $db->loadObject();
		
		// return the record to the caller
		return $record;		
		
		}
	
}