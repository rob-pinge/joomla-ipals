<?php
/**
 * @copyright	Copyright (c) 2017 Most Wanted Web Services, Inc. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

// no direct access
defined('_JEXEC') or die;

?>
<div id="filter_container" class="uk-form">
  <fieldset data-uk-margin>
    <?php if ($this->params->get('keyword_filter') == 1) : ?>
        <input type="text" id="keyword" placeholder="Keyword">
    <?php endif; ?>
    <?php if ($this->params->get('category_filter') == 1) : ?>
        <div  class="uk-button uk-form-select" data-uk-form-select>
        <span></span>
        <i class="uk-icon-caret-down"></i>
        <select id="categoryDd">
          <option value="">All Property Categories:</option>
          <?php foreach ($this->categorylist as $item) { echo "<option value=" . $item->id . ">" . $item->title . "</option>"; } ?>
        </select>
        </div>
	<?php endif; ?>
	<?php if ($this->params->get('transtype_filter') == 1) : ?>
        <div  class="uk-button uk-form-select" data-uk-form-select>
        <span></span>
        <i class="uk-icon-caret-down"></i>
        <select id="transtypeDd">
          <option value="">All Transaction Types:</option>
          <?php foreach ($this->transactiontypeslist as $item) { echo "<option value=" . $item->id . ">" . $item->name . "</option>"; } ?>
        </select>
        </div>
	<?php endif; ?>
    <?php if ($this->params->get('mktstatus_filter') == 1) : ?>
        <div  class="uk-button uk-form-select" data-uk-form-select>
        <span></span>
        <i class="uk-icon-caret-down"></i>
        <select id="marketstatusDd">
          <option value="">All Market Statuses:</option>
          <?php foreach ($this->marketstatuslist as $item) { echo "<option value=" . $item->id . ">" . $item->name . "</option>"; } ?>
        </select>
        </div>
	<?php endif; ?>
    <?php if ($this->params->get('agent_filter') == 1) : ?>
        <div  class="uk-button uk-form-select" data-uk-form-select>
        <span></span>
        <i class="uk-icon-caret-down"></i>
        <select id="agentDd">
          <option value="">All Agents:</option>
          <?php foreach ($this->agentlist as $item) { echo "<option value=" . $item->id . ">" . $item->name . "</option>"; } ?>
        </select>
        </div>
	<?php endif; ?>
    <?php if ($this->params->get('state_filter') == 1) : ?>
        <div  class="uk-button uk-form-select" data-uk-form-select>
        <span></span>
        <i class="uk-icon-caret-down"></i>
        <select id="stateDd">
          <option value="">All States:</option>
          <?php foreach ($this->statelist as $item) { echo "<option value=" . $item->id . ">" . $item->name . "</option>"; } ?>
        </select>
        </div>
	<?php endif; ?>
    <?php if ($this->params->get('city_filter') == 1) : ?>
        <div  class="uk-button uk-form-select" data-uk-form-select>
        <span></span>
        <i class="uk-icon-caret-down"></i>
        <select id="cityDd">
          <option value="">All Cities:</option>
          <?php foreach ($this->citylist as $item) { echo "<option value=" . $item->id . ">" . $item->name . "</option>"; } ?>
        </select>
        </div>
	<?php endif; ?>
	<?php if ($this->params->get('waterfront_filter') == 1): ?>
        <div  class="uk-button uk-form-select" data-uk-form-select>
        <span></span>
        <i class="uk-icon-caret-down"></i>
        <select id="waterfront">
          <option value="">Waterfront:</option>
          <option value="1">Yes</option>
		  <option value="0">No</option>
        </select>
        </div>
	<?php endif; ?>
    <?php if ($this->params->get('beds_filter') == 1) : ?>
        <div  class="uk-button uk-form-select" data-uk-form-select>
        <span></span>
        <i class="uk-icon-caret-down"></i>
        <select name="minbedsDd" id='minbedsDd'>
            <option value="">Min beds:</option>
            <?php
                $minbeds = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
                foreach ($minbeds as $minbed) {
                    echo "<option value=" . $minbed . ">" . $minbed . "</option>";
                }
            ?>
        </select>
        </div>
	<?php endif; ?>
    <?php if ($this->params->get('baths_filter') == 1) : ?>
        <div  class="uk-button uk-form-select" data-uk-form-select>
        <span></span>
        <i class="uk-icon-caret-down"></i>
        <select name="minbathDd" id='minbathDd'>
          <option value="">Min baths:</option>
          <?php
                        $minbaths = ['1 or more', '2 or more', '3 or more', '4 or more', '5 or more'];
                        foreach ($minbaths as $key => $val) {
                            echo "<option value=" . $key . ">" . $val . "</option>";
                        }
                        ?>
        </select>
        </div>
	<?php endif; ?>
    <?php if ($this->params->get('area_filter') == 1) : ?>
        <div  class="uk-button uk-form-select" data-uk-form-select>
        <span></span>
        <i class="uk-icon-caret-down"></i>
        <select name="min_area" id='min_area'>
          <option value="">Min area:</option>
          <?php
                        $areas = [0, 500, 1000, 1500, 2000, 2500, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000, 15000, 20000];
                        foreach ($areas as $area) {
                            echo "<option value=" . $area . ">" . $area . "</option>";
                        }
                        ?>
        </select>
        </div>
        <div  class="uk-button uk-form-select" data-uk-form-select>
        <span></span>
        <i class="uk-icon-caret-down"></i>
        <select name="max_area" id='max_area'>
          <option value="">Max area:</option>
            <?php
                $areas = [25000, 20000, 15000, 10000, 9000, 8000, 7000, 6000, 5000, 4000, 3000, 2500, 2000, 1500, 1000, 500, 0];
                foreach ($areas as $area) {
                    echo "<option value=" . $area . ">" . $area . "</option>";
                }
            ?>
        </select>
        </div>
	<?php endif; ?>
    <?php if ($this->params->get('price_filter') == 1) : ?>
        <fieldset data-uk-margin>
            <input type="number" id="min_price" placeholder="Min Price">
            <input type="number" id="max_price" placeholder="Max Price">
        </fieldset>
	<?php endif; ?>
    <?php if ($this->params->get('land_filter') == 1) : ?>
        <input type="number" name='min_land' id='min_land' placeholder="Min Land">
        <input type="number" name='max_land' id='max_land' placeholder="Max Land">
	<?php endif; ?>
  </fieldset>
</div>
