<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		3.0.0
	@build			21st September, 2017
	@created		16th August, 2017
	@package		iPALS
	@subpackage		script.php
	@author			Most Wanted Web Services, Inc. <http://retsforealestate.com>	
	@copyright		Copyright (C) 2015. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
  ____  _____  _____  __  __  __      __       ___  _____  __  __  ____  _____  _  _  ____  _  _  ____ 
 (_  _)(  _  )(  _  )(  \/  )(  )    /__\     / __)(  _  )(  \/  )(  _ \(  _  )( \( )( ___)( \( )(_  _)
.-_)(   )(_)(  )(_)(  )    (  )(__  /(__)\   ( (__  )(_)(  )    (  )___/ )(_)(  )  (  )__)  )  (   )(  
\____) (_____)(_____)(_/\/\_)(____)(__)(__)   \___)(_____)(_/\/\_)(__)  (_____)(_)\_)(____)(_)\_) (__) 

/------------------------------------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

JHTML::_('behavior.modal');
jimport('joomla.installer.installer');
jimport('joomla.installer.helper');

/**
 * Script File of Ipals Component
 */
class com_ipalsInstallerScript
{
	/**
	 * method to install the component
	 *
	 * @return void
	 */
	function install($parent)
	{

	}

	/**
	 * method to uninstall the component
	 *
	 * @return void
	 */
	function uninstall($parent)
	{
		// [Interpretation 4156] Get Application object
		$app = JFactory::getApplication();

		// [Interpretation 4158] Get The Database object
		$db = JFactory::getDbo();

		// [Interpretation 4167] Create a new query object.
		$query = $db->getQuery(true);
		// [Interpretation 4169] Select id from content type table
		$query->select($db->quoteName('type_id'));
		$query->from($db->quoteName('#__content_types'));
		// [Interpretation 4172] Where Setting alias is found
		$query->where( $db->quoteName('type_alias') . ' = '. $db->quote('com_ipals.setting') );
		$db->setQuery($query);
		// [Interpretation 4175] Execute query to see if alias is found
		$db->execute();
		$setting_found = $db->getNumRows();
		// [Interpretation 4178] Now check if there were any rows
		if ($setting_found)
		{
			// [Interpretation 4181] Since there are load the needed  setting type ids
			$setting_ids = $db->loadColumn();
			// [Interpretation 4185] Remove Setting from the content type table
			$setting_condition = array( $db->quoteName('type_alias') . ' = '. $db->quote('com_ipals.setting') );
			// [Interpretation 4187] Create a new query object.
			$query = $db->getQuery(true);
			$query->delete($db->quoteName('#__content_types'));
			$query->where($setting_condition);
			$db->setQuery($query);
			// [Interpretation 4192] Execute the query to remove Setting items
			$setting_done = $db->execute();
			if ($setting_done);
			{
				// [Interpretation 4196] If succesfully remove Setting add queued success message.
				$app->enqueueMessage(JText::_('The (com_ipals.setting) type alias was removed from the <b>#__content_type</b> table'));
			}

			// [Interpretation 4202] Remove Setting items from the contentitem tag map table
			$setting_condition = array( $db->quoteName('type_alias') . ' = '. $db->quote('com_ipals.setting') );
			// [Interpretation 4204] Create a new query object.
			$query = $db->getQuery(true);
			$query->delete($db->quoteName('#__contentitem_tag_map'));
			$query->where($setting_condition);
			$db->setQuery($query);
			// [Interpretation 4209] Execute the query to remove Setting items
			$setting_done = $db->execute();
			if ($setting_done);
			{
				// [Interpretation 4213] If succesfully remove Setting add queued success message.
				$app->enqueueMessage(JText::_('The (com_ipals.setting) type alias was removed from the <b>#__contentitem_tag_map</b> table'));
			}

			// [Interpretation 4219] Remove Setting items from the ucm content table
			$setting_condition = array( $db->quoteName('core_type_alias') . ' = ' . $db->quote('com_ipals.setting') );
			// [Interpretation 4221] Create a new query object.
			$query = $db->getQuery(true);
			$query->delete($db->quoteName('#__ucm_content'));
			$query->where($setting_condition);
			$db->setQuery($query);
			// [Interpretation 4226] Execute the query to remove Setting items
			$setting_done = $db->execute();
			if ($setting_done);
			{
				// [Interpretation 4230] If succesfully remove Setting add queued success message.
				$app->enqueueMessage(JText::_('The (com_ipals.setting) type alias was removed from the <b>#__ucm_content</b> table'));
			}

			// [Interpretation 4236] Make sure that all the Setting items are cleared from DB
			foreach ($setting_ids as $setting_id)
			{
				// [Interpretation 4241] Remove Setting items from the ucm base table
				$setting_condition = array( $db->quoteName('ucm_type_id') . ' = ' . $setting_id);
				// [Interpretation 4243] Create a new query object.
				$query = $db->getQuery(true);
				$query->delete($db->quoteName('#__ucm_base'));
				$query->where($setting_condition);
				$db->setQuery($query);
				// [Interpretation 4248] Execute the query to remove Setting items
				$db->execute();

				// [Interpretation 4252] Remove Setting items from the ucm history table
				$setting_condition = array( $db->quoteName('ucm_type_id') . ' = ' . $setting_id);
				// [Interpretation 4254] Create a new query object.
				$query = $db->getQuery(true);
				$query->delete($db->quoteName('#__ucm_history'));
				$query->where($setting_condition);
				$db->setQuery($query);
				// [Interpretation 4259] Execute the query to remove Setting items
				$db->execute();
			}
		}

		// [Interpretation 4167] Create a new query object.
		$query = $db->getQuery(true);
		// [Interpretation 4169] Select id from content type table
		$query->select($db->quoteName('type_id'));
		$query->from($db->quoteName('#__content_types'));
		// [Interpretation 4172] Where Field alias is found
		$query->where( $db->quoteName('type_alias') . ' = '. $db->quote('com_ipals.field') );
		$db->setQuery($query);
		// [Interpretation 4175] Execute query to see if alias is found
		$db->execute();
		$field_found = $db->getNumRows();
		// [Interpretation 4178] Now check if there were any rows
		if ($field_found)
		{
			// [Interpretation 4181] Since there are load the needed  field type ids
			$field_ids = $db->loadColumn();
			// [Interpretation 4185] Remove Field from the content type table
			$field_condition = array( $db->quoteName('type_alias') . ' = '. $db->quote('com_ipals.field') );
			// [Interpretation 4187] Create a new query object.
			$query = $db->getQuery(true);
			$query->delete($db->quoteName('#__content_types'));
			$query->where($field_condition);
			$db->setQuery($query);
			// [Interpretation 4192] Execute the query to remove Field items
			$field_done = $db->execute();
			if ($field_done);
			{
				// [Interpretation 4196] If succesfully remove Field add queued success message.
				$app->enqueueMessage(JText::_('The (com_ipals.field) type alias was removed from the <b>#__content_type</b> table'));
			}

			// [Interpretation 4202] Remove Field items from the contentitem tag map table
			$field_condition = array( $db->quoteName('type_alias') . ' = '. $db->quote('com_ipals.field') );
			// [Interpretation 4204] Create a new query object.
			$query = $db->getQuery(true);
			$query->delete($db->quoteName('#__contentitem_tag_map'));
			$query->where($field_condition);
			$db->setQuery($query);
			// [Interpretation 4209] Execute the query to remove Field items
			$field_done = $db->execute();
			if ($field_done);
			{
				// [Interpretation 4213] If succesfully remove Field add queued success message.
				$app->enqueueMessage(JText::_('The (com_ipals.field) type alias was removed from the <b>#__contentitem_tag_map</b> table'));
			}

			// [Interpretation 4219] Remove Field items from the ucm content table
			$field_condition = array( $db->quoteName('core_type_alias') . ' = ' . $db->quote('com_ipals.field') );
			// [Interpretation 4221] Create a new query object.
			$query = $db->getQuery(true);
			$query->delete($db->quoteName('#__ucm_content'));
			$query->where($field_condition);
			$db->setQuery($query);
			// [Interpretation 4226] Execute the query to remove Field items
			$field_done = $db->execute();
			if ($field_done);
			{
				// [Interpretation 4230] If succesfully remove Field add queued success message.
				$app->enqueueMessage(JText::_('The (com_ipals.field) type alias was removed from the <b>#__ucm_content</b> table'));
			}

			// [Interpretation 4236] Make sure that all the Field items are cleared from DB
			foreach ($field_ids as $field_id)
			{
				// [Interpretation 4241] Remove Field items from the ucm base table
				$field_condition = array( $db->quoteName('ucm_type_id') . ' = ' . $field_id);
				// [Interpretation 4243] Create a new query object.
				$query = $db->getQuery(true);
				$query->delete($db->quoteName('#__ucm_base'));
				$query->where($field_condition);
				$db->setQuery($query);
				// [Interpretation 4248] Execute the query to remove Field items
				$db->execute();

				// [Interpretation 4252] Remove Field items from the ucm history table
				$field_condition = array( $db->quoteName('ucm_type_id') . ' = ' . $field_id);
				// [Interpretation 4254] Create a new query object.
				$query = $db->getQuery(true);
				$query->delete($db->quoteName('#__ucm_history'));
				$query->where($field_condition);
				$db->setQuery($query);
				// [Interpretation 4259] Execute the query to remove Field items
				$db->execute();
			}
		}

		// [Interpretation 4167] Create a new query object.
		$query = $db->getQuery(true);
		// [Interpretation 4169] Select id from content type table
		$query->select($db->quoteName('type_id'));
		$query->from($db->quoteName('#__content_types'));
		// [Interpretation 4172] Where Query_field alias is found
		$query->where( $db->quoteName('type_alias') . ' = '. $db->quote('com_ipals.query_field') );
		$db->setQuery($query);
		// [Interpretation 4175] Execute query to see if alias is found
		$db->execute();
		$query_field_found = $db->getNumRows();
		// [Interpretation 4178] Now check if there were any rows
		if ($query_field_found)
		{
			// [Interpretation 4181] Since there are load the needed  query_field type ids
			$query_field_ids = $db->loadColumn();
			// [Interpretation 4185] Remove Query_field from the content type table
			$query_field_condition = array( $db->quoteName('type_alias') . ' = '. $db->quote('com_ipals.query_field') );
			// [Interpretation 4187] Create a new query object.
			$query = $db->getQuery(true);
			$query->delete($db->quoteName('#__content_types'));
			$query->where($query_field_condition);
			$db->setQuery($query);
			// [Interpretation 4192] Execute the query to remove Query_field items
			$query_field_done = $db->execute();
			if ($query_field_done);
			{
				// [Interpretation 4196] If succesfully remove Query_field add queued success message.
				$app->enqueueMessage(JText::_('The (com_ipals.query_field) type alias was removed from the <b>#__content_type</b> table'));
			}

			// [Interpretation 4202] Remove Query_field items from the contentitem tag map table
			$query_field_condition = array( $db->quoteName('type_alias') . ' = '. $db->quote('com_ipals.query_field') );
			// [Interpretation 4204] Create a new query object.
			$query = $db->getQuery(true);
			$query->delete($db->quoteName('#__contentitem_tag_map'));
			$query->where($query_field_condition);
			$db->setQuery($query);
			// [Interpretation 4209] Execute the query to remove Query_field items
			$query_field_done = $db->execute();
			if ($query_field_done);
			{
				// [Interpretation 4213] If succesfully remove Query_field add queued success message.
				$app->enqueueMessage(JText::_('The (com_ipals.query_field) type alias was removed from the <b>#__contentitem_tag_map</b> table'));
			}

			// [Interpretation 4219] Remove Query_field items from the ucm content table
			$query_field_condition = array( $db->quoteName('core_type_alias') . ' = ' . $db->quote('com_ipals.query_field') );
			// [Interpretation 4221] Create a new query object.
			$query = $db->getQuery(true);
			$query->delete($db->quoteName('#__ucm_content'));
			$query->where($query_field_condition);
			$db->setQuery($query);
			// [Interpretation 4226] Execute the query to remove Query_field items
			$query_field_done = $db->execute();
			if ($query_field_done);
			{
				// [Interpretation 4230] If succesfully remove Query_field add queued success message.
				$app->enqueueMessage(JText::_('The (com_ipals.query_field) type alias was removed from the <b>#__ucm_content</b> table'));
			}

			// [Interpretation 4236] Make sure that all the Query_field items are cleared from DB
			foreach ($query_field_ids as $query_field_id)
			{
				// [Interpretation 4241] Remove Query_field items from the ucm base table
				$query_field_condition = array( $db->quoteName('ucm_type_id') . ' = ' . $query_field_id);
				// [Interpretation 4243] Create a new query object.
				$query = $db->getQuery(true);
				$query->delete($db->quoteName('#__ucm_base'));
				$query->where($query_field_condition);
				$db->setQuery($query);
				// [Interpretation 4248] Execute the query to remove Query_field items
				$db->execute();

				// [Interpretation 4252] Remove Query_field items from the ucm history table
				$query_field_condition = array( $db->quoteName('ucm_type_id') . ' = ' . $query_field_id);
				// [Interpretation 4254] Create a new query object.
				$query = $db->getQuery(true);
				$query->delete($db->quoteName('#__ucm_history'));
				$query->where($query_field_condition);
				$db->setQuery($query);
				// [Interpretation 4259] Execute the query to remove Query_field items
				$db->execute();
			}
		}

		// [Interpretation 4267] If All related items was removed queued success message.
		$app->enqueueMessage(JText::_('All related items was removed from the <b>#__ucm_base</b> table'));
		$app->enqueueMessage(JText::_('All related items was removed from the <b>#__ucm_history</b> table'));

		// [Interpretation 4272] Remove ipals assets from the assets table
		$ipals_condition = array( $db->quoteName('name') . ' LIKE ' . $db->quote('com_ipals%') );

		// [Interpretation 4274] Create a new query object.
		$query = $db->getQuery(true);
		$query->delete($db->quoteName('#__assets'));
		$query->where($ipals_condition);
		$db->setQuery($query);
		$query_field_done = $db->execute();
		if ($query_field_done);
		{
			// [Interpretation 4282] If succesfully remove ipals add queued success message.
			$app->enqueueMessage(JText::_('All related items was removed from the <b>#__assets</b> table'));
		}

		// little notice as after service, in case of bad experience with component.
		echo '<h2>Did something go wrong? Are you disappointed?</h2>
		<p>Please let me know at <a href="mailto:mwceo@mwweb.host">mwceo@mwweb.host</a>.
		<br />We at Most Wanted Web Services, Inc. are committed to building extensions that performs proficiently! You can help us, really!
		<br />Send me your thoughts on improvements that is needed, trust me, I will be very grateful!
		<br />Visit us at <a href="http://retsforealestate.com" target="_blank">http://retsforealestate.com</a> today!</p>';
	}

	/**
	 * method to update the component
	 *
	 * @return void
	 */
	function update($parent)
	{
		
	}

	/**
	 * method to run before an install/update/uninstall method
	 *
	 * @return void
	 */
	function preflight($type, $parent)
	{
		// get application
		$app = JFactory::getApplication();
		// is redundant ...hmmm
		if ($type == 'uninstall')
		{
			return true;
		}
		// the default for both install and update
		$jversion = new JVersion();
		if (!$jversion->isCompatible('3.6.0'))
		{
			$app->enqueueMessage('Please upgrade to at least Joomla! 3.6.0 before continuing!', 'error');
			return false;
		}
		// do any updates needed
		if ($type == 'update')
		{
		}
		// do any install needed
		if ($type == 'install')
		{
		}
	}

	/**
	 * method to run after an install/update/uninstall method
	 *
	 * @return void
	 */
	function postflight($type, $parent)
	{
		// set the default component settings
		if ($type == 'install')
		{

			// [Interpretation 4006] Get The Database object
			$db = JFactory::getDbo();

			// [Interpretation 4013] Create the setting content type object.
			$setting = new stdClass();
			$setting->type_title = 'Ipals Setting';
			$setting->type_alias = 'com_ipals.setting';
			$setting->table = '{"special": {"dbtable": "#__ipals_setting","key": "id","type": "Setting","prefix": "ipalsTable","config": "array()"},"common": {"dbtable": "#__ucm_content","key": "ucm_id","type": "Corecontent","prefix": "JTable","config": "array()"}}';
			$setting->field_mappings = '{"common": {"core_content_item_id": "id","core_title": "name","core_state": "published","core_alias": "alias","core_created_time": "created","core_modified_time": "modified","core_body": "null","core_hits": "hits","core_publish_up": "null","core_publish_down": "null","core_access": "access","core_params": "params","core_featured": "null","core_metadata": "metadata","core_language": "null","core_images": "null","core_urls": "null","core_version": "version","core_ordering": "ordering","core_metakey": "metakey","core_metadesc": "metadesc","core_catid": "null","core_xreference": "null","asset_id": "asset_id"},"special": {"retsserver":"retsserver","name":"name","retsusername":"retsusername","retslogin":"retslogin","retspassword":"retspassword","defaultagent":"defaultagent","rets_version":"rets_version","thumbpostfix":"thumbpostfix","retsheaderversion":"retsheaderversion","retsuseragent":"retsuseragent","cleanupkey":"cleanupkey","processgeocode":"processgeocode","fullheight":"fullheight","ohcommercial":"ohcommercial","quickphoto":"quickphoto","alias":"alias","openhouseresource":"openhouseresource","agentmidtable":"agentmidtable","ohcustomthree":"ohcustomthree","agenttable":"agenttable","miscretsid":"miscretsid","getphotos":"getphotos","imgkey":"imgkey","officetable":"officetable","imagetable":"imagetable","thumbheight":"thumbheight","propertymidtable":"propertymidtable","thumbpath":"thumbpath","propertytable":"propertytable","amazonbucket":"amazonbucket","settingstable":"settingstable","siteroot":"siteroot","openhousetable":"openhousetable","agentgeocodeaddress":"agentgeocodeaddress","amenitiestable":"amenitiestable","ohresidential":"ohresidential","dbrefreshrate":"dbrefreshrate","ohcustomone":"ohcustomone","batchlimit":"batchlimit","ohcustomfive":"ohcustomfive","processlength":"processlength","lastrun":"lastrun","processlengthtype":"processlengthtype","cleanupresource":"cleanupresource","limit":"limit","extkey":"extkey","serverbreather":"serverbreather","mediaresource":"mediaresource","retsdebugmode":"retsdebugmode","retsphotoobjectname":"retsphotoobjectname","updateonduplicatekey":"updateonduplicatekey","myradiovalue":"myradiovalue","verboselevel":"verboselevel","thumbwidth":"thumbwidth","dateformat":"dateformat","fullwidth":"fullwidth","agentresource":"agentresource","thumbprefix":"thumbprefix","agentclass":"agentclass","amazonsthree":"amazonsthree","officeresource":"officeresource","amazonendpoint":"amazonendpoint","officeclass":"officeclass","imagepath":"imagepath","statusaccept":"statusaccept","temppath":"temppath","geocodeaddress":"geocodeaddress","statusdelete":"statusdelete","progeocode":"progeocode","statusdeletevalues":"statusdeletevalues","agentid":"agentid","ohland":"ohland","propertyresource":"propertyresource","ohmultifamily":"ohmultifamily","ohcustomtwo":"ohcustomtwo","residential":"residential","ohcustomfour":"ohcustomfour","land":"land","ohcustomsix":"ohcustomsix","commercial":"commercial","defaultoffice":"defaultoffice","multifamily":"multifamily","plugin":"plugin","customone":"customone","cleanupscript":"cleanupscript","customtwo":"customtwo","cleanupclasses":"cleanupclasses","customthree":"customthree","cleanupretsquery":"cleanupretsquery","customfour":"customfour","extphotos":"extphotos","customfive":"customfive","imgurl":"imgurl","customsix":"customsix","mediaclass":"mediaclass"}}';
			$setting->router = 'IpalsHelperRoute::getSettingRoute';
			$setting->content_history_options = '{"formFile": "administrator/components/com_ipals/models/forms/setting.xml","hideFields": ["asset_id","checked_out","checked_out_time","version"],"ignoreChanges": ["modified_by","modified","checked_out","checked_out_time","version","hits"],"convertToInt": ["published","ordering","processgeocode","fullheight","quickphoto","getphotos","thumbheight","amazonbucket","retsdebugmode","updateonduplicatekey","myradiovalue","verboselevel","thumbwidth","fullwidth","amazonsthree","amazonendpoint","progeocode","plugin","cleanupscript"],"displayLookup": [{"sourceColumn": "created_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "access","targetTable": "#__viewlevels","targetColumn": "id","displayColumn": "title"},{"sourceColumn": "modified_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"}]}';

			// [Interpretation 4031] Set the object into the content types table.
			$setting_Inserted = $db->insertObject('#__content_types', $setting);

			// [Interpretation 4013] Create the field content type object.
			$field = new stdClass();
			$field->type_title = 'Ipals Field';
			$field->type_alias = 'com_ipals.field';
			$field->table = '{"special": {"dbtable": "#__ipals_field","key": "id","type": "Field","prefix": "ipalsTable","config": "array()"},"common": {"dbtable": "#__ucm_content","key": "ucm_id","type": "Corecontent","prefix": "JTable","config": "array()"}}';
			$field->field_mappings = '{"common": {"core_content_item_id": "id","core_title": "field","core_state": "published","core_alias": "null","core_created_time": "created","core_modified_time": "modified","core_body": "null","core_hits": "hits","core_publish_up": "null","core_publish_down": "null","core_access": "access","core_params": "params","core_featured": "null","core_metadata": "metadata","core_language": "null","core_images": "null","core_urls": "null","core_version": "version","core_ordering": "ordering","core_metakey": "metakey","core_metadesc": "metadesc","core_catid": "null","core_xreference": "null","asset_id": "asset_id"},"special": {"field":"field","retskey":"retskey","actionfields":"actionfields","fieldstype":"fieldstype","settingsid":"settingsid","description":"description"}}';
			$field->router = 'IpalsHelperRoute::getFieldRoute';
			$field->content_history_options = '{"formFile": "administrator/components/com_ipals/models/forms/field.xml","hideFields": ["asset_id","checked_out","checked_out_time","version"],"ignoreChanges": ["modified_by","modified","checked_out","checked_out_time","version","hits"],"convertToInt": ["published","ordering","settingsid"],"displayLookup": [{"sourceColumn": "created_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "access","targetTable": "#__viewlevels","targetColumn": "id","displayColumn": "title"},{"sourceColumn": "modified_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "settingsid","targetTable": "#__ipals_setting","targetColumn": "id","displayColumn": "name"}]}';

			// [Interpretation 4031] Set the object into the content types table.
			$field_Inserted = $db->insertObject('#__content_types', $field);

			// [Interpretation 4013] Create the query_field content type object.
			$query_field = new stdClass();
			$query_field->type_title = 'Ipals Query_field';
			$query_field->type_alias = 'com_ipals.query_field';
			$query_field->table = '{"special": {"dbtable": "#__ipals_query_field","key": "id","type": "Query_field","prefix": "ipalsTable","config": "array()"},"common": {"dbtable": "#__ucm_content","key": "ucm_id","type": "Corecontent","prefix": "JTable","config": "array()"}}';
			$query_field->field_mappings = '{"common": {"core_content_item_id": "id","core_title": "retskey","core_state": "published","core_alias": "null","core_created_time": "created","core_modified_time": "modified","core_body": "null","core_hits": "hits","core_publish_up": "null","core_publish_down": "null","core_access": "access","core_params": "params","core_featured": "null","core_metadata": "metadata","core_language": "null","core_images": "null","core_urls": "null","core_version": "version","core_ordering": "ordering","core_metakey": "metakey","core_metadesc": "metadesc","core_catid": "null","core_xreference": "null","asset_id": "asset_id"},"special": {"retskey":"retskey","value":"value","querytype":"querytype","settingsid":"settingsid","auto":"auto","group":"group","description":"description"}}';
			$query_field->router = 'IpalsHelperRoute::getQuery_fieldRoute';
			$query_field->content_history_options = '{"formFile": "administrator/components/com_ipals/models/forms/query_field.xml","hideFields": ["asset_id","checked_out","checked_out_time","version"],"ignoreChanges": ["modified_by","modified","checked_out","checked_out_time","version","hits"],"convertToInt": ["published","ordering","settingsid","auto"],"displayLookup": [{"sourceColumn": "created_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "access","targetTable": "#__viewlevels","targetColumn": "id","displayColumn": "title"},{"sourceColumn": "modified_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "settingsid","targetTable": "#__ipals_setting","targetColumn": "id","displayColumn": "name"}]}';

			// [Interpretation 4031] Set the object into the content types table.
			$query_field_Inserted = $db->insertObject('#__content_types', $query_field);


			// [Interpretation 4093] Install the global extenstion params.
			$query = $db->getQuery(true);
			// [Interpretation 4101] Field to update.
			$fields = array(
				$db->quoteName('params') . ' = ' . $db->quote('{"autorName":"Most Wanted Web Services, Inc.","autorEmail":"mwceo@mwweb.host","settings_request_id":"0","check_in":"-1 day","save_history":"1","history_limit":"10","uikit_load":"1","uikit_min":"","uikit_style":""}'),
			);
			// [Interpretation 4105] Condition.
			$conditions = array(
				$db->quoteName('element') . ' = ' . $db->quote('com_ipals')
			);
			$query->update($db->quoteName('#__extensions'))->set($fields)->where($conditions);
			$db->setQuery($query);
			$allDone = $db->execute();

			echo '<a target="_blank" href="http://retsforealestate.com" title="iPALS">
				<img src="components/com_ipals/assets/images/vdm-component.png"/>
				</a>';
		}
		// do any updates needed
		if ($type == 'update')
		{

			// [Interpretation 4006] Get The Database object
			$db = JFactory::getDbo();

			// [Interpretation 4013] Create the setting content type object.
			$setting = new stdClass();
			$setting->type_title = 'Ipals Setting';
			$setting->type_alias = 'com_ipals.setting';
			$setting->table = '{"special": {"dbtable": "#__ipals_setting","key": "id","type": "Setting","prefix": "ipalsTable","config": "array()"},"common": {"dbtable": "#__ucm_content","key": "ucm_id","type": "Corecontent","prefix": "JTable","config": "array()"}}';
			$setting->field_mappings = '{"common": {"core_content_item_id": "id","core_title": "name","core_state": "published","core_alias": "alias","core_created_time": "created","core_modified_time": "modified","core_body": "null","core_hits": "hits","core_publish_up": "null","core_publish_down": "null","core_access": "access","core_params": "params","core_featured": "null","core_metadata": "metadata","core_language": "null","core_images": "null","core_urls": "null","core_version": "version","core_ordering": "ordering","core_metakey": "metakey","core_metadesc": "metadesc","core_catid": "null","core_xreference": "null","asset_id": "asset_id"},"special": {"retsserver":"retsserver","name":"name","retsusername":"retsusername","retslogin":"retslogin","retspassword":"retspassword","defaultagent":"defaultagent","rets_version":"rets_version","thumbpostfix":"thumbpostfix","retsheaderversion":"retsheaderversion","retsuseragent":"retsuseragent","cleanupkey":"cleanupkey","processgeocode":"processgeocode","fullheight":"fullheight","ohcommercial":"ohcommercial","quickphoto":"quickphoto","alias":"alias","openhouseresource":"openhouseresource","agentmidtable":"agentmidtable","ohcustomthree":"ohcustomthree","agenttable":"agenttable","miscretsid":"miscretsid","getphotos":"getphotos","imgkey":"imgkey","officetable":"officetable","imagetable":"imagetable","thumbheight":"thumbheight","propertymidtable":"propertymidtable","thumbpath":"thumbpath","propertytable":"propertytable","amazonbucket":"amazonbucket","settingstable":"settingstable","siteroot":"siteroot","openhousetable":"openhousetable","agentgeocodeaddress":"agentgeocodeaddress","amenitiestable":"amenitiestable","ohresidential":"ohresidential","dbrefreshrate":"dbrefreshrate","ohcustomone":"ohcustomone","batchlimit":"batchlimit","ohcustomfive":"ohcustomfive","processlength":"processlength","lastrun":"lastrun","processlengthtype":"processlengthtype","cleanupresource":"cleanupresource","limit":"limit","extkey":"extkey","serverbreather":"serverbreather","mediaresource":"mediaresource","retsdebugmode":"retsdebugmode","retsphotoobjectname":"retsphotoobjectname","updateonduplicatekey":"updateonduplicatekey","myradiovalue":"myradiovalue","verboselevel":"verboselevel","thumbwidth":"thumbwidth","dateformat":"dateformat","fullwidth":"fullwidth","agentresource":"agentresource","thumbprefix":"thumbprefix","agentclass":"agentclass","amazonsthree":"amazonsthree","officeresource":"officeresource","amazonendpoint":"amazonendpoint","officeclass":"officeclass","imagepath":"imagepath","statusaccept":"statusaccept","temppath":"temppath","geocodeaddress":"geocodeaddress","statusdelete":"statusdelete","progeocode":"progeocode","statusdeletevalues":"statusdeletevalues","agentid":"agentid","ohland":"ohland","propertyresource":"propertyresource","ohmultifamily":"ohmultifamily","ohcustomtwo":"ohcustomtwo","residential":"residential","ohcustomfour":"ohcustomfour","land":"land","ohcustomsix":"ohcustomsix","commercial":"commercial","defaultoffice":"defaultoffice","multifamily":"multifamily","plugin":"plugin","customone":"customone","cleanupscript":"cleanupscript","customtwo":"customtwo","cleanupclasses":"cleanupclasses","customthree":"customthree","cleanupretsquery":"cleanupretsquery","customfour":"customfour","extphotos":"extphotos","customfive":"customfive","imgurl":"imgurl","customsix":"customsix","mediaclass":"mediaclass"}}';
			$setting->router = 'IpalsHelperRoute::getSettingRoute';
			$setting->content_history_options = '{"formFile": "administrator/components/com_ipals/models/forms/setting.xml","hideFields": ["asset_id","checked_out","checked_out_time","version"],"ignoreChanges": ["modified_by","modified","checked_out","checked_out_time","version","hits"],"convertToInt": ["published","ordering","processgeocode","fullheight","quickphoto","getphotos","thumbheight","amazonbucket","retsdebugmode","updateonduplicatekey","myradiovalue","verboselevel","thumbwidth","fullwidth","amazonsthree","amazonendpoint","progeocode","plugin","cleanupscript"],"displayLookup": [{"sourceColumn": "created_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "access","targetTable": "#__viewlevels","targetColumn": "id","displayColumn": "title"},{"sourceColumn": "modified_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"}]}';

			// [Interpretation 4022] Check if setting type is already in content_type DB.
			$setting_id = null;
			$query = $db->getQuery(true);
			$query->select($db->quoteName(array('type_id')));
			$query->from($db->quoteName('#__content_types'));
			$query->where($db->quoteName('type_alias') . ' LIKE '. $db->quote($setting->type_alias));
			$db->setQuery($query);
			$db->execute();

			// [Interpretation 4031] Set the object into the content types table.
			if ($db->getNumRows())
			{
				$setting->type_id = $db->loadResult();
				$setting_Updated = $db->updateObject('#__content_types', $setting, 'type_id');
			}
			else
			{
				$setting_Inserted = $db->insertObject('#__content_types', $setting);
			}

			// [Interpretation 4013] Create the field content type object.
			$field = new stdClass();
			$field->type_title = 'Ipals Field';
			$field->type_alias = 'com_ipals.field';
			$field->table = '{"special": {"dbtable": "#__ipals_field","key": "id","type": "Field","prefix": "ipalsTable","config": "array()"},"common": {"dbtable": "#__ucm_content","key": "ucm_id","type": "Corecontent","prefix": "JTable","config": "array()"}}';
			$field->field_mappings = '{"common": {"core_content_item_id": "id","core_title": "field","core_state": "published","core_alias": "null","core_created_time": "created","core_modified_time": "modified","core_body": "null","core_hits": "hits","core_publish_up": "null","core_publish_down": "null","core_access": "access","core_params": "params","core_featured": "null","core_metadata": "metadata","core_language": "null","core_images": "null","core_urls": "null","core_version": "version","core_ordering": "ordering","core_metakey": "metakey","core_metadesc": "metadesc","core_catid": "null","core_xreference": "null","asset_id": "asset_id"},"special": {"field":"field","retskey":"retskey","actionfields":"actionfields","fieldstype":"fieldstype","settingsid":"settingsid","description":"description"}}';
			$field->router = 'IpalsHelperRoute::getFieldRoute';
			$field->content_history_options = '{"formFile": "administrator/components/com_ipals/models/forms/field.xml","hideFields": ["asset_id","checked_out","checked_out_time","version"],"ignoreChanges": ["modified_by","modified","checked_out","checked_out_time","version","hits"],"convertToInt": ["published","ordering","settingsid"],"displayLookup": [{"sourceColumn": "created_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "access","targetTable": "#__viewlevels","targetColumn": "id","displayColumn": "title"},{"sourceColumn": "modified_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "settingsid","targetTable": "#__ipals_setting","targetColumn": "id","displayColumn": "name"}]}';

			// [Interpretation 4022] Check if field type is already in content_type DB.
			$field_id = null;
			$query = $db->getQuery(true);
			$query->select($db->quoteName(array('type_id')));
			$query->from($db->quoteName('#__content_types'));
			$query->where($db->quoteName('type_alias') . ' LIKE '. $db->quote($field->type_alias));
			$db->setQuery($query);
			$db->execute();

			// [Interpretation 4031] Set the object into the content types table.
			if ($db->getNumRows())
			{
				$field->type_id = $db->loadResult();
				$field_Updated = $db->updateObject('#__content_types', $field, 'type_id');
			}
			else
			{
				$field_Inserted = $db->insertObject('#__content_types', $field);
			}

			// [Interpretation 4013] Create the query_field content type object.
			$query_field = new stdClass();
			$query_field->type_title = 'Ipals Query_field';
			$query_field->type_alias = 'com_ipals.query_field';
			$query_field->table = '{"special": {"dbtable": "#__ipals_query_field","key": "id","type": "Query_field","prefix": "ipalsTable","config": "array()"},"common": {"dbtable": "#__ucm_content","key": "ucm_id","type": "Corecontent","prefix": "JTable","config": "array()"}}';
			$query_field->field_mappings = '{"common": {"core_content_item_id": "id","core_title": "retskey","core_state": "published","core_alias": "null","core_created_time": "created","core_modified_time": "modified","core_body": "null","core_hits": "hits","core_publish_up": "null","core_publish_down": "null","core_access": "access","core_params": "params","core_featured": "null","core_metadata": "metadata","core_language": "null","core_images": "null","core_urls": "null","core_version": "version","core_ordering": "ordering","core_metakey": "metakey","core_metadesc": "metadesc","core_catid": "null","core_xreference": "null","asset_id": "asset_id"},"special": {"retskey":"retskey","value":"value","querytype":"querytype","settingsid":"settingsid","auto":"auto","group":"group","description":"description"}}';
			$query_field->router = 'IpalsHelperRoute::getQuery_fieldRoute';
			$query_field->content_history_options = '{"formFile": "administrator/components/com_ipals/models/forms/query_field.xml","hideFields": ["asset_id","checked_out","checked_out_time","version"],"ignoreChanges": ["modified_by","modified","checked_out","checked_out_time","version","hits"],"convertToInt": ["published","ordering","settingsid","auto"],"displayLookup": [{"sourceColumn": "created_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "access","targetTable": "#__viewlevels","targetColumn": "id","displayColumn": "title"},{"sourceColumn": "modified_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "settingsid","targetTable": "#__ipals_setting","targetColumn": "id","displayColumn": "name"}]}';

			// [Interpretation 4022] Check if query_field type is already in content_type DB.
			$query_field_id = null;
			$query = $db->getQuery(true);
			$query->select($db->quoteName(array('type_id')));
			$query->from($db->quoteName('#__content_types'));
			$query->where($db->quoteName('type_alias') . ' LIKE '. $db->quote($query_field->type_alias));
			$db->setQuery($query);
			$db->execute();

			// [Interpretation 4031] Set the object into the content types table.
			if ($db->getNumRows())
			{
				$query_field->type_id = $db->loadResult();
				$query_field_Updated = $db->updateObject('#__content_types', $query_field, 'type_id');
			}
			else
			{
				$query_field_Inserted = $db->insertObject('#__content_types', $query_field);
			}


			echo '<a target="_blank" href="http://retsforealestate.com" title="iPALS">
				<img src="components/com_ipals/assets/images/vdm-component.png"/>
				</a>
				<h3>Upgrade to Version 3.0.0 Was Successful! Let us know if anything is not working as expected.</h3>';
		}
	}
}
