<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		3.0.0
	@build			21st September, 2017
	@created		16th August, 2017
	@package		iPALS
	@subpackage		query_fields.php
	@author			Most Wanted Web Services, Inc. <http://retsforealestate.com>	
	@copyright		Copyright (C) 2015. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
  ____  _____  _____  __  __  __      __       ___  _____  __  __  ____  _____  _  _  ____  _  _  ____ 
 (_  _)(  _  )(  _  )(  \/  )(  )    /__\     / __)(  _  )(  \/  )(  _ \(  _  )( \( )( ___)( \( )(_  _)
.-_)(   )(_)(  )(_)(  )    (  )(__  /(__)\   ( (__  )(_)(  )    (  )___/ )(_)(  )  (  )__)  )  (   )(  
\____) (_____)(_____)(_/\/\_)(____)(__)(__)   \___)(_____)(_/\/\_)(__)  (_____)(_)\_)(____)(_)\_) (__) 

/------------------------------------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import the Joomla modellist library
jimport('joomla.application.component.modellist');

/**
 * Query_fields Model
 */
class IpalsModelQuery_fields extends JModelList
{
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
        {
			$config['filter_fields'] = array(
				'a.id','id',
				'a.published','published',
				'a.ordering','ordering',
				'a.created_by','created_by',
				'a.modified_by','modified_by',
				'a.retskey','retskey',
				'a.value','value',
				'a.querytype','querytype',
				'a.settingsid','settingsid'
			);
		}

		parent::__construct($config);
	}
	
	/**
	 * Method to auto-populate the model state.
	 *
	 * @return  void
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		$app = JFactory::getApplication();

		// Adjust the context to support modal layouts.
		if ($layout = $app->input->get('layout'))
		{
			$this->context .= '.' . $layout;
		}
		$retskey = $this->getUserStateFromRequest($this->context . '.filter.retskey', 'filter_retskey');
		$this->setState('filter.retskey', $retskey);

		$value = $this->getUserStateFromRequest($this->context . '.filter.value', 'filter_value');
		$this->setState('filter.value', $value);

		$querytype = $this->getUserStateFromRequest($this->context . '.filter.querytype', 'filter_querytype');
		$this->setState('filter.querytype', $querytype);

		$settingsid = $this->getUserStateFromRequest($this->context . '.filter.settingsid', 'filter_settingsid');
		$this->setState('filter.settingsid', $settingsid);
        
		$sorting = $this->getUserStateFromRequest($this->context . '.filter.sorting', 'filter_sorting', 0, 'int');
		$this->setState('filter.sorting', $sorting);
        
		$access = $this->getUserStateFromRequest($this->context . '.filter.access', 'filter_access', 0, 'int');
		$this->setState('filter.access', $access);
        
		$search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$published = $this->getUserStateFromRequest($this->context . '.filter.published', 'filter_published', '');
		$this->setState('filter.published', $published);
        
		$created_by = $this->getUserStateFromRequest($this->context . '.filter.created_by', 'filter_created_by', '');
		$this->setState('filter.created_by', $created_by);

		$created = $this->getUserStateFromRequest($this->context . '.filter.created', 'filter_created');
		$this->setState('filter.created', $created);

		// List state information.
		parent::populateState($ordering, $direction);
	}
	
	/**
	 * Method to get an array of data items.
	 *
	 * @return  mixed  An array of data items on success, false on failure.
	 */
	public function getItems()
	{ 
		// [Interpretation 11051] check in items
		$this->checkInNow();

		// load parent items
		$items = parent::getItems(); 

		// [Interpretation 11403] set selection value to a translatable value
		if (IpalsHelper::checkArray($items))
		{
			foreach ($items as $nr => &$item)
			{
				// [Interpretation 11410] convert querytype
				$item->querytype = $this->selectionTranslation($item->querytype, 'querytype');
			}
		}
 
        
		// return items
		return $items;
	}

	/**
	* Method to convert selection values to translatable string.
	*
	* @return translatable string
	*/
	public function selectionTranslation($value,$name)
	{
		// [Interpretation 11436] Array of querytype language strings
		if ($name === 'querytype')
		{
			$querytypeArray = array(
				0 => 'COM_IPALS_QUERY_FIELD_GLOBAL',
				1 => 'COM_IPALS_QUERY_FIELD_PROPERTY',
				2 => 'COM_IPALS_QUERY_FIELD_OFFICE',
				3 => 'COM_IPALS_QUERY_FIELD_AGENT'
			);
			// [Interpretation 11467] Now check if value is found in this array
			if (isset($querytypeArray[$value]) && IpalsHelper::checkString($querytypeArray[$value]))
			{
				return $querytypeArray[$value];
			}
		}
		return $value;
	}
	
	/**
	 * Method to build an SQL query to load the list data.
	 *
	 * @return	string	An SQL query
	 */
	protected function getListQuery()
	{
		// [Interpretation 7883] Get the user object.
		$user = JFactory::getUser();
		// [Interpretation 7885] Create a new query object.
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);

		// [Interpretation 7888] Select some fields
		$query->select('a.*');

		// [Interpretation 7895] From the ipals_item table
		$query->from($db->quoteName('#__ipals_query_field', 'a'));

		// [Interpretation 8039] From the ipals_setting table.
		$query->select($db->quoteName('g.name','settingsid_name'));
		$query->join('LEFT', $db->quoteName('#__ipals_setting', 'g') . ' ON (' . $db->quoteName('a.settingsid') . ' = ' . $db->quoteName('g.id') . ')');

		// [Interpretation 7906] Filter by published state
		$published = $this->getState('filter.published');
		if (is_numeric($published))
		{
			$query->where('a.published = ' . (int) $published);
		}
		elseif ($published === '')
		{
			$query->where('(a.published = 0 OR a.published = 1)');
		}

		// [Interpretation 7918] Join over the asset groups.
		$query->select('ag.title AS access_level');
		$query->join('LEFT', '#__viewlevels AS ag ON ag.id = a.access');
		// [Interpretation 7921] Filter by access level.
		if ($access = $this->getState('filter.access'))
		{
			$query->where('a.access = ' . (int) $access);
		}
		// [Interpretation 7926] Implement View Level Access
		if (!$user->authorise('core.options', 'com_ipals'))
		{
			$groups = implode(',', $user->getAuthorisedViewLevels());
			$query->where('a.access IN (' . $groups . ')');
		}
		// [Interpretation 8003] Filter by search.
		$search = $this->getState('filter.search');
		if (!empty($search))
		{
			if (stripos($search, 'id:') === 0)
			{
				$query->where('a.id = ' . (int) substr($search, 3));
			}
			else
			{
				$search = $db->quote('%' . $db->escape($search) . '%');
				$query->where('(a.retskey LIKE '.$search.' OR a.value LIKE '.$search.' OR a.description LIKE '.$search.')');
			}
		}

		// [Interpretation 8198] Filter by Querytype.
		if ($querytype = $this->getState('filter.querytype'))
		{
			$query->where('a.querytype = ' . $db->quote($db->escape($querytype)));
		}
		// [Interpretation 8189] Filter by settingsid.
		if ($settingsid = $this->getState('filter.settingsid'))
		{
			$query->where('a.settingsid = ' . $db->quote($db->escape($settingsid)));
		}

		// [Interpretation 7962] Add the list ordering clause.
		$orderCol = $this->state->get('list.ordering', 'a.id');
		$orderDirn = $this->state->get('list.direction', 'asc');	
		if ($orderCol != '')
		{
			$query->order($db->escape($orderCol . ' ' . $orderDirn));
		}

		return $query;
	}

	/**
	* Method to get list export data.
	*
	* @return mixed  An array of data items on success, false on failure.
	*/
	public function getExportData($pks)
	{
		// [Interpretation 7635] setup the query
		if (IpalsHelper::checkArray($pks))
		{
			// [Interpretation 7638] Set a value to know this is exporting method.
			$_export = true;
			// [Interpretation 7640] Get the user object.
			$user = JFactory::getUser();
			// [Interpretation 7642] Create a new query object.
			$db = JFactory::getDBO();
			$query = $db->getQuery(true);

			// [Interpretation 7645] Select some fields
			$query->select('a.*');

			// [Interpretation 7647] From the ipals_query_field table
			$query->from($db->quoteName('#__ipals_query_field', 'a'));
			$query->where('a.id IN (' . implode(',',$pks) . ')');
			// [Interpretation 7655] Implement View Level Access
			if (!$user->authorise('core.options', 'com_ipals'))
			{
				$groups = implode(',', $user->getAuthorisedViewLevels());
				$query->where('a.access IN (' . $groups . ')');
			}

			// [Interpretation 7662] Order the results by ordering
			$query->order('a.ordering  ASC');

			// [Interpretation 7664] Load the items
			$db->setQuery($query);
			$db->execute();
			if ($db->getNumRows())
			{
				$items = $db->loadObjectList();

				// [Interpretation 11341] set values to display correctly.
				if (IpalsHelper::checkArray($items))
				{
					foreach ($items as $nr => &$item)
					{
						// [Interpretation 11353] unset the values we don't want exported.
						unset($item->asset_id);
						unset($item->checked_out);
						unset($item->checked_out_time);
					}
				}
				// [Interpretation 11362] Add headers to items array.
				$headers = $this->getExImPortHeaders();
				if (IpalsHelper::checkObject($headers))
				{
					array_unshift($items,$headers);
				}
				return $items;
			}
		}
		return false;
	}

	/**
	* Method to get header.
	*
	* @return mixed  An array of data items on success, false on failure.
	*/
	public function getExImPortHeaders()
	{
		// [Interpretation 7686] Get a db connection.
		$db = JFactory::getDbo();
		// [Interpretation 7688] get the columns
		$columns = $db->getTableColumns("#__ipals_query_field");
		if (IpalsHelper::checkArray($columns))
		{
			// [Interpretation 7692] remove the headers you don't import/export.
			unset($columns['asset_id']);
			unset($columns['checked_out']);
			unset($columns['checked_out_time']);
			$headers = new stdClass();
			foreach ($columns as $column => $type)
			{
				$headers->{$column} = $column;
			}
			return $headers;
		}
		return false;
	} 
	
	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * @return  string  A store id.
	 *
	 */
	protected function getStoreId($id = '')
	{
		// [Interpretation 10653] Compile the store id.
		$id .= ':' . $this->getState('filter.id');
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.published');
		$id .= ':' . $this->getState('filter.ordering');
		$id .= ':' . $this->getState('filter.created_by');
		$id .= ':' . $this->getState('filter.modified_by');
		$id .= ':' . $this->getState('filter.retskey');
		$id .= ':' . $this->getState('filter.value');
		$id .= ':' . $this->getState('filter.querytype');
		$id .= ':' . $this->getState('filter.settingsid');

		return parent::getStoreId($id);
	}

	/**
	* Build an SQL query to checkin all items left checked out longer then a set time.
	*
	* @return  a bool
	*
	*/
	protected function checkInNow()
	{
		// [Interpretation 11067] Get set check in time
		$time = JComponentHelper::getParams('com_ipals')->get('check_in');
		
		if ($time)
		{

			// [Interpretation 11072] Get a db connection.
			$db = JFactory::getDbo();
			// [Interpretation 11074] reset query
			$query = $db->getQuery(true);
			$query->select('*');
			$query->from($db->quoteName('#__ipals_query_field'));
			$db->setQuery($query);
			$db->execute();
			if ($db->getNumRows())
			{
				// [Interpretation 11082] Get Yesterdays date
				$date = JFactory::getDate()->modify($time)->toSql();
				// [Interpretation 11084] reset query
				$query = $db->getQuery(true);

				// [Interpretation 11086] Fields to update.
				$fields = array(
					$db->quoteName('checked_out_time') . '=\'0000-00-00 00:00:00\'',
					$db->quoteName('checked_out') . '=0'
				);

				// [Interpretation 11091] Conditions for which records should be updated.
				$conditions = array(
					$db->quoteName('checked_out') . '!=0', 
					$db->quoteName('checked_out_time') . '<\''.$date.'\''
				);

				// [Interpretation 11096] Check table
				$query->update($db->quoteName('#__ipals_query_field'))->set($fields)->where($conditions); 

				$db->setQuery($query);

				$db->execute();
			}
		}

		return false;
	}
}
