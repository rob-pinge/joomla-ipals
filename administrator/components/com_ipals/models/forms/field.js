/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		3.0.0
	@build			21st September, 2017
	@created		16th August, 2017
	@package		iPALS
	@subpackage		field.js
	@author			Most Wanted Web Services, Inc. <http://retsforealestate.com>	
	@copyright		Copyright (C) 2015. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
  ____  _____  _____  __  __  __      __       ___  _____  __  __  ____  _____  _  _  ____  _  _  ____ 
 (_  _)(  _  )(  _  )(  \/  )(  )    /__\     / __)(  _  )(  \/  )(  _ \(  _  )( \( )( ___)( \( )(_  _)
.-_)(   )(_)(  )(_)(  )    (  )(__  /(__)\   ( (__  )(_)(  )    (  )___/ )(_)(  )  (  )__)  )  (   )(  
\____) (_____)(_____)(_/\/\_)(____)(__)(__)   \___)(_____)(_/\/\_)(__)  (_____)(_)\_)(____)(_)\_) (__) 

/------------------------------------------------------------------------------------------------------*/

// Initial Script
jQuery(document).ready(function()
{
	var actionfields_vvvvvvw = jQuery("#jform_actionfields").val();
	vvvvvvw(actionfields_vvvvvvw);

	var actionfields_vvvvvvx = jQuery("#jform_actionfields").val();
	vvvvvvx(actionfields_vvvvvvx);
});

// the vvvvvvw function
function vvvvvvw(actionfields_vvvvvvw)
{
	if (isSet(actionfields_vvvvvvw) && actionfields_vvvvvvw.constructor !== Array)
	{
		var temp_vvvvvvw = actionfields_vvvvvvw;
		var actionfields_vvvvvvw = [];
		actionfields_vvvvvvw.push(temp_vvvvvvw);
	}
	else if (!isSet(actionfields_vvvvvvw))
	{
		var actionfields_vvvvvvw = [];
	}
	var actionfields = actionfields_vvvvvvw.some(actionfields_vvvvvvw_SomeFunc);


	// [Interpretation 8421] set this function logic
	if (actionfields)
	{
	}
	else
	{
	}
}

// the vvvvvvw Some function
function actionfields_vvvvvvw_SomeFunc(actionfields_vvvvvvw)
{
	// [Interpretation 8408] set the function logic
	if (actionfields_vvvvvvw == 0 || actionfields_vvvvvvw == 1 || actionfields_vvvvvvw == 2 || actionfields_vvvvvvw == 3 || actionfields_vvvvvvw == 5 || actionfields_vvvvvvw == 6 || actionfields_vvvvvvw == 7 || actionfields_vvvvvvw == 8)
	{
		return true;
	}
	return false;
}

// the vvvvvvx function
function vvvvvvx(actionfields_vvvvvvx)
{
	if (isSet(actionfields_vvvvvvx) && actionfields_vvvvvvx.constructor !== Array)
	{
		var temp_vvvvvvx = actionfields_vvvvvvx;
		var actionfields_vvvvvvx = [];
		actionfields_vvvvvvx.push(temp_vvvvvvx);
	}
	else if (!isSet(actionfields_vvvvvvx))
	{
		var actionfields_vvvvvvx = [];
	}
	var actionfields = actionfields_vvvvvvx.some(actionfields_vvvvvvx_SomeFunc);


	// [Interpretation 8421] set this function logic
	if (actionfields)
	{
	}
	else
	{
	}
}

// the vvvvvvx Some function
function actionfields_vvvvvvx_SomeFunc(actionfields_vvvvvvx)
{
	// [Interpretation 8408] set the function logic
	if (actionfields_vvvvvvx == 0 || actionfields_vvvvvvx == 1 || actionfields_vvvvvvx == 2 || actionfields_vvvvvvx == 3 || actionfields_vvvvvvx == 4 || actionfields_vvvvvvx == 6 || actionfields_vvvvvvx == 7 || actionfields_vvvvvvx == 8)
	{
		return true;
	}
	return false;
}

// the isSet function
function isSet(val)
{
	if ((val != undefined) && (val != null) && 0 !== val.length){
		return true;
	}
	return false;
} 
