/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		3.0.0
	@build			21st September, 2017
	@created		16th August, 2017
	@package		iPALS
	@subpackage		setting.js
	@author			Most Wanted Web Services, Inc. <http://retsforealestate.com>	
	@copyright		Copyright (C) 2015. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
  ____  _____  _____  __  __  __      __       ___  _____  __  __  ____  _____  _  _  ____  _  _  ____ 
 (_  _)(  _  )(  _  )(  \/  )(  )    /__\     / __)(  _  )(  \/  )(  _ \(  _  )( \( )( ___)( \( )(_  _)
.-_)(   )(_)(  )(_)(  )    (  )(__  /(__)\   ( (__  )(_)(  )    (  )___/ )(_)(  )  (  )__)  )  (   )(  
\____) (_____)(_____)(_/\/\_)(____)(__)(__)   \___)(_____)(_/\/\_)(__)  (_____)(_)\_)(____)(_)\_) (__) 

/------------------------------------------------------------------------------------------------------*/

// Some Global Values
jform_vvvvvvvvvv_required = false;
jform_vvvvvvvvvw_required = false;

// Initial Script
jQuery(document).ready(function()
{
	var processgeocode_vvvvvvv = jQuery("#jform_processgeocode input[type='radio']:checked").val();
	vvvvvvv(processgeocode_vvvvvvv);
});

// the vvvvvvv function
function vvvvvvv(processgeocode_vvvvvvv)
{
	// [Interpretation 8443] set the function logic
	if (processgeocode_vvvvvvv == 1)
	{
		jQuery('#jform_agentgeocodeaddress').closest('.control-group').show();
		if (jform_vvvvvvvvvv_required)
		{
			updateFieldRequired('agentgeocodeaddress',0);
			jQuery('#jform_agentgeocodeaddress').prop('required','required');
			jQuery('#jform_agentgeocodeaddress').attr('aria-required',true);
			jQuery('#jform_agentgeocodeaddress').addClass('required');
			jform_vvvvvvvvvv_required = false;
		}

		jQuery('#jform_geocodeaddress').closest('.control-group').show();
		if (jform_vvvvvvvvvw_required)
		{
			updateFieldRequired('geocodeaddress',0);
			jQuery('#jform_geocodeaddress').prop('required','required');
			jQuery('#jform_geocodeaddress').attr('aria-required',true);
			jQuery('#jform_geocodeaddress').addClass('required');
			jform_vvvvvvvvvw_required = false;
		}

		jQuery('#jform_progeocode').closest('.control-group').show();
	}
	else
	{
		jQuery('#jform_agentgeocodeaddress').closest('.control-group').hide();
		if (!jform_vvvvvvvvvv_required)
		{
			updateFieldRequired('agentgeocodeaddress',1);
			jQuery('#jform_agentgeocodeaddress').removeAttr('required');
			jQuery('#jform_agentgeocodeaddress').removeAttr('aria-required');
			jQuery('#jform_agentgeocodeaddress').removeClass('required');
			jform_vvvvvvvvvv_required = true;
		}
		jQuery('#jform_geocodeaddress').closest('.control-group').hide();
		if (!jform_vvvvvvvvvw_required)
		{
			updateFieldRequired('geocodeaddress',1);
			jQuery('#jform_geocodeaddress').removeAttr('required');
			jQuery('#jform_geocodeaddress').removeAttr('aria-required');
			jQuery('#jform_geocodeaddress').removeClass('required');
			jform_vvvvvvvvvw_required = true;
		}
		jQuery('#jform_progeocode').closest('.control-group').hide();
	}
}

// update required fields
function updateFieldRequired(name,status)
{
	var not_required = jQuery('#jform_not_required').val();

	if(status == 1)
	{
		if (isSet(not_required) && not_required != 0)
		{
			not_required = not_required+','+name;
		}
		else
		{
			not_required = ','+name;
		}
	}
	else
	{
		if (isSet(not_required) && not_required != 0)
		{
			not_required = not_required.replace(','+name,'');
		}
	}

	jQuery('#jform_not_required').val(not_required);
}

// the isSet function
function isSet(val)
{
	if ((val != undefined) && (val != null) && 0 !== val.length){
		return true;
	}
	return false;
} 
