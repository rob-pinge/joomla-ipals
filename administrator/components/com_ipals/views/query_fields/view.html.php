<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		3.0.0
	@build			21st September, 2017
	@created		16th August, 2017
	@package		iPALS
	@subpackage		view.html.php
	@author			Most Wanted Web Services, Inc. <http://retsforealestate.com>	
	@copyright		Copyright (C) 2015. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
  ____  _____  _____  __  __  __      __       ___  _____  __  __  ____  _____  _  _  ____  _  _  ____ 
 (_  _)(  _  )(  _  )(  \/  )(  )    /__\     / __)(  _  )(  \/  )(  _ \(  _  )( \( )( ___)( \( )(_  _)
.-_)(   )(_)(  )(_)(  )    (  )(__  /(__)\   ( (__  )(_)(  )    (  )___/ )(_)(  )  (  )__)  )  (   )(  
\____) (_____)(_____)(_/\/\_)(____)(__)(__)   \___)(_____)(_/\/\_)(__)  (_____)(_)\_)(____)(_)\_) (__) 

/------------------------------------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla view library
jimport('joomla.application.component.view');

/**
 * Ipals View class for the Query_fields
 */
class IpalsViewQuery_fields extends JViewLegacy
{
	/**
	 * Query_fields view display method
	 * @return void
	 */
	function display($tpl = null)
	{
		if ($this->getLayout() !== 'modal')
		{
			// Include helper submenu
			IpalsHelper::addSubmenu('query_fields');
		}

		// Check for errors.
		if (count($errors = $this->get('Errors')))
                {
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}

		// Assign data to the view
		$this->items 		= $this->get('Items');
		$this->pagination 	= $this->get('Pagination');
		$this->state		= $this->get('State');
		$this->user 		= JFactory::getUser();
		$this->listOrder	= $this->escape($this->state->get('list.ordering'));
		$this->listDirn		= $this->escape($this->state->get('list.direction'));
		$this->saveOrder	= $this->listOrder == 'ordering';
                // get global action permissions
		$this->canDo		= IpalsHelper::getActions('query_field');
		$this->canEdit		= $this->canDo->get('core.edit');
		$this->canState		= $this->canDo->get('core.edit.state');
		$this->canCreate	= $this->canDo->get('core.create');
		$this->canDelete	= $this->canDo->get('core.delete');
		$this->canBatch	= $this->canDo->get('core.batch');

		// We don't need toolbar in the modal window.
		if ($this->getLayout() !== 'modal')
		{
			$this->addToolbar();
			$this->sidebar = JHtmlSidebar::render();
                        // load the batch html
                        if ($this->canCreate && $this->canEdit && $this->canState)
                        {
                                $this->batchDisplay = JHtmlBatch_::render();
                        }
		}

		// Display the template
		parent::display($tpl);

		// Set the document
		$this->setDocument();
	}

	/**
	 * Setting the toolbar
	 */
	protected function addToolBar()
	{
		JToolBarHelper::title(JText::_('COM_IPALS_QUERY_FIELDS'), 'joomla');
		JHtmlSidebar::setAction('index.php?option=com_ipals&view=query_fields');
                JFormHelper::addFieldPath(JPATH_COMPONENT . '/models/fields');

		if ($this->canCreate)
                {
			JToolBarHelper::addNew('query_field.add');
		}

                // Only load if there are items
                if (IpalsHelper::checkArray($this->items))
		{
                        if ($this->canEdit)
                        {
                            JToolBarHelper::editList('query_field.edit');
                        }

                        if ($this->canState)
                        {
                            JToolBarHelper::publishList('query_fields.publish');
                            JToolBarHelper::unpublishList('query_fields.unpublish');
                            JToolBarHelper::archiveList('query_fields.archive');

                            if ($this->canDo->get('core.admin'))
                            {
                                JToolBarHelper::checkin('query_fields.checkin');
                            }
                        }

                        // Add a batch button
                        if ($this->canBatch && $this->canCreate && $this->canEdit && $this->canState)
                        {
                                // Get the toolbar object instance
                                $bar = JToolBar::getInstance('toolbar');
                                // set the batch button name
                                $title = JText::_('JTOOLBAR_BATCH');
                                // Instantiate a new JLayoutFile instance and render the batch button
                                $layout = new JLayoutFile('joomla.toolbar.batch');
                                // add the button to the page
                                $dhtml = $layout->render(array('title' => $title));
                                $bar->appendButton('Custom', $dhtml, 'batch');
                        } 

                        if ($this->state->get('filter.published') == -2 && ($this->canState && $this->canDelete))
                        {
                            JToolbarHelper::deleteList('', 'query_fields.delete', 'JTOOLBAR_EMPTY_TRASH');
                        }
                        elseif ($this->canState && $this->canDelete)
                        {
                                JToolbarHelper::trash('query_fields.trash');
                        }

			if ($this->canDo->get('core.export') && $this->canDo->get('query_field.export'))
			{
				JToolBarHelper::custom('query_fields.exportData', 'download', '', 'COM_IPALS_EXPORT_DATA', true);
			}
                } 

		if ($this->canDo->get('core.import') && $this->canDo->get('query_field.import'))
		{
			JToolBarHelper::custom('query_fields.importData', 'upload', '', 'COM_IPALS_IMPORT_DATA', false);
		}

                // set help url for this view if found
                $help_url = IpalsHelper::getHelpUrl('query_fields');
                if (IpalsHelper::checkString($help_url))
                {
                        JToolbarHelper::help('COM_IPALS_HELP_MANAGER', false, $help_url);
                }

                // add the options comp button
                if ($this->canDo->get('core.admin') || $this->canDo->get('core.options'))
                {
                        JToolBarHelper::preferences('com_ipals');
                }

                if ($this->canState)
                {
			JHtmlSidebar::addFilter(
				JText::_('JOPTION_SELECT_PUBLISHED'),
				'filter_published',
				JHtml::_('select.options', JHtml::_('jgrid.publishedOptions'), 'value', 'text', $this->state->get('filter.published'), true)
			);
                        // only load if batch allowed
                        if ($this->canBatch)
                        {
                            JHtmlBatch_::addListSelection(
                                JText::_('COM_IPALS_KEEP_ORIGINAL_STATE'),
                                'batch[published]',
                                JHtml::_('select.options', JHtml::_('jgrid.publishedOptions', array('all' => false)), 'value', 'text', '', true)
                            );
                        }
		}

		JHtmlSidebar::addFilter(
			JText::_('JOPTION_SELECT_ACCESS'),
			'filter_access',
			JHtml::_('select.options', JHtml::_('access.assetgroups'), 'value', 'text', $this->state->get('filter.access'))
		);

		if ($this->canBatch && $this->canCreate && $this->canEdit)
		{
			JHtmlBatch_::addListSelection(
                                JText::_('COM_IPALS_KEEP_ORIGINAL_ACCESS'),
                                'batch[access]',
                                JHtml::_('select.options', JHtml::_('access.assetgroups'), 'value', 'text')
			);
                }  

		// [Interpretation 9591] Set Querytype Selection
		$this->querytypeOptions = $this->getTheQuerytypeSelections();
		if ($this->querytypeOptions)
		{
			// [Interpretation 9595] Querytype Filter
			JHtmlSidebar::addFilter(
				'- Select '.JText::_('COM_IPALS_QUERY_FIELD_QUERYTYPE_LABEL').' -',
				'filter_querytype',
				JHtml::_('select.options', $this->querytypeOptions, 'value', 'text', $this->state->get('filter.querytype'))
			);

			if ($this->canBatch && $this->canCreate && $this->canEdit)
			{
				// [Interpretation 9604] Querytype Batch Selection
				JHtmlBatch_::addListSelection(
					'- Keep Original '.JText::_('COM_IPALS_QUERY_FIELD_QUERYTYPE_LABEL').' -',
					'batch[querytype]',
					JHtml::_('select.options', $this->querytypeOptions, 'value', 'text')
				);
			}
		}

		// [Interpretation 9557] Set Settingsid Name Selection
		$this->settingsidNameOptions = JFormHelper::loadFieldType('Ipalssettings')->getOptions();
		if ($this->settingsidNameOptions)
		{
			// [Interpretation 9561] Settingsid Name Filter
			JHtmlSidebar::addFilter(
				'- Select '.JText::_('COM_IPALS_QUERY_FIELD_SETTINGSID_LABEL').' -',
				'filter_settingsid',
				JHtml::_('select.options', $this->settingsidNameOptions, 'value', 'text', $this->state->get('filter.settingsid'))
			);

			if ($this->canBatch && $this->canCreate && $this->canEdit)
			{
				// [Interpretation 9570] Settingsid Name Batch Selection
				JHtmlBatch_::addListSelection(
					'- Keep Original '.JText::_('COM_IPALS_QUERY_FIELD_SETTINGSID_LABEL').' -',
					'batch[settingsid]',
					JHtml::_('select.options', $this->settingsidNameOptions, 'value', 'text')
				);
			}
		}
	}

	/**
	 * Method to set up the document properties
	 *
	 * @return void
	 */
	protected function setDocument()
	{
		$document = JFactory::getDocument();
		$document->setTitle(JText::_('COM_IPALS_QUERY_FIELDS'));
		$document->addStyleSheet(JURI::root() . "administrator/components/com_ipals/assets/css/query_fields.css");
	}

        /**
	 * Escapes a value for output in a view script.
	 *
	 * @param   mixed  $var  The output to escape.
	 *
	 * @return  mixed  The escaped value.
	 */
	public function escape($var)
	{
		if(strlen($var) > 50)
		{
                        // use the helper htmlEscape method instead and shorten the string
			return IpalsHelper::htmlEscape($var, $this->_charset, true);
		}
                // use the helper htmlEscape method instead.
		return IpalsHelper::htmlEscape($var, $this->_charset);
	}

	/**
	 * Returns an array of fields the table can be sorted by
	 *
	 * @return  array  Array containing the field name to sort by as the key and display text as value
	 */
	protected function getSortFields()
	{
		return array(
			'a.sorting' => JText::_('JGRID_HEADING_ORDERING'),
			'a.published' => JText::_('JSTATUS'),
			'a.retskey' => JText::_('COM_IPALS_QUERY_FIELD_RETSKEY_LABEL'),
			'a.value' => JText::_('COM_IPALS_QUERY_FIELD_VALUE_LABEL'),
			'a.querytype' => JText::_('COM_IPALS_QUERY_FIELD_QUERYTYPE_LABEL'),
			'g.name' => JText::_('COM_IPALS_QUERY_FIELD_SETTINGSID_LABEL'),
			'a.id' => JText::_('JGRID_HEADING_ID')
		);
	} 

	protected function getTheQuerytypeSelections()
	{
		// [Interpretation 9462] Get a db connection.
		$db = JFactory::getDbo();

		// [Interpretation 9464] Create a new query object.
		$query = $db->getQuery(true);

		// [Interpretation 9466] Select the text.
		$query->select($db->quoteName('querytype'));
		$query->from($db->quoteName('#__ipals_query_field'));
		$query->order($db->quoteName('querytype') . ' ASC');

		// [Interpretation 9470] Reset the query using our newly populated query object.
		$db->setQuery($query);

		$results = $db->loadColumn();

		if ($results)
		{
			// [Interpretation 9478] get model
			$model = $this->getModel();
			$results = array_unique($results);
			$_filter = array();
			foreach ($results as $querytype)
			{
				// [Interpretation 9489] Translate the querytype selection
				$text = $model->selectionTranslation($querytype,'querytype');
				// [Interpretation 9491] Now add the querytype and its text to the options array
				$_filter[] = JHtml::_('select.option', $querytype, JText::_($text));
			}
			return $_filter;
		}
		return false;
	}
}
