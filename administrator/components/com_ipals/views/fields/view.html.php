<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		3.0.0
	@build			21st September, 2017
	@created		16th August, 2017
	@package		iPALS
	@subpackage		view.html.php
	@author			Most Wanted Web Services, Inc. <http://retsforealestate.com>	
	@copyright		Copyright (C) 2015. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
  ____  _____  _____  __  __  __      __       ___  _____  __  __  ____  _____  _  _  ____  _  _  ____ 
 (_  _)(  _  )(  _  )(  \/  )(  )    /__\     / __)(  _  )(  \/  )(  _ \(  _  )( \( )( ___)( \( )(_  _)
.-_)(   )(_)(  )(_)(  )    (  )(__  /(__)\   ( (__  )(_)(  )    (  )___/ )(_)(  )  (  )__)  )  (   )(  
\____) (_____)(_____)(_/\/\_)(____)(__)(__)   \___)(_____)(_/\/\_)(__)  (_____)(_)\_)(____)(_)\_) (__) 

/------------------------------------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla view library
jimport('joomla.application.component.view');

/**
 * Ipals View class for the Fields
 */
class IpalsViewFields extends JViewLegacy
{
	/**
	 * Fields view display method
	 * @return void
	 */
	function display($tpl = null)
	{
		if ($this->getLayout() !== 'modal')
		{
			// Include helper submenu
			IpalsHelper::addSubmenu('fields');
		}

		// Check for errors.
		if (count($errors = $this->get('Errors')))
                {
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}

		// Assign data to the view
		$this->items 		= $this->get('Items');
		$this->pagination 	= $this->get('Pagination');
		$this->state		= $this->get('State');
		$this->user 		= JFactory::getUser();
		$this->listOrder	= $this->escape($this->state->get('list.ordering'));
		$this->listDirn		= $this->escape($this->state->get('list.direction'));
		$this->saveOrder	= $this->listOrder == 'ordering';
                // get global action permissions
		$this->canDo		= IpalsHelper::getActions('field');
		$this->canEdit		= $this->canDo->get('core.edit');
		$this->canState		= $this->canDo->get('core.edit.state');
		$this->canCreate	= $this->canDo->get('core.create');
		$this->canDelete	= $this->canDo->get('core.delete');
		$this->canBatch	= $this->canDo->get('core.batch');

		// We don't need toolbar in the modal window.
		if ($this->getLayout() !== 'modal')
		{
			$this->addToolbar();
			$this->sidebar = JHtmlSidebar::render();
                        // load the batch html
                        if ($this->canCreate && $this->canEdit && $this->canState)
                        {
                                $this->batchDisplay = JHtmlBatch_::render();
                        }
		}

		// Display the template
		parent::display($tpl);

		// Set the document
		$this->setDocument();
	}

	/**
	 * Setting the toolbar
	 */
	protected function addToolBar()
	{
		JToolBarHelper::title(JText::_('COM_IPALS_FIELDS'), 'joomla');
		JHtmlSidebar::setAction('index.php?option=com_ipals&view=fields');
                JFormHelper::addFieldPath(JPATH_COMPONENT . '/models/fields');

		if ($this->canCreate)
                {
			JToolBarHelper::addNew('field.add');
		}

                // Only load if there are items
                if (IpalsHelper::checkArray($this->items))
		{
                        if ($this->canEdit)
                        {
                            JToolBarHelper::editList('field.edit');
                        }

                        if ($this->canState)
                        {
                            JToolBarHelper::publishList('fields.publish');
                            JToolBarHelper::unpublishList('fields.unpublish');
                            JToolBarHelper::archiveList('fields.archive');

                            if ($this->canDo->get('core.admin'))
                            {
                                JToolBarHelper::checkin('fields.checkin');
                            }
                        }

                        // Add a batch button
                        if ($this->canBatch && $this->canCreate && $this->canEdit && $this->canState)
                        {
                                // Get the toolbar object instance
                                $bar = JToolBar::getInstance('toolbar');
                                // set the batch button name
                                $title = JText::_('JTOOLBAR_BATCH');
                                // Instantiate a new JLayoutFile instance and render the batch button
                                $layout = new JLayoutFile('joomla.toolbar.batch');
                                // add the button to the page
                                $dhtml = $layout->render(array('title' => $title));
                                $bar->appendButton('Custom', $dhtml, 'batch');
                        } 

                        if ($this->state->get('filter.published') == -2 && ($this->canState && $this->canDelete))
                        {
                            JToolbarHelper::deleteList('', 'fields.delete', 'JTOOLBAR_EMPTY_TRASH');
                        }
                        elseif ($this->canState && $this->canDelete)
                        {
                                JToolbarHelper::trash('fields.trash');
                        }

			if ($this->canDo->get('core.export') && $this->canDo->get('field.export'))
			{
				JToolBarHelper::custom('fields.exportData', 'download', '', 'COM_IPALS_EXPORT_DATA', true);
			}
                } 

		if ($this->canDo->get('core.import') && $this->canDo->get('field.import'))
		{
			JToolBarHelper::custom('fields.importData', 'upload', '', 'COM_IPALS_IMPORT_DATA', false);
		}

                // set help url for this view if found
                $help_url = IpalsHelper::getHelpUrl('fields');
                if (IpalsHelper::checkString($help_url))
                {
                        JToolbarHelper::help('COM_IPALS_HELP_MANAGER', false, $help_url);
                }

                // add the options comp button
                if ($this->canDo->get('core.admin') || $this->canDo->get('core.options'))
                {
                        JToolBarHelper::preferences('com_ipals');
                }

                if ($this->canState)
                {
			JHtmlSidebar::addFilter(
				JText::_('JOPTION_SELECT_PUBLISHED'),
				'filter_published',
				JHtml::_('select.options', JHtml::_('jgrid.publishedOptions'), 'value', 'text', $this->state->get('filter.published'), true)
			);
                        // only load if batch allowed
                        if ($this->canBatch)
                        {
                            JHtmlBatch_::addListSelection(
                                JText::_('COM_IPALS_KEEP_ORIGINAL_STATE'),
                                'batch[published]',
                                JHtml::_('select.options', JHtml::_('jgrid.publishedOptions', array('all' => false)), 'value', 'text', '', true)
                            );
                        }
		}

		JHtmlSidebar::addFilter(
			JText::_('JOPTION_SELECT_ACCESS'),
			'filter_access',
			JHtml::_('select.options', JHtml::_('access.assetgroups'), 'value', 'text', $this->state->get('filter.access'))
		);

		if ($this->canBatch && $this->canCreate && $this->canEdit)
		{
			JHtmlBatch_::addListSelection(
                                JText::_('COM_IPALS_KEEP_ORIGINAL_ACCESS'),
                                'batch[access]',
                                JHtml::_('select.options', JHtml::_('access.assetgroups'), 'value', 'text')
			);
                }  

		// [Interpretation 9591] Set Actionfields Selection
		$this->actionfieldsOptions = $this->getTheActionfieldsSelections();
		if ($this->actionfieldsOptions)
		{
			// [Interpretation 9595] Actionfields Filter
			JHtmlSidebar::addFilter(
				'- Select '.JText::_('COM_IPALS_FIELD_ACTIONFIELDS_LABEL').' -',
				'filter_actionfields',
				JHtml::_('select.options', $this->actionfieldsOptions, 'value', 'text', $this->state->get('filter.actionfields'))
			);

			if ($this->canBatch && $this->canCreate && $this->canEdit)
			{
				// [Interpretation 9604] Actionfields Batch Selection
				JHtmlBatch_::addListSelection(
					'- Keep Original '.JText::_('COM_IPALS_FIELD_ACTIONFIELDS_LABEL').' -',
					'batch[actionfields]',
					JHtml::_('select.options', $this->actionfieldsOptions, 'value', 'text')
				);
			}
		}

		// [Interpretation 9591] Set Fieldstype Selection
		$this->fieldstypeOptions = $this->getTheFieldstypeSelections();
		if ($this->fieldstypeOptions)
		{
			// [Interpretation 9595] Fieldstype Filter
			JHtmlSidebar::addFilter(
				'- Select '.JText::_('COM_IPALS_FIELD_FIELDSTYPE_LABEL').' -',
				'filter_fieldstype',
				JHtml::_('select.options', $this->fieldstypeOptions, 'value', 'text', $this->state->get('filter.fieldstype'))
			);

			if ($this->canBatch && $this->canCreate && $this->canEdit)
			{
				// [Interpretation 9604] Fieldstype Batch Selection
				JHtmlBatch_::addListSelection(
					'- Keep Original '.JText::_('COM_IPALS_FIELD_FIELDSTYPE_LABEL').' -',
					'batch[fieldstype]',
					JHtml::_('select.options', $this->fieldstypeOptions, 'value', 'text')
				);
			}
		}
	}

	/**
	 * Method to set up the document properties
	 *
	 * @return void
	 */
	protected function setDocument()
	{
		$document = JFactory::getDocument();
		$document->setTitle(JText::_('COM_IPALS_FIELDS'));
		$document->addStyleSheet(JURI::root() . "administrator/components/com_ipals/assets/css/fields.css");
	}

        /**
	 * Escapes a value for output in a view script.
	 *
	 * @param   mixed  $var  The output to escape.
	 *
	 * @return  mixed  The escaped value.
	 */
	public function escape($var)
	{
		if(strlen($var) > 50)
		{
                        // use the helper htmlEscape method instead and shorten the string
			return IpalsHelper::htmlEscape($var, $this->_charset, true);
		}
                // use the helper htmlEscape method instead.
		return IpalsHelper::htmlEscape($var, $this->_charset);
	}

	/**
	 * Returns an array of fields the table can be sorted by
	 *
	 * @return  array  Array containing the field name to sort by as the key and display text as value
	 */
	protected function getSortFields()
	{
		return array(
			'a.sorting' => JText::_('JGRID_HEADING_ORDERING'),
			'a.published' => JText::_('JSTATUS'),
			'a.field' => JText::_('COM_IPALS_FIELD_FIELD_LABEL'),
			'a.retskey' => JText::_('COM_IPALS_FIELD_RETSKEY_LABEL'),
			'a.actionfields' => JText::_('COM_IPALS_FIELD_ACTIONFIELDS_LABEL'),
			'a.fieldstype' => JText::_('COM_IPALS_FIELD_FIELDSTYPE_LABEL'),
			'a.id' => JText::_('JGRID_HEADING_ID')
		);
	} 

	protected function getTheActionfieldsSelections()
	{
		// [Interpretation 9462] Get a db connection.
		$db = JFactory::getDbo();

		// [Interpretation 9464] Create a new query object.
		$query = $db->getQuery(true);

		// [Interpretation 9466] Select the text.
		$query->select($db->quoteName('actionfields'));
		$query->from($db->quoteName('#__ipals_field'));
		$query->order($db->quoteName('actionfields') . ' ASC');

		// [Interpretation 9470] Reset the query using our newly populated query object.
		$db->setQuery($query);

		$results = $db->loadColumn();

		if ($results)
		{
			// [Interpretation 9478] get model
			$model = $this->getModel();
			$results = array_unique($results);
			$_filter = array();
			foreach ($results as $actionfields)
			{
				// [Interpretation 9489] Translate the actionfields selection
				$text = $model->selectionTranslation($actionfields,'actionfields');
				// [Interpretation 9491] Now add the actionfields and its text to the options array
				$_filter[] = JHtml::_('select.option', $actionfields, JText::_($text));
			}
			return $_filter;
		}
		return false;
	}

	protected function getTheFieldstypeSelections()
	{
		// [Interpretation 9462] Get a db connection.
		$db = JFactory::getDbo();

		// [Interpretation 9464] Create a new query object.
		$query = $db->getQuery(true);

		// [Interpretation 9466] Select the text.
		$query->select($db->quoteName('fieldstype'));
		$query->from($db->quoteName('#__ipals_field'));
		$query->order($db->quoteName('fieldstype') . ' ASC');

		// [Interpretation 9470] Reset the query using our newly populated query object.
		$db->setQuery($query);

		$results = $db->loadColumn();

		if ($results)
		{
			// [Interpretation 9478] get model
			$model = $this->getModel();
			$results = array_unique($results);
			$_filter = array();
			foreach ($results as $fieldstype)
			{
				// [Interpretation 9489] Translate the fieldstype selection
				$text = $model->selectionTranslation($fieldstype,'fieldstype');
				// [Interpretation 9491] Now add the fieldstype and its text to the options array
				$_filter[] = JHtml::_('select.option', $fieldstype, JText::_($text));
			}
			return $_filter;
		}
		return false;
	}
}
