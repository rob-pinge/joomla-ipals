<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		3.0.0
	@build			21st September, 2017
	@created		16th August, 2017
	@package		iPALS
	@subpackage		edit.php
	@author			Most Wanted Web Services, Inc. <http://retsforealestate.com>	
	@copyright		Copyright (C) 2015. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
  ____  _____  _____  __  __  __      __       ___  _____  __  __  ____  _____  _  _  ____  _  _  ____ 
 (_  _)(  _  )(  _  )(  \/  )(  )    /__\     / __)(  _  )(  \/  )(  _ \(  _  )( \( )( ___)( \( )(_  _)
.-_)(   )(_)(  )(_)(  )    (  )(__  /(__)\   ( (__  )(_)(  )    (  )___/ )(_)(  )  (  )__)  )  (   )(  
\____) (_____)(_____)(_/\/\_)(____)(__)(__)   \___)(_____)(_/\/\_)(__)  (_____)(_)\_)(____)(_)\_) (__) 

/------------------------------------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');
$componentParams = JComponentHelper::getParams('com_ipals');
?>
<script type="text/javascript">
	// waiting spinner
	var outerDiv = jQuery('body');
	jQuery('<div id="loading"></div>')
		.css("background", "rgba(255, 255, 255, .8) url('components/com_ipals/assets/images/import.gif') 50% 15% no-repeat")
		.css("top", outerDiv.position().top - jQuery(window).scrollTop())
		.css("left", outerDiv.position().left - jQuery(window).scrollLeft())
		.css("width", outerDiv.width())
		.css("height", outerDiv.height())
		.css("position", "fixed")
		.css("opacity", "0.80")
		.css("-ms-filter", "progid:DXImageTransform.Microsoft.Alpha(Opacity = 80)")
		.css("filter", "alpha(opacity = 80)")
		.css("display", "none")
		.appendTo(outerDiv);
	jQuery('#loading').show();
	// when page is ready remove and show
	jQuery(window).load(function() {
		jQuery('#ipals_loader').fadeIn('fast');
		jQuery('#loading').hide();
	});
</script>
<div id="ipals_loader" style="display: none;">
<form action="<?php echo JRoute::_('index.php?option=com_ipals&layout=edit&id='.(int) $this->item->id.$this->referral); ?>" method="post" name="adminForm" id="adminForm" class="form-validate" enctype="multipart/form-data">

	<?php echo JLayoutHelper::render('setting.rets_login_above', $this); ?>
<div class="form-horizontal">

	<?php echo JHtml::_('bootstrap.startTabSet', 'settingTab', array('active' => 'rets_login')); ?>

	<?php echo JHtml::_('bootstrap.addTab', 'settingTab', 'rets_login', JText::_('COM_IPALS_SETTING_RETS_LOGIN', true)); ?>
		<div class="row-fluid form-horizontal-desktop">
			<div class="span6">
				<?php echo JLayoutHelper::render('setting.rets_login_left', $this); ?>
			</div>
			<div class="span6">
				<?php echo JLayoutHelper::render('setting.rets_login_right', $this); ?>
			</div>
		</div>
	<?php echo JHtml::_('bootstrap.endTab'); ?>

	<?php echo JHtml::_('bootstrap.addTab', 'settingTab', 'database', JText::_('COM_IPALS_SETTING_DATABASE', true)); ?>
		<div class="row-fluid form-horizontal-desktop">
			<div class="span6">
				<?php echo JLayoutHelper::render('setting.database_left', $this); ?>
			</div>
			<div class="span6">
				<?php echo JLayoutHelper::render('setting.database_right', $this); ?>
			</div>
		</div>
	<?php echo JHtml::_('bootstrap.endTab'); ?>

	<?php echo JHtml::_('bootstrap.addTab', 'settingTab', 'process_lengthdebug', JText::_('COM_IPALS_SETTING_PROCESS_LENGTHDEBUG', true)); ?>
		<div class="row-fluid form-horizontal-desktop">
			<div class="span6">
				<?php echo JLayoutHelper::render('setting.process_lengthdebug_left', $this); ?>
			</div>
			<div class="span6">
				<?php echo JLayoutHelper::render('setting.process_lengthdebug_right', $this); ?>
			</div>
		</div>
	<?php echo JHtml::_('bootstrap.endTab'); ?>

	<?php echo JHtml::_('bootstrap.addTab', 'settingTab', 'rets_query_data', JText::_('COM_IPALS_SETTING_RETS_QUERY_DATA', true)); ?>
		<div class="row-fluid form-horizontal-desktop">
			<div class="span6">
				<?php echo JLayoutHelper::render('setting.rets_query_data_left', $this); ?>
			</div>
			<div class="span6">
				<?php echo JLayoutHelper::render('setting.rets_query_data_right', $this); ?>
			</div>
		</div>
	<?php echo JHtml::_('bootstrap.endTab'); ?>

	<?php echo JHtml::_('bootstrap.addTab', 'settingTab', 'image', JText::_('COM_IPALS_SETTING_IMAGE', true)); ?>
		<div class="row-fluid form-horizontal-desktop">
			<div class="span12">
				<?php echo JLayoutHelper::render('setting.image_left', $this); ?>
			</div>
		</div>
	<?php echo JHtml::_('bootstrap.endTab'); ?>

	<?php echo JHtml::_('bootstrap.addTab', 'settingTab', 'geocode', JText::_('COM_IPALS_SETTING_GEOCODE', true)); ?>
		<div class="row-fluid form-horizontal-desktop">
			<div class="span12">
				<?php echo JLayoutHelper::render('setting.geocode_left', $this); ?>
			</div>
		</div>
	<?php echo JHtml::_('bootstrap.endTab'); ?>

	<?php echo JHtml::_('bootstrap.addTab', 'settingTab', 'open_houses', JText::_('COM_IPALS_SETTING_OPEN_HOUSES', true)); ?>
		<div class="row-fluid form-horizontal-desktop">
			<div class="span12">
				<?php echo JLayoutHelper::render('setting.open_houses_left', $this); ?>
			</div>
		</div>
	<?php echo JHtml::_('bootstrap.endTab'); ?>

	<?php echo JHtml::_('bootstrap.addTab', 'settingTab', 'misc', JText::_('COM_IPALS_SETTING_MISC', true)); ?>
		<div class="row-fluid form-horizontal-desktop">
			<div class="span12">
				<?php echo JLayoutHelper::render('setting.misc_left', $this); ?>
			</div>
		</div>
	<?php echo JHtml::_('bootstrap.endTab'); ?>

	<?php echo JHtml::_('bootstrap.addTab', 'settingTab', 'housekeeping', JText::_('COM_IPALS_SETTING_HOUSEKEEPING', true)); ?>
		<div class="row-fluid form-horizontal-desktop">
			<div class="span12">
				<?php echo JLayoutHelper::render('setting.housekeeping_left', $this); ?>
			</div>
		</div>
	<?php echo JHtml::_('bootstrap.endTab'); ?>

	<?php echo JHtml::_('bootstrap.addTab', 'settingTab', 'image_class', JText::_('COM_IPALS_SETTING_IMAGE_CLASS', true)); ?>
		<div class="row-fluid form-horizontal-desktop">
			<div class="span12">
				<?php echo JLayoutHelper::render('setting.image_class_left', $this); ?>
			</div>
		</div>
	<?php echo JHtml::_('bootstrap.endTab'); ?>

	<?php if ($this->canDo->get('core.delete') || $this->canDo->get('core.edit.created_by') || $this->canDo->get('core.edit.state') || $this->canDo->get('core.edit.created')) : ?>
	<?php echo JHtml::_('bootstrap.addTab', 'settingTab', 'publishing', JText::_('COM_IPALS_SETTING_PUBLISHING', true)); ?>
		<div class="row-fluid form-horizontal-desktop">
			<div class="span6">
				<?php echo JLayoutHelper::render('setting.publishing', $this); ?>
			</div>
			<div class="span6">
				<?php echo JLayoutHelper::render('setting.metadata', $this); ?>
			</div>
		</div>
	<?php echo JHtml::_('bootstrap.endTab'); ?>
	<?php endif; ?>

	<?php if ($this->canDo->get('core.admin')) : ?>
	<?php echo JHtml::_('bootstrap.addTab', 'settingTab', 'permissions', JText::_('COM_IPALS_SETTING_PERMISSION', true)); ?>
		<div class="row-fluid form-horizontal-desktop">
			<div class="span12">
				<fieldset class="adminform">
					<div class="adminformlist">
					<?php foreach ($this->form->getFieldset('accesscontrol') as $field): ?>
						<div>
							<?php echo $field->label; echo $field->input;?>
						</div>
						<div class="clearfix"></div>
					<?php endforeach; ?>
					</div>
				</fieldset>
			</div>
		</div>
	<?php echo JHtml::_('bootstrap.endTab'); ?>
	<?php endif; ?>

	<?php echo JHtml::_('bootstrap.endTabSet'); ?>

	<div>
		<input type="hidden" name="task" value="setting.edit" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
	</div>
</div>
</form>
</div>

<script type="text/javascript">

// #jform_processgeocode listeners for processgeocode_vvvvvvv function
jQuery('#jform_processgeocode').on('keyup',function()
{
	var processgeocode_vvvvvvv = jQuery("#jform_processgeocode input[type='radio']:checked").val();
	vvvvvvv(processgeocode_vvvvvvv);

});
jQuery('#adminForm').on('change', '#jform_processgeocode',function (e)
{
	e.preventDefault();
	var processgeocode_vvvvvvv = jQuery("#jform_processgeocode input[type='radio']:checked").val();
	vvvvvvv(processgeocode_vvvvvvv);

});



	//autofill empty alias field on save
	if( (jQuery('#jform_alias').val() == '' )){
		var alias;
		var street = jQuery('#jform_street').val() != ''
			? jQuery('#jform_street').val().replace(/\s+/g, '-').replace(/\.+/g,'').toLowerCase() + '-' : '';

		var city = jQuery('#jform_cityid').val() != ''
			? jQuery("#jform_cityid option:selected").text().toLowerCase() + '-' : '';

		var state = jQuery('#jform_stateid').val() != ''
			?  jQuery("#jform_stateid option:selected").text().toLowerCase() + '-': '';

		var postcode = jQuery('#jform_postcode').val() != ''
			?  jQuery('#jform_postcode').val() : '';

		alias = street + city + state + postcode;

        if (alias.charAt(name.length - 1) == '-') {
            alias = alias.substr(0, alias.length - 1);
        }

        jQuery('#jform_alias').val(alias);

	}

</script>
