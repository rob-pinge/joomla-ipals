<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		2.0.0
	@build			22nd September, 2017
	@created		1st May, 2016
	@package		Most Wanted Real Estate
	@subpackage		script.php
	@author			Most Wanted Web Services, Inc. <http://mostwantedrealestatesites.com>	
	@copyright		Copyright (C) 2015-2017. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
	
	Most Wanted Real Estate Component
	
/------------------------------------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

JHTML::_('behavior.modal');
jimport('joomla.installer.installer');
jimport('joomla.installer.helper');

/**
 * Script File of Mostwantedrealestate Component
 */
class com_mostwantedrealestateInstallerScript
{
	/**
	 * method to install the component
	 *
	 * @return void
	 */
	function install($parent)
	{

	}

	/**
	 * method to uninstall the component
	 *
	 * @return void
	 */
	function uninstall($parent)
	{
		// [Interpretation 4156] Get Application object
		$app = JFactory::getApplication();

		// [Interpretation 4158] Get The Database object
		$db = JFactory::getDbo();

		// [Interpretation 4167] Create a new query object.
		$query = $db->getQuery(true);
		// [Interpretation 4169] Select id from content type table
		$query->select($db->quoteName('type_id'));
		$query->from($db->quoteName('#__content_types'));
		// [Interpretation 4172] Where Country alias is found
		$query->where( $db->quoteName('type_alias') . ' = '. $db->quote('com_mostwantedrealestate.country') );
		$db->setQuery($query);
		// [Interpretation 4175] Execute query to see if alias is found
		$db->execute();
		$country_found = $db->getNumRows();
		// [Interpretation 4178] Now check if there were any rows
		if ($country_found)
		{
			// [Interpretation 4181] Since there are load the needed  country type ids
			$country_ids = $db->loadColumn();
			// [Interpretation 4185] Remove Country from the content type table
			$country_condition = array( $db->quoteName('type_alias') . ' = '. $db->quote('com_mostwantedrealestate.country') );
			// [Interpretation 4187] Create a new query object.
			$query = $db->getQuery(true);
			$query->delete($db->quoteName('#__content_types'));
			$query->where($country_condition);
			$db->setQuery($query);
			// [Interpretation 4192] Execute the query to remove Country items
			$country_done = $db->execute();
			if ($country_done);
			{
				// [Interpretation 4196] If succesfully remove Country add queued success message.
				$app->enqueueMessage(JText::_('The (com_mostwantedrealestate.country) type alias was removed from the <b>#__content_type</b> table'));
			}

			// [Interpretation 4202] Remove Country items from the contentitem tag map table
			$country_condition = array( $db->quoteName('type_alias') . ' = '. $db->quote('com_mostwantedrealestate.country') );
			// [Interpretation 4204] Create a new query object.
			$query = $db->getQuery(true);
			$query->delete($db->quoteName('#__contentitem_tag_map'));
			$query->where($country_condition);
			$db->setQuery($query);
			// [Interpretation 4209] Execute the query to remove Country items
			$country_done = $db->execute();
			if ($country_done);
			{
				// [Interpretation 4213] If succesfully remove Country add queued success message.
				$app->enqueueMessage(JText::_('The (com_mostwantedrealestate.country) type alias was removed from the <b>#__contentitem_tag_map</b> table'));
			}

			// [Interpretation 4219] Remove Country items from the ucm content table
			$country_condition = array( $db->quoteName('core_type_alias') . ' = ' . $db->quote('com_mostwantedrealestate.country') );
			// [Interpretation 4221] Create a new query object.
			$query = $db->getQuery(true);
			$query->delete($db->quoteName('#__ucm_content'));
			$query->where($country_condition);
			$db->setQuery($query);
			// [Interpretation 4226] Execute the query to remove Country items
			$country_done = $db->execute();
			if ($country_done);
			{
				// [Interpretation 4230] If succesfully remove Country add queued success message.
				$app->enqueueMessage(JText::_('The (com_mostwantedrealestate.country) type alias was removed from the <b>#__ucm_content</b> table'));
			}

			// [Interpretation 4236] Make sure that all the Country items are cleared from DB
			foreach ($country_ids as $country_id)
			{
				// [Interpretation 4241] Remove Country items from the ucm base table
				$country_condition = array( $db->quoteName('ucm_type_id') . ' = ' . $country_id);
				// [Interpretation 4243] Create a new query object.
				$query = $db->getQuery(true);
				$query->delete($db->quoteName('#__ucm_base'));
				$query->where($country_condition);
				$db->setQuery($query);
				// [Interpretation 4248] Execute the query to remove Country items
				$db->execute();

				// [Interpretation 4252] Remove Country items from the ucm history table
				$country_condition = array( $db->quoteName('ucm_type_id') . ' = ' . $country_id);
				// [Interpretation 4254] Create a new query object.
				$query = $db->getQuery(true);
				$query->delete($db->quoteName('#__ucm_history'));
				$query->where($country_condition);
				$db->setQuery($query);
				// [Interpretation 4259] Execute the query to remove Country items
				$db->execute();
			}
		}

		// [Interpretation 4167] Create a new query object.
		$query = $db->getQuery(true);
		// [Interpretation 4169] Select id from content type table
		$query->select($db->quoteName('type_id'));
		$query->from($db->quoteName('#__content_types'));
		// [Interpretation 4172] Where State alias is found
		$query->where( $db->quoteName('type_alias') . ' = '. $db->quote('com_mostwantedrealestate.state') );
		$db->setQuery($query);
		// [Interpretation 4175] Execute query to see if alias is found
		$db->execute();
		$state_found = $db->getNumRows();
		// [Interpretation 4178] Now check if there were any rows
		if ($state_found)
		{
			// [Interpretation 4181] Since there are load the needed  state type ids
			$state_ids = $db->loadColumn();
			// [Interpretation 4185] Remove State from the content type table
			$state_condition = array( $db->quoteName('type_alias') . ' = '. $db->quote('com_mostwantedrealestate.state') );
			// [Interpretation 4187] Create a new query object.
			$query = $db->getQuery(true);
			$query->delete($db->quoteName('#__content_types'));
			$query->where($state_condition);
			$db->setQuery($query);
			// [Interpretation 4192] Execute the query to remove State items
			$state_done = $db->execute();
			if ($state_done);
			{
				// [Interpretation 4196] If succesfully remove State add queued success message.
				$app->enqueueMessage(JText::_('The (com_mostwantedrealestate.state) type alias was removed from the <b>#__content_type</b> table'));
			}

			// [Interpretation 4202] Remove State items from the contentitem tag map table
			$state_condition = array( $db->quoteName('type_alias') . ' = '. $db->quote('com_mostwantedrealestate.state') );
			// [Interpretation 4204] Create a new query object.
			$query = $db->getQuery(true);
			$query->delete($db->quoteName('#__contentitem_tag_map'));
			$query->where($state_condition);
			$db->setQuery($query);
			// [Interpretation 4209] Execute the query to remove State items
			$state_done = $db->execute();
			if ($state_done);
			{
				// [Interpretation 4213] If succesfully remove State add queued success message.
				$app->enqueueMessage(JText::_('The (com_mostwantedrealestate.state) type alias was removed from the <b>#__contentitem_tag_map</b> table'));
			}

			// [Interpretation 4219] Remove State items from the ucm content table
			$state_condition = array( $db->quoteName('core_type_alias') . ' = ' . $db->quote('com_mostwantedrealestate.state') );
			// [Interpretation 4221] Create a new query object.
			$query = $db->getQuery(true);
			$query->delete($db->quoteName('#__ucm_content'));
			$query->where($state_condition);
			$db->setQuery($query);
			// [Interpretation 4226] Execute the query to remove State items
			$state_done = $db->execute();
			if ($state_done);
			{
				// [Interpretation 4230] If succesfully remove State add queued success message.
				$app->enqueueMessage(JText::_('The (com_mostwantedrealestate.state) type alias was removed from the <b>#__ucm_content</b> table'));
			}

			// [Interpretation 4236] Make sure that all the State items are cleared from DB
			foreach ($state_ids as $state_id)
			{
				// [Interpretation 4241] Remove State items from the ucm base table
				$state_condition = array( $db->quoteName('ucm_type_id') . ' = ' . $state_id);
				// [Interpretation 4243] Create a new query object.
				$query = $db->getQuery(true);
				$query->delete($db->quoteName('#__ucm_base'));
				$query->where($state_condition);
				$db->setQuery($query);
				// [Interpretation 4248] Execute the query to remove State items
				$db->execute();

				// [Interpretation 4252] Remove State items from the ucm history table
				$state_condition = array( $db->quoteName('ucm_type_id') . ' = ' . $state_id);
				// [Interpretation 4254] Create a new query object.
				$query = $db->getQuery(true);
				$query->delete($db->quoteName('#__ucm_history'));
				$query->where($state_condition);
				$db->setQuery($query);
				// [Interpretation 4259] Execute the query to remove State items
				$db->execute();
			}
		}

		// [Interpretation 4167] Create a new query object.
		$query = $db->getQuery(true);
		// [Interpretation 4169] Select id from content type table
		$query->select($db->quoteName('type_id'));
		$query->from($db->quoteName('#__content_types'));
		// [Interpretation 4172] Where City alias is found
		$query->where( $db->quoteName('type_alias') . ' = '. $db->quote('com_mostwantedrealestate.city') );
		$db->setQuery($query);
		// [Interpretation 4175] Execute query to see if alias is found
		$db->execute();
		$city_found = $db->getNumRows();
		// [Interpretation 4178] Now check if there were any rows
		if ($city_found)
		{
			// [Interpretation 4181] Since there are load the needed  city type ids
			$city_ids = $db->loadColumn();
			// [Interpretation 4185] Remove City from the content type table
			$city_condition = array( $db->quoteName('type_alias') . ' = '. $db->quote('com_mostwantedrealestate.city') );
			// [Interpretation 4187] Create a new query object.
			$query = $db->getQuery(true);
			$query->delete($db->quoteName('#__content_types'));
			$query->where($city_condition);
			$db->setQuery($query);
			// [Interpretation 4192] Execute the query to remove City items
			$city_done = $db->execute();
			if ($city_done);
			{
				// [Interpretation 4196] If succesfully remove City add queued success message.
				$app->enqueueMessage(JText::_('The (com_mostwantedrealestate.city) type alias was removed from the <b>#__content_type</b> table'));
			}

			// [Interpretation 4202] Remove City items from the contentitem tag map table
			$city_condition = array( $db->quoteName('type_alias') . ' = '. $db->quote('com_mostwantedrealestate.city') );
			// [Interpretation 4204] Create a new query object.
			$query = $db->getQuery(true);
			$query->delete($db->quoteName('#__contentitem_tag_map'));
			$query->where($city_condition);
			$db->setQuery($query);
			// [Interpretation 4209] Execute the query to remove City items
			$city_done = $db->execute();
			if ($city_done);
			{
				// [Interpretation 4213] If succesfully remove City add queued success message.
				$app->enqueueMessage(JText::_('The (com_mostwantedrealestate.city) type alias was removed from the <b>#__contentitem_tag_map</b> table'));
			}

			// [Interpretation 4219] Remove City items from the ucm content table
			$city_condition = array( $db->quoteName('core_type_alias') . ' = ' . $db->quote('com_mostwantedrealestate.city') );
			// [Interpretation 4221] Create a new query object.
			$query = $db->getQuery(true);
			$query->delete($db->quoteName('#__ucm_content'));
			$query->where($city_condition);
			$db->setQuery($query);
			// [Interpretation 4226] Execute the query to remove City items
			$city_done = $db->execute();
			if ($city_done);
			{
				// [Interpretation 4230] If succesfully remove City add queued success message.
				$app->enqueueMessage(JText::_('The (com_mostwantedrealestate.city) type alias was removed from the <b>#__ucm_content</b> table'));
			}

			// [Interpretation 4236] Make sure that all the City items are cleared from DB
			foreach ($city_ids as $city_id)
			{
				// [Interpretation 4241] Remove City items from the ucm base table
				$city_condition = array( $db->quoteName('ucm_type_id') . ' = ' . $city_id);
				// [Interpretation 4243] Create a new query object.
				$query = $db->getQuery(true);
				$query->delete($db->quoteName('#__ucm_base'));
				$query->where($city_condition);
				$db->setQuery($query);
				// [Interpretation 4248] Execute the query to remove City items
				$db->execute();

				// [Interpretation 4252] Remove City items from the ucm history table
				$city_condition = array( $db->quoteName('ucm_type_id') . ' = ' . $city_id);
				// [Interpretation 4254] Create a new query object.
				$query = $db->getQuery(true);
				$query->delete($db->quoteName('#__ucm_history'));
				$query->where($city_condition);
				$db->setQuery($query);
				// [Interpretation 4259] Execute the query to remove City items
				$db->execute();
			}
		}

		// [Interpretation 4167] Create a new query object.
		$query = $db->getQuery(true);
		// [Interpretation 4169] Select id from content type table
		$query->select($db->quoteName('type_id'));
		$query->from($db->quoteName('#__content_types'));
		// [Interpretation 4172] Where Agency alias is found
		$query->where( $db->quoteName('type_alias') . ' = '. $db->quote('com_mostwantedrealestate.agency') );
		$db->setQuery($query);
		// [Interpretation 4175] Execute query to see if alias is found
		$db->execute();
		$agency_found = $db->getNumRows();
		// [Interpretation 4178] Now check if there were any rows
		if ($agency_found)
		{
			// [Interpretation 4181] Since there are load the needed  agency type ids
			$agency_ids = $db->loadColumn();
			// [Interpretation 4185] Remove Agency from the content type table
			$agency_condition = array( $db->quoteName('type_alias') . ' = '. $db->quote('com_mostwantedrealestate.agency') );
			// [Interpretation 4187] Create a new query object.
			$query = $db->getQuery(true);
			$query->delete($db->quoteName('#__content_types'));
			$query->where($agency_condition);
			$db->setQuery($query);
			// [Interpretation 4192] Execute the query to remove Agency items
			$agency_done = $db->execute();
			if ($agency_done);
			{
				// [Interpretation 4196] If succesfully remove Agency add queued success message.
				$app->enqueueMessage(JText::_('The (com_mostwantedrealestate.agency) type alias was removed from the <b>#__content_type</b> table'));
			}

			// [Interpretation 4202] Remove Agency items from the contentitem tag map table
			$agency_condition = array( $db->quoteName('type_alias') . ' = '. $db->quote('com_mostwantedrealestate.agency') );
			// [Interpretation 4204] Create a new query object.
			$query = $db->getQuery(true);
			$query->delete($db->quoteName('#__contentitem_tag_map'));
			$query->where($agency_condition);
			$db->setQuery($query);
			// [Interpretation 4209] Execute the query to remove Agency items
			$agency_done = $db->execute();
			if ($agency_done);
			{
				// [Interpretation 4213] If succesfully remove Agency add queued success message.
				$app->enqueueMessage(JText::_('The (com_mostwantedrealestate.agency) type alias was removed from the <b>#__contentitem_tag_map</b> table'));
			}

			// [Interpretation 4219] Remove Agency items from the ucm content table
			$agency_condition = array( $db->quoteName('core_type_alias') . ' = ' . $db->quote('com_mostwantedrealestate.agency') );
			// [Interpretation 4221] Create a new query object.
			$query = $db->getQuery(true);
			$query->delete($db->quoteName('#__ucm_content'));
			$query->where($agency_condition);
			$db->setQuery($query);
			// [Interpretation 4226] Execute the query to remove Agency items
			$agency_done = $db->execute();
			if ($agency_done);
			{
				// [Interpretation 4230] If succesfully remove Agency add queued success message.
				$app->enqueueMessage(JText::_('The (com_mostwantedrealestate.agency) type alias was removed from the <b>#__ucm_content</b> table'));
			}

			// [Interpretation 4236] Make sure that all the Agency items are cleared from DB
			foreach ($agency_ids as $agency_id)
			{
				// [Interpretation 4241] Remove Agency items from the ucm base table
				$agency_condition = array( $db->quoteName('ucm_type_id') . ' = ' . $agency_id);
				// [Interpretation 4243] Create a new query object.
				$query = $db->getQuery(true);
				$query->delete($db->quoteName('#__ucm_base'));
				$query->where($agency_condition);
				$db->setQuery($query);
				// [Interpretation 4248] Execute the query to remove Agency items
				$db->execute();

				// [Interpretation 4252] Remove Agency items from the ucm history table
				$agency_condition = array( $db->quoteName('ucm_type_id') . ' = ' . $agency_id);
				// [Interpretation 4254] Create a new query object.
				$query = $db->getQuery(true);
				$query->delete($db->quoteName('#__ucm_history'));
				$query->where($agency_condition);
				$db->setQuery($query);
				// [Interpretation 4259] Execute the query to remove Agency items
				$db->execute();
			}
		}

		// [Interpretation 4167] Create a new query object.
		$query = $db->getQuery(true);
		// [Interpretation 4169] Select id from content type table
		$query->select($db->quoteName('type_id'));
		$query->from($db->quoteName('#__content_types'));
		// [Interpretation 4172] Where Agent alias is found
		$query->where( $db->quoteName('type_alias') . ' = '. $db->quote('com_mostwantedrealestate.agent') );
		$db->setQuery($query);
		// [Interpretation 4175] Execute query to see if alias is found
		$db->execute();
		$agent_found = $db->getNumRows();
		// [Interpretation 4178] Now check if there were any rows
		if ($agent_found)
		{
			// [Interpretation 4181] Since there are load the needed  agent type ids
			$agent_ids = $db->loadColumn();
			// [Interpretation 4185] Remove Agent from the content type table
			$agent_condition = array( $db->quoteName('type_alias') . ' = '. $db->quote('com_mostwantedrealestate.agent') );
			// [Interpretation 4187] Create a new query object.
			$query = $db->getQuery(true);
			$query->delete($db->quoteName('#__content_types'));
			$query->where($agent_condition);
			$db->setQuery($query);
			// [Interpretation 4192] Execute the query to remove Agent items
			$agent_done = $db->execute();
			if ($agent_done);
			{
				// [Interpretation 4196] If succesfully remove Agent add queued success message.
				$app->enqueueMessage(JText::_('The (com_mostwantedrealestate.agent) type alias was removed from the <b>#__content_type</b> table'));
			}

			// [Interpretation 4202] Remove Agent items from the contentitem tag map table
			$agent_condition = array( $db->quoteName('type_alias') . ' = '. $db->quote('com_mostwantedrealestate.agent') );
			// [Interpretation 4204] Create a new query object.
			$query = $db->getQuery(true);
			$query->delete($db->quoteName('#__contentitem_tag_map'));
			$query->where($agent_condition);
			$db->setQuery($query);
			// [Interpretation 4209] Execute the query to remove Agent items
			$agent_done = $db->execute();
			if ($agent_done);
			{
				// [Interpretation 4213] If succesfully remove Agent add queued success message.
				$app->enqueueMessage(JText::_('The (com_mostwantedrealestate.agent) type alias was removed from the <b>#__contentitem_tag_map</b> table'));
			}

			// [Interpretation 4219] Remove Agent items from the ucm content table
			$agent_condition = array( $db->quoteName('core_type_alias') . ' = ' . $db->quote('com_mostwantedrealestate.agent') );
			// [Interpretation 4221] Create a new query object.
			$query = $db->getQuery(true);
			$query->delete($db->quoteName('#__ucm_content'));
			$query->where($agent_condition);
			$db->setQuery($query);
			// [Interpretation 4226] Execute the query to remove Agent items
			$agent_done = $db->execute();
			if ($agent_done);
			{
				// [Interpretation 4230] If succesfully remove Agent add queued success message.
				$app->enqueueMessage(JText::_('The (com_mostwantedrealestate.agent) type alias was removed from the <b>#__ucm_content</b> table'));
			}

			// [Interpretation 4236] Make sure that all the Agent items are cleared from DB
			foreach ($agent_ids as $agent_id)
			{
				// [Interpretation 4241] Remove Agent items from the ucm base table
				$agent_condition = array( $db->quoteName('ucm_type_id') . ' = ' . $agent_id);
				// [Interpretation 4243] Create a new query object.
				$query = $db->getQuery(true);
				$query->delete($db->quoteName('#__ucm_base'));
				$query->where($agent_condition);
				$db->setQuery($query);
				// [Interpretation 4248] Execute the query to remove Agent items
				$db->execute();

				// [Interpretation 4252] Remove Agent items from the ucm history table
				$agent_condition = array( $db->quoteName('ucm_type_id') . ' = ' . $agent_id);
				// [Interpretation 4254] Create a new query object.
				$query = $db->getQuery(true);
				$query->delete($db->quoteName('#__ucm_history'));
				$query->where($agent_condition);
				$db->setQuery($query);
				// [Interpretation 4259] Execute the query to remove Agent items
				$db->execute();
			}
		}

		// [Interpretation 4167] Create a new query object.
		$query = $db->getQuery(true);
		// [Interpretation 4169] Select id from content type table
		$query->select($db->quoteName('type_id'));
		$query->from($db->quoteName('#__content_types'));
		// [Interpretation 4172] Where Agent catid alias is found
		$query->where( $db->quoteName('type_alias') . ' = '. $db->quote('com_mostwantedrealestate.agents.category') );
		$db->setQuery($query);
		// [Interpretation 4175] Execute query to see if alias is found
		$db->execute();
		$agent_catid_found = $db->getNumRows();
		// [Interpretation 4178] Now check if there were any rows
		if ($agent_catid_found)
		{
			// [Interpretation 4181] Since there are load the needed  agent_catid type ids
			$agent_catid_ids = $db->loadColumn();
			// [Interpretation 4185] Remove Agent catid from the content type table
			$agent_catid_condition = array( $db->quoteName('type_alias') . ' = '. $db->quote('com_mostwantedrealestate.agents.category') );
			// [Interpretation 4187] Create a new query object.
			$query = $db->getQuery(true);
			$query->delete($db->quoteName('#__content_types'));
			$query->where($agent_catid_condition);
			$db->setQuery($query);
			// [Interpretation 4192] Execute the query to remove Agent catid items
			$agent_catid_done = $db->execute();
			if ($agent_catid_done);
			{
				// [Interpretation 4196] If succesfully remove Agent catid add queued success message.
				$app->enqueueMessage(JText::_('The (com_mostwantedrealestate.agents.category) type alias was removed from the <b>#__content_type</b> table'));
			}

			// [Interpretation 4202] Remove Agent catid items from the contentitem tag map table
			$agent_catid_condition = array( $db->quoteName('type_alias') . ' = '. $db->quote('com_mostwantedrealestate.agents.category') );
			// [Interpretation 4204] Create a new query object.
			$query = $db->getQuery(true);
			$query->delete($db->quoteName('#__contentitem_tag_map'));
			$query->where($agent_catid_condition);
			$db->setQuery($query);
			// [Interpretation 4209] Execute the query to remove Agent catid items
			$agent_catid_done = $db->execute();
			if ($agent_catid_done);
			{
				// [Interpretation 4213] If succesfully remove Agent catid add queued success message.
				$app->enqueueMessage(JText::_('The (com_mostwantedrealestate.agents.category) type alias was removed from the <b>#__contentitem_tag_map</b> table'));
			}

			// [Interpretation 4219] Remove Agent catid items from the ucm content table
			$agent_catid_condition = array( $db->quoteName('core_type_alias') . ' = ' . $db->quote('com_mostwantedrealestate.agents.category') );
			// [Interpretation 4221] Create a new query object.
			$query = $db->getQuery(true);
			$query->delete($db->quoteName('#__ucm_content'));
			$query->where($agent_catid_condition);
			$db->setQuery($query);
			// [Interpretation 4226] Execute the query to remove Agent catid items
			$agent_catid_done = $db->execute();
			if ($agent_catid_done);
			{
				// [Interpretation 4230] If succesfully remove Agent catid add queued success message.
				$app->enqueueMessage(JText::_('The (com_mostwantedrealestate.agents.category) type alias was removed from the <b>#__ucm_content</b> table'));
			}

			// [Interpretation 4236] Make sure that all the Agent catid items are cleared from DB
			foreach ($agent_catid_ids as $agent_catid_id)
			{
				// [Interpretation 4241] Remove Agent catid items from the ucm base table
				$agent_catid_condition = array( $db->quoteName('ucm_type_id') . ' = ' . $agent_catid_id);
				// [Interpretation 4243] Create a new query object.
				$query = $db->getQuery(true);
				$query->delete($db->quoteName('#__ucm_base'));
				$query->where($agent_catid_condition);
				$db->setQuery($query);
				// [Interpretation 4248] Execute the query to remove Agent catid items
				$db->execute();

				// [Interpretation 4252] Remove Agent catid items from the ucm history table
				$agent_catid_condition = array( $db->quoteName('ucm_type_id') . ' = ' . $agent_catid_id);
				// [Interpretation 4254] Create a new query object.
				$query = $db->getQuery(true);
				$query->delete($db->quoteName('#__ucm_history'));
				$query->where($agent_catid_condition);
				$db->setQuery($query);
				// [Interpretation 4259] Execute the query to remove Agent catid items
				$db->execute();
			}
		}

		// [Interpretation 4167] Create a new query object.
		$query = $db->getQuery(true);
		// [Interpretation 4169] Select id from content type table
		$query->select($db->quoteName('type_id'));
		$query->from($db->quoteName('#__content_types'));
		// [Interpretation 4172] Where Property alias is found
		$query->where( $db->quoteName('type_alias') . ' = '. $db->quote('com_mostwantedrealestate.property') );
		$db->setQuery($query);
		// [Interpretation 4175] Execute query to see if alias is found
		$db->execute();
		$property_found = $db->getNumRows();
		// [Interpretation 4178] Now check if there were any rows
		if ($property_found)
		{
			// [Interpretation 4181] Since there are load the needed  property type ids
			$property_ids = $db->loadColumn();
			// [Interpretation 4185] Remove Property from the content type table
			$property_condition = array( $db->quoteName('type_alias') . ' = '. $db->quote('com_mostwantedrealestate.property') );
			// [Interpretation 4187] Create a new query object.
			$query = $db->getQuery(true);
			$query->delete($db->quoteName('#__content_types'));
			$query->where($property_condition);
			$db->setQuery($query);
			// [Interpretation 4192] Execute the query to remove Property items
			$property_done = $db->execute();
			if ($property_done);
			{
				// [Interpretation 4196] If succesfully remove Property add queued success message.
				$app->enqueueMessage(JText::_('The (com_mostwantedrealestate.property) type alias was removed from the <b>#__content_type</b> table'));
			}

			// [Interpretation 4202] Remove Property items from the contentitem tag map table
			$property_condition = array( $db->quoteName('type_alias') . ' = '. $db->quote('com_mostwantedrealestate.property') );
			// [Interpretation 4204] Create a new query object.
			$query = $db->getQuery(true);
			$query->delete($db->quoteName('#__contentitem_tag_map'));
			$query->where($property_condition);
			$db->setQuery($query);
			// [Interpretation 4209] Execute the query to remove Property items
			$property_done = $db->execute();
			if ($property_done);
			{
				// [Interpretation 4213] If succesfully remove Property add queued success message.
				$app->enqueueMessage(JText::_('The (com_mostwantedrealestate.property) type alias was removed from the <b>#__contentitem_tag_map</b> table'));
			}

			// [Interpretation 4219] Remove Property items from the ucm content table
			$property_condition = array( $db->quoteName('core_type_alias') . ' = ' . $db->quote('com_mostwantedrealestate.property') );
			// [Interpretation 4221] Create a new query object.
			$query = $db->getQuery(true);
			$query->delete($db->quoteName('#__ucm_content'));
			$query->where($property_condition);
			$db->setQuery($query);
			// [Interpretation 4226] Execute the query to remove Property items
			$property_done = $db->execute();
			if ($property_done);
			{
				// [Interpretation 4230] If succesfully remove Property add queued success message.
				$app->enqueueMessage(JText::_('The (com_mostwantedrealestate.property) type alias was removed from the <b>#__ucm_content</b> table'));
			}

			// [Interpretation 4236] Make sure that all the Property items are cleared from DB
			foreach ($property_ids as $property_id)
			{
				// [Interpretation 4241] Remove Property items from the ucm base table
				$property_condition = array( $db->quoteName('ucm_type_id') . ' = ' . $property_id);
				// [Interpretation 4243] Create a new query object.
				$query = $db->getQuery(true);
				$query->delete($db->quoteName('#__ucm_base'));
				$query->where($property_condition);
				$db->setQuery($query);
				// [Interpretation 4248] Execute the query to remove Property items
				$db->execute();

				// [Interpretation 4252] Remove Property items from the ucm history table
				$property_condition = array( $db->quoteName('ucm_type_id') . ' = ' . $property_id);
				// [Interpretation 4254] Create a new query object.
				$query = $db->getQuery(true);
				$query->delete($db->quoteName('#__ucm_history'));
				$query->where($property_condition);
				$db->setQuery($query);
				// [Interpretation 4259] Execute the query to remove Property items
				$db->execute();
			}
		}

		// [Interpretation 4167] Create a new query object.
		$query = $db->getQuery(true);
		// [Interpretation 4169] Select id from content type table
		$query->select($db->quoteName('type_id'));
		$query->from($db->quoteName('#__content_types'));
		// [Interpretation 4172] Where Property catid alias is found
		$query->where( $db->quoteName('type_alias') . ' = '. $db->quote('com_mostwantedrealestate.properties.category') );
		$db->setQuery($query);
		// [Interpretation 4175] Execute query to see if alias is found
		$db->execute();
		$property_catid_found = $db->getNumRows();
		// [Interpretation 4178] Now check if there were any rows
		if ($property_catid_found)
		{
			// [Interpretation 4181] Since there are load the needed  property_catid type ids
			$property_catid_ids = $db->loadColumn();
			// [Interpretation 4185] Remove Property catid from the content type table
			$property_catid_condition = array( $db->quoteName('type_alias') . ' = '. $db->quote('com_mostwantedrealestate.properties.category') );
			// [Interpretation 4187] Create a new query object.
			$query = $db->getQuery(true);
			$query->delete($db->quoteName('#__content_types'));
			$query->where($property_catid_condition);
			$db->setQuery($query);
			// [Interpretation 4192] Execute the query to remove Property catid items
			$property_catid_done = $db->execute();
			if ($property_catid_done);
			{
				// [Interpretation 4196] If succesfully remove Property catid add queued success message.
				$app->enqueueMessage(JText::_('The (com_mostwantedrealestate.properties.category) type alias was removed from the <b>#__content_type</b> table'));
			}

			// [Interpretation 4202] Remove Property catid items from the contentitem tag map table
			$property_catid_condition = array( $db->quoteName('type_alias') . ' = '. $db->quote('com_mostwantedrealestate.properties.category') );
			// [Interpretation 4204] Create a new query object.
			$query = $db->getQuery(true);
			$query->delete($db->quoteName('#__contentitem_tag_map'));
			$query->where($property_catid_condition);
			$db->setQuery($query);
			// [Interpretation 4209] Execute the query to remove Property catid items
			$property_catid_done = $db->execute();
			if ($property_catid_done);
			{
				// [Interpretation 4213] If succesfully remove Property catid add queued success message.
				$app->enqueueMessage(JText::_('The (com_mostwantedrealestate.properties.category) type alias was removed from the <b>#__contentitem_tag_map</b> table'));
			}

			// [Interpretation 4219] Remove Property catid items from the ucm content table
			$property_catid_condition = array( $db->quoteName('core_type_alias') . ' = ' . $db->quote('com_mostwantedrealestate.properties.category') );
			// [Interpretation 4221] Create a new query object.
			$query = $db->getQuery(true);
			$query->delete($db->quoteName('#__ucm_content'));
			$query->where($property_catid_condition);
			$db->setQuery($query);
			// [Interpretation 4226] Execute the query to remove Property catid items
			$property_catid_done = $db->execute();
			if ($property_catid_done);
			{
				// [Interpretation 4230] If succesfully remove Property catid add queued success message.
				$app->enqueueMessage(JText::_('The (com_mostwantedrealestate.properties.category) type alias was removed from the <b>#__ucm_content</b> table'));
			}

			// [Interpretation 4236] Make sure that all the Property catid items are cleared from DB
			foreach ($property_catid_ids as $property_catid_id)
			{
				// [Interpretation 4241] Remove Property catid items from the ucm base table
				$property_catid_condition = array( $db->quoteName('ucm_type_id') . ' = ' . $property_catid_id);
				// [Interpretation 4243] Create a new query object.
				$query = $db->getQuery(true);
				$query->delete($db->quoteName('#__ucm_base'));
				$query->where($property_catid_condition);
				$db->setQuery($query);
				// [Interpretation 4248] Execute the query to remove Property catid items
				$db->execute();

				// [Interpretation 4252] Remove Property catid items from the ucm history table
				$property_catid_condition = array( $db->quoteName('ucm_type_id') . ' = ' . $property_catid_id);
				// [Interpretation 4254] Create a new query object.
				$query = $db->getQuery(true);
				$query->delete($db->quoteName('#__ucm_history'));
				$query->where($property_catid_condition);
				$db->setQuery($query);
				// [Interpretation 4259] Execute the query to remove Property catid items
				$db->execute();
			}
		}

		// [Interpretation 4167] Create a new query object.
		$query = $db->getQuery(true);
		// [Interpretation 4169] Select id from content type table
		$query->select($db->quoteName('type_id'));
		$query->from($db->quoteName('#__content_types'));
		// [Interpretation 4172] Where Market_status alias is found
		$query->where( $db->quoteName('type_alias') . ' = '. $db->quote('com_mostwantedrealestate.market_status') );
		$db->setQuery($query);
		// [Interpretation 4175] Execute query to see if alias is found
		$db->execute();
		$market_status_found = $db->getNumRows();
		// [Interpretation 4178] Now check if there were any rows
		if ($market_status_found)
		{
			// [Interpretation 4181] Since there are load the needed  market_status type ids
			$market_status_ids = $db->loadColumn();
			// [Interpretation 4185] Remove Market_status from the content type table
			$market_status_condition = array( $db->quoteName('type_alias') . ' = '. $db->quote('com_mostwantedrealestate.market_status') );
			// [Interpretation 4187] Create a new query object.
			$query = $db->getQuery(true);
			$query->delete($db->quoteName('#__content_types'));
			$query->where($market_status_condition);
			$db->setQuery($query);
			// [Interpretation 4192] Execute the query to remove Market_status items
			$market_status_done = $db->execute();
			if ($market_status_done);
			{
				// [Interpretation 4196] If succesfully remove Market_status add queued success message.
				$app->enqueueMessage(JText::_('The (com_mostwantedrealestate.market_status) type alias was removed from the <b>#__content_type</b> table'));
			}

			// [Interpretation 4202] Remove Market_status items from the contentitem tag map table
			$market_status_condition = array( $db->quoteName('type_alias') . ' = '. $db->quote('com_mostwantedrealestate.market_status') );
			// [Interpretation 4204] Create a new query object.
			$query = $db->getQuery(true);
			$query->delete($db->quoteName('#__contentitem_tag_map'));
			$query->where($market_status_condition);
			$db->setQuery($query);
			// [Interpretation 4209] Execute the query to remove Market_status items
			$market_status_done = $db->execute();
			if ($market_status_done);
			{
				// [Interpretation 4213] If succesfully remove Market_status add queued success message.
				$app->enqueueMessage(JText::_('The (com_mostwantedrealestate.market_status) type alias was removed from the <b>#__contentitem_tag_map</b> table'));
			}

			// [Interpretation 4219] Remove Market_status items from the ucm content table
			$market_status_condition = array( $db->quoteName('core_type_alias') . ' = ' . $db->quote('com_mostwantedrealestate.market_status') );
			// [Interpretation 4221] Create a new query object.
			$query = $db->getQuery(true);
			$query->delete($db->quoteName('#__ucm_content'));
			$query->where($market_status_condition);
			$db->setQuery($query);
			// [Interpretation 4226] Execute the query to remove Market_status items
			$market_status_done = $db->execute();
			if ($market_status_done);
			{
				// [Interpretation 4230] If succesfully remove Market_status add queued success message.
				$app->enqueueMessage(JText::_('The (com_mostwantedrealestate.market_status) type alias was removed from the <b>#__ucm_content</b> table'));
			}

			// [Interpretation 4236] Make sure that all the Market_status items are cleared from DB
			foreach ($market_status_ids as $market_status_id)
			{
				// [Interpretation 4241] Remove Market_status items from the ucm base table
				$market_status_condition = array( $db->quoteName('ucm_type_id') . ' = ' . $market_status_id);
				// [Interpretation 4243] Create a new query object.
				$query = $db->getQuery(true);
				$query->delete($db->quoteName('#__ucm_base'));
				$query->where($market_status_condition);
				$db->setQuery($query);
				// [Interpretation 4248] Execute the query to remove Market_status items
				$db->execute();

				// [Interpretation 4252] Remove Market_status items from the ucm history table
				$market_status_condition = array( $db->quoteName('ucm_type_id') . ' = ' . $market_status_id);
				// [Interpretation 4254] Create a new query object.
				$query = $db->getQuery(true);
				$query->delete($db->quoteName('#__ucm_history'));
				$query->where($market_status_condition);
				$db->setQuery($query);
				// [Interpretation 4259] Execute the query to remove Market_status items
				$db->execute();
			}
		}

		// [Interpretation 4167] Create a new query object.
		$query = $db->getQuery(true);
		// [Interpretation 4169] Select id from content type table
		$query->select($db->quoteName('type_id'));
		$query->from($db->quoteName('#__content_types'));
		// [Interpretation 4172] Where Transaction_type alias is found
		$query->where( $db->quoteName('type_alias') . ' = '. $db->quote('com_mostwantedrealestate.transaction_type') );
		$db->setQuery($query);
		// [Interpretation 4175] Execute query to see if alias is found
		$db->execute();
		$transaction_type_found = $db->getNumRows();
		// [Interpretation 4178] Now check if there were any rows
		if ($transaction_type_found)
		{
			// [Interpretation 4181] Since there are load the needed  transaction_type type ids
			$transaction_type_ids = $db->loadColumn();
			// [Interpretation 4185] Remove Transaction_type from the content type table
			$transaction_type_condition = array( $db->quoteName('type_alias') . ' = '. $db->quote('com_mostwantedrealestate.transaction_type') );
			// [Interpretation 4187] Create a new query object.
			$query = $db->getQuery(true);
			$query->delete($db->quoteName('#__content_types'));
			$query->where($transaction_type_condition);
			$db->setQuery($query);
			// [Interpretation 4192] Execute the query to remove Transaction_type items
			$transaction_type_done = $db->execute();
			if ($transaction_type_done);
			{
				// [Interpretation 4196] If succesfully remove Transaction_type add queued success message.
				$app->enqueueMessage(JText::_('The (com_mostwantedrealestate.transaction_type) type alias was removed from the <b>#__content_type</b> table'));
			}

			// [Interpretation 4202] Remove Transaction_type items from the contentitem tag map table
			$transaction_type_condition = array( $db->quoteName('type_alias') . ' = '. $db->quote('com_mostwantedrealestate.transaction_type') );
			// [Interpretation 4204] Create a new query object.
			$query = $db->getQuery(true);
			$query->delete($db->quoteName('#__contentitem_tag_map'));
			$query->where($transaction_type_condition);
			$db->setQuery($query);
			// [Interpretation 4209] Execute the query to remove Transaction_type items
			$transaction_type_done = $db->execute();
			if ($transaction_type_done);
			{
				// [Interpretation 4213] If succesfully remove Transaction_type add queued success message.
				$app->enqueueMessage(JText::_('The (com_mostwantedrealestate.transaction_type) type alias was removed from the <b>#__contentitem_tag_map</b> table'));
			}

			// [Interpretation 4219] Remove Transaction_type items from the ucm content table
			$transaction_type_condition = array( $db->quoteName('core_type_alias') . ' = ' . $db->quote('com_mostwantedrealestate.transaction_type') );
			// [Interpretation 4221] Create a new query object.
			$query = $db->getQuery(true);
			$query->delete($db->quoteName('#__ucm_content'));
			$query->where($transaction_type_condition);
			$db->setQuery($query);
			// [Interpretation 4226] Execute the query to remove Transaction_type items
			$transaction_type_done = $db->execute();
			if ($transaction_type_done);
			{
				// [Interpretation 4230] If succesfully remove Transaction_type add queued success message.
				$app->enqueueMessage(JText::_('The (com_mostwantedrealestate.transaction_type) type alias was removed from the <b>#__ucm_content</b> table'));
			}

			// [Interpretation 4236] Make sure that all the Transaction_type items are cleared from DB
			foreach ($transaction_type_ids as $transaction_type_id)
			{
				// [Interpretation 4241] Remove Transaction_type items from the ucm base table
				$transaction_type_condition = array( $db->quoteName('ucm_type_id') . ' = ' . $transaction_type_id);
				// [Interpretation 4243] Create a new query object.
				$query = $db->getQuery(true);
				$query->delete($db->quoteName('#__ucm_base'));
				$query->where($transaction_type_condition);
				$db->setQuery($query);
				// [Interpretation 4248] Execute the query to remove Transaction_type items
				$db->execute();

				// [Interpretation 4252] Remove Transaction_type items from the ucm history table
				$transaction_type_condition = array( $db->quoteName('ucm_type_id') . ' = ' . $transaction_type_id);
				// [Interpretation 4254] Create a new query object.
				$query = $db->getQuery(true);
				$query->delete($db->quoteName('#__ucm_history'));
				$query->where($transaction_type_condition);
				$db->setQuery($query);
				// [Interpretation 4259] Execute the query to remove Transaction_type items
				$db->execute();
			}
		}

		// [Interpretation 4167] Create a new query object.
		$query = $db->getQuery(true);
		// [Interpretation 4169] Select id from content type table
		$query->select($db->quoteName('type_id'));
		$query->from($db->quoteName('#__content_types'));
		// [Interpretation 4172] Where Rental_frequency alias is found
		$query->where( $db->quoteName('type_alias') . ' = '. $db->quote('com_mostwantedrealestate.rental_frequency') );
		$db->setQuery($query);
		// [Interpretation 4175] Execute query to see if alias is found
		$db->execute();
		$rental_frequency_found = $db->getNumRows();
		// [Interpretation 4178] Now check if there were any rows
		if ($rental_frequency_found)
		{
			// [Interpretation 4181] Since there are load the needed  rental_frequency type ids
			$rental_frequency_ids = $db->loadColumn();
			// [Interpretation 4185] Remove Rental_frequency from the content type table
			$rental_frequency_condition = array( $db->quoteName('type_alias') . ' = '. $db->quote('com_mostwantedrealestate.rental_frequency') );
			// [Interpretation 4187] Create a new query object.
			$query = $db->getQuery(true);
			$query->delete($db->quoteName('#__content_types'));
			$query->where($rental_frequency_condition);
			$db->setQuery($query);
			// [Interpretation 4192] Execute the query to remove Rental_frequency items
			$rental_frequency_done = $db->execute();
			if ($rental_frequency_done);
			{
				// [Interpretation 4196] If succesfully remove Rental_frequency add queued success message.
				$app->enqueueMessage(JText::_('The (com_mostwantedrealestate.rental_frequency) type alias was removed from the <b>#__content_type</b> table'));
			}

			// [Interpretation 4202] Remove Rental_frequency items from the contentitem tag map table
			$rental_frequency_condition = array( $db->quoteName('type_alias') . ' = '. $db->quote('com_mostwantedrealestate.rental_frequency') );
			// [Interpretation 4204] Create a new query object.
			$query = $db->getQuery(true);
			$query->delete($db->quoteName('#__contentitem_tag_map'));
			$query->where($rental_frequency_condition);
			$db->setQuery($query);
			// [Interpretation 4209] Execute the query to remove Rental_frequency items
			$rental_frequency_done = $db->execute();
			if ($rental_frequency_done);
			{
				// [Interpretation 4213] If succesfully remove Rental_frequency add queued success message.
				$app->enqueueMessage(JText::_('The (com_mostwantedrealestate.rental_frequency) type alias was removed from the <b>#__contentitem_tag_map</b> table'));
			}

			// [Interpretation 4219] Remove Rental_frequency items from the ucm content table
			$rental_frequency_condition = array( $db->quoteName('core_type_alias') . ' = ' . $db->quote('com_mostwantedrealestate.rental_frequency') );
			// [Interpretation 4221] Create a new query object.
			$query = $db->getQuery(true);
			$query->delete($db->quoteName('#__ucm_content'));
			$query->where($rental_frequency_condition);
			$db->setQuery($query);
			// [Interpretation 4226] Execute the query to remove Rental_frequency items
			$rental_frequency_done = $db->execute();
			if ($rental_frequency_done);
			{
				// [Interpretation 4230] If succesfully remove Rental_frequency add queued success message.
				$app->enqueueMessage(JText::_('The (com_mostwantedrealestate.rental_frequency) type alias was removed from the <b>#__ucm_content</b> table'));
			}

			// [Interpretation 4236] Make sure that all the Rental_frequency items are cleared from DB
			foreach ($rental_frequency_ids as $rental_frequency_id)
			{
				// [Interpretation 4241] Remove Rental_frequency items from the ucm base table
				$rental_frequency_condition = array( $db->quoteName('ucm_type_id') . ' = ' . $rental_frequency_id);
				// [Interpretation 4243] Create a new query object.
				$query = $db->getQuery(true);
				$query->delete($db->quoteName('#__ucm_base'));
				$query->where($rental_frequency_condition);
				$db->setQuery($query);
				// [Interpretation 4248] Execute the query to remove Rental_frequency items
				$db->execute();

				// [Interpretation 4252] Remove Rental_frequency items from the ucm history table
				$rental_frequency_condition = array( $db->quoteName('ucm_type_id') . ' = ' . $rental_frequency_id);
				// [Interpretation 4254] Create a new query object.
				$query = $db->getQuery(true);
				$query->delete($db->quoteName('#__ucm_history'));
				$query->where($rental_frequency_condition);
				$db->setQuery($query);
				// [Interpretation 4259] Execute the query to remove Rental_frequency items
				$db->execute();
			}
		}

		// [Interpretation 4167] Create a new query object.
		$query = $db->getQuery(true);
		// [Interpretation 4169] Select id from content type table
		$query->select($db->quoteName('type_id'));
		$query->from($db->quoteName('#__content_types'));
		// [Interpretation 4172] Where Rent_type alias is found
		$query->where( $db->quoteName('type_alias') . ' = '. $db->quote('com_mostwantedrealestate.rent_type') );
		$db->setQuery($query);
		// [Interpretation 4175] Execute query to see if alias is found
		$db->execute();
		$rent_type_found = $db->getNumRows();
		// [Interpretation 4178] Now check if there were any rows
		if ($rent_type_found)
		{
			// [Interpretation 4181] Since there are load the needed  rent_type type ids
			$rent_type_ids = $db->loadColumn();
			// [Interpretation 4185] Remove Rent_type from the content type table
			$rent_type_condition = array( $db->quoteName('type_alias') . ' = '. $db->quote('com_mostwantedrealestate.rent_type') );
			// [Interpretation 4187] Create a new query object.
			$query = $db->getQuery(true);
			$query->delete($db->quoteName('#__content_types'));
			$query->where($rent_type_condition);
			$db->setQuery($query);
			// [Interpretation 4192] Execute the query to remove Rent_type items
			$rent_type_done = $db->execute();
			if ($rent_type_done);
			{
				// [Interpretation 4196] If succesfully remove Rent_type add queued success message.
				$app->enqueueMessage(JText::_('The (com_mostwantedrealestate.rent_type) type alias was removed from the <b>#__content_type</b> table'));
			}

			// [Interpretation 4202] Remove Rent_type items from the contentitem tag map table
			$rent_type_condition = array( $db->quoteName('type_alias') . ' = '. $db->quote('com_mostwantedrealestate.rent_type') );
			// [Interpretation 4204] Create a new query object.
			$query = $db->getQuery(true);
			$query->delete($db->quoteName('#__contentitem_tag_map'));
			$query->where($rent_type_condition);
			$db->setQuery($query);
			// [Interpretation 4209] Execute the query to remove Rent_type items
			$rent_type_done = $db->execute();
			if ($rent_type_done);
			{
				// [Interpretation 4213] If succesfully remove Rent_type add queued success message.
				$app->enqueueMessage(JText::_('The (com_mostwantedrealestate.rent_type) type alias was removed from the <b>#__contentitem_tag_map</b> table'));
			}

			// [Interpretation 4219] Remove Rent_type items from the ucm content table
			$rent_type_condition = array( $db->quoteName('core_type_alias') . ' = ' . $db->quote('com_mostwantedrealestate.rent_type') );
			// [Interpretation 4221] Create a new query object.
			$query = $db->getQuery(true);
			$query->delete($db->quoteName('#__ucm_content'));
			$query->where($rent_type_condition);
			$db->setQuery($query);
			// [Interpretation 4226] Execute the query to remove Rent_type items
			$rent_type_done = $db->execute();
			if ($rent_type_done);
			{
				// [Interpretation 4230] If succesfully remove Rent_type add queued success message.
				$app->enqueueMessage(JText::_('The (com_mostwantedrealestate.rent_type) type alias was removed from the <b>#__ucm_content</b> table'));
			}

			// [Interpretation 4236] Make sure that all the Rent_type items are cleared from DB
			foreach ($rent_type_ids as $rent_type_id)
			{
				// [Interpretation 4241] Remove Rent_type items from the ucm base table
				$rent_type_condition = array( $db->quoteName('ucm_type_id') . ' = ' . $rent_type_id);
				// [Interpretation 4243] Create a new query object.
				$query = $db->getQuery(true);
				$query->delete($db->quoteName('#__ucm_base'));
				$query->where($rent_type_condition);
				$db->setQuery($query);
				// [Interpretation 4248] Execute the query to remove Rent_type items
				$db->execute();

				// [Interpretation 4252] Remove Rent_type items from the ucm history table
				$rent_type_condition = array( $db->quoteName('ucm_type_id') . ' = ' . $rent_type_id);
				// [Interpretation 4254] Create a new query object.
				$query = $db->getQuery(true);
				$query->delete($db->quoteName('#__ucm_history'));
				$query->where($rent_type_condition);
				$db->setQuery($query);
				// [Interpretation 4259] Execute the query to remove Rent_type items
				$db->execute();
			}
		}

		// [Interpretation 4167] Create a new query object.
		$query = $db->getQuery(true);
		// [Interpretation 4169] Select id from content type table
		$query->select($db->quoteName('type_id'));
		$query->from($db->quoteName('#__content_types'));
		// [Interpretation 4172] Where Feature_type alias is found
		$query->where( $db->quoteName('type_alias') . ' = '. $db->quote('com_mostwantedrealestate.feature_type') );
		$db->setQuery($query);
		// [Interpretation 4175] Execute query to see if alias is found
		$db->execute();
		$feature_type_found = $db->getNumRows();
		// [Interpretation 4178] Now check if there were any rows
		if ($feature_type_found)
		{
			// [Interpretation 4181] Since there are load the needed  feature_type type ids
			$feature_type_ids = $db->loadColumn();
			// [Interpretation 4185] Remove Feature_type from the content type table
			$feature_type_condition = array( $db->quoteName('type_alias') . ' = '. $db->quote('com_mostwantedrealestate.feature_type') );
			// [Interpretation 4187] Create a new query object.
			$query = $db->getQuery(true);
			$query->delete($db->quoteName('#__content_types'));
			$query->where($feature_type_condition);
			$db->setQuery($query);
			// [Interpretation 4192] Execute the query to remove Feature_type items
			$feature_type_done = $db->execute();
			if ($feature_type_done);
			{
				// [Interpretation 4196] If succesfully remove Feature_type add queued success message.
				$app->enqueueMessage(JText::_('The (com_mostwantedrealestate.feature_type) type alias was removed from the <b>#__content_type</b> table'));
			}

			// [Interpretation 4202] Remove Feature_type items from the contentitem tag map table
			$feature_type_condition = array( $db->quoteName('type_alias') . ' = '. $db->quote('com_mostwantedrealestate.feature_type') );
			// [Interpretation 4204] Create a new query object.
			$query = $db->getQuery(true);
			$query->delete($db->quoteName('#__contentitem_tag_map'));
			$query->where($feature_type_condition);
			$db->setQuery($query);
			// [Interpretation 4209] Execute the query to remove Feature_type items
			$feature_type_done = $db->execute();
			if ($feature_type_done);
			{
				// [Interpretation 4213] If succesfully remove Feature_type add queued success message.
				$app->enqueueMessage(JText::_('The (com_mostwantedrealestate.feature_type) type alias was removed from the <b>#__contentitem_tag_map</b> table'));
			}

			// [Interpretation 4219] Remove Feature_type items from the ucm content table
			$feature_type_condition = array( $db->quoteName('core_type_alias') . ' = ' . $db->quote('com_mostwantedrealestate.feature_type') );
			// [Interpretation 4221] Create a new query object.
			$query = $db->getQuery(true);
			$query->delete($db->quoteName('#__ucm_content'));
			$query->where($feature_type_condition);
			$db->setQuery($query);
			// [Interpretation 4226] Execute the query to remove Feature_type items
			$feature_type_done = $db->execute();
			if ($feature_type_done);
			{
				// [Interpretation 4230] If succesfully remove Feature_type add queued success message.
				$app->enqueueMessage(JText::_('The (com_mostwantedrealestate.feature_type) type alias was removed from the <b>#__ucm_content</b> table'));
			}

			// [Interpretation 4236] Make sure that all the Feature_type items are cleared from DB
			foreach ($feature_type_ids as $feature_type_id)
			{
				// [Interpretation 4241] Remove Feature_type items from the ucm base table
				$feature_type_condition = array( $db->quoteName('ucm_type_id') . ' = ' . $feature_type_id);
				// [Interpretation 4243] Create a new query object.
				$query = $db->getQuery(true);
				$query->delete($db->quoteName('#__ucm_base'));
				$query->where($feature_type_condition);
				$db->setQuery($query);
				// [Interpretation 4248] Execute the query to remove Feature_type items
				$db->execute();

				// [Interpretation 4252] Remove Feature_type items from the ucm history table
				$feature_type_condition = array( $db->quoteName('ucm_type_id') . ' = ' . $feature_type_id);
				// [Interpretation 4254] Create a new query object.
				$query = $db->getQuery(true);
				$query->delete($db->quoteName('#__ucm_history'));
				$query->where($feature_type_condition);
				$db->setQuery($query);
				// [Interpretation 4259] Execute the query to remove Feature_type items
				$db->execute();
			}
		}

		// [Interpretation 4167] Create a new query object.
		$query = $db->getQuery(true);
		// [Interpretation 4169] Select id from content type table
		$query->select($db->quoteName('type_id'));
		$query->from($db->quoteName('#__content_types'));
		// [Interpretation 4172] Where Favorite_listing alias is found
		$query->where( $db->quoteName('type_alias') . ' = '. $db->quote('com_mostwantedrealestate.favorite_listing') );
		$db->setQuery($query);
		// [Interpretation 4175] Execute query to see if alias is found
		$db->execute();
		$favorite_listing_found = $db->getNumRows();
		// [Interpretation 4178] Now check if there were any rows
		if ($favorite_listing_found)
		{
			// [Interpretation 4181] Since there are load the needed  favorite_listing type ids
			$favorite_listing_ids = $db->loadColumn();
			// [Interpretation 4185] Remove Favorite_listing from the content type table
			$favorite_listing_condition = array( $db->quoteName('type_alias') . ' = '. $db->quote('com_mostwantedrealestate.favorite_listing') );
			// [Interpretation 4187] Create a new query object.
			$query = $db->getQuery(true);
			$query->delete($db->quoteName('#__content_types'));
			$query->where($favorite_listing_condition);
			$db->setQuery($query);
			// [Interpretation 4192] Execute the query to remove Favorite_listing items
			$favorite_listing_done = $db->execute();
			if ($favorite_listing_done);
			{
				// [Interpretation 4196] If succesfully remove Favorite_listing add queued success message.
				$app->enqueueMessage(JText::_('The (com_mostwantedrealestate.favorite_listing) type alias was removed from the <b>#__content_type</b> table'));
			}

			// [Interpretation 4202] Remove Favorite_listing items from the contentitem tag map table
			$favorite_listing_condition = array( $db->quoteName('type_alias') . ' = '. $db->quote('com_mostwantedrealestate.favorite_listing') );
			// [Interpretation 4204] Create a new query object.
			$query = $db->getQuery(true);
			$query->delete($db->quoteName('#__contentitem_tag_map'));
			$query->where($favorite_listing_condition);
			$db->setQuery($query);
			// [Interpretation 4209] Execute the query to remove Favorite_listing items
			$favorite_listing_done = $db->execute();
			if ($favorite_listing_done);
			{
				// [Interpretation 4213] If succesfully remove Favorite_listing add queued success message.
				$app->enqueueMessage(JText::_('The (com_mostwantedrealestate.favorite_listing) type alias was removed from the <b>#__contentitem_tag_map</b> table'));
			}

			// [Interpretation 4219] Remove Favorite_listing items from the ucm content table
			$favorite_listing_condition = array( $db->quoteName('core_type_alias') . ' = ' . $db->quote('com_mostwantedrealestate.favorite_listing') );
			// [Interpretation 4221] Create a new query object.
			$query = $db->getQuery(true);
			$query->delete($db->quoteName('#__ucm_content'));
			$query->where($favorite_listing_condition);
			$db->setQuery($query);
			// [Interpretation 4226] Execute the query to remove Favorite_listing items
			$favorite_listing_done = $db->execute();
			if ($favorite_listing_done);
			{
				// [Interpretation 4230] If succesfully remove Favorite_listing add queued success message.
				$app->enqueueMessage(JText::_('The (com_mostwantedrealestate.favorite_listing) type alias was removed from the <b>#__ucm_content</b> table'));
			}

			// [Interpretation 4236] Make sure that all the Favorite_listing items are cleared from DB
			foreach ($favorite_listing_ids as $favorite_listing_id)
			{
				// [Interpretation 4241] Remove Favorite_listing items from the ucm base table
				$favorite_listing_condition = array( $db->quoteName('ucm_type_id') . ' = ' . $favorite_listing_id);
				// [Interpretation 4243] Create a new query object.
				$query = $db->getQuery(true);
				$query->delete($db->quoteName('#__ucm_base'));
				$query->where($favorite_listing_condition);
				$db->setQuery($query);
				// [Interpretation 4248] Execute the query to remove Favorite_listing items
				$db->execute();

				// [Interpretation 4252] Remove Favorite_listing items from the ucm history table
				$favorite_listing_condition = array( $db->quoteName('ucm_type_id') . ' = ' . $favorite_listing_id);
				// [Interpretation 4254] Create a new query object.
				$query = $db->getQuery(true);
				$query->delete($db->quoteName('#__ucm_history'));
				$query->where($favorite_listing_condition);
				$db->setQuery($query);
				// [Interpretation 4259] Execute the query to remove Favorite_listing items
				$db->execute();
			}
		}

		// [Interpretation 4167] Create a new query object.
		$query = $db->getQuery(true);
		// [Interpretation 4169] Select id from content type table
		$query->select($db->quoteName('type_id'));
		$query->from($db->quoteName('#__content_types'));
		// [Interpretation 4172] Where Image alias is found
		$query->where( $db->quoteName('type_alias') . ' = '. $db->quote('com_mostwantedrealestate.image') );
		$db->setQuery($query);
		// [Interpretation 4175] Execute query to see if alias is found
		$db->execute();
		$image_found = $db->getNumRows();
		// [Interpretation 4178] Now check if there were any rows
		if ($image_found)
		{
			// [Interpretation 4181] Since there are load the needed  image type ids
			$image_ids = $db->loadColumn();
			// [Interpretation 4185] Remove Image from the content type table
			$image_condition = array( $db->quoteName('type_alias') . ' = '. $db->quote('com_mostwantedrealestate.image') );
			// [Interpretation 4187] Create a new query object.
			$query = $db->getQuery(true);
			$query->delete($db->quoteName('#__content_types'));
			$query->where($image_condition);
			$db->setQuery($query);
			// [Interpretation 4192] Execute the query to remove Image items
			$image_done = $db->execute();
			if ($image_done);
			{
				// [Interpretation 4196] If succesfully remove Image add queued success message.
				$app->enqueueMessage(JText::_('The (com_mostwantedrealestate.image) type alias was removed from the <b>#__content_type</b> table'));
			}

			// [Interpretation 4202] Remove Image items from the contentitem tag map table
			$image_condition = array( $db->quoteName('type_alias') . ' = '. $db->quote('com_mostwantedrealestate.image') );
			// [Interpretation 4204] Create a new query object.
			$query = $db->getQuery(true);
			$query->delete($db->quoteName('#__contentitem_tag_map'));
			$query->where($image_condition);
			$db->setQuery($query);
			// [Interpretation 4209] Execute the query to remove Image items
			$image_done = $db->execute();
			if ($image_done);
			{
				// [Interpretation 4213] If succesfully remove Image add queued success message.
				$app->enqueueMessage(JText::_('The (com_mostwantedrealestate.image) type alias was removed from the <b>#__contentitem_tag_map</b> table'));
			}

			// [Interpretation 4219] Remove Image items from the ucm content table
			$image_condition = array( $db->quoteName('core_type_alias') . ' = ' . $db->quote('com_mostwantedrealestate.image') );
			// [Interpretation 4221] Create a new query object.
			$query = $db->getQuery(true);
			$query->delete($db->quoteName('#__ucm_content'));
			$query->where($image_condition);
			$db->setQuery($query);
			// [Interpretation 4226] Execute the query to remove Image items
			$image_done = $db->execute();
			if ($image_done);
			{
				// [Interpretation 4230] If succesfully remove Image add queued success message.
				$app->enqueueMessage(JText::_('The (com_mostwantedrealestate.image) type alias was removed from the <b>#__ucm_content</b> table'));
			}

			// [Interpretation 4236] Make sure that all the Image items are cleared from DB
			foreach ($image_ids as $image_id)
			{
				// [Interpretation 4241] Remove Image items from the ucm base table
				$image_condition = array( $db->quoteName('ucm_type_id') . ' = ' . $image_id);
				// [Interpretation 4243] Create a new query object.
				$query = $db->getQuery(true);
				$query->delete($db->quoteName('#__ucm_base'));
				$query->where($image_condition);
				$db->setQuery($query);
				// [Interpretation 4248] Execute the query to remove Image items
				$db->execute();

				// [Interpretation 4252] Remove Image items from the ucm history table
				$image_condition = array( $db->quoteName('ucm_type_id') . ' = ' . $image_id);
				// [Interpretation 4254] Create a new query object.
				$query = $db->getQuery(true);
				$query->delete($db->quoteName('#__ucm_history'));
				$query->where($image_condition);
				$db->setQuery($query);
				// [Interpretation 4259] Execute the query to remove Image items
				$db->execute();
			}
		}

		// [Interpretation 4267] If All related items was removed queued success message.
		$app->enqueueMessage(JText::_('All related items was removed from the <b>#__ucm_base</b> table'));
		$app->enqueueMessage(JText::_('All related items was removed from the <b>#__ucm_history</b> table'));

		// [Interpretation 4272] Remove mostwantedrealestate assets from the assets table
		$mostwantedrealestate_condition = array( $db->quoteName('name') . ' LIKE ' . $db->quote('com_mostwantedrealestate%') );

		// [Interpretation 4274] Create a new query object.
		$query = $db->getQuery(true);
		$query->delete($db->quoteName('#__assets'));
		$query->where($mostwantedrealestate_condition);
		$db->setQuery($query);
		$image_done = $db->execute();
		if ($image_done);
		{
			// [Interpretation 4282] If succesfully remove mostwantedrealestate add queued success message.
			$app->enqueueMessage(JText::_('All related items was removed from the <b>#__assets</b> table'));
		}

		// little notice as after service, in case of bad experience with component.
		echo '<h2>Did something go wrong? Are you disappointed?</h2>
		<p>Please let me know at <a href="mailto:sales@mwweb.host">sales@mwweb.host</a>.
		<br />We at Most Wanted Web Services, Inc. are committed to building extensions that performs proficiently! You can help us, really!
		<br />Send me your thoughts on improvements that is needed, trust me, I will be very grateful!
		<br />Visit us at <a href="http://mostwantedrealestatesites.com" target="_blank">http://mostwantedrealestatesites.com</a> today!</p>';
	}

	/**
	 * method to update the component
	 *
	 * @return void
	 */
	function update($parent)
	{
		
	}

	/**
	 * method to run before an install/update/uninstall method
	 *
	 * @return void
	 */
	function preflight($type, $parent)
	{
		// get application
		$app = JFactory::getApplication();
		// is redundant ...hmmm
		if ($type == 'uninstall')
		{
			return true;
		}
		// the default for both install and update
		$jversion = new JVersion();
		if (!$jversion->isCompatible('3.6.0'))
		{
			$app->enqueueMessage('Please upgrade to at least Joomla! 3.6.0 before continuing!', 'error');
			return false;
		}
		// do any updates needed
		if ($type == 'update')
		{
		}
		// do any install needed
		if ($type == 'install')
		{
		}
	}

	/**
	 * method to run after an install/update/uninstall method
	 *
	 * @return void
	 */
	function postflight($type, $parent)
	{
		// set the default component settings
		if ($type == 'install')
		{

			// [Interpretation 4006] Get The Database object
			$db = JFactory::getDbo();

			// [Interpretation 4013] Create the country content type object.
			$country = new stdClass();
			$country->type_title = 'Mostwantedrealestate Country';
			$country->type_alias = 'com_mostwantedrealestate.country';
			$country->table = '{"special": {"dbtable": "#__mostwantedrealestate_country","key": "id","type": "Country","prefix": "mostwantedrealestateTable","config": "array()"},"common": {"dbtable": "#__ucm_content","key": "ucm_id","type": "Corecontent","prefix": "JTable","config": "array()"}}';
			$country->field_mappings = '{"common": {"core_content_item_id": "id","core_title": "name","core_state": "published","core_alias": "alias","core_created_time": "created","core_modified_time": "modified","core_body": "description","core_hits": "hits","core_publish_up": "null","core_publish_down": "null","core_access": "access","core_params": "params","core_featured": "null","core_metadata": "metadata","core_language": "null","core_images": "null","core_urls": "null","core_version": "version","core_ordering": "ordering","core_metakey": "metakey","core_metadesc": "metadesc","core_catid": "null","core_xreference": "null","asset_id": "asset_id"},"special": {"name":"name","alias":"alias","latitude":"latitude","image":"image","owncoords":"owncoords","description":"description","longitude":"longitude"}}';
			$country->router = 'MostwantedrealestateHelperRoute::getCountryRoute';
			$country->content_history_options = '{"formFile": "administrator/components/com_mostwantedrealestate/models/forms/country.xml","hideFields": ["asset_id","checked_out","checked_out_time","version"],"ignoreChanges": ["modified_by","modified","checked_out","checked_out_time","version","hits"],"convertToInt": ["published","ordering","owncoords"],"displayLookup": [{"sourceColumn": "created_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "access","targetTable": "#__viewlevels","targetColumn": "id","displayColumn": "title"},{"sourceColumn": "modified_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"}]}';

			// [Interpretation 4031] Set the object into the content types table.
			$country_Inserted = $db->insertObject('#__content_types', $country);

			// [Interpretation 4013] Create the state content type object.
			$state = new stdClass();
			$state->type_title = 'Mostwantedrealestate State';
			$state->type_alias = 'com_mostwantedrealestate.state';
			$state->table = '{"special": {"dbtable": "#__mostwantedrealestate_state","key": "id","type": "State","prefix": "mostwantedrealestateTable","config": "array()"},"common": {"dbtable": "#__ucm_content","key": "ucm_id","type": "Corecontent","prefix": "JTable","config": "array()"}}';
			$state->field_mappings = '{"common": {"core_content_item_id": "id","core_title": "name","core_state": "published","core_alias": "alias","core_created_time": "created","core_modified_time": "modified","core_body": "description","core_hits": "hits","core_publish_up": "null","core_publish_down": "null","core_access": "access","core_params": "params","core_featured": "null","core_metadata": "metadata","core_language": "null","core_images": "null","core_urls": "null","core_version": "version","core_ordering": "ordering","core_metakey": "metakey","core_metadesc": "metadesc","core_catid": "null","core_xreference": "null","asset_id": "asset_id"},"special": {"name":"name","alias":"alias","description":"description","latitude":"latitude","image":"image","owncoords":"owncoords","countryid":"countryid","longitude":"longitude"}}';
			$state->router = 'MostwantedrealestateHelperRoute::getStateRoute';
			$state->content_history_options = '{"formFile": "administrator/components/com_mostwantedrealestate/models/forms/state.xml","hideFields": ["asset_id","checked_out","checked_out_time","version"],"ignoreChanges": ["modified_by","modified","checked_out","checked_out_time","version","hits"],"convertToInt": ["published","ordering","owncoords","countryid"],"displayLookup": [{"sourceColumn": "created_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "access","targetTable": "#__viewlevels","targetColumn": "id","displayColumn": "title"},{"sourceColumn": "modified_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "countryid","targetTable": "#__mostwantedrealestate_country","targetColumn": "id","displayColumn": "name"}]}';

			// [Interpretation 4031] Set the object into the content types table.
			$state_Inserted = $db->insertObject('#__content_types', $state);

			// [Interpretation 4013] Create the city content type object.
			$city = new stdClass();
			$city->type_title = 'Mostwantedrealestate City';
			$city->type_alias = 'com_mostwantedrealestate.city';
			$city->table = '{"special": {"dbtable": "#__mostwantedrealestate_city","key": "id","type": "City","prefix": "mostwantedrealestateTable","config": "array()"},"common": {"dbtable": "#__ucm_content","key": "ucm_id","type": "Corecontent","prefix": "JTable","config": "array()"}}';
			$city->field_mappings = '{"common": {"core_content_item_id": "id","core_title": "name","core_state": "published","core_alias": "alias","core_created_time": "created","core_modified_time": "modified","core_body": "description","core_hits": "hits","core_publish_up": "null","core_publish_down": "null","core_access": "access","core_params": "params","core_featured": "null","core_metadata": "metadata","core_language": "null","core_images": "null","core_urls": "null","core_version": "version","core_ordering": "ordering","core_metakey": "metakey","core_metadesc": "metadesc","core_catid": "null","core_xreference": "null","asset_id": "asset_id"},"special": {"name":"name","alias":"alias","latitude":"latitude","description":"description","image":"image","owncoords":"owncoords","stateid":"stateid","longitude":"longitude"}}';
			$city->router = 'MostwantedrealestateHelperRoute::getCityRoute';
			$city->content_history_options = '{"formFile": "administrator/components/com_mostwantedrealestate/models/forms/city.xml","hideFields": ["asset_id","checked_out","checked_out_time","version"],"ignoreChanges": ["modified_by","modified","checked_out","checked_out_time","version","hits"],"convertToInt": ["published","ordering","owncoords","stateid"],"displayLookup": [{"sourceColumn": "created_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "access","targetTable": "#__viewlevels","targetColumn": "id","displayColumn": "title"},{"sourceColumn": "modified_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "stateid","targetTable": "#__mostwantedrealestate_state","targetColumn": "id","displayColumn": "name"}]}';

			// [Interpretation 4031] Set the object into the content types table.
			$city_Inserted = $db->insertObject('#__content_types', $city);

			// [Interpretation 4013] Create the agency content type object.
			$agency = new stdClass();
			$agency->type_title = 'Mostwantedrealestate Agency';
			$agency->type_alias = 'com_mostwantedrealestate.agency';
			$agency->table = '{"special": {"dbtable": "#__mostwantedrealestate_agency","key": "id","type": "Agency","prefix": "mostwantedrealestateTable","config": "array()"},"common": {"dbtable": "#__ucm_content","key": "ucm_id","type": "Corecontent","prefix": "JTable","config": "array()"}}';
			$agency->field_mappings = '{"common": {"core_content_item_id": "id","core_title": "name","core_state": "published","core_alias": "alias","core_created_time": "created","core_modified_time": "modified","core_body": "description","core_hits": "hits","core_publish_up": "null","core_publish_down": "null","core_access": "access","core_params": "params","core_featured": "null","core_metadata": "metadata","core_language": "null","core_images": "null","core_urls": "null","core_version": "version","core_ordering": "ordering","core_metakey": "metakey","core_metadesc": "metadesc","core_catid": "null","core_xreference": "null","asset_id": "asset_id"},"special": {"name":"name","street":"street","cityid":"cityid","stateid":"stateid","postcode":"postcode","owncoords":"owncoords","streettwo":"streettwo","image":"image","default_agency_yn":"default_agency_yn","longitude":"longitude","countryid":"countryid","alias":"alias","description":"description","email":"email","website":"website","featured":"featured","license":"license","rets_source":"rets_source","phone":"phone","latitude":"latitude","fax":"fax"}}';
			$agency->router = 'MostwantedrealestateHelperRoute::getAgencyRoute';
			$agency->content_history_options = '{"formFile": "administrator/components/com_mostwantedrealestate/models/forms/agency.xml","hideFields": ["asset_id","checked_out","checked_out_time","version"],"ignoreChanges": ["modified_by","modified","checked_out","checked_out_time","version","hits"],"convertToInt": ["published","ordering","cityid","stateid","owncoords","default_agency_yn","countryid","featured"],"displayLookup": [{"sourceColumn": "created_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "access","targetTable": "#__viewlevels","targetColumn": "id","displayColumn": "title"},{"sourceColumn": "modified_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "cityid","targetTable": "#__mostwantedrealestate_city","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "stateid","targetTable": "#__mostwantedrealestate_state","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "countryid","targetTable": "#__mostwantedrealestate_country","targetColumn": "id","displayColumn": "name"}]}';

			// [Interpretation 4031] Set the object into the content types table.
			$agency_Inserted = $db->insertObject('#__content_types', $agency);

			// [Interpretation 4013] Create the agent content type object.
			$agent = new stdClass();
			$agent->type_title = 'Mostwantedrealestate Agent';
			$agent->type_alias = 'com_mostwantedrealestate.agent';
			$agent->table = '{"special": {"dbtable": "#__mostwantedrealestate_agent","key": "id","type": "Agent","prefix": "mostwantedrealestateTable","config": "array()"},"common": {"dbtable": "#__ucm_content","key": "ucm_id","type": "Corecontent","prefix": "JTable","config": "array()"}}';
			$agent->field_mappings = '{"common": {"core_content_item_id": "id","core_title": "name","core_state": "published","core_alias": "alias","core_created_time": "created","core_modified_time": "modified","core_body": "bio","core_hits": "hits","core_publish_up": "null","core_publish_down": "null","core_access": "access","core_params": "params","core_featured": "null","core_metadata": "metadata","core_language": "null","core_images": "null","core_urls": "null","core_version": "version","core_ordering": "ordering","core_metakey": "metakey","core_metadesc": "metadesc","core_catid": "catid","core_xreference": "null","asset_id": "asset_id"},"special": {"name":"name","cityid":"cityid","stateid":"stateid","uid":"uid","longitude":"longitude","postcode":"postcode","viewad":"viewad","streettwo":"streettwo","countryid":"countryid","skype":"skype","owncoords":"owncoords","street":"street","fbook":"fbook","email":"email","youtube":"youtube","phone":"phone","website":"website","mobile":"mobile","default_agent_yn":"default_agent_yn","fax":"fax","rets_source":"rets_source","image":"image","latitude":"latitude","bio":"bio","pinterest":"pinterest","twitter":"twitter","alias":"alias","linkedin":"linkedin","gplus":"gplus","agencyid":"agencyid","instagram":"instagram","featured":"featured","blog":"blog"}}';
			$agent->router = 'MostwantedrealestateHelperRoute::getAgentRoute';
			$agent->content_history_options = '{"formFile": "administrator/components/com_mostwantedrealestate/models/forms/agent.xml","hideFields": ["asset_id","checked_out","checked_out_time","version"],"ignoreChanges": ["modified_by","modified","checked_out","checked_out_time","version","hits"],"convertToInt": ["published","ordering","cityid","stateid","uid","viewad","countryid","owncoords","default_agent_yn","catid","agencyid","featured"],"displayLookup": [{"sourceColumn": "catid","targetTable": "#__categories","targetColumn": "id","displayColumn": "title"},{"sourceColumn": "created_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "access","targetTable": "#__viewlevels","targetColumn": "id","displayColumn": "title"},{"sourceColumn": "modified_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "cityid","targetTable": "#__mostwantedrealestate_city","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "stateid","targetTable": "#__mostwantedrealestate_state","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "countryid","targetTable": "#__mostwantedrealestate_country","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "agencyid","targetTable": "#__mostwantedrealestate_agency","targetColumn": "id","displayColumn": "name"}]}';

			// [Interpretation 4031] Set the object into the content types table.
			$agent_Inserted = $db->insertObject('#__content_types', $agent);

			// [Interpretation 4013] Create the agent category content type object.
			$agent_category = new stdClass();
			$agent_category->type_title = 'Mostwantedrealestate Agent Catid';
			$agent_category->type_alias = 'com_mostwantedrealestate.agents.category';
			$agent_category->table = '{"special":{"dbtable":"#__categories","key":"id","type":"Category","prefix":"JTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}';
			$agent_category->field_mappings = '{"common":{"core_content_item_id":"id","core_title":"title","core_state":"published","core_alias":"alias","core_created_time":"created_time","core_modified_time":"modified_time","core_body":"description", "core_hits":"hits","core_publish_up":"null","core_publish_down":"null","core_access":"access", "core_params":"params", "core_featured":"null", "core_metadata":"metadata", "core_language":"language", "core_images":"null", "core_urls":"null", "core_version":"version", "core_ordering":"null", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"parent_id", "core_xreference":"null", "asset_id":"asset_id"}, "special":{"parent_id":"parent_id","lft":"lft","rgt":"rgt","level":"level","path":"path","extension":"extension","note":"note"}}';
			$agent_category->router = 'MostwantedrealestateHelperRoute::getCategoryRoute';
			$agent_category->content_history_options = '{"formFile":"administrator\/components\/com_categories\/models\/forms\/category.xml", "hideFields":["asset_id","checked_out","checked_out_time","version","lft","rgt","level","path","extension"], "ignoreChanges":["modified_user_id", "modified_time", "checked_out", "checked_out_time", "version", "hits", "path"],"convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"created_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"parent_id","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"}]}';

			// [Interpretation 4031] Set the object into the content types table.
			$agent_category_Inserted = $db->insertObject('#__content_types', $agent_category);

			// [Interpretation 4013] Create the property content type object.
			$property = new stdClass();
			$property->type_title = 'Mostwantedrealestate Property';
			$property->type_alias = 'com_mostwantedrealestate.property';
			$property->table = '{"special": {"dbtable": "#__mostwantedrealestate_property","key": "id","type": "Property","prefix": "mostwantedrealestateTable","config": "array()"},"common": {"dbtable": "#__ucm_content","key": "ucm_id","type": "Corecontent","prefix": "JTable","config": "array()"}}';
			$property->field_mappings = '{"common": {"core_content_item_id": "id","core_title": "name","core_state": "published","core_alias": "null","core_created_time": "created","core_modified_time": "modified","core_body": "propdesc","core_hits": "hits","core_publish_up": "null","core_publish_down": "null","core_access": "access","core_params": "params","core_featured": "null","core_metadata": "metadata","core_language": "null","core_images": "null","core_urls": "null","core_version": "version","core_ordering": "ordering","core_metakey": "metakey","core_metadesc": "metadesc","core_catid": "catid","core_xreference": "null","asset_id": "asset_id"},"special": {"name":"name","mls_id":"mls_id","cityid":"cityid","stateid":"stateid","featured":"featured","pmstartdate":"pmstartdate","fixtures":"fixtures","mls_image":"mls_image","county":"county","viewad":"viewad","flplone":"flplone","showprice":"showprice","customfive":"customfive","price":"price","totalrents":"totalrents","closeprice":"closeprice","offpeak":"offpeak","closedate":"closedate","taxannual":"taxannual","priceview":"priceview","ctport":"ctport","mls_disclaimer":"mls_disclaimer","storage":"storage","postcode":"postcode","currentuse":"currentuse","alias":"alias","countryid":"countryid","grazing":"grazing","rets_source":"rets_source","mediaurl":"mediaurl","listingtype":"listingtype","openhouse":"openhouse","streettwo":"streettwo","averageutilelec":"averageutilelec","propdesc":"propdesc","viewbooking":"viewbooking","highschool":"highschool","returns":"returns","customseven":"customseven","sewer":"sewer","soleagency":"soleagency","listoffice":"listoffice","bldg_name":"bldg_name","agent":"agent","bustype":"bustype","colistagent":"colistagent","percentwarehouse":"percentwarehouse","mkt_stats":"mkt_stats","parkinggarage":"parkinggarage","trans_type":"trans_type","tenancytype":"tenancytype","owncoords":"owncoords","landtype":"landtype","latitude":"latitude","rainfall":"rainfall","longitude":"longitude","irrigation":"irrigation","deposit":"deposit","bedrooms":"bedrooms","pdfinfoone":"pdfinfoone","bathrooms":"bathrooms","covenantsyn":"covenantsyn","fullbaths":"fullbaths","hofees":"hofees","thqtrbaths":"thqtrbaths","utilities":"utilities","halfbaths":"halfbaths","terms":"terms","qtrbaths":"qtrbaths","propmgt_price":"propmgt_price","squarefeet":"squarefeet","private":"private","sqftlower":"sqftlower","elementary":"elementary","sqftmainlevel":"sqftmainlevel","street":"street","sqftupper":"sqftupper","customsix":"customsix","style":"style","customeight":"customeight","year":"year","waterresources":"waterresources","yearremodeled":"yearremodeled","zoning":"zoning","exteriorfinish":"exteriorfinish","roof":"roof","flooring":"flooring","porchpatio":"porchpatio","takings":"takings","frontage":"frontage","netprofit":"netprofit","waterfront":"waterfront","bussubtype":"bussubtype","waterfronttype":"waterfronttype","percentoffice":"percentoffice","welldepth":"welldepth","loadingfac":"loadingfac","subdivision":"subdivision","carryingcap":"carryingcap","landareasqft":"landareasqft","bldgsqft":"bldgsqft","acrestotal":"acrestotal","numunits":"numunits","lotdimensions":"lotdimensions","tentantpdutilities":"tentantpdutilities","totalrooms":"totalrooms","otherrooms":"otherrooms","stock":"stock","livingarea":"livingarea","fittings":"fittings","ensuite":"ensuite","soiltype":"soiltype","garagetype":"garagetype","cropping":"cropping","parkingcarport":"parkingcarport","rent_type":"rent_type","stories":"stories","freq":"freq","basementandfoundation":"basementandfoundation","sleeps":"sleeps","basementsize":"basementsize","mediatype":"mediatype","basementpctfinished":"basementpctfinished","pdfinfotwo":"pdfinfotwo","heating":"heating","flpltwo":"flpltwo","cooling":"cooling","houseconstruction":"houseconstruction","fencing":"fencing","annualinsurance":"annualinsurance","phoneavailableyn":"phoneavailableyn","taxyear":"taxyear","garbagedisposalyn":"garbagedisposalyn","electricservice":"electricservice","familyroompresent":"familyroompresent","averageutilgas":"averageutilgas","laundryroompresent":"laundryroompresent","pm_price_override":"pm_price_override","kitchenpresent":"kitchenpresent","pmenddate":"pmenddate","livingroompresent":"livingroompresent","propmgt_description":"propmgt_description","parkingspaceyn":"parkingspaceyn","availdate":"availdate","customone":"customone","ctown":"ctown","customtwo":"customtwo","schooldist":"schooldist","customthree":"customthree","midschool":"midschool","customfour":"customfour","university":"university"}}';
			$property->router = 'MostwantedrealestateHelperRoute::getPropertyRoute';
			$property->content_history_options = '{"formFile": "administrator/components/com_mostwantedrealestate/models/forms/property.xml","hideFields": ["asset_id","checked_out","checked_out_time","version"],"ignoreChanges": ["modified_by","modified","checked_out","checked_out_time","version","hits"],"convertToInt": ["published","ordering","cityid","stateid","catid","featured","viewad","showprice","countryid","listingtype","openhouse","viewbooking","soleagency","listoffice","agent","colistagent","mkt_stats","trans_type","owncoords","bedrooms","covenantsyn","fullbaths","thqtrbaths","halfbaths","qtrbaths","waterfront","ensuite","rent_type","stories","freq","sleeps","mediatype","phoneavailableyn","garbagedisposalyn","familyroompresent","laundryroompresent","kitchenpresent","livingroompresent","parkingspaceyn"],"displayLookup": [{"sourceColumn": "catid","targetTable": "#__categories","targetColumn": "id","displayColumn": "title"},{"sourceColumn": "created_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "access","targetTable": "#__viewlevels","targetColumn": "id","displayColumn": "title"},{"sourceColumn": "modified_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "cityid","targetTable": "#__mostwantedrealestate_city","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "stateid","targetTable": "#__mostwantedrealestate_state","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "countryid","targetTable": "#__mostwantedrealestate_country","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "sewer","targetTable": "#__mostwantedrealestate_feature_type","targetColumn": "id","displayColumn": "featurename"},{"sourceColumn": "listoffice","targetTable": "#__mostwantedrealestate_agency","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "agent","targetTable": "#__mostwantedrealestate_agent","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "colistagent","targetTable": "#__mostwantedrealestate_agent","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "mkt_stats","targetTable": "#__mostwantedrealestate_market_status","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "trans_type","targetTable": "#__mostwantedrealestate_transaction_type","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "terms","targetTable": "#__mostwantedrealestate_feature_type","targetColumn": "id","displayColumn": "featurename"},{"sourceColumn": "style","targetTable": "#__mostwantedrealestate_feature_type","targetColumn": "id","displayColumn": "featurename"},{"sourceColumn": "waterresources","targetTable": "#__mostwantedrealestate_feature_type","targetColumn": "id","displayColumn": "featurename"},{"sourceColumn": "zoning","targetTable": "#__mostwantedrealestate_feature_type","targetColumn": "id","displayColumn": "featurename"},{"sourceColumn": "exteriorfinish","targetTable": "#__mostwantedrealestate_feature_type","targetColumn": "id","displayColumn": "featurename"},{"sourceColumn": "roof","targetTable": "#__mostwantedrealestate_feature_type","targetColumn": "id","displayColumn": "featurename"},{"sourceColumn": "porchpatio","targetTable": "#__mostwantedrealestate_feature_type","targetColumn": "id","displayColumn": "featurename"},{"sourceColumn": "frontage","targetTable": "#__mostwantedrealestate_feature_type","targetColumn": "id","displayColumn": "featurename"},{"sourceColumn": "garagetype","targetTable": "#__mostwantedrealestate_feature_type","targetColumn": "id","displayColumn": "featurename"},{"sourceColumn": "rent_type","targetTable": "#__mostwantedrealestate_rent_type","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "freq","targetTable": "#__mostwantedrealestate_rental_frequency","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "basementandfoundation","targetTable": "#__mostwantedrealestate_feature_type","targetColumn": "id","displayColumn": "featurename"},{"sourceColumn": "heating","targetTable": "#__mostwantedrealestate_feature_type","targetColumn": "id","displayColumn": "featurename"},{"sourceColumn": "cooling","targetTable": "#__mostwantedrealestate_feature_type","targetColumn": "id","displayColumn": "featurename"},{"sourceColumn": "fencing","targetTable": "#__mostwantedrealestate_feature_type","targetColumn": "id","displayColumn": "featurename"}]}';

			// [Interpretation 4031] Set the object into the content types table.
			$property_Inserted = $db->insertObject('#__content_types', $property);

			// [Interpretation 4013] Create the property category content type object.
			$property_category = new stdClass();
			$property_category->type_title = 'Mostwantedrealestate Property Catid';
			$property_category->type_alias = 'com_mostwantedrealestate.properties.category';
			$property_category->table = '{"special":{"dbtable":"#__categories","key":"id","type":"Category","prefix":"JTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}';
			$property_category->field_mappings = '{"common":{"core_content_item_id":"id","core_title":"title","core_state":"published","core_alias":"alias","core_created_time":"created_time","core_modified_time":"modified_time","core_body":"description", "core_hits":"hits","core_publish_up":"null","core_publish_down":"null","core_access":"access", "core_params":"params", "core_featured":"null", "core_metadata":"metadata", "core_language":"language", "core_images":"null", "core_urls":"null", "core_version":"version", "core_ordering":"null", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"parent_id", "core_xreference":"null", "asset_id":"asset_id"}, "special":{"parent_id":"parent_id","lft":"lft","rgt":"rgt","level":"level","path":"path","extension":"extension","note":"note"}}';
			$property_category->router = 'MostwantedrealestateHelperRoute::getCategoryRoute';
			$property_category->content_history_options = '{"formFile":"administrator\/components\/com_categories\/models\/forms\/category.xml", "hideFields":["asset_id","checked_out","checked_out_time","version","lft","rgt","level","path","extension"], "ignoreChanges":["modified_user_id", "modified_time", "checked_out", "checked_out_time", "version", "hits", "path"],"convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"created_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"parent_id","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"}]}';

			// [Interpretation 4031] Set the object into the content types table.
			$property_category_Inserted = $db->insertObject('#__content_types', $property_category);

			// [Interpretation 4013] Create the market_status content type object.
			$market_status = new stdClass();
			$market_status->type_title = 'Mostwantedrealestate Market_status';
			$market_status->type_alias = 'com_mostwantedrealestate.market_status';
			$market_status->table = '{"special": {"dbtable": "#__mostwantedrealestate_market_status","key": "id","type": "Market_status","prefix": "mostwantedrealestateTable","config": "array()"},"common": {"dbtable": "#__ucm_content","key": "ucm_id","type": "Corecontent","prefix": "JTable","config": "array()"}}';
			$market_status->field_mappings = '{"common": {"core_content_item_id": "id","core_title": "name","core_state": "published","core_alias": "alias","core_created_time": "created","core_modified_time": "modified","core_body": "null","core_hits": "hits","core_publish_up": "null","core_publish_down": "null","core_access": "access","core_params": "params","core_featured": "null","core_metadata": "metadata","core_language": "null","core_images": "null","core_urls": "null","core_version": "version","core_ordering": "ordering","core_metakey": "metakey","core_metadesc": "metadesc","core_catid": "null","core_xreference": "null","asset_id": "asset_id"},"special": {"name":"name","alias":"alias"}}';
			$market_status->router = 'MostwantedrealestateHelperRoute::getMarket_statusRoute';
			$market_status->content_history_options = '{"formFile": "administrator/components/com_mostwantedrealestate/models/forms/market_status.xml","hideFields": ["asset_id","checked_out","checked_out_time","version"],"ignoreChanges": ["modified_by","modified","checked_out","checked_out_time","version","hits"],"convertToInt": ["published","ordering"],"displayLookup": [{"sourceColumn": "created_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "access","targetTable": "#__viewlevels","targetColumn": "id","displayColumn": "title"},{"sourceColumn": "modified_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"}]}';

			// [Interpretation 4031] Set the object into the content types table.
			$market_status_Inserted = $db->insertObject('#__content_types', $market_status);

			// [Interpretation 4013] Create the transaction_type content type object.
			$transaction_type = new stdClass();
			$transaction_type->type_title = 'Mostwantedrealestate Transaction_type';
			$transaction_type->type_alias = 'com_mostwantedrealestate.transaction_type';
			$transaction_type->table = '{"special": {"dbtable": "#__mostwantedrealestate_transaction_type","key": "id","type": "Transaction_type","prefix": "mostwantedrealestateTable","config": "array()"},"common": {"dbtable": "#__ucm_content","key": "ucm_id","type": "Corecontent","prefix": "JTable","config": "array()"}}';
			$transaction_type->field_mappings = '{"common": {"core_content_item_id": "id","core_title": "name","core_state": "published","core_alias": "alias","core_created_time": "created","core_modified_time": "modified","core_body": "null","core_hits": "hits","core_publish_up": "null","core_publish_down": "null","core_access": "access","core_params": "params","core_featured": "null","core_metadata": "metadata","core_language": "null","core_images": "null","core_urls": "null","core_version": "version","core_ordering": "ordering","core_metakey": "metakey","core_metadesc": "metadesc","core_catid": "null","core_xreference": "null","asset_id": "asset_id"},"special": {"name":"name","alias":"alias"}}';
			$transaction_type->router = 'MostwantedrealestateHelperRoute::getTransaction_typeRoute';
			$transaction_type->content_history_options = '{"formFile": "administrator/components/com_mostwantedrealestate/models/forms/transaction_type.xml","hideFields": ["asset_id","checked_out","checked_out_time","version"],"ignoreChanges": ["modified_by","modified","checked_out","checked_out_time","version","hits"],"convertToInt": ["published","ordering"],"displayLookup": [{"sourceColumn": "created_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "access","targetTable": "#__viewlevels","targetColumn": "id","displayColumn": "title"},{"sourceColumn": "modified_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"}]}';

			// [Interpretation 4031] Set the object into the content types table.
			$transaction_type_Inserted = $db->insertObject('#__content_types', $transaction_type);

			// [Interpretation 4013] Create the rental_frequency content type object.
			$rental_frequency = new stdClass();
			$rental_frequency->type_title = 'Mostwantedrealestate Rental_frequency';
			$rental_frequency->type_alias = 'com_mostwantedrealestate.rental_frequency';
			$rental_frequency->table = '{"special": {"dbtable": "#__mostwantedrealestate_rental_frequency","key": "id","type": "Rental_frequency","prefix": "mostwantedrealestateTable","config": "array()"},"common": {"dbtable": "#__ucm_content","key": "ucm_id","type": "Corecontent","prefix": "JTable","config": "array()"}}';
			$rental_frequency->field_mappings = '{"common": {"core_content_item_id": "id","core_title": "name","core_state": "published","core_alias": "alias","core_created_time": "created","core_modified_time": "modified","core_body": "null","core_hits": "hits","core_publish_up": "null","core_publish_down": "null","core_access": "access","core_params": "params","core_featured": "null","core_metadata": "metadata","core_language": "null","core_images": "null","core_urls": "null","core_version": "version","core_ordering": "ordering","core_metakey": "metakey","core_metadesc": "metadesc","core_catid": "null","core_xreference": "null","asset_id": "asset_id"},"special": {"name":"name","alias":"alias"}}';
			$rental_frequency->router = 'MostwantedrealestateHelperRoute::getRental_frequencyRoute';
			$rental_frequency->content_history_options = '{"formFile": "administrator/components/com_mostwantedrealestate/models/forms/rental_frequency.xml","hideFields": ["asset_id","checked_out","checked_out_time","version"],"ignoreChanges": ["modified_by","modified","checked_out","checked_out_time","version","hits"],"convertToInt": ["published","ordering"],"displayLookup": [{"sourceColumn": "created_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "access","targetTable": "#__viewlevels","targetColumn": "id","displayColumn": "title"},{"sourceColumn": "modified_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"}]}';

			// [Interpretation 4031] Set the object into the content types table.
			$rental_frequency_Inserted = $db->insertObject('#__content_types', $rental_frequency);

			// [Interpretation 4013] Create the rent_type content type object.
			$rent_type = new stdClass();
			$rent_type->type_title = 'Mostwantedrealestate Rent_type';
			$rent_type->type_alias = 'com_mostwantedrealestate.rent_type';
			$rent_type->table = '{"special": {"dbtable": "#__mostwantedrealestate_rent_type","key": "id","type": "Rent_type","prefix": "mostwantedrealestateTable","config": "array()"},"common": {"dbtable": "#__ucm_content","key": "ucm_id","type": "Corecontent","prefix": "JTable","config": "array()"}}';
			$rent_type->field_mappings = '{"common": {"core_content_item_id": "id","core_title": "name","core_state": "published","core_alias": "alias","core_created_time": "created","core_modified_time": "modified","core_body": "null","core_hits": "hits","core_publish_up": "null","core_publish_down": "null","core_access": "access","core_params": "params","core_featured": "null","core_metadata": "metadata","core_language": "null","core_images": "null","core_urls": "null","core_version": "version","core_ordering": "ordering","core_metakey": "metakey","core_metadesc": "metadesc","core_catid": "null","core_xreference": "null","asset_id": "asset_id"},"special": {"name":"name","alias":"alias"}}';
			$rent_type->router = 'MostwantedrealestateHelperRoute::getRent_typeRoute';
			$rent_type->content_history_options = '{"formFile": "administrator/components/com_mostwantedrealestate/models/forms/rent_type.xml","hideFields": ["asset_id","checked_out","checked_out_time","version"],"ignoreChanges": ["modified_by","modified","checked_out","checked_out_time","version","hits"],"convertToInt": ["published","ordering"],"displayLookup": [{"sourceColumn": "created_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "access","targetTable": "#__viewlevels","targetColumn": "id","displayColumn": "title"},{"sourceColumn": "modified_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"}]}';

			// [Interpretation 4031] Set the object into the content types table.
			$rent_type_Inserted = $db->insertObject('#__content_types', $rent_type);

			// [Interpretation 4013] Create the feature_type content type object.
			$feature_type = new stdClass();
			$feature_type->type_title = 'Mostwantedrealestate Feature_type';
			$feature_type->type_alias = 'com_mostwantedrealestate.feature_type';
			$feature_type->table = '{"special": {"dbtable": "#__mostwantedrealestate_feature_type","key": "id","type": "Feature_type","prefix": "mostwantedrealestateTable","config": "array()"},"common": {"dbtable": "#__ucm_content","key": "ucm_id","type": "Corecontent","prefix": "JTable","config": "array()"}}';
			$feature_type->field_mappings = '{"common": {"core_content_item_id": "id","core_title": "featurename","core_state": "published","core_alias": "alias","core_created_time": "created","core_modified_time": "modified","core_body": "null","core_hits": "hits","core_publish_up": "null","core_publish_down": "null","core_access": "access","core_params": "params","core_featured": "null","core_metadata": "metadata","core_language": "null","core_images": "null","core_urls": "null","core_version": "version","core_ordering": "ordering","core_metakey": "metakey","core_metadesc": "metadesc","core_catid": "null","core_xreference": "null","asset_id": "asset_id"},"special": {"featurename":"featurename","featuretype":"featuretype","alias":"alias"}}';
			$feature_type->router = 'MostwantedrealestateHelperRoute::getFeature_typeRoute';
			$feature_type->content_history_options = '{"formFile": "administrator/components/com_mostwantedrealestate/models/forms/feature_type.xml","hideFields": ["asset_id","checked_out","checked_out_time","version"],"ignoreChanges": ["modified_by","modified","checked_out","checked_out_time","version","hits"],"convertToInt": ["published","ordering","featuretype"],"displayLookup": [{"sourceColumn": "created_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "access","targetTable": "#__viewlevels","targetColumn": "id","displayColumn": "title"},{"sourceColumn": "modified_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"}]}';

			// [Interpretation 4031] Set the object into the content types table.
			$feature_type_Inserted = $db->insertObject('#__content_types', $feature_type);

			// [Interpretation 4013] Create the favorite_listing content type object.
			$favorite_listing = new stdClass();
			$favorite_listing->type_title = 'Mostwantedrealestate Favorite_listing';
			$favorite_listing->type_alias = 'com_mostwantedrealestate.favorite_listing';
			$favorite_listing->table = '{"special": {"dbtable": "#__mostwantedrealestate_favorite_listing","key": "id","type": "Favorite_listing","prefix": "mostwantedrealestateTable","config": "array()"},"common": {"dbtable": "#__ucm_content","key": "ucm_id","type": "Corecontent","prefix": "JTable","config": "array()"}}';
			$favorite_listing->field_mappings = '{"common": {"core_content_item_id": "id","core_title": "null","core_state": "published","core_alias": "null","core_created_time": "created","core_modified_time": "modified","core_body": "null","core_hits": "hits","core_publish_up": "null","core_publish_down": "null","core_access": "access","core_params": "params","core_featured": "null","core_metadata": "metadata","core_language": "null","core_images": "null","core_urls": "null","core_version": "version","core_ordering": "ordering","core_metakey": "metakey","core_metadesc": "metadesc","core_catid": "null","core_xreference": "null","asset_id": "asset_id"},"special": {"uid":"uid","propertyid":"propertyid"}}';
			$favorite_listing->router = 'MostwantedrealestateHelperRoute::getFavorite_listingRoute';
			$favorite_listing->content_history_options = '{"formFile": "administrator/components/com_mostwantedrealestate/models/forms/favorite_listing.xml","hideFields": ["asset_id","checked_out","checked_out_time","version"],"ignoreChanges": ["modified_by","modified","checked_out","checked_out_time","version","hits"],"convertToInt": ["published","ordering","uid","propertyid"],"displayLookup": [{"sourceColumn": "created_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "access","targetTable": "#__viewlevels","targetColumn": "id","displayColumn": "title"},{"sourceColumn": "modified_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "propertyid","targetTable": "#__mostwantedrealestate_property","targetColumn": "id","displayColumn": "name"}]}';

			// [Interpretation 4031] Set the object into the content types table.
			$favorite_listing_Inserted = $db->insertObject('#__content_types', $favorite_listing);

			// [Interpretation 4013] Create the image content type object.
			$image = new stdClass();
			$image->type_title = 'Mostwantedrealestate Image';
			$image->type_alias = 'com_mostwantedrealestate.image';
			$image->table = '{"special": {"dbtable": "#__mostwantedrealestate_image","key": "id","type": "Image","prefix": "mostwantedrealestateTable","config": "array()"},"common": {"dbtable": "#__ucm_content","key": "ucm_id","type": "Corecontent","prefix": "JTable","config": "array()"}}';
			$image->field_mappings = '{"common": {"core_content_item_id": "id","core_title": "null","core_state": "published","core_alias": "null","core_created_time": "created","core_modified_time": "modified","core_body": "null","core_hits": "hits","core_publish_up": "null","core_publish_down": "null","core_access": "access","core_params": "params","core_featured": "null","core_metadata": "metadata","core_language": "null","core_images": "null","core_urls": "null","core_version": "version","core_ordering": "ordering","core_metakey": "metakey","core_metadesc": "metadesc","core_catid": "null","core_xreference": "null","asset_id": "asset_id"},"special": {"propid":"propid","path":"path","filename":"filename","type":"type","rets_source":"rets_source","title":"title","description":"description"}}';
			$image->router = 'MostwantedrealestateHelperRoute::getImageRoute';
			$image->content_history_options = '{"formFile": "administrator/components/com_mostwantedrealestate/models/forms/image.xml","hideFields": ["asset_id","checked_out","checked_out_time","version"],"ignoreChanges": ["modified_by","modified","checked_out","checked_out_time","version","hits"],"convertToInt": ["published","ordering","propid"],"displayLookup": [{"sourceColumn": "created_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "access","targetTable": "#__viewlevels","targetColumn": "id","displayColumn": "title"},{"sourceColumn": "modified_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"}]}';

			// [Interpretation 4031] Set the object into the content types table.
			$image_Inserted = $db->insertObject('#__content_types', $image);


			// [Interpretation 4068] Install the global extenstion assets permission.
			$query = $db->getQuery(true);
			// [Interpretation 4076] Field to update.
			$fields = array(
				$db->quoteName('rules') . ' = ' . $db->quote('{"site.categories.access":{"1":1},"site.category.access":{"1":1},"site.cities.access":{"1":1},"site.states.access":{"1":1},"site.countries.access":{"1":1},"site.agencies.access":{"1":1},"site.agents.access":{"1":1},"site.properties.access":{"1":1},"site.agency.access":{"1":1},"site.agentview.access":{"1":1},"site.country.access":{"1":1},"site.state.access":{"1":1},"site.city.access":{"1":1},"site.property.access":{"1":1},"site.hotsheet.access":{"1":1},"site.favorites.access":{"1":1},"site.featured.access":{"1":1},"site.openhouses.access":{"1":1},"site.transactiontype.access":{"1":1}}'),
			);
			// [Interpretation 4080] Condition.
			$conditions = array(
				$db->quoteName('name') . ' = ' . $db->quote('com_mostwantedrealestate')
			);
			$query->update($db->quoteName('#__assets'))->set($fields)->where($conditions);
			$db->setQuery($query);
			$allDone = $db->execute();

			// [Interpretation 4093] Install the global extenstion params.
			$query = $db->getQuery(true);
			// [Interpretation 4101] Field to update.
			$fields = array(
				$db->quoteName('params') . ' = ' . $db->quote('{"autorName":"Most Wanted Web Services, Inc.","autorEmail":"sales@mwweb.host","mw_uselistmap":"1","map_provider":"1","gmapsapi":"AIzaSyBGJCt7Bf1kIvwc7Bu-BAkhrpnprq3RNQo","bingmapsapi":"AtiX-jfTaf1r_GbbiU2yZwk6z_-JBTg5wWY43Y7DDBxH4Ope9P1Zw4IKyV0woSCa","mw_mapheight":"600","mw_mapwidth":"1000","latitude":"47.6149942","longitude":"-122.4759886","category_properties_display":"1","cities_display":"1","states_display":"1","countries_display":"1","agents_display":"1","agencies_display":"1","transactiontype_request_id":"0","properties_display":"1","properties_thumb_type":"0","map_type":"1","zoom":"8","sqft_type":"1","keyword_filter":"1","category_filter":"1","transtype_filter":"1","mktstatus_filter":"1","agent_filter":"1","state_filter":"1","city_filter":"1","beds_filter":"1","baths_filter":"1","area_filter":"1","price_filter":"1","land_filter":"1","waterfront_filter":"0","openhouse_display":"1","oh_thumb_type":"0","oh_map_type":"1","oh_keyword_filter":"1","oh_category_filter":"1","oh_transtype_filter":"1","oh_mktstatus_filter":"1","oh_agent_filter":"1","oh_state_filter":"1","oh_city_filter":"1","oh_beds_filter":"1","oh_baths_filter":"1","oh_area_filter":"1","oh_price_filter":"1","oh_land_filter":"1","oh_waterfront_filter":"0","featured_display":"1","featured_map_type":"1","featured_keyword_filter":"1","featured_category_filter":"1","featured_transtype_filter":"1","featured_mktstatus_filter":"1","featured_agent_filter":"1","featured_state_filter":"1","featured_city_filter":"1","featured_beds_filter":"1","featured_baths_filter":"1","featured_area_filter":"1","featured_price_filter":"1","featured_land_filter":"1","featured_waterfront_filter":"0","category_display":"1","category_map_zoom":"1","cat_thumb_type":"0","city_request_id":"1","city_properties_display":"1","city_map_zoom":"12","show_city_image":"1","city_thumb_type":"0","state_request_id":"0","state_properties_display":"1","state_map_zoom":"12","show_state_image":"1","state_thumb_type":"0","country_request_id":"0","country_properties_display":"1","country_map_zoom":"12","show_country_image":"1","country_thumb_type":"0","agent_request_id":"0","agent_properties_display":"1","agent_map_zoom":"12","agent_thumb_type":"0","agency_request_id":"0","agency_properties_display":"1","agency_map_zoom":"12","agency_thumb_type":"0","property_request_id":"0","property_layout":"1","property_map_zoom":"12","property_data_layout":"0","property_slideshow":"1","slideshow_transtype":"1","slider_autoplay_duration":"3000","mw_areameasure":"1","mw_usesimilar":"2","mw_simnum":"1","mw_usesecondary":"0","mw_country":"0","transtype_display":"1","transtype_thumb_type":"0","transtype_keyword_filter":"1","transtype_category_filter":"1","transtype_mktstatus_filter":"1","transtype_agent_filter":"1","transtype_state_filter":"1","transtype_city_filter":"1","transtype_beds_filter":"1","transtype_baths_filter":"1","transtype_area_filter":"1","transtype_price_filter":"1","transtype_land_filter":"1","waterfront_land_filter":"0","hotsheet_display":"1","hotsheet_map_type":"1","hotsheet_keyword_filter":"1","hotsheet_category_filter":"1","hotsheet_mktstatus_filter":"1","hotsheet_transtype_filter":"1","hotsheet_price_filter":"1","hotsheet_land_filter":"1","hotsheet_beds_filter":"1","hotsheet_baths_filter":"1","hotsheet_agent_filter":"1","hotsheet_city_filter":"1","hotsheet_state_filter":"1","hotsheet_area_filter":"1","hotsheet_waterfront_filter":"0","check_in":"-1 day","save_history":"1","history_limit":"10","uikit_load":"1","uikit_min":"","uikit_style":""}'),
			);
			// [Interpretation 4105] Condition.
			$conditions = array(
				$db->quoteName('element') . ' = ' . $db->quote('com_mostwantedrealestate')
			);
			$query->update($db->quoteName('#__extensions'))->set($fields)->where($conditions);
			$db->setQuery($query);
			$allDone = $db->execute();

			echo '<a target="_blank" href="http://mostwantedrealestatesites.com" title="Most Wanted Real Estate">
				<img src="components/com_mostwantedrealestate/assets/images/vdm-component.png"/>
				</a>';
		}
		// do any updates needed
		if ($type == 'update')
		{

			// [Interpretation 4006] Get The Database object
			$db = JFactory::getDbo();

			// [Interpretation 4013] Create the country content type object.
			$country = new stdClass();
			$country->type_title = 'Mostwantedrealestate Country';
			$country->type_alias = 'com_mostwantedrealestate.country';
			$country->table = '{"special": {"dbtable": "#__mostwantedrealestate_country","key": "id","type": "Country","prefix": "mostwantedrealestateTable","config": "array()"},"common": {"dbtable": "#__ucm_content","key": "ucm_id","type": "Corecontent","prefix": "JTable","config": "array()"}}';
			$country->field_mappings = '{"common": {"core_content_item_id": "id","core_title": "name","core_state": "published","core_alias": "alias","core_created_time": "created","core_modified_time": "modified","core_body": "description","core_hits": "hits","core_publish_up": "null","core_publish_down": "null","core_access": "access","core_params": "params","core_featured": "null","core_metadata": "metadata","core_language": "null","core_images": "null","core_urls": "null","core_version": "version","core_ordering": "ordering","core_metakey": "metakey","core_metadesc": "metadesc","core_catid": "null","core_xreference": "null","asset_id": "asset_id"},"special": {"name":"name","alias":"alias","latitude":"latitude","image":"image","owncoords":"owncoords","description":"description","longitude":"longitude"}}';
			$country->router = 'MostwantedrealestateHelperRoute::getCountryRoute';
			$country->content_history_options = '{"formFile": "administrator/components/com_mostwantedrealestate/models/forms/country.xml","hideFields": ["asset_id","checked_out","checked_out_time","version"],"ignoreChanges": ["modified_by","modified","checked_out","checked_out_time","version","hits"],"convertToInt": ["published","ordering","owncoords"],"displayLookup": [{"sourceColumn": "created_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "access","targetTable": "#__viewlevels","targetColumn": "id","displayColumn": "title"},{"sourceColumn": "modified_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"}]}';

			// [Interpretation 4022] Check if country type is already in content_type DB.
			$country_id = null;
			$query = $db->getQuery(true);
			$query->select($db->quoteName(array('type_id')));
			$query->from($db->quoteName('#__content_types'));
			$query->where($db->quoteName('type_alias') . ' LIKE '. $db->quote($country->type_alias));
			$db->setQuery($query);
			$db->execute();

			// [Interpretation 4031] Set the object into the content types table.
			if ($db->getNumRows())
			{
				$country->type_id = $db->loadResult();
				$country_Updated = $db->updateObject('#__content_types', $country, 'type_id');
			}
			else
			{
				$country_Inserted = $db->insertObject('#__content_types', $country);
			}

			// [Interpretation 4013] Create the state content type object.
			$state = new stdClass();
			$state->type_title = 'Mostwantedrealestate State';
			$state->type_alias = 'com_mostwantedrealestate.state';
			$state->table = '{"special": {"dbtable": "#__mostwantedrealestate_state","key": "id","type": "State","prefix": "mostwantedrealestateTable","config": "array()"},"common": {"dbtable": "#__ucm_content","key": "ucm_id","type": "Corecontent","prefix": "JTable","config": "array()"}}';
			$state->field_mappings = '{"common": {"core_content_item_id": "id","core_title": "name","core_state": "published","core_alias": "alias","core_created_time": "created","core_modified_time": "modified","core_body": "description","core_hits": "hits","core_publish_up": "null","core_publish_down": "null","core_access": "access","core_params": "params","core_featured": "null","core_metadata": "metadata","core_language": "null","core_images": "null","core_urls": "null","core_version": "version","core_ordering": "ordering","core_metakey": "metakey","core_metadesc": "metadesc","core_catid": "null","core_xreference": "null","asset_id": "asset_id"},"special": {"name":"name","alias":"alias","description":"description","latitude":"latitude","image":"image","owncoords":"owncoords","countryid":"countryid","longitude":"longitude"}}';
			$state->router = 'MostwantedrealestateHelperRoute::getStateRoute';
			$state->content_history_options = '{"formFile": "administrator/components/com_mostwantedrealestate/models/forms/state.xml","hideFields": ["asset_id","checked_out","checked_out_time","version"],"ignoreChanges": ["modified_by","modified","checked_out","checked_out_time","version","hits"],"convertToInt": ["published","ordering","owncoords","countryid"],"displayLookup": [{"sourceColumn": "created_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "access","targetTable": "#__viewlevels","targetColumn": "id","displayColumn": "title"},{"sourceColumn": "modified_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "countryid","targetTable": "#__mostwantedrealestate_country","targetColumn": "id","displayColumn": "name"}]}';

			// [Interpretation 4022] Check if state type is already in content_type DB.
			$state_id = null;
			$query = $db->getQuery(true);
			$query->select($db->quoteName(array('type_id')));
			$query->from($db->quoteName('#__content_types'));
			$query->where($db->quoteName('type_alias') . ' LIKE '. $db->quote($state->type_alias));
			$db->setQuery($query);
			$db->execute();

			// [Interpretation 4031] Set the object into the content types table.
			if ($db->getNumRows())
			{
				$state->type_id = $db->loadResult();
				$state_Updated = $db->updateObject('#__content_types', $state, 'type_id');
			}
			else
			{
				$state_Inserted = $db->insertObject('#__content_types', $state);
			}

			// [Interpretation 4013] Create the city content type object.
			$city = new stdClass();
			$city->type_title = 'Mostwantedrealestate City';
			$city->type_alias = 'com_mostwantedrealestate.city';
			$city->table = '{"special": {"dbtable": "#__mostwantedrealestate_city","key": "id","type": "City","prefix": "mostwantedrealestateTable","config": "array()"},"common": {"dbtable": "#__ucm_content","key": "ucm_id","type": "Corecontent","prefix": "JTable","config": "array()"}}';
			$city->field_mappings = '{"common": {"core_content_item_id": "id","core_title": "name","core_state": "published","core_alias": "alias","core_created_time": "created","core_modified_time": "modified","core_body": "description","core_hits": "hits","core_publish_up": "null","core_publish_down": "null","core_access": "access","core_params": "params","core_featured": "null","core_metadata": "metadata","core_language": "null","core_images": "null","core_urls": "null","core_version": "version","core_ordering": "ordering","core_metakey": "metakey","core_metadesc": "metadesc","core_catid": "null","core_xreference": "null","asset_id": "asset_id"},"special": {"name":"name","alias":"alias","latitude":"latitude","description":"description","image":"image","owncoords":"owncoords","stateid":"stateid","longitude":"longitude"}}';
			$city->router = 'MostwantedrealestateHelperRoute::getCityRoute';
			$city->content_history_options = '{"formFile": "administrator/components/com_mostwantedrealestate/models/forms/city.xml","hideFields": ["asset_id","checked_out","checked_out_time","version"],"ignoreChanges": ["modified_by","modified","checked_out","checked_out_time","version","hits"],"convertToInt": ["published","ordering","owncoords","stateid"],"displayLookup": [{"sourceColumn": "created_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "access","targetTable": "#__viewlevels","targetColumn": "id","displayColumn": "title"},{"sourceColumn": "modified_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "stateid","targetTable": "#__mostwantedrealestate_state","targetColumn": "id","displayColumn": "name"}]}';

			// [Interpretation 4022] Check if city type is already in content_type DB.
			$city_id = null;
			$query = $db->getQuery(true);
			$query->select($db->quoteName(array('type_id')));
			$query->from($db->quoteName('#__content_types'));
			$query->where($db->quoteName('type_alias') . ' LIKE '. $db->quote($city->type_alias));
			$db->setQuery($query);
			$db->execute();

			// [Interpretation 4031] Set the object into the content types table.
			if ($db->getNumRows())
			{
				$city->type_id = $db->loadResult();
				$city_Updated = $db->updateObject('#__content_types', $city, 'type_id');
			}
			else
			{
				$city_Inserted = $db->insertObject('#__content_types', $city);
			}

			// [Interpretation 4013] Create the agency content type object.
			$agency = new stdClass();
			$agency->type_title = 'Mostwantedrealestate Agency';
			$agency->type_alias = 'com_mostwantedrealestate.agency';
			$agency->table = '{"special": {"dbtable": "#__mostwantedrealestate_agency","key": "id","type": "Agency","prefix": "mostwantedrealestateTable","config": "array()"},"common": {"dbtable": "#__ucm_content","key": "ucm_id","type": "Corecontent","prefix": "JTable","config": "array()"}}';
			$agency->field_mappings = '{"common": {"core_content_item_id": "id","core_title": "name","core_state": "published","core_alias": "alias","core_created_time": "created","core_modified_time": "modified","core_body": "description","core_hits": "hits","core_publish_up": "null","core_publish_down": "null","core_access": "access","core_params": "params","core_featured": "null","core_metadata": "metadata","core_language": "null","core_images": "null","core_urls": "null","core_version": "version","core_ordering": "ordering","core_metakey": "metakey","core_metadesc": "metadesc","core_catid": "null","core_xreference": "null","asset_id": "asset_id"},"special": {"name":"name","street":"street","cityid":"cityid","stateid":"stateid","postcode":"postcode","owncoords":"owncoords","streettwo":"streettwo","image":"image","default_agency_yn":"default_agency_yn","longitude":"longitude","countryid":"countryid","alias":"alias","description":"description","email":"email","website":"website","featured":"featured","license":"license","rets_source":"rets_source","phone":"phone","latitude":"latitude","fax":"fax"}}';
			$agency->router = 'MostwantedrealestateHelperRoute::getAgencyRoute';
			$agency->content_history_options = '{"formFile": "administrator/components/com_mostwantedrealestate/models/forms/agency.xml","hideFields": ["asset_id","checked_out","checked_out_time","version"],"ignoreChanges": ["modified_by","modified","checked_out","checked_out_time","version","hits"],"convertToInt": ["published","ordering","cityid","stateid","owncoords","default_agency_yn","countryid","featured"],"displayLookup": [{"sourceColumn": "created_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "access","targetTable": "#__viewlevels","targetColumn": "id","displayColumn": "title"},{"sourceColumn": "modified_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "cityid","targetTable": "#__mostwantedrealestate_city","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "stateid","targetTable": "#__mostwantedrealestate_state","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "countryid","targetTable": "#__mostwantedrealestate_country","targetColumn": "id","displayColumn": "name"}]}';

			// [Interpretation 4022] Check if agency type is already in content_type DB.
			$agency_id = null;
			$query = $db->getQuery(true);
			$query->select($db->quoteName(array('type_id')));
			$query->from($db->quoteName('#__content_types'));
			$query->where($db->quoteName('type_alias') . ' LIKE '. $db->quote($agency->type_alias));
			$db->setQuery($query);
			$db->execute();

			// [Interpretation 4031] Set the object into the content types table.
			if ($db->getNumRows())
			{
				$agency->type_id = $db->loadResult();
				$agency_Updated = $db->updateObject('#__content_types', $agency, 'type_id');
			}
			else
			{
				$agency_Inserted = $db->insertObject('#__content_types', $agency);
			}

			// [Interpretation 4013] Create the agent content type object.
			$agent = new stdClass();
			$agent->type_title = 'Mostwantedrealestate Agent';
			$agent->type_alias = 'com_mostwantedrealestate.agent';
			$agent->table = '{"special": {"dbtable": "#__mostwantedrealestate_agent","key": "id","type": "Agent","prefix": "mostwantedrealestateTable","config": "array()"},"common": {"dbtable": "#__ucm_content","key": "ucm_id","type": "Corecontent","prefix": "JTable","config": "array()"}}';
			$agent->field_mappings = '{"common": {"core_content_item_id": "id","core_title": "name","core_state": "published","core_alias": "alias","core_created_time": "created","core_modified_time": "modified","core_body": "bio","core_hits": "hits","core_publish_up": "null","core_publish_down": "null","core_access": "access","core_params": "params","core_featured": "null","core_metadata": "metadata","core_language": "null","core_images": "null","core_urls": "null","core_version": "version","core_ordering": "ordering","core_metakey": "metakey","core_metadesc": "metadesc","core_catid": "catid","core_xreference": "null","asset_id": "asset_id"},"special": {"name":"name","cityid":"cityid","stateid":"stateid","uid":"uid","longitude":"longitude","postcode":"postcode","viewad":"viewad","streettwo":"streettwo","countryid":"countryid","skype":"skype","owncoords":"owncoords","street":"street","fbook":"fbook","email":"email","youtube":"youtube","phone":"phone","website":"website","mobile":"mobile","default_agent_yn":"default_agent_yn","fax":"fax","rets_source":"rets_source","image":"image","latitude":"latitude","bio":"bio","pinterest":"pinterest","twitter":"twitter","alias":"alias","linkedin":"linkedin","gplus":"gplus","agencyid":"agencyid","instagram":"instagram","featured":"featured","blog":"blog"}}';
			$agent->router = 'MostwantedrealestateHelperRoute::getAgentRoute';
			$agent->content_history_options = '{"formFile": "administrator/components/com_mostwantedrealestate/models/forms/agent.xml","hideFields": ["asset_id","checked_out","checked_out_time","version"],"ignoreChanges": ["modified_by","modified","checked_out","checked_out_time","version","hits"],"convertToInt": ["published","ordering","cityid","stateid","uid","viewad","countryid","owncoords","default_agent_yn","catid","agencyid","featured"],"displayLookup": [{"sourceColumn": "catid","targetTable": "#__categories","targetColumn": "id","displayColumn": "title"},{"sourceColumn": "created_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "access","targetTable": "#__viewlevels","targetColumn": "id","displayColumn": "title"},{"sourceColumn": "modified_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "cityid","targetTable": "#__mostwantedrealestate_city","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "stateid","targetTable": "#__mostwantedrealestate_state","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "countryid","targetTable": "#__mostwantedrealestate_country","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "agencyid","targetTable": "#__mostwantedrealestate_agency","targetColumn": "id","displayColumn": "name"}]}';

			// [Interpretation 4022] Check if agent type is already in content_type DB.
			$agent_id = null;
			$query = $db->getQuery(true);
			$query->select($db->quoteName(array('type_id')));
			$query->from($db->quoteName('#__content_types'));
			$query->where($db->quoteName('type_alias') . ' LIKE '. $db->quote($agent->type_alias));
			$db->setQuery($query);
			$db->execute();

			// [Interpretation 4031] Set the object into the content types table.
			if ($db->getNumRows())
			{
				$agent->type_id = $db->loadResult();
				$agent_Updated = $db->updateObject('#__content_types', $agent, 'type_id');
			}
			else
			{
				$agent_Inserted = $db->insertObject('#__content_types', $agent);
			}

			// [Interpretation 4013] Create the agent category content type object.
			$agent_category = new stdClass();
			$agent_category->type_title = 'Mostwantedrealestate Agent Catid';
			$agent_category->type_alias = 'com_mostwantedrealestate.agents.category';
			$agent_category->table = '{"special":{"dbtable":"#__categories","key":"id","type":"Category","prefix":"JTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}';
			$agent_category->field_mappings = '{"common":{"core_content_item_id":"id","core_title":"title","core_state":"published","core_alias":"alias","core_created_time":"created_time","core_modified_time":"modified_time","core_body":"description", "core_hits":"hits","core_publish_up":"null","core_publish_down":"null","core_access":"access", "core_params":"params", "core_featured":"null", "core_metadata":"metadata", "core_language":"language", "core_images":"null", "core_urls":"null", "core_version":"version", "core_ordering":"null", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"parent_id", "core_xreference":"null", "asset_id":"asset_id"}, "special":{"parent_id":"parent_id","lft":"lft","rgt":"rgt","level":"level","path":"path","extension":"extension","note":"note"}}';
			$agent_category->router = 'MostwantedrealestateHelperRoute::getCategoryRoute';
			$agent_category->content_history_options = '{"formFile":"administrator\/components\/com_categories\/models\/forms\/category.xml", "hideFields":["asset_id","checked_out","checked_out_time","version","lft","rgt","level","path","extension"], "ignoreChanges":["modified_user_id", "modified_time", "checked_out", "checked_out_time", "version", "hits", "path"],"convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"created_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"parent_id","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"}]}';

			// [Interpretation 4022] Check if agent category type is already in content_type DB.
			$agent_category_id = null;
			$query = $db->getQuery(true);
			$query->select($db->quoteName(array('type_id')));
			$query->from($db->quoteName('#__content_types'));
			$query->where($db->quoteName('type_alias') . ' LIKE '. $db->quote($agent_category->type_alias));
			$db->setQuery($query);
			$db->execute();

			// [Interpretation 4031] Set the object into the content types table.
			if ($db->getNumRows())
			{
				$agent_category->type_id = $db->loadResult();
				$agent_category_Updated = $db->updateObject('#__content_types', $agent_category, 'type_id');
			}
			else
			{
				$agent_category_Inserted = $db->insertObject('#__content_types', $agent_category);
			}

			// [Interpretation 4013] Create the property content type object.
			$property = new stdClass();
			$property->type_title = 'Mostwantedrealestate Property';
			$property->type_alias = 'com_mostwantedrealestate.property';
			$property->table = '{"special": {"dbtable": "#__mostwantedrealestate_property","key": "id","type": "Property","prefix": "mostwantedrealestateTable","config": "array()"},"common": {"dbtable": "#__ucm_content","key": "ucm_id","type": "Corecontent","prefix": "JTable","config": "array()"}}';
			$property->field_mappings = '{"common": {"core_content_item_id": "id","core_title": "name","core_state": "published","core_alias": "null","core_created_time": "created","core_modified_time": "modified","core_body": "propdesc","core_hits": "hits","core_publish_up": "null","core_publish_down": "null","core_access": "access","core_params": "params","core_featured": "null","core_metadata": "metadata","core_language": "null","core_images": "null","core_urls": "null","core_version": "version","core_ordering": "ordering","core_metakey": "metakey","core_metadesc": "metadesc","core_catid": "catid","core_xreference": "null","asset_id": "asset_id"},"special": {"name":"name","mls_id":"mls_id","cityid":"cityid","stateid":"stateid","featured":"featured","pmstartdate":"pmstartdate","fixtures":"fixtures","mls_image":"mls_image","county":"county","viewad":"viewad","flplone":"flplone","showprice":"showprice","customfive":"customfive","price":"price","totalrents":"totalrents","closeprice":"closeprice","offpeak":"offpeak","closedate":"closedate","taxannual":"taxannual","priceview":"priceview","ctport":"ctport","mls_disclaimer":"mls_disclaimer","storage":"storage","postcode":"postcode","currentuse":"currentuse","alias":"alias","countryid":"countryid","grazing":"grazing","rets_source":"rets_source","mediaurl":"mediaurl","listingtype":"listingtype","openhouse":"openhouse","streettwo":"streettwo","averageutilelec":"averageutilelec","propdesc":"propdesc","viewbooking":"viewbooking","highschool":"highschool","returns":"returns","customseven":"customseven","sewer":"sewer","soleagency":"soleagency","listoffice":"listoffice","bldg_name":"bldg_name","agent":"agent","bustype":"bustype","colistagent":"colistagent","percentwarehouse":"percentwarehouse","mkt_stats":"mkt_stats","parkinggarage":"parkinggarage","trans_type":"trans_type","tenancytype":"tenancytype","owncoords":"owncoords","landtype":"landtype","latitude":"latitude","rainfall":"rainfall","longitude":"longitude","irrigation":"irrigation","deposit":"deposit","bedrooms":"bedrooms","pdfinfoone":"pdfinfoone","bathrooms":"bathrooms","covenantsyn":"covenantsyn","fullbaths":"fullbaths","hofees":"hofees","thqtrbaths":"thqtrbaths","utilities":"utilities","halfbaths":"halfbaths","terms":"terms","qtrbaths":"qtrbaths","propmgt_price":"propmgt_price","squarefeet":"squarefeet","private":"private","sqftlower":"sqftlower","elementary":"elementary","sqftmainlevel":"sqftmainlevel","street":"street","sqftupper":"sqftupper","customsix":"customsix","style":"style","customeight":"customeight","year":"year","waterresources":"waterresources","yearremodeled":"yearremodeled","zoning":"zoning","exteriorfinish":"exteriorfinish","roof":"roof","flooring":"flooring","porchpatio":"porchpatio","takings":"takings","frontage":"frontage","netprofit":"netprofit","waterfront":"waterfront","bussubtype":"bussubtype","waterfronttype":"waterfronttype","percentoffice":"percentoffice","welldepth":"welldepth","loadingfac":"loadingfac","subdivision":"subdivision","carryingcap":"carryingcap","landareasqft":"landareasqft","bldgsqft":"bldgsqft","acrestotal":"acrestotal","numunits":"numunits","lotdimensions":"lotdimensions","tentantpdutilities":"tentantpdutilities","totalrooms":"totalrooms","otherrooms":"otherrooms","stock":"stock","livingarea":"livingarea","fittings":"fittings","ensuite":"ensuite","soiltype":"soiltype","garagetype":"garagetype","cropping":"cropping","parkingcarport":"parkingcarport","rent_type":"rent_type","stories":"stories","freq":"freq","basementandfoundation":"basementandfoundation","sleeps":"sleeps","basementsize":"basementsize","mediatype":"mediatype","basementpctfinished":"basementpctfinished","pdfinfotwo":"pdfinfotwo","heating":"heating","flpltwo":"flpltwo","cooling":"cooling","houseconstruction":"houseconstruction","fencing":"fencing","annualinsurance":"annualinsurance","phoneavailableyn":"phoneavailableyn","taxyear":"taxyear","garbagedisposalyn":"garbagedisposalyn","electricservice":"electricservice","familyroompresent":"familyroompresent","averageutilgas":"averageutilgas","laundryroompresent":"laundryroompresent","pm_price_override":"pm_price_override","kitchenpresent":"kitchenpresent","pmenddate":"pmenddate","livingroompresent":"livingroompresent","propmgt_description":"propmgt_description","parkingspaceyn":"parkingspaceyn","availdate":"availdate","customone":"customone","ctown":"ctown","customtwo":"customtwo","schooldist":"schooldist","customthree":"customthree","midschool":"midschool","customfour":"customfour","university":"university"}}';
			$property->router = 'MostwantedrealestateHelperRoute::getPropertyRoute';
			$property->content_history_options = '{"formFile": "administrator/components/com_mostwantedrealestate/models/forms/property.xml","hideFields": ["asset_id","checked_out","checked_out_time","version"],"ignoreChanges": ["modified_by","modified","checked_out","checked_out_time","version","hits"],"convertToInt": ["published","ordering","cityid","stateid","catid","featured","viewad","showprice","countryid","listingtype","openhouse","viewbooking","soleagency","listoffice","agent","colistagent","mkt_stats","trans_type","owncoords","bedrooms","covenantsyn","fullbaths","thqtrbaths","halfbaths","qtrbaths","waterfront","ensuite","rent_type","stories","freq","sleeps","mediatype","phoneavailableyn","garbagedisposalyn","familyroompresent","laundryroompresent","kitchenpresent","livingroompresent","parkingspaceyn"],"displayLookup": [{"sourceColumn": "catid","targetTable": "#__categories","targetColumn": "id","displayColumn": "title"},{"sourceColumn": "created_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "access","targetTable": "#__viewlevels","targetColumn": "id","displayColumn": "title"},{"sourceColumn": "modified_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "cityid","targetTable": "#__mostwantedrealestate_city","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "stateid","targetTable": "#__mostwantedrealestate_state","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "countryid","targetTable": "#__mostwantedrealestate_country","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "sewer","targetTable": "#__mostwantedrealestate_feature_type","targetColumn": "id","displayColumn": "featurename"},{"sourceColumn": "listoffice","targetTable": "#__mostwantedrealestate_agency","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "agent","targetTable": "#__mostwantedrealestate_agent","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "colistagent","targetTable": "#__mostwantedrealestate_agent","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "mkt_stats","targetTable": "#__mostwantedrealestate_market_status","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "trans_type","targetTable": "#__mostwantedrealestate_transaction_type","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "terms","targetTable": "#__mostwantedrealestate_feature_type","targetColumn": "id","displayColumn": "featurename"},{"sourceColumn": "style","targetTable": "#__mostwantedrealestate_feature_type","targetColumn": "id","displayColumn": "featurename"},{"sourceColumn": "waterresources","targetTable": "#__mostwantedrealestate_feature_type","targetColumn": "id","displayColumn": "featurename"},{"sourceColumn": "zoning","targetTable": "#__mostwantedrealestate_feature_type","targetColumn": "id","displayColumn": "featurename"},{"sourceColumn": "exteriorfinish","targetTable": "#__mostwantedrealestate_feature_type","targetColumn": "id","displayColumn": "featurename"},{"sourceColumn": "roof","targetTable": "#__mostwantedrealestate_feature_type","targetColumn": "id","displayColumn": "featurename"},{"sourceColumn": "porchpatio","targetTable": "#__mostwantedrealestate_feature_type","targetColumn": "id","displayColumn": "featurename"},{"sourceColumn": "frontage","targetTable": "#__mostwantedrealestate_feature_type","targetColumn": "id","displayColumn": "featurename"},{"sourceColumn": "garagetype","targetTable": "#__mostwantedrealestate_feature_type","targetColumn": "id","displayColumn": "featurename"},{"sourceColumn": "rent_type","targetTable": "#__mostwantedrealestate_rent_type","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "freq","targetTable": "#__mostwantedrealestate_rental_frequency","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "basementandfoundation","targetTable": "#__mostwantedrealestate_feature_type","targetColumn": "id","displayColumn": "featurename"},{"sourceColumn": "heating","targetTable": "#__mostwantedrealestate_feature_type","targetColumn": "id","displayColumn": "featurename"},{"sourceColumn": "cooling","targetTable": "#__mostwantedrealestate_feature_type","targetColumn": "id","displayColumn": "featurename"},{"sourceColumn": "fencing","targetTable": "#__mostwantedrealestate_feature_type","targetColumn": "id","displayColumn": "featurename"}]}';

			// [Interpretation 4022] Check if property type is already in content_type DB.
			$property_id = null;
			$query = $db->getQuery(true);
			$query->select($db->quoteName(array('type_id')));
			$query->from($db->quoteName('#__content_types'));
			$query->where($db->quoteName('type_alias') . ' LIKE '. $db->quote($property->type_alias));
			$db->setQuery($query);
			$db->execute();

			// [Interpretation 4031] Set the object into the content types table.
			if ($db->getNumRows())
			{
				$property->type_id = $db->loadResult();
				$property_Updated = $db->updateObject('#__content_types', $property, 'type_id');
			}
			else
			{
				$property_Inserted = $db->insertObject('#__content_types', $property);
			}

			// [Interpretation 4013] Create the property category content type object.
			$property_category = new stdClass();
			$property_category->type_title = 'Mostwantedrealestate Property Catid';
			$property_category->type_alias = 'com_mostwantedrealestate.properties.category';
			$property_category->table = '{"special":{"dbtable":"#__categories","key":"id","type":"Category","prefix":"JTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}';
			$property_category->field_mappings = '{"common":{"core_content_item_id":"id","core_title":"title","core_state":"published","core_alias":"alias","core_created_time":"created_time","core_modified_time":"modified_time","core_body":"description", "core_hits":"hits","core_publish_up":"null","core_publish_down":"null","core_access":"access", "core_params":"params", "core_featured":"null", "core_metadata":"metadata", "core_language":"language", "core_images":"null", "core_urls":"null", "core_version":"version", "core_ordering":"null", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"parent_id", "core_xreference":"null", "asset_id":"asset_id"}, "special":{"parent_id":"parent_id","lft":"lft","rgt":"rgt","level":"level","path":"path","extension":"extension","note":"note"}}';
			$property_category->router = 'MostwantedrealestateHelperRoute::getCategoryRoute';
			$property_category->content_history_options = '{"formFile":"administrator\/components\/com_categories\/models\/forms\/category.xml", "hideFields":["asset_id","checked_out","checked_out_time","version","lft","rgt","level","path","extension"], "ignoreChanges":["modified_user_id", "modified_time", "checked_out", "checked_out_time", "version", "hits", "path"],"convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"created_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"parent_id","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"}]}';

			// [Interpretation 4022] Check if property category type is already in content_type DB.
			$property_category_id = null;
			$query = $db->getQuery(true);
			$query->select($db->quoteName(array('type_id')));
			$query->from($db->quoteName('#__content_types'));
			$query->where($db->quoteName('type_alias') . ' LIKE '. $db->quote($property_category->type_alias));
			$db->setQuery($query);
			$db->execute();

			// [Interpretation 4031] Set the object into the content types table.
			if ($db->getNumRows())
			{
				$property_category->type_id = $db->loadResult();
				$property_category_Updated = $db->updateObject('#__content_types', $property_category, 'type_id');
			}
			else
			{
				$property_category_Inserted = $db->insertObject('#__content_types', $property_category);
			}

			// [Interpretation 4013] Create the market_status content type object.
			$market_status = new stdClass();
			$market_status->type_title = 'Mostwantedrealestate Market_status';
			$market_status->type_alias = 'com_mostwantedrealestate.market_status';
			$market_status->table = '{"special": {"dbtable": "#__mostwantedrealestate_market_status","key": "id","type": "Market_status","prefix": "mostwantedrealestateTable","config": "array()"},"common": {"dbtable": "#__ucm_content","key": "ucm_id","type": "Corecontent","prefix": "JTable","config": "array()"}}';
			$market_status->field_mappings = '{"common": {"core_content_item_id": "id","core_title": "name","core_state": "published","core_alias": "alias","core_created_time": "created","core_modified_time": "modified","core_body": "null","core_hits": "hits","core_publish_up": "null","core_publish_down": "null","core_access": "access","core_params": "params","core_featured": "null","core_metadata": "metadata","core_language": "null","core_images": "null","core_urls": "null","core_version": "version","core_ordering": "ordering","core_metakey": "metakey","core_metadesc": "metadesc","core_catid": "null","core_xreference": "null","asset_id": "asset_id"},"special": {"name":"name","alias":"alias"}}';
			$market_status->router = 'MostwantedrealestateHelperRoute::getMarket_statusRoute';
			$market_status->content_history_options = '{"formFile": "administrator/components/com_mostwantedrealestate/models/forms/market_status.xml","hideFields": ["asset_id","checked_out","checked_out_time","version"],"ignoreChanges": ["modified_by","modified","checked_out","checked_out_time","version","hits"],"convertToInt": ["published","ordering"],"displayLookup": [{"sourceColumn": "created_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "access","targetTable": "#__viewlevels","targetColumn": "id","displayColumn": "title"},{"sourceColumn": "modified_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"}]}';

			// [Interpretation 4022] Check if market_status type is already in content_type DB.
			$market_status_id = null;
			$query = $db->getQuery(true);
			$query->select($db->quoteName(array('type_id')));
			$query->from($db->quoteName('#__content_types'));
			$query->where($db->quoteName('type_alias') . ' LIKE '. $db->quote($market_status->type_alias));
			$db->setQuery($query);
			$db->execute();

			// [Interpretation 4031] Set the object into the content types table.
			if ($db->getNumRows())
			{
				$market_status->type_id = $db->loadResult();
				$market_status_Updated = $db->updateObject('#__content_types', $market_status, 'type_id');
			}
			else
			{
				$market_status_Inserted = $db->insertObject('#__content_types', $market_status);
			}

			// [Interpretation 4013] Create the transaction_type content type object.
			$transaction_type = new stdClass();
			$transaction_type->type_title = 'Mostwantedrealestate Transaction_type';
			$transaction_type->type_alias = 'com_mostwantedrealestate.transaction_type';
			$transaction_type->table = '{"special": {"dbtable": "#__mostwantedrealestate_transaction_type","key": "id","type": "Transaction_type","prefix": "mostwantedrealestateTable","config": "array()"},"common": {"dbtable": "#__ucm_content","key": "ucm_id","type": "Corecontent","prefix": "JTable","config": "array()"}}';
			$transaction_type->field_mappings = '{"common": {"core_content_item_id": "id","core_title": "name","core_state": "published","core_alias": "alias","core_created_time": "created","core_modified_time": "modified","core_body": "null","core_hits": "hits","core_publish_up": "null","core_publish_down": "null","core_access": "access","core_params": "params","core_featured": "null","core_metadata": "metadata","core_language": "null","core_images": "null","core_urls": "null","core_version": "version","core_ordering": "ordering","core_metakey": "metakey","core_metadesc": "metadesc","core_catid": "null","core_xreference": "null","asset_id": "asset_id"},"special": {"name":"name","alias":"alias"}}';
			$transaction_type->router = 'MostwantedrealestateHelperRoute::getTransaction_typeRoute';
			$transaction_type->content_history_options = '{"formFile": "administrator/components/com_mostwantedrealestate/models/forms/transaction_type.xml","hideFields": ["asset_id","checked_out","checked_out_time","version"],"ignoreChanges": ["modified_by","modified","checked_out","checked_out_time","version","hits"],"convertToInt": ["published","ordering"],"displayLookup": [{"sourceColumn": "created_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "access","targetTable": "#__viewlevels","targetColumn": "id","displayColumn": "title"},{"sourceColumn": "modified_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"}]}';

			// [Interpretation 4022] Check if transaction_type type is already in content_type DB.
			$transaction_type_id = null;
			$query = $db->getQuery(true);
			$query->select($db->quoteName(array('type_id')));
			$query->from($db->quoteName('#__content_types'));
			$query->where($db->quoteName('type_alias') . ' LIKE '. $db->quote($transaction_type->type_alias));
			$db->setQuery($query);
			$db->execute();

			// [Interpretation 4031] Set the object into the content types table.
			if ($db->getNumRows())
			{
				$transaction_type->type_id = $db->loadResult();
				$transaction_type_Updated = $db->updateObject('#__content_types', $transaction_type, 'type_id');
			}
			else
			{
				$transaction_type_Inserted = $db->insertObject('#__content_types', $transaction_type);
			}

			// [Interpretation 4013] Create the rental_frequency content type object.
			$rental_frequency = new stdClass();
			$rental_frequency->type_title = 'Mostwantedrealestate Rental_frequency';
			$rental_frequency->type_alias = 'com_mostwantedrealestate.rental_frequency';
			$rental_frequency->table = '{"special": {"dbtable": "#__mostwantedrealestate_rental_frequency","key": "id","type": "Rental_frequency","prefix": "mostwantedrealestateTable","config": "array()"},"common": {"dbtable": "#__ucm_content","key": "ucm_id","type": "Corecontent","prefix": "JTable","config": "array()"}}';
			$rental_frequency->field_mappings = '{"common": {"core_content_item_id": "id","core_title": "name","core_state": "published","core_alias": "alias","core_created_time": "created","core_modified_time": "modified","core_body": "null","core_hits": "hits","core_publish_up": "null","core_publish_down": "null","core_access": "access","core_params": "params","core_featured": "null","core_metadata": "metadata","core_language": "null","core_images": "null","core_urls": "null","core_version": "version","core_ordering": "ordering","core_metakey": "metakey","core_metadesc": "metadesc","core_catid": "null","core_xreference": "null","asset_id": "asset_id"},"special": {"name":"name","alias":"alias"}}';
			$rental_frequency->router = 'MostwantedrealestateHelperRoute::getRental_frequencyRoute';
			$rental_frequency->content_history_options = '{"formFile": "administrator/components/com_mostwantedrealestate/models/forms/rental_frequency.xml","hideFields": ["asset_id","checked_out","checked_out_time","version"],"ignoreChanges": ["modified_by","modified","checked_out","checked_out_time","version","hits"],"convertToInt": ["published","ordering"],"displayLookup": [{"sourceColumn": "created_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "access","targetTable": "#__viewlevels","targetColumn": "id","displayColumn": "title"},{"sourceColumn": "modified_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"}]}';

			// [Interpretation 4022] Check if rental_frequency type is already in content_type DB.
			$rental_frequency_id = null;
			$query = $db->getQuery(true);
			$query->select($db->quoteName(array('type_id')));
			$query->from($db->quoteName('#__content_types'));
			$query->where($db->quoteName('type_alias') . ' LIKE '. $db->quote($rental_frequency->type_alias));
			$db->setQuery($query);
			$db->execute();

			// [Interpretation 4031] Set the object into the content types table.
			if ($db->getNumRows())
			{
				$rental_frequency->type_id = $db->loadResult();
				$rental_frequency_Updated = $db->updateObject('#__content_types', $rental_frequency, 'type_id');
			}
			else
			{
				$rental_frequency_Inserted = $db->insertObject('#__content_types', $rental_frequency);
			}

			// [Interpretation 4013] Create the rent_type content type object.
			$rent_type = new stdClass();
			$rent_type->type_title = 'Mostwantedrealestate Rent_type';
			$rent_type->type_alias = 'com_mostwantedrealestate.rent_type';
			$rent_type->table = '{"special": {"dbtable": "#__mostwantedrealestate_rent_type","key": "id","type": "Rent_type","prefix": "mostwantedrealestateTable","config": "array()"},"common": {"dbtable": "#__ucm_content","key": "ucm_id","type": "Corecontent","prefix": "JTable","config": "array()"}}';
			$rent_type->field_mappings = '{"common": {"core_content_item_id": "id","core_title": "name","core_state": "published","core_alias": "alias","core_created_time": "created","core_modified_time": "modified","core_body": "null","core_hits": "hits","core_publish_up": "null","core_publish_down": "null","core_access": "access","core_params": "params","core_featured": "null","core_metadata": "metadata","core_language": "null","core_images": "null","core_urls": "null","core_version": "version","core_ordering": "ordering","core_metakey": "metakey","core_metadesc": "metadesc","core_catid": "null","core_xreference": "null","asset_id": "asset_id"},"special": {"name":"name","alias":"alias"}}';
			$rent_type->router = 'MostwantedrealestateHelperRoute::getRent_typeRoute';
			$rent_type->content_history_options = '{"formFile": "administrator/components/com_mostwantedrealestate/models/forms/rent_type.xml","hideFields": ["asset_id","checked_out","checked_out_time","version"],"ignoreChanges": ["modified_by","modified","checked_out","checked_out_time","version","hits"],"convertToInt": ["published","ordering"],"displayLookup": [{"sourceColumn": "created_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "access","targetTable": "#__viewlevels","targetColumn": "id","displayColumn": "title"},{"sourceColumn": "modified_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"}]}';

			// [Interpretation 4022] Check if rent_type type is already in content_type DB.
			$rent_type_id = null;
			$query = $db->getQuery(true);
			$query->select($db->quoteName(array('type_id')));
			$query->from($db->quoteName('#__content_types'));
			$query->where($db->quoteName('type_alias') . ' LIKE '. $db->quote($rent_type->type_alias));
			$db->setQuery($query);
			$db->execute();

			// [Interpretation 4031] Set the object into the content types table.
			if ($db->getNumRows())
			{
				$rent_type->type_id = $db->loadResult();
				$rent_type_Updated = $db->updateObject('#__content_types', $rent_type, 'type_id');
			}
			else
			{
				$rent_type_Inserted = $db->insertObject('#__content_types', $rent_type);
			}

			// [Interpretation 4013] Create the feature_type content type object.
			$feature_type = new stdClass();
			$feature_type->type_title = 'Mostwantedrealestate Feature_type';
			$feature_type->type_alias = 'com_mostwantedrealestate.feature_type';
			$feature_type->table = '{"special": {"dbtable": "#__mostwantedrealestate_feature_type","key": "id","type": "Feature_type","prefix": "mostwantedrealestateTable","config": "array()"},"common": {"dbtable": "#__ucm_content","key": "ucm_id","type": "Corecontent","prefix": "JTable","config": "array()"}}';
			$feature_type->field_mappings = '{"common": {"core_content_item_id": "id","core_title": "featurename","core_state": "published","core_alias": "alias","core_created_time": "created","core_modified_time": "modified","core_body": "null","core_hits": "hits","core_publish_up": "null","core_publish_down": "null","core_access": "access","core_params": "params","core_featured": "null","core_metadata": "metadata","core_language": "null","core_images": "null","core_urls": "null","core_version": "version","core_ordering": "ordering","core_metakey": "metakey","core_metadesc": "metadesc","core_catid": "null","core_xreference": "null","asset_id": "asset_id"},"special": {"featurename":"featurename","featuretype":"featuretype","alias":"alias"}}';
			$feature_type->router = 'MostwantedrealestateHelperRoute::getFeature_typeRoute';
			$feature_type->content_history_options = '{"formFile": "administrator/components/com_mostwantedrealestate/models/forms/feature_type.xml","hideFields": ["asset_id","checked_out","checked_out_time","version"],"ignoreChanges": ["modified_by","modified","checked_out","checked_out_time","version","hits"],"convertToInt": ["published","ordering","featuretype"],"displayLookup": [{"sourceColumn": "created_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "access","targetTable": "#__viewlevels","targetColumn": "id","displayColumn": "title"},{"sourceColumn": "modified_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"}]}';

			// [Interpretation 4022] Check if feature_type type is already in content_type DB.
			$feature_type_id = null;
			$query = $db->getQuery(true);
			$query->select($db->quoteName(array('type_id')));
			$query->from($db->quoteName('#__content_types'));
			$query->where($db->quoteName('type_alias') . ' LIKE '. $db->quote($feature_type->type_alias));
			$db->setQuery($query);
			$db->execute();

			// [Interpretation 4031] Set the object into the content types table.
			if ($db->getNumRows())
			{
				$feature_type->type_id = $db->loadResult();
				$feature_type_Updated = $db->updateObject('#__content_types', $feature_type, 'type_id');
			}
			else
			{
				$feature_type_Inserted = $db->insertObject('#__content_types', $feature_type);
			}

			// [Interpretation 4013] Create the favorite_listing content type object.
			$favorite_listing = new stdClass();
			$favorite_listing->type_title = 'Mostwantedrealestate Favorite_listing';
			$favorite_listing->type_alias = 'com_mostwantedrealestate.favorite_listing';
			$favorite_listing->table = '{"special": {"dbtable": "#__mostwantedrealestate_favorite_listing","key": "id","type": "Favorite_listing","prefix": "mostwantedrealestateTable","config": "array()"},"common": {"dbtable": "#__ucm_content","key": "ucm_id","type": "Corecontent","prefix": "JTable","config": "array()"}}';
			$favorite_listing->field_mappings = '{"common": {"core_content_item_id": "id","core_title": "null","core_state": "published","core_alias": "null","core_created_time": "created","core_modified_time": "modified","core_body": "null","core_hits": "hits","core_publish_up": "null","core_publish_down": "null","core_access": "access","core_params": "params","core_featured": "null","core_metadata": "metadata","core_language": "null","core_images": "null","core_urls": "null","core_version": "version","core_ordering": "ordering","core_metakey": "metakey","core_metadesc": "metadesc","core_catid": "null","core_xreference": "null","asset_id": "asset_id"},"special": {"uid":"uid","propertyid":"propertyid"}}';
			$favorite_listing->router = 'MostwantedrealestateHelperRoute::getFavorite_listingRoute';
			$favorite_listing->content_history_options = '{"formFile": "administrator/components/com_mostwantedrealestate/models/forms/favorite_listing.xml","hideFields": ["asset_id","checked_out","checked_out_time","version"],"ignoreChanges": ["modified_by","modified","checked_out","checked_out_time","version","hits"],"convertToInt": ["published","ordering","uid","propertyid"],"displayLookup": [{"sourceColumn": "created_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "access","targetTable": "#__viewlevels","targetColumn": "id","displayColumn": "title"},{"sourceColumn": "modified_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "propertyid","targetTable": "#__mostwantedrealestate_property","targetColumn": "id","displayColumn": "name"}]}';

			// [Interpretation 4022] Check if favorite_listing type is already in content_type DB.
			$favorite_listing_id = null;
			$query = $db->getQuery(true);
			$query->select($db->quoteName(array('type_id')));
			$query->from($db->quoteName('#__content_types'));
			$query->where($db->quoteName('type_alias') . ' LIKE '. $db->quote($favorite_listing->type_alias));
			$db->setQuery($query);
			$db->execute();

			// [Interpretation 4031] Set the object into the content types table.
			if ($db->getNumRows())
			{
				$favorite_listing->type_id = $db->loadResult();
				$favorite_listing_Updated = $db->updateObject('#__content_types', $favorite_listing, 'type_id');
			}
			else
			{
				$favorite_listing_Inserted = $db->insertObject('#__content_types', $favorite_listing);
			}

			// [Interpretation 4013] Create the image content type object.
			$image = new stdClass();
			$image->type_title = 'Mostwantedrealestate Image';
			$image->type_alias = 'com_mostwantedrealestate.image';
			$image->table = '{"special": {"dbtable": "#__mostwantedrealestate_image","key": "id","type": "Image","prefix": "mostwantedrealestateTable","config": "array()"},"common": {"dbtable": "#__ucm_content","key": "ucm_id","type": "Corecontent","prefix": "JTable","config": "array()"}}';
			$image->field_mappings = '{"common": {"core_content_item_id": "id","core_title": "null","core_state": "published","core_alias": "null","core_created_time": "created","core_modified_time": "modified","core_body": "null","core_hits": "hits","core_publish_up": "null","core_publish_down": "null","core_access": "access","core_params": "params","core_featured": "null","core_metadata": "metadata","core_language": "null","core_images": "null","core_urls": "null","core_version": "version","core_ordering": "ordering","core_metakey": "metakey","core_metadesc": "metadesc","core_catid": "null","core_xreference": "null","asset_id": "asset_id"},"special": {"propid":"propid","path":"path","filename":"filename","type":"type","rets_source":"rets_source","title":"title","description":"description"}}';
			$image->router = 'MostwantedrealestateHelperRoute::getImageRoute';
			$image->content_history_options = '{"formFile": "administrator/components/com_mostwantedrealestate/models/forms/image.xml","hideFields": ["asset_id","checked_out","checked_out_time","version"],"ignoreChanges": ["modified_by","modified","checked_out","checked_out_time","version","hits"],"convertToInt": ["published","ordering","propid"],"displayLookup": [{"sourceColumn": "created_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"},{"sourceColumn": "access","targetTable": "#__viewlevels","targetColumn": "id","displayColumn": "title"},{"sourceColumn": "modified_by","targetTable": "#__users","targetColumn": "id","displayColumn": "name"}]}';

			// [Interpretation 4022] Check if image type is already in content_type DB.
			$image_id = null;
			$query = $db->getQuery(true);
			$query->select($db->quoteName(array('type_id')));
			$query->from($db->quoteName('#__content_types'));
			$query->where($db->quoteName('type_alias') . ' LIKE '. $db->quote($image->type_alias));
			$db->setQuery($query);
			$db->execute();

			// [Interpretation 4031] Set the object into the content types table.
			if ($db->getNumRows())
			{
				$image->type_id = $db->loadResult();
				$image_Updated = $db->updateObject('#__content_types', $image, 'type_id');
			}
			else
			{
				$image_Inserted = $db->insertObject('#__content_types', $image);
			}


			echo '<a target="_blank" href="http://mostwantedrealestatesites.com" title="Most Wanted Real Estate">
				<img src="components/com_mostwantedrealestate/assets/images/vdm-component.png"/>
				</a>
				<h3>Upgrade to Version 2.0.0 Was Successful! Let us know if anything is not working as expected.</h3>';
		}
	}
}
