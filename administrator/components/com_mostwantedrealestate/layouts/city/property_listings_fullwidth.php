<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		2.0.0
	@build			22nd September, 2017
	@created		1st May, 2016
	@package		Most Wanted Real Estate
	@subpackage		property_listings_fullwidth.php
	@author			Most Wanted Web Services, Inc. <http://mostwantedrealestatesites.com>	
	@copyright		Copyright (C) 2015-2017. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
	
	Most Wanted Real Estate Component
	
/------------------------------------------------------------------------------------------------------*/

// No direct access to this file

defined('_JEXEC') or die('Restricted access');

// set the defaults
$items	= $displayData->vwdproperty_listings;
$user	= JFactory::getUser();
$id	= $displayData->item->id;
$edit	= "index.php?option=com_mostwantedrealestate&view=properties&task=property.edit";
$ref	= ($id) ? "&ref=city&refid=".$id : "";
$new	= "index.php?option=com_mostwantedrealestate&view=property&layout=edit".$ref;
$can	= MostwantedrealestateHelper::getActions('property');

?>
<div class="form-vertical">
<?php if ($can->get('core.create')): ?>
	<a class="btn btn-small btn-success" href="<?php echo $new; ?>"><span class="icon-new icon-white"></span> <?php echo JText::_('COM_MOSTWANTEDREALESTATE_NEW'); ?></a><br /><br />
<?php endif; ?>
<?php if (MostwantedrealestateHelper::checkArray($items)): ?>
<table class="footable table data properties metro-blue" data-page-size="20" data-filter="#filter_properties">
<thead>
	<tr>
		<th data-toggle="true">
			<?php echo JText::_('COM_MOSTWANTEDREALESTATE_PROPERTY_NAME_LABEL'); ?>
		</th>
		<th data-hide="phone">
			<?php echo JText::_('COM_MOSTWANTEDREALESTATE_PROPERTY_MLS_ID_LABEL'); ?>
		</th>
		<th data-hide="phone">
			<?php echo JText::_('COM_MOSTWANTEDREALESTATE_PROPERTY_CITYID_LABEL'); ?>
		</th>
		<th data-hide="phone,tablet">
			<?php echo JText::_('COM_MOSTWANTEDREALESTATE_PROPERTY_STATEID_LABEL'); ?>
		</th>
		<th data-hide="phone,tablet">
			<?php echo JText::_('COM_MOSTWANTEDREALESTATE_PROPERTY_PROPERTY_CATEGORY'); ?>
		</th>
		<th data-hide="phone,tablet">
			<?php echo JText::_('COM_MOSTWANTEDREALESTATE_PROPERTY_FEATURED_LABEL'); ?>
		</th>
		<th width="10" data-hide="phone,tablet">
			<?php echo JText::_('COM_MOSTWANTEDREALESTATE_PROPERTY_STATUS'); ?>
		</th>
		<th width="5" data-type="numeric" data-hide="phone,tablet">
			<?php echo JText::_('COM_MOSTWANTEDREALESTATE_PROPERTY_ID'); ?>
		</th>
	</tr>
</thead>
<tbody>
<?php foreach ($items as $i => $item): ?>
	<?php
		$canCheckin = $user->authorise('core.manage', 'com_checkin') || $item->checked_out == $user->id || $item->checked_out == 0;
		$userChkOut = JFactory::getUser($item->checked_out);
		$canDo = MostwantedrealestateHelper::getActions('property',$item,'properties');
	?>
	<tr>
		<td class="nowrap">
			<?php if ($canDo->get('core.edit')): ?>
				<a href="<?php echo $edit; ?>&id=<?php echo $item->id; ?>&ref=city&refid=<?php echo $id; ?>"><?php echo $displayData->escape($item->name); ?></a>
					<?php if ($item->checked_out): ?>
						<?php echo JHtml::_('jgrid.checkedout', $i, $userChkOut->name, $item->checked_out_time, 'properties.', $canCheckin); ?>
					<?php endif; ?>
			<?php else: ?>
				<div class="name"><?php echo $displayData->escape($item->name); ?></div>
			<?php endif; ?>
		</td>
		<td class="nowrap">
			<?php if ($canDo->get('core.edit')): ?>
				<a href="<?php echo $edit; ?>&id=<?php echo $item->id; ?>&ref=city&refid=<?php echo $id; ?>"><?php echo $displayData->escape($item->mls_id); ?></a>
					<?php if ($item->checked_out): ?>
						<?php echo JHtml::_('jgrid.checkedout', $i, $userChkOut->name, $item->checked_out_time, 'properties.', $canCheckin); ?>
					<?php endif; ?>
			<?php else: ?>
				<div class="name"><?php echo $displayData->escape($item->mls_id); ?></div>
			<?php endif; ?>
		</td>
		<td>
			<?php echo $displayData->escape($item->cityid_name); ?>
		</td>
		<td>
			<?php echo $displayData->escape($item->stateid_name); ?>
		</td>
		<td>
			<?php echo $displayData->escape($item->category_title); ?>
		</td>
		<td>
			<?php echo JText::_($item->featured); ?>
		</td>
		<?php if ($item->published == 1):?>
			<td class="center"  data-value="1">
				<span class="status-metro status-published" title="<?php echo JText::_('PUBLISHED');  ?>">
					<?php echo JText::_('PUBLISHED'); ?>
				</span>
			</td>
		<?php elseif ($item->published == 0):?>
			<td class="center"  data-value="2">
				<span class="status-metro status-inactive" title="<?php echo JText::_('INACTIVE');  ?>">
					<?php echo JText::_('INACTIVE'); ?>
				</span>
			</td>
		<?php elseif ($item->published == 2):?>
			<td class="center"  data-value="3">
				<span class="status-metro status-archived" title="<?php echo JText::_('ARCHIVED');  ?>">
					<?php echo JText::_('ARCHIVED'); ?>
				</span>
			</td>
		<?php elseif ($item->published == -2):?>
			<td class="center"  data-value="4">
				<span class="status-metro status-trashed" title="<?php echo JText::_('ARCHIVED');  ?>">
					<?php echo JText::_('ARCHIVED'); ?>
				</span>
			</td>
		<?php endif; ?>
		<td class="nowrap center hidden-phone">
			<?php echo $item->id; ?>
		</td>
	</tr>
<?php endforeach; ?>
</tbody>
<tfoot class="hide-if-no-paging">
	<tr>
		<td colspan="8">
			<div class="pagination pagination-centered"></div>
		</td>
	</tr>
</tfoot>
</table>
<?php else: ?>
	<div class="alert alert-no-items">
		<?php echo JText::_('JGLOBAL_NO_MATCHING_RESULTS'); ?>
	</div>
<?php endif; ?>
</div>
