/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		2.0.0
	@build			22nd September, 2017
	@created		1st May, 2016
	@package		Most Wanted Real Estate
	@subpackage		submitbutton.js
	@author			Most Wanted Web Services, Inc. <http://mostwantedrealestatesites.com>	
	@copyright		Copyright (C) 2015-2017. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
	
	Most Wanted Real Estate Component
	
/------------------------------------------------------------------------------------------------------*/

Joomla.submitbutton = function(task)
{
/***[INSERTED$$$$]***//*4*/
	//autofill empty name field on save
	if( (jQuery('#jform_name').val() == '' )){
		var name;
		var street = jQuery('#jform_street').val() != ''
			? jQuery('#jform_street').val().replace(/\s+/g, ' ').replace(/\.+/g,'') + ' ' : '';

		var city = jQuery('#jform_cityid').val() != ''
			? jQuery("#jform_cityid option:selected").text() + ', ' : '';

		var state = jQuery('#jform_stateid').val() != ''
			?  jQuery("#jform_stateid option:selected").text() + ' ': '';

		var postcode = jQuery('#jform_postcode').val() != ''
			?  jQuery('#jform_postcode').val() : '';

		name = street + city + state + postcode;

        if (name.charAt(name.length - 1) == ' ') {
            name = name.substr(0, name.length - 1);
        }

        jQuery('#jform_name').val(name);

	}

/***[/INSERTED$$$$]***/
	if (task == ''){
		return false;
	} else { 
		var isValid=true;
		var action = task.split('.');
		if (action[1] != 'cancel' && action[1] != 'close'){
			var forms = $$('form.form-validate');
			for (var i=0;i<forms.length;i++){
				if (!document.formvalidator.isValid(forms[i])){
					isValid = false;
					break;
				}
			}
		}
		if (isValid){
			Joomla.submitform(task);
			return true;
		} else {
			alert(Joomla.JText._('property, some values are not acceptable.','Some values are unacceptable'));
			return false;
		}
	}
}