<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		2.0.0
	@build			22nd September, 2017
	@created		1st May, 2016
	@package		Most Wanted Real Estate
	@subpackage		edit.php
	@author			Most Wanted Web Services, Inc. <http://mostwantedrealestatesites.com>	
	@copyright		Copyright (C) 2015-2017. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
	
	Most Wanted Real Estate Component
	
/------------------------------------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');
$componentParams = JComponentHelper::getParams('com_mostwantedrealestate');
?>
<script type="text/javascript">
	// waiting spinner
	var outerDiv = jQuery('body');
	jQuery('<div id="loading"></div>')
		.css("background", "rgba(255, 255, 255, .8) url('components/com_mostwantedrealestate/assets/images/import.gif') 50% 15% no-repeat")
		.css("top", outerDiv.position().top - jQuery(window).scrollTop())
		.css("left", outerDiv.position().left - jQuery(window).scrollLeft())
		.css("width", outerDiv.width())
		.css("height", outerDiv.height())
		.css("position", "fixed")
		.css("opacity", "0.80")
		.css("-ms-filter", "progid:DXImageTransform.Microsoft.Alpha(Opacity = 80)")
		.css("filter", "alpha(opacity = 80)")
		.css("display", "none")
		.appendTo(outerDiv);
	jQuery('#loading').show();
	// when page is ready remove and show
	jQuery(window).load(function() {
		jQuery('#mostwantedrealestate_loader').fadeIn('fast');
		jQuery('#loading').hide();
	});
</script>
<div id="mostwantedrealestate_loader" style="display: none;">
<form action="<?php echo JRoute::_('index.php?option=com_mostwantedrealestate&layout=edit&id='.(int) $this->item->id.$this->referral); ?>" method="post" name="adminForm" id="adminForm" class="form-validate" enctype="multipart/form-data">

	<?php echo JLayoutHelper::render('property.main_details_above', $this); ?>
<div class="form-horizontal">
	<div class="span9">

	<?php echo JHtml::_('bootstrap.startTabSet', 'propertyTab', array('active' => 'main_details')); ?>

	<?php echo JHtml::_('bootstrap.addTab', 'propertyTab', 'main_details', JText::_('COM_MOSTWANTEDREALESTATE_PROPERTY_MAIN_DETAILS', true)); ?>
		<div class="row-fluid form-horizontal-desktop">
			<div class="span6">
				<?php echo JLayoutHelper::render('property.main_details_left', $this); ?>
			</div>
			<div class="span6">
				<?php echo JLayoutHelper::render('property.main_details_right', $this); ?>
			</div>
		</div>
	<?php echo JHtml::_('bootstrap.endTab'); ?>

	<?php echo JHtml::_('bootstrap.addTab', 'propertyTab', 'map_details', JText::_('COM_MOSTWANTEDREALESTATE_PROPERTY_MAP_DETAILS', true)); ?>
		<div class="row-fluid form-horizontal-desktop">
			<div class="span12">
				<?php echo JLayoutHelper::render('property.map_details_left', $this); ?>
			</div>
		</div>
		<div class="row-fluid form-horizontal-desktop">
			<div class="span12">
				<?php echo JLayoutHelper::render('property.map_details_fullwidth', $this); ?>
			</div>
		</div>
<!--[INSERTED$$$$]--><!--2-->
		<div class="span12">
		    <?php if($componentParams['map_provider'] == '1'): ?>				
			<div id="googleMap" style="height:<?php echo (int)$componentParams['mw_mapheight'].'px'?>; width:<?php echo (int)$componentParams['mw_mapwidth'].'px'?>;"></div>		
			<input type="hidden" id="googleMapRes" value="<?php echo $componentParams['zoom'];?>" />										
			<!--<input type="hidden" id="googleMapApikey" value="<?php echo $componentParams['gmapsapi'];?>" />-->										
			<?php endif; ?>	
                        
                        <?php if($componentParams['map_provider'] == '2'): ?>				
			<div id="bingMap" style="height:<?php echo (int)$componentParams['mw_mapheight'].'px'?>; width:<?php echo (int)$componentParams['mw_mapwidth'].'px'?>;;  position: absolute;"></div>		
			<input type="hidden" id="bingMapRes" value="<?php echo $componentParams['zoom'];?>" />										
			<input type="hidden" id="bingMapApikey" value="<?php echo $componentParams['bingmapsapi'];?>" />										
			<?php endif; ?>	
		</div>
<!--[/INSERTED$$$$]-->
	<?php echo JHtml::_('bootstrap.endTab'); ?>

	<?php echo JHtml::_('bootstrap.addTab', 'propertyTab', 'general', JText::_('COM_MOSTWANTEDREALESTATE_PROPERTY_GENERAL', true)); ?>
		<div class="row-fluid form-horizontal-desktop">
			<div class="span6">
				<?php echo JLayoutHelper::render('property.general_left', $this); ?>
			</div>
			<div class="span6">
				<?php echo JLayoutHelper::render('property.general_right', $this); ?>
			</div>
		</div>
	<?php echo JHtml::_('bootstrap.endTab'); ?>

	<?php echo JHtml::_('bootstrap.addTab', 'propertyTab', 'residential', JText::_('COM_MOSTWANTEDREALESTATE_PROPERTY_RESIDENTIAL', true)); ?>
		<div class="row-fluid form-horizontal-desktop">
			<div class="span6">
				<?php echo JLayoutHelper::render('property.residential_left', $this); ?>
			</div>
			<div class="span6">
				<?php echo JLayoutHelper::render('property.residential_right', $this); ?>
			</div>
		</div>
		<div class="row-fluid form-horizontal-desktop">
			<div class="span12">
				<?php echo JLayoutHelper::render('property.residential_fullwidth', $this); ?>
			</div>
		</div>
	<?php echo JHtml::_('bootstrap.endTab'); ?>

	<?php echo JHtml::_('bootstrap.addTab', 'propertyTab', 'features', JText::_('COM_MOSTWANTEDREALESTATE_PROPERTY_FEATURES', true)); ?>
		<div class="row-fluid form-horizontal-desktop">
			<div class="span12">
				<?php echo JLayoutHelper::render('property.features_left', $this); ?>
			</div>
		</div>
	<?php echo JHtml::_('bootstrap.endTab'); ?>

	<?php echo JHtml::_('bootstrap.addTab', 'propertyTab', 'commercial', JText::_('COM_MOSTWANTEDREALESTATE_PROPERTY_COMMERCIAL', true)); ?>
		<div class="row-fluid form-horizontal-desktop">
			<div class="span6">
				<?php echo JLayoutHelper::render('property.commercial_left', $this); ?>
			</div>
			<div class="span6">
				<?php echo JLayoutHelper::render('property.commercial_right', $this); ?>
			</div>
		</div>
	<?php echo JHtml::_('bootstrap.endTab'); ?>

	<?php echo JHtml::_('bootstrap.addTab', 'propertyTab', 'multifamily', JText::_('COM_MOSTWANTEDREALESTATE_PROPERTY_MULTIFAMILY', true)); ?>
		<div class="row-fluid form-horizontal-desktop">
			<div class="span6">
				<?php echo JLayoutHelper::render('property.multifamily_left', $this); ?>
			</div>
			<div class="span6">
				<?php echo JLayoutHelper::render('property.multifamily_right', $this); ?>
			</div>
		</div>
		<div class="row-fluid form-horizontal-desktop">
			<div class="span12">
				<?php echo JLayoutHelper::render('property.multifamily_fullwidth', $this); ?>
			</div>
		</div>
	<?php echo JHtml::_('bootstrap.endTab'); ?>

	<?php echo JHtml::_('bootstrap.addTab', 'propertyTab', 'farmranch', JText::_('COM_MOSTWANTEDREALESTATE_PROPERTY_FARMRANCH', true)); ?>
		<div class="row-fluid form-horizontal-desktop">
			<div class="span6">
				<?php echo JLayoutHelper::render('property.farmranch_left', $this); ?>
			</div>
			<div class="span6">
				<?php echo JLayoutHelper::render('property.farmranch_right', $this); ?>
			</div>
		</div>
	<?php echo JHtml::_('bootstrap.endTab'); ?>

	<?php echo JHtml::_('bootstrap.addTab', 'propertyTab', 'rentals', JText::_('COM_MOSTWANTEDREALESTATE_PROPERTY_RENTALS', true)); ?>
		<div class="row-fluid form-horizontal-desktop">
			<div class="span12">
				<?php echo JLayoutHelper::render('property.rentals_left', $this); ?>
			</div>
		</div>
	<?php echo JHtml::_('bootstrap.endTab'); ?>

	<?php echo JHtml::_('bootstrap.addTab', 'propertyTab', 'media', JText::_('COM_MOSTWANTEDREALESTATE_PROPERTY_MEDIA', true)); ?>
		<div class="row-fluid form-horizontal-desktop">
			<div class="span12">
				<?php echo JLayoutHelper::render('property.media_left', $this); ?>
			</div>
<!--[INSERTED$$$$]--><!--3-->
                    <?php if((int) $this->item->id) : ?>

                    <div class="span11">
                                <!-- (start) Section to add images using fileinput -->
                                <div id="actions" class="row">

                                    <div class="control-group">
                                        Upload Property Images or Drop here
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <input id="images"  name='file[]' type="file" multiple >
                                        </div>
                                    </div>
                                </div>
                                <!-- (end) Section to add images using fileinput -->

                            </div>
                            
                            
                    <?php //var_dump($this->item->images);?>
                    <script>

                        jQuery("#images").fileinput({
                            uploadAsync: false,
                            overwriteInitial: false,
                            allowedFileExtensions: ['jpg','jpeg','png','gif'],
                            uploadUrl: "<?php echo JURI::root().'administrator/index.php?option=com_mostwantedrealestate&task=property.uploadImage&propertyId='.(int) $this->item->id; ?>", // your upload server url
                            initialPreview: [
                                <?php foreach($this->item->images as $image):?>
                                '<img src="<?php echo JURI::root().$image->path.$image->filename; ?>" class="file-preview-image" title="<?php echo $image->title ?>" alt="<?php echo $image->title ?>" >',
                                <?php endforeach; ?>
                            ],
                            initialPreviewConfig: [
                                <?php foreach($this->item->images as $image): ?>
                                {
                                    size: <?php echo filesize(JPATH_ROOT.$image->path.$image->original_filename);?>,
                                    key: <?php echo $image->id; ?>,
                                    caption: '<?php echo $image->title ?>',
                                    width: '160px',
                                    url: "<?php echo JURI::root().'administrator/index.php?option=com_mostwantedrealestate&task=property.deleteImage&propertyId='.(int) $this->item->id; ?>"
                                },
                                <?php endforeach; ?>
                            ]


                        });

                        /*function fireFileSorted(){
                         jQuery(document).on("filesorted", '#images',function(event, params){
                         console.log('file sorted');
                         });
                         }

                         jQuery(document).ready(fireFileSorted);*/

                        jQuery(function(){
                            jQuery(document).on("filesorted", '#images',function(event, params){
                                data = params.stack.map(function(value,index){
                                    var obj = {
                                        key: value.key,
                                        index: index
                                    };
                                    return obj;
                                });
                                console.log(data);
                                jQuery.ajax({
                                    url: "<?php echo JURI::root().'administrator/index.php?option=com_mostwantedrealestate&task=property.orderImages&propertyId='.(int) $this->item->id; ?>",
                                    data: {ordering:JSON.stringify(data)},
                                    method: 'POST',
                                    
                                    success: function (data) {
                                    
                                    }
                                });
                            });
                        });
                    </script>
                <?php endif; ?>
<!--[/INSERTED$$$$]-->
		</div>
	<?php echo JHtml::_('bootstrap.endTab'); ?>

	<?php echo JHtml::_('bootstrap.addTab', 'propertyTab', 'open_houses', JText::_('COM_MOSTWANTEDREALESTATE_PROPERTY_OPEN_HOUSES', true)); ?>
		<div class="row-fluid form-horizontal-desktop">
			<div class="span12">
				<?php echo JLayoutHelper::render('property.open_houses_left', $this); ?>
			</div>
		</div>
	<?php echo JHtml::_('bootstrap.endTab'); ?>

	<?php echo JHtml::_('bootstrap.addTab', 'propertyTab', 'financial', JText::_('COM_MOSTWANTEDREALESTATE_PROPERTY_FINANCIAL', true)); ?>
		<div class="row-fluid form-horizontal-desktop">
			<div class="span6">
				<?php echo JLayoutHelper::render('property.financial_left', $this); ?>
			</div>
			<div class="span6">
				<?php echo JLayoutHelper::render('property.financial_right', $this); ?>
			</div>
		</div>
	<?php echo JHtml::_('bootstrap.endTab'); ?>

	<?php echo JHtml::_('bootstrap.addTab', 'propertyTab', 'area_info', JText::_('COM_MOSTWANTEDREALESTATE_PROPERTY_AREA_INFO', true)); ?>
		<div class="row-fluid form-horizontal-desktop">
			<div class="span6">
				<?php echo JLayoutHelper::render('property.area_info_left', $this); ?>
			</div>
			<div class="span6">
				<?php echo JLayoutHelper::render('property.area_info_right', $this); ?>
			</div>
		</div>
	<?php echo JHtml::_('bootstrap.endTab'); ?>

	<?php if ($this->canDo->get('core.delete') || $this->canDo->get('core.edit.created_by') || $this->canDo->get('core.edit.state') || $this->canDo->get('core.edit.created')) : ?>
	<?php echo JHtml::_('bootstrap.addTab', 'propertyTab', 'publishing', JText::_('COM_MOSTWANTEDREALESTATE_PROPERTY_PUBLISHING', true)); ?>
		<div class="row-fluid form-horizontal-desktop">
			<div class="span6">
				<?php echo JLayoutHelper::render('property.publishing', $this); ?>
			</div>
			<div class="span6">
				<?php echo JLayoutHelper::render('property.metadata', $this); ?>
			</div>
		</div>
	<?php echo JHtml::_('bootstrap.endTab'); ?>
	<?php endif; ?>

	<?php if ($this->canDo->get('core.admin')) : ?>
	<?php echo JHtml::_('bootstrap.addTab', 'propertyTab', 'permissions', JText::_('COM_MOSTWANTEDREALESTATE_PROPERTY_PERMISSION', true)); ?>
		<div class="row-fluid form-horizontal-desktop">
			<div class="span12">
				<fieldset class="adminform">
					<div class="adminformlist">
					<?php foreach ($this->form->getFieldset('accesscontrol') as $field): ?>
						<div>
							<?php echo $field->label; echo $field->input;?>
						</div>
						<div class="clearfix"></div>
					<?php endforeach; ?>
					</div>
				</fieldset>
			</div>
		</div>
	<?php echo JHtml::_('bootstrap.endTab'); ?>
	<?php endif; ?>

	<?php echo JHtml::_('bootstrap.endTabSet'); ?>

	<div>
		<input type="hidden" name="task" value="property.edit" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
	</div>
</div><div class="span3">
	<?php echo JLayoutHelper::render('property.main_details_rightside', $this); ?>
</div>
</form>
</div>

<script type="text/javascript">

// #jform_showprice listeners for showprice_vvvvvwa function
jQuery('#jform_showprice').on('keyup',function()
{
	var showprice_vvvvvwa = jQuery("#jform_showprice input[type='radio']:checked").val();
	vvvvvwa(showprice_vvvvvwa);

});
jQuery('#adminForm').on('change', '#jform_showprice',function (e)
{
	e.preventDefault();
	var showprice_vvvvvwa = jQuery("#jform_showprice input[type='radio']:checked").val();
	vvvvvwa(showprice_vvvvvwa);

});

// #jform_showprice listeners for showprice_vvvvvwb function
jQuery('#jform_showprice').on('keyup',function()
{
	var showprice_vvvvvwb = jQuery("#jform_showprice input[type='radio']:checked").val();
	vvvvvwb(showprice_vvvvvwb);

});
jQuery('#adminForm').on('change', '#jform_showprice',function (e)
{
	e.preventDefault();
	var showprice_vvvvvwb = jQuery("#jform_showprice input[type='radio']:checked").val();
	vvvvvwb(showprice_vvvvvwb);

});

// #jform_openhouse listeners for openhouse_vvvvvwc function
jQuery('#jform_openhouse').on('keyup',function()
{
	var openhouse_vvvvvwc = jQuery("#jform_openhouse input[type='radio']:checked").val();
	vvvvvwc(openhouse_vvvvvwc);

});
jQuery('#adminForm').on('change', '#jform_openhouse',function (e)
{
	e.preventDefault();
	var openhouse_vvvvvwc = jQuery("#jform_openhouse input[type='radio']:checked").val();
	vvvvvwc(openhouse_vvvvvwc);

});

// #jform_pm_price_override listeners for pm_price_override_vvvvvwd function
jQuery('#jform_pm_price_override').on('keyup',function()
{
	var pm_price_override_vvvvvwd = jQuery("#jform_pm_price_override input[type='radio']:checked").val();
	vvvvvwd(pm_price_override_vvvvvwd);

});
jQuery('#adminForm').on('change', '#jform_pm_price_override',function (e)
{
	e.preventDefault();
	var pm_price_override_vvvvvwd = jQuery("#jform_pm_price_override input[type='radio']:checked").val();
	vvvvvwd(pm_price_override_vvvvvwd);

});

// #jform_owncoords listeners for owncoords_vvvvvwe function
jQuery('#jform_owncoords').on('keyup',function()
{
	var owncoords_vvvvvwe = jQuery("#jform_owncoords input[type='radio']:checked").val();
	vvvvvwe(owncoords_vvvvvwe);

});
jQuery('#adminForm').on('change', '#jform_owncoords',function (e)
{
	e.preventDefault();
	var owncoords_vvvvvwe = jQuery("#jform_owncoords input[type='radio']:checked").val();
	vvvvvwe(owncoords_vvvvvwe);

});

// #jform_viewbooking listeners for viewbooking_vvvvvwf function
jQuery('#jform_viewbooking').on('keyup',function()
{
	var viewbooking_vvvvvwf = jQuery("#jform_viewbooking input[type='radio']:checked").val();
	vvvvvwf(viewbooking_vvvvvwf);

});
jQuery('#adminForm').on('change', '#jform_viewbooking',function (e)
{
	e.preventDefault();
	var viewbooking_vvvvvwf = jQuery("#jform_viewbooking input[type='radio']:checked").val();
	vvvvvwf(viewbooking_vvvvvwf);

});

// #jform_waterfront listeners for waterfront_vvvvvwg function
jQuery('#jform_waterfront').on('keyup',function()
{
	var waterfront_vvvvvwg = jQuery("#jform_waterfront input[type='radio']:checked").val();
	vvvvvwg(waterfront_vvvvvwg);

});
jQuery('#adminForm').on('change', '#jform_waterfront',function (e)
{
	e.preventDefault();
	var waterfront_vvvvvwg = jQuery("#jform_waterfront input[type='radio']:checked").val();
	vvvvvwg(waterfront_vvvvvwg);

});



	//autofill empty name field on save
	if( (jQuery('#jform_name').val() == '' )){
		var name;
		var street = jQuery('#jform_street').val() != ''
			? jQuery('#jform_street').val().replace(/\s+/g, '-').replace(/\.+/g,'').toLowerCase() + '-' : '';

		var city = jQuery('#jform_cityid').val() != ''
			? jQuery("#jform_cityid option:selected").text().toLowerCase() + '-' : '';

		var state = jQuery('#jform_stateid').val() != ''
			?  jQuery("#jform_stateid option:selected").text().toLowerCase() + '-': '';

		var postcode = jQuery('#jform_postcode').val() != ''
			?  jQuery('#jform_postcode').val() : '';

		name = street + city + state + postcode;

        if (name.charAt(name.length - 1) == '-') {
            name = name.substr(0, name.length - 1);
        }

        jQuery('#jform_name').val(name);

	}

<?php
// make sure these frameworks also load.
JHtml::_('jquery.framework');
JHtml::_('jquery.ui');
$doc = JFactory::getDocument();
//loaded from the code.jquery.com site
$doc->addStylesheet('https://code.jquery.com/ui/1.12.0/themes/smoothness/jquery-ui.css');
$doc->addStylesheet('https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.6.3/jquery-ui-timepicker-addon.min.css');
$doc->addScript('https://code.jquery.com/ui/1.12.1/jquery-ui.min.js');
$doc->addScript('https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.6.3/jquery-ui-timepicker-addon.min.js');
?>

// Date Time Picker
<?php $fieldNrs = range(1,50,1); $fieldType = array('ohend'=>'openhouseinfo','ohstart'=>'openhouseinfo');?>
jQuery('input.form-field-repeatable').on('row-add',function(e){
<?php foreach($fieldType as $type=>$field):?>
<?php foreach($fieldNrs as $nr):?>
	jQuery('#jform_<?php echo $field; ?>_fields_<?php echo $type; ?>-<?php echo $nr;?>').datetimepicker(
			{
				minDate:-1,
				prevText:'',
				nextText:'',
				maxDate:'+36M',
				firstDay:1,
				dateFormat:'yy-mm-dd',
				onSelect: function(dateText, inst){
				jQuery('#jform_<?php echo $field; ?>_fields_<?php echo $type; ?>-<?php echo $nr; ?>').val(dateText);
						}
					});
<?php endforeach; ?>
<?php endforeach; ?>
});
    // Image Code
    <?php
    // make sure these frameworks also load.
    JHtml::_('jquery.framework');
    JHtml::_('jquery.ui');
    $doc = JFactory::getDocument();
    //loaded from the code.jquery.com site
    $doc->addStylesheet('http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css');
    $doc->addScript('http://code.jquery.com/ui/1.10.3/jquery-ui.min.js');
    $doc->addScript(JURI::base().'components/com_mostwantedrealestate/assets/js/sortable.js');
    $doc->addScript(JURI::base().'components/com_mostwantedrealestate/assets/js/fileinput.js');
//    $doc->addStylesheet('http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css');
    $doc->addStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
    $doc->addStylesheet(JURI::base().'components/com_mostwantedrealestate/assets/css/fileinput.css');
    $doc->addStylesheet(JURI::base().'components/com_mostwantedrealestate/assets/css/fileinput2.css');
    ?>
// (start) Section to initiate google map
<?php if($componentParams['map_provider'] == '1'): 
$doc->addScript('https://maps.googleapis.com/maps/api/js?key=' . $componentParams['gmapsapi'] );
$doc->addScript(JURI::base().'components/com_mostwantedrealestate/assets/js/googlemap.js');
?>
var mapTabClicked = false;
jQuery(document).ready(function (){
    jQuery('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                mapTabClicked = true;
		var liText = jQuery(this).text();
		if(liText == 'Map Details')
		{
                     var lat = document.getElementById("jform_latitude").value;
                     var long = document.getElementById("jform_longitude").value;
                    initialize(lat, long);
                    var center = map.getCenter();
                   google.maps.event.trigger(map, 'resize');
		map.setCenter(center);        
                }
             
    });
	
});
<?php endif; ?>
// (end) Section to initiate google map


// (start) Section to initiate Bing map
<?php if($componentParams['map_provider'] == '2'): 
    $doc->addScript('http://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=7.0');
    $doc->addScript(JURI::base().'components/com_mostwantedrealestate/assets/js/bingmap.js');
?>
        var mapTabClicked = false;
jQuery(function (){
    jQuery('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		var liText = jQuery(this).text();
		if(liText == 'Map Details')
		{
            mapTabClicked = true;
            GetMap();
        }
    });
});
<?php endif; ?>
// (end) Section to initiate Bing map

jQuery(function (){
    
    jQuery('#jform_owncoords .btn').click(function(){
            var mapOption = jQuery("#jform_owncoords input[type='radio']:checked").val();
            if(mapOption == '0') jQuery('#getlatlong').hide();
            else jQuery('#getlatlong').show();
    });
            
    jQuery('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    
        var liText = jQuery(this).text();
        if(liText == 'Map Details'){
            mapTabClicked = true;
            var mapOption = jQuery("#jform_owncoords input[type='radio']:checked").val();
            if(mapOption == '0') jQuery('#getlatlong').hide();
        }
        
        if(liText == 'Media'){
            jQuery('.file-zoom-content').hide();
        }
    
    });

});

    var addressChangeTimeOut = null;
    var streetControl   = jQuery( "#jform_street" );
    var cityControl     = jQuery( "#jform_cityid" );
    var stateControl    = jQuery( "#jform_stateid" );
    var postcodeControl = jQuery( "#jform_postcode" );
    var countryControl  = jQuery( "#jform_countryid" );

        function updateAddressAndReGeocode(){
            geocodeAddress.street   = streetControl.val()   != '' ? streetControl.val() : '';

            geocodeAddress.city     = cityControl.find('option:selected').val()      != '' ? cityControl.find('option:selected').text() : '';

            geocodeAddress.state    = stateControl.find('option:selected').val()     != '' ? stateControl.find('option:selected').text() : '';

            geocodeAddress.postcode = postcodeControl.val()  != '' ? postcodeControl.val() : '';

            geocodeAddress.country  = countryControl.find('option:selected').val()   != '' ? countryControl.find('option:selected').text() : '';
            
            reGeocode();
        }

    var geocodeAddress = {
        street: "",
        city: "",
        state: "",
        postcode: "",
        country: ""
    };
    
    jQuery(function(){

        jQuery(streetControl).on('keyup',function(){
            if(addressChangeTimeOut != null) clearTimeout(addressChangeTimeOut);
            addressChangeTimeOut = setTimeout(updateAddressAndReGeocode, 1000);
        });

        jQuery(cityControl).on('change',function(){
            updateAddressAndReGeocode();
        });

        jQuery(stateControl).on('change',function()   {
            updateAddressAndReGeocode();
        });

        jQuery(postcodeControl).on('keyup',function(){
            if(addressChangeTimeOut != null) clearTimeout(addressChangeTimeOut);
            addressChangeTimeOut = setTimeout(updateAddressAndReGeocode, 1000);
        });

        jQuery(countryControl).on('change',function(){
            updateAddressAndReGeocode();
        });

        jQuery( "#jform_owncoords1" ).on('change',function(){
            if(jQuery(this).is(':checked')){updateAddressAndReGeocode();}
        });
        
    });

</script>
