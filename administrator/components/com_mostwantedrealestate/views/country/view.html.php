<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		2.0.0
	@build			22nd September, 2017
	@created		1st May, 2016
	@package		Most Wanted Real Estate
	@subpackage		view.html.php
	@author			Most Wanted Web Services, Inc. <http://mostwantedrealestatesites.com>	
	@copyright		Copyright (C) 2015-2017. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
	
	Most Wanted Real Estate Component
	
/------------------------------------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla view library
jimport('joomla.application.component.view');

/**
 * Country View class
 */
class MostwantedrealestateViewCountry extends JViewLegacy
{
	/**
	 * display method of View
	 * @return void
	 */
	public function display($tpl = null)
	{
		// Check for errors.
		if (count($errors = $this->get('Errors')))
                {
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}

		// Assign the variables
		$this->form 		= $this->get('Form');
		$this->item 		= $this->get('Item');
		$this->script 		= $this->get('Script');
		$this->state		= $this->get('State');
                // get action permissions
		$this->canDo		= MostwantedrealestateHelper::getActions('country',$this->item);
		// get input
		$jinput = JFactory::getApplication()->input;
		$this->ref 		= $jinput->get('ref', 0, 'word');
		$this->refid            = $jinput->get('refid', 0, 'int');
		$this->referral         = '';
		if ($this->refid)
                {
                        // return to the item that refered to this item
                        $this->referral = '&ref='.(string)$this->ref.'&refid='.(int)$this->refid;
                }
                elseif($this->ref)
                {
                        // return to the list view that refered to this item
                        $this->referral = '&ref='.(string)$this->ref;
                }

		// [Interpretation 6711] Get Linked view data
		$this->vvxproperty_listings		= $this->get('Vvxproperty_listings');

		// Set the toolbar
		$this->addToolBar();

		// Display the template
		parent::display($tpl);

		// Set the document
		$this->setDocument();
	}


	/**
	 * Setting the toolbar
	 */
	protected function addToolBar()
	{
		JFactory::getApplication()->input->set('hidemainmenu', true);
		$user = JFactory::getUser();
		$userId	= $user->id;
		$isNew = $this->item->id == 0;

		JToolbarHelper::title( JText::_($isNew ? 'COM_MOSTWANTEDREALESTATE_COUNTRY_NEW' : 'COM_MOSTWANTEDREALESTATE_COUNTRY_EDIT'), 'pencil-2 article-add');
		// [Interpretation 10763] Built the actions for new and existing records.
		if ($this->refid || $this->ref)
		{
			if ($this->canDo->get('core.create') && $isNew)
			{
				// [Interpretation 10775] We can create the record.
				JToolBarHelper::save('country.save', 'JTOOLBAR_SAVE');
			}
			elseif ($this->canDo->get('core.edit'))
			{
				// [Interpretation 10787] We can save the record.
				JToolBarHelper::save('country.save', 'JTOOLBAR_SAVE');
			}
			if ($isNew)
			{
				// [Interpretation 10792] Do not creat but cancel.
				JToolBarHelper::cancel('country.cancel', 'JTOOLBAR_CANCEL');
			}
			else
			{
				// [Interpretation 10797] We can close it.
				JToolBarHelper::cancel('country.cancel', 'JTOOLBAR_CLOSE');
			}
		}
		else
		{
			if ($isNew)
			{
				// [Interpretation 10805] For new records, check the create permission.
				if ($this->canDo->get('core.create'))
				{
					JToolBarHelper::apply('country.apply', 'JTOOLBAR_APPLY');
					JToolBarHelper::save('country.save', 'JTOOLBAR_SAVE');
					JToolBarHelper::custom('country.save2new', 'save-new.png', 'save-new_f2.png', 'JTOOLBAR_SAVE_AND_NEW', false);
				};
				JToolBarHelper::cancel('country.cancel', 'JTOOLBAR_CANCEL');
			}
			else
			{
				if ($this->canDo->get('core.edit'))
				{
					// [Interpretation 10832] We can save the new record
					JToolBarHelper::apply('country.apply', 'JTOOLBAR_APPLY');
					JToolBarHelper::save('country.save', 'JTOOLBAR_SAVE');
					// [Interpretation 10835] We can save this record, but check the create permission to see
					// [Interpretation 10836] if we can return to make a new one.
					if ($this->canDo->get('core.create'))
					{
						JToolBarHelper::custom('country.save2new', 'save-new.png', 'save-new_f2.png', 'JTOOLBAR_SAVE_AND_NEW', false);
					}
				}
				$canVersion = ($this->canDo->get('core.version') && $this->canDo->get('country.version'));
				if ($this->state->params->get('save_history', 1) && $this->canDo->get('core.edit') && $canVersion)
				{
					JToolbarHelper::versions('com_mostwantedrealestate.country', $this->item->id);
				}
				if ($this->canDo->get('core.create'))
				{
					JToolBarHelper::custom('country.save2copy', 'save-copy.png', 'save-copy_f2.png', 'JTOOLBAR_SAVE_AS_COPY', false);
				}
				JToolBarHelper::cancel('country.cancel', 'JTOOLBAR_CLOSE');
			}
		}
		JToolbarHelper::divider();
		// [Interpretation 10888] set help url for this view if found
		$help_url = MostwantedrealestateHelper::getHelpUrl('country');
		if (MostwantedrealestateHelper::checkString($help_url))
		{
			JToolbarHelper::help('COM_MOSTWANTEDREALESTATE_HELP_MANAGER', false, $help_url);
		}
	}

        /**
	 * Escapes a value for output in a view script.
	 *
	 * @param   mixed  $var  The output to escape.
	 *
	 * @return  mixed  The escaped value.
	 */
	public function escape($var)
	{
		if(strlen($var) > 30)
		{
    		// use the helper htmlEscape method instead and shorten the string
			return MostwantedrealestateHelper::htmlEscape($var, $this->_charset, true, 30);
		}
                // use the helper htmlEscape method instead.
		return MostwantedrealestateHelper::htmlEscape($var, $this->_charset);
	}

	/**
	 * Method to set up the document properties
	 *
	 * @return void
	 */
	protected function setDocument()
	{
		$isNew = ($this->item->id < 1);
		$document = JFactory::getDocument();
		$document->setTitle(JText::_($isNew ? 'COM_MOSTWANTEDREALESTATE_COUNTRY_NEW' : 'COM_MOSTWANTEDREALESTATE_COUNTRY_EDIT'));
		$document->addStyleSheet(JURI::root() . "administrator/components/com_mostwantedrealestate/assets/css/country.css"); 

		// [Interpretation 6775] Add the CSS for Footable.
		$document->addStyleSheet(JURI::root() .'media/com_mostwantedrealestate/footable/css/footable.core.min.css');

		// [Interpretation 6777] Use the Metro Style
		if (!isset($this->fooTableStyle) || 0 == $this->fooTableStyle)
		{
			$document->addStyleSheet(JURI::root() .'media/com_mostwantedrealestate/footable/css/footable.metro.min.css');
		}
		// [Interpretation 6782] Use the Legacy Style.
		elseif (isset($this->fooTableStyle) && 1 == $this->fooTableStyle)
		{
			$document->addStyleSheet(JURI::root() .'media/com_mostwantedrealestate/footable/css/footable.standalone.min.css');
		}

		// [Interpretation 6787] Add the JavaScript for Footable
		$document->addScript(JURI::root() .'media/com_mostwantedrealestate/footable/js/footable.js');
		$document->addScript(JURI::root() .'media/com_mostwantedrealestate/footable/js/footable.sort.js');
		$document->addScript(JURI::root() .'media/com_mostwantedrealestate/footable/js/footable.filter.js');
		$document->addScript(JURI::root() .'media/com_mostwantedrealestate/footable/js/footable.paginate.js');

		$footable = "jQuery(document).ready(function() { jQuery(function () { jQuery('.footable').footable(); }); jQuery('.nav-tabs').on('click', 'li', function() { setTimeout(tableFix, 10); }); }); function tableFix() { jQuery('.footable').trigger('footable_resize'); }";
		$document->addScriptDeclaration($footable);

		$document->addScript(JURI::root() . $this->script);
		$document->addScript(JURI::root() . "administrator/components/com_mostwantedrealestate/views/country/submitbutton.js"); 
		JText::script('view not acceptable. Error');
	}
}
