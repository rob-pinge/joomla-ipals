<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		2.0.0
	@build			16th September, 2017
	@created		1st May, 2016
	@package		Most Wanted Real Estate
	@subpackage		edit.php
	@author			Most Wanted Web Services, Inc. <http://mostwantedrealestatesites.com>	
	@copyright		Copyright (C) 2015-2017. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
	
	Most Wanted Real Estate Component
	
/------------------------------------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');
$componentParams = JComponentHelper::getParams('com_mostwantedrealestate');
?>
<script type="text/javascript">
	// waiting spinner
	var outerDiv = jQuery('body');
	jQuery('<div id="loading"></div>')
		.css("background", "rgba(255, 255, 255, .8) url('components/com_mostwantedrealestate/assets/images/import.gif') 50% 15% no-repeat")
		.css("top", outerDiv.position().top - jQuery(window).scrollTop())
		.css("left", outerDiv.position().left - jQuery(window).scrollLeft())
		.css("width", outerDiv.width())
		.css("height", outerDiv.height())
		.css("position", "fixed")
		.css("opacity", "0.80")
		.css("-ms-filter", "progid:DXImageTransform.Microsoft.Alpha(Opacity = 80)")
		.css("filter", "alpha(opacity = 80)")
		.css("display", "none")
		.appendTo(outerDiv);
	jQuery('#loading').show();
	// when page is ready remove and show
	jQuery(window).load(function() {
		jQuery('#mostwantedrealestate_loader').fadeIn('fast');
		jQuery('#loading').hide();
	});
</script>
<div id="mostwantedrealestate_loader" style="display: none;">
<form action="<?php echo JRoute::_('index.php?option=com_mostwantedrealestate&layout=edit&id='.(int) $this->item->id.$this->referral); ?>" method="post" name="adminForm" id="adminForm" class="form-validate" enctype="multipart/form-data">

	<?php echo JLayoutHelper::render('help_document.details_above', $this); ?>
<div class="form-horizontal">

	<?php echo JHtml::_('bootstrap.startTabSet', 'help_documentTab', array('active' => 'details')); ?>

	<?php echo JHtml::_('bootstrap.addTab', 'help_documentTab', 'details', JText::_('COM_MOSTWANTEDREALESTATE_HELP_DOCUMENT_DETAILS', true)); ?>
		<div class="row-fluid form-horizontal-desktop">
			<div class="span6">
				<?php echo JLayoutHelper::render('help_document.details_left', $this); ?>
			</div>
			<div class="span6">
				<?php echo JLayoutHelper::render('help_document.details_right', $this); ?>
			</div>
		</div>
		<div class="row-fluid form-horizontal-desktop">
			<div class="span12">
				<?php echo JLayoutHelper::render('help_document.details_fullwidth', $this); ?>
			</div>
		</div>
	<?php echo JHtml::_('bootstrap.endTab'); ?>

	<?php if ($this->canDo->get('help_document.delete') || $this->canDo->get('core.edit.created_by') || $this->canDo->get('help_document.edit.state') || $this->canDo->get('core.edit.created')) : ?>
	<?php echo JHtml::_('bootstrap.addTab', 'help_documentTab', 'publishing', JText::_('COM_MOSTWANTEDREALESTATE_HELP_DOCUMENT_PUBLISHING', true)); ?>
		<div class="row-fluid form-horizontal-desktop">
			<div class="span6">
				<?php echo JLayoutHelper::render('help_document.publishing', $this); ?>
			</div>
			<div class="span6">
				<?php echo JLayoutHelper::render('help_document.metadata', $this); ?>
			</div>
		</div>
	<?php echo JHtml::_('bootstrap.endTab'); ?>
	<?php endif; ?>

	<?php if ($this->canDo->get('core.admin')) : ?>
	<?php echo JHtml::_('bootstrap.addTab', 'help_documentTab', 'permissions', JText::_('COM_MOSTWANTEDREALESTATE_HELP_DOCUMENT_PERMISSION', true)); ?>
		<div class="row-fluid form-horizontal-desktop">
			<div class="span12">
				<fieldset class="adminform">
					<div class="adminformlist">
					<?php foreach ($this->form->getFieldset('accesscontrol') as $field): ?>
						<div>
							<?php echo $field->label; echo $field->input;?>
						</div>
						<div class="clearfix"></div>
					<?php endforeach; ?>
					</div>
				</fieldset>
			</div>
		</div>
	<?php echo JHtml::_('bootstrap.endTab'); ?>
	<?php endif; ?>

	<?php echo JHtml::_('bootstrap.endTabSet'); ?>

	<div>
		<input type="hidden" name="task" value="help_document.edit" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
	</div>
</div>

<div class="clearfix"></div>
<?php echo JLayoutHelper::render('help_document.details_under', $this); ?>
</form>
</div>

<script type="text/javascript">

// #jform_location listeners for location_vvvvvwh function
jQuery('#jform_location').on('keyup',function()
{
	var location_vvvvvwh = jQuery("#jform_location input[type='radio']:checked").val();
	vvvvvwh(location_vvvvvwh);

});
jQuery('#adminForm').on('change', '#jform_location',function (e)
{
	e.preventDefault();
	var location_vvvvvwh = jQuery("#jform_location input[type='radio']:checked").val();
	vvvvvwh(location_vvvvvwh);

});

// #jform_location listeners for location_vvvvvwi function
jQuery('#jform_location').on('keyup',function()
{
	var location_vvvvvwi = jQuery("#jform_location input[type='radio']:checked").val();
	vvvvvwi(location_vvvvvwi);

});
jQuery('#adminForm').on('change', '#jform_location',function (e)
{
	e.preventDefault();
	var location_vvvvvwi = jQuery("#jform_location input[type='radio']:checked").val();
	vvvvvwi(location_vvvvvwi);

});

// #jform_type listeners for type_vvvvvwj function
jQuery('#jform_type').on('keyup',function()
{
	var type_vvvvvwj = jQuery("#jform_type").val();
	vvvvvwj(type_vvvvvwj);

});
jQuery('#adminForm').on('change', '#jform_type',function (e)
{
	e.preventDefault();
	var type_vvvvvwj = jQuery("#jform_type").val();
	vvvvvwj(type_vvvvvwj);

});

// #jform_type listeners for type_vvvvvwk function
jQuery('#jform_type').on('keyup',function()
{
	var type_vvvvvwk = jQuery("#jform_type").val();
	vvvvvwk(type_vvvvvwk);

});
jQuery('#adminForm').on('change', '#jform_type',function (e)
{
	e.preventDefault();
	var type_vvvvvwk = jQuery("#jform_type").val();
	vvvvvwk(type_vvvvvwk);

});

// #jform_type listeners for type_vvvvvwl function
jQuery('#jform_type').on('keyup',function()
{
	var type_vvvvvwl = jQuery("#jform_type").val();
	vvvvvwl(type_vvvvvwl);

});
jQuery('#adminForm').on('change', '#jform_type',function (e)
{
	e.preventDefault();
	var type_vvvvvwl = jQuery("#jform_type").val();
	vvvvvwl(type_vvvvvwl);

});

// #jform_target listeners for target_vvvvvwm function
jQuery('#jform_target').on('keyup',function()
{
	var target_vvvvvwm = jQuery("#jform_target input[type='radio']:checked").val();
	vvvvvwm(target_vvvvvwm);

});
jQuery('#adminForm').on('change', '#jform_target',function (e)
{
	e.preventDefault();
	var target_vvvvvwm = jQuery("#jform_target input[type='radio']:checked").val();
	vvvvvwm(target_vvvvvwm);

});

</script>
