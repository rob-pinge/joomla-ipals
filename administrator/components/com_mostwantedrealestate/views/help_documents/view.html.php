<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		2.0.0
	@build			16th September, 2017
	@created		1st May, 2016
	@package		Most Wanted Real Estate
	@subpackage		view.html.php
	@author			Most Wanted Web Services, Inc. <http://mostwantedrealestatesites.com>	
	@copyright		Copyright (C) 2015-2017. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
	
	Most Wanted Real Estate Component
	
/------------------------------------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla view library
jimport('joomla.application.component.view');

/**
 * Mostwantedrealestate View class for the Help_documents
 */
class MostwantedrealestateViewHelp_documents extends JViewLegacy
{
	/**
	 * Help_documents view display method
	 * @return void
	 */
	function display($tpl = null)
	{
		if ($this->getLayout() !== 'modal')
		{
			// Include helper submenu
			MostwantedrealestateHelper::addSubmenu('help_documents');
		}

		// Check for errors.
		if (count($errors = $this->get('Errors')))
                {
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}

		// Assign data to the view
		$this->items 		= $this->get('Items');
		$this->pagination 	= $this->get('Pagination');
		$this->state		= $this->get('State');
		$this->user 		= JFactory::getUser();
		$this->listOrder	= $this->escape($this->state->get('list.ordering'));
		$this->listDirn		= $this->escape($this->state->get('list.direction'));
		$this->saveOrder	= $this->listOrder == 'ordering';
                // get global action permissions
		$this->canDo		= MostwantedrealestateHelper::getActions('help_document');
		$this->canEdit		= $this->canDo->get('help_document.edit');
		$this->canState		= $this->canDo->get('help_document.edit.state');
		$this->canCreate	= $this->canDo->get('help_document.create');
		$this->canDelete	= $this->canDo->get('help_document.delete');
		$this->canBatch	= $this->canDo->get('core.batch');

		// We don't need toolbar in the modal window.
		if ($this->getLayout() !== 'modal')
		{
			$this->addToolbar();
			$this->sidebar = JHtmlSidebar::render();
                        // load the batch html
                        if ($this->canCreate && $this->canEdit && $this->canState)
                        {
                                $this->batchDisplay = JHtmlBatch_::render();
                        }
		}

		// Display the template
		parent::display($tpl);

		// Set the document
		$this->setDocument();
	}

	/**
	 * Setting the toolbar
	 */
	protected function addToolBar()
	{
		JToolBarHelper::title(JText::_('COM_MOSTWANTEDREALESTATE_HELP_DOCUMENTS'), 'joomla');
		JHtmlSidebar::setAction('index.php?option=com_mostwantedrealestate&view=help_documents');
                JFormHelper::addFieldPath(JPATH_COMPONENT . '/models/fields');

		if ($this->canCreate)
                {
			JToolBarHelper::addNew('help_document.add');
		}

                // Only load if there are items
                if (MostwantedrealestateHelper::checkArray($this->items))
		{
                        if ($this->canEdit)
                        {
                            JToolBarHelper::editList('help_document.edit');
                        }

                        if ($this->canState)
                        {
                            JToolBarHelper::publishList('help_documents.publish');
                            JToolBarHelper::unpublishList('help_documents.unpublish');
                            JToolBarHelper::archiveList('help_documents.archive');

                            if ($this->canDo->get('core.admin'))
                            {
                                JToolBarHelper::checkin('help_documents.checkin');
                            }
                        }

                        // Add a batch button
                        if ($this->canBatch && $this->canCreate && $this->canEdit && $this->canState)
                        {
                                // Get the toolbar object instance
                                $bar = JToolBar::getInstance('toolbar');
                                // set the batch button name
                                $title = JText::_('JTOOLBAR_BATCH');
                                // Instantiate a new JLayoutFile instance and render the batch button
                                $layout = new JLayoutFile('joomla.toolbar.batch');
                                // add the button to the page
                                $dhtml = $layout->render(array('title' => $title));
                                $bar->appendButton('Custom', $dhtml, 'batch');
                        } 

                        if ($this->state->get('filter.published') == -2 && ($this->canState && $this->canDelete))
                        {
                            JToolbarHelper::deleteList('', 'help_documents.delete', 'JTOOLBAR_EMPTY_TRASH');
                        }
                        elseif ($this->canState && $this->canDelete)
                        {
                                JToolbarHelper::trash('help_documents.trash');
                        }

			if ($this->canDo->get('core.export') && $this->canDo->get('help_document.export'))
			{
				JToolBarHelper::custom('help_documents.exportData', 'download', '', 'COM_MOSTWANTEDREALESTATE_EXPORT_DATA', true);
			}
                } 

		if ($this->canDo->get('core.import') && $this->canDo->get('help_document.import'))
		{
			JToolBarHelper::custom('help_documents.importData', 'upload', '', 'COM_MOSTWANTEDREALESTATE_IMPORT_DATA', false);
		}

                // set help url for this view if found
                $help_url = MostwantedrealestateHelper::getHelpUrl('help_documents');
                if (MostwantedrealestateHelper::checkString($help_url))
                {
                        JToolbarHelper::help('COM_MOSTWANTEDREALESTATE_HELP_MANAGER', false, $help_url);
                }

                // add the options comp button
                if ($this->canDo->get('core.admin') || $this->canDo->get('core.options'))
                {
                        JToolBarHelper::preferences('com_mostwantedrealestate');
                }

                if ($this->canState)
                {
			JHtmlSidebar::addFilter(
				JText::_('JOPTION_SELECT_PUBLISHED'),
				'filter_published',
				JHtml::_('select.options', JHtml::_('jgrid.publishedOptions'), 'value', 'text', $this->state->get('filter.published'), true)
			);
                        // only load if batch allowed
                        if ($this->canBatch)
                        {
                            JHtmlBatch_::addListSelection(
                                JText::_('COM_MOSTWANTEDREALESTATE_KEEP_ORIGINAL_STATE'),
                                'batch[published]',
                                JHtml::_('select.options', JHtml::_('jgrid.publishedOptions', array('all' => false)), 'value', 'text', '', true)
                            );
                        }
		}

		JHtmlSidebar::addFilter(
			JText::_('JOPTION_SELECT_ACCESS'),
			'filter_access',
			JHtml::_('select.options', JHtml::_('access.assetgroups'), 'value', 'text', $this->state->get('filter.access'))
		);

		if ($this->canBatch && $this->canCreate && $this->canEdit)
		{
			JHtmlBatch_::addListSelection(
                                JText::_('COM_MOSTWANTEDREALESTATE_KEEP_ORIGINAL_ACCESS'),
                                'batch[access]',
                                JHtml::_('select.options', JHtml::_('access.assetgroups'), 'value', 'text')
			);
                }  

		// [Interpretation 9591] Set Type Selection
		$this->typeOptions = $this->getTheTypeSelections();
		if ($this->typeOptions)
		{
			// [Interpretation 9595] Type Filter
			JHtmlSidebar::addFilter(
				'- Select '.JText::_('COM_MOSTWANTEDREALESTATE_HELP_DOCUMENT_TYPE_LABEL').' -',
				'filter_type',
				JHtml::_('select.options', $this->typeOptions, 'value', 'text', $this->state->get('filter.type'))
			);

			if ($this->canBatch && $this->canCreate && $this->canEdit)
			{
				// [Interpretation 9604] Type Batch Selection
				JHtmlBatch_::addListSelection(
					'- Keep Original '.JText::_('COM_MOSTWANTEDREALESTATE_HELP_DOCUMENT_TYPE_LABEL').' -',
					'batch[type]',
					JHtml::_('select.options', $this->typeOptions, 'value', 'text')
				);
			}
		}

		// [Interpretation 9591] Set Location Selection
		$this->locationOptions = $this->getTheLocationSelections();
		if ($this->locationOptions)
		{
			// [Interpretation 9595] Location Filter
			JHtmlSidebar::addFilter(
				'- Select '.JText::_('COM_MOSTWANTEDREALESTATE_HELP_DOCUMENT_LOCATION_LABEL').' -',
				'filter_location',
				JHtml::_('select.options', $this->locationOptions, 'value', 'text', $this->state->get('filter.location'))
			);

			if ($this->canBatch && $this->canCreate && $this->canEdit)
			{
				// [Interpretation 9604] Location Batch Selection
				JHtmlBatch_::addListSelection(
					'- Keep Original '.JText::_('COM_MOSTWANTEDREALESTATE_HELP_DOCUMENT_LOCATION_LABEL').' -',
					'batch[location]',
					JHtml::_('select.options', $this->locationOptions, 'value', 'text')
				);
			}
		}

		// [Interpretation 9557] Set Admin View Selection
		$this->admin_viewOptions = JFormHelper::loadFieldType('Adminviewfolderlist')->getOptions();
		if ($this->admin_viewOptions)
		{
			// [Interpretation 9561] Admin View Filter
			JHtmlSidebar::addFilter(
				'- Select '.JText::_('COM_MOSTWANTEDREALESTATE_HELP_DOCUMENT_ADMIN_VIEW_LABEL').' -',
				'filter_admin_view',
				JHtml::_('select.options', $this->admin_viewOptions, 'value', 'text', $this->state->get('filter.admin_view'))
			);

			if ($this->canBatch && $this->canCreate && $this->canEdit)
			{
				// [Interpretation 9570] Admin View Batch Selection
				JHtmlBatch_::addListSelection(
					'- Keep Original '.JText::_('COM_MOSTWANTEDREALESTATE_HELP_DOCUMENT_ADMIN_VIEW_LABEL').' -',
					'batch[admin_view]',
					JHtml::_('select.options', $this->admin_viewOptions, 'value', 'text')
				);
			}
		}

		// [Interpretation 9557] Set Site View Selection
		$this->site_viewOptions = JFormHelper::loadFieldType('Siteviewfolderlist')->getOptions();
		if ($this->site_viewOptions)
		{
			// [Interpretation 9561] Site View Filter
			JHtmlSidebar::addFilter(
				'- Select '.JText::_('COM_MOSTWANTEDREALESTATE_HELP_DOCUMENT_SITE_VIEW_LABEL').' -',
				'filter_site_view',
				JHtml::_('select.options', $this->site_viewOptions, 'value', 'text', $this->state->get('filter.site_view'))
			);

			if ($this->canBatch && $this->canCreate && $this->canEdit)
			{
				// [Interpretation 9570] Site View Batch Selection
				JHtmlBatch_::addListSelection(
					'- Keep Original '.JText::_('COM_MOSTWANTEDREALESTATE_HELP_DOCUMENT_SITE_VIEW_LABEL').' -',
					'batch[site_view]',
					JHtml::_('select.options', $this->site_viewOptions, 'value', 'text')
				);
			}
		}
	}

	/**
	 * Method to set up the document properties
	 *
	 * @return void
	 */
	protected function setDocument()
	{
		$document = JFactory::getDocument();
		$document->setTitle(JText::_('COM_MOSTWANTEDREALESTATE_HELP_DOCUMENTS'));
		$document->addStyleSheet(JURI::root() . "administrator/components/com_mostwantedrealestate/assets/css/help_documents.css");
	}

        /**
	 * Escapes a value for output in a view script.
	 *
	 * @param   mixed  $var  The output to escape.
	 *
	 * @return  mixed  The escaped value.
	 */
	public function escape($var)
	{
		if(strlen($var) > 50)
		{
                        // use the helper htmlEscape method instead and shorten the string
			return MostwantedrealestateHelper::htmlEscape($var, $this->_charset, true);
		}
                // use the helper htmlEscape method instead.
		return MostwantedrealestateHelper::htmlEscape($var, $this->_charset);
	}

	/**
	 * Returns an array of fields the table can be sorted by
	 *
	 * @return  array  Array containing the field name to sort by as the key and display text as value
	 */
	protected function getSortFields()
	{
		return array(
			'a.sorting' => JText::_('JGRID_HEADING_ORDERING'),
			'a.published' => JText::_('JSTATUS'),
			'a.title' => JText::_('COM_MOSTWANTEDREALESTATE_HELP_DOCUMENT_TITLE_LABEL'),
			'a.type' => JText::_('COM_MOSTWANTEDREALESTATE_HELP_DOCUMENT_TYPE_LABEL'),
			'a.location' => JText::_('COM_MOSTWANTEDREALESTATE_HELP_DOCUMENT_LOCATION_LABEL'),
			'g.' => JText::_('COM_MOSTWANTEDREALESTATE_HELP_DOCUMENT_ADMIN_VIEW_LABEL'),
			'h.' => JText::_('COM_MOSTWANTEDREALESTATE_HELP_DOCUMENT_SITE_VIEW_LABEL'),
			'a.id' => JText::_('JGRID_HEADING_ID')
		);
	} 

	protected function getTheTypeSelections()
	{
		// [Interpretation 9462] Get a db connection.
		$db = JFactory::getDbo();

		// [Interpretation 9464] Create a new query object.
		$query = $db->getQuery(true);

		// [Interpretation 9466] Select the text.
		$query->select($db->quoteName('type'));
		$query->from($db->quoteName('#__mostwantedrealestate_help_document'));
		$query->order($db->quoteName('type') . ' ASC');

		// [Interpretation 9470] Reset the query using our newly populated query object.
		$db->setQuery($query);

		$results = $db->loadColumn();

		if ($results)
		{
			// [Interpretation 9478] get model
			$model = $this->getModel();
			$results = array_unique($results);
			$_filter = array();
			foreach ($results as $type)
			{
				// [Interpretation 9489] Translate the type selection
				$text = $model->selectionTranslation($type,'type');
				// [Interpretation 9491] Now add the type and its text to the options array
				$_filter[] = JHtml::_('select.option', $type, JText::_($text));
			}
			return $_filter;
		}
		return false;
	}

	protected function getTheLocationSelections()
	{
		// [Interpretation 9462] Get a db connection.
		$db = JFactory::getDbo();

		// [Interpretation 9464] Create a new query object.
		$query = $db->getQuery(true);

		// [Interpretation 9466] Select the text.
		$query->select($db->quoteName('location'));
		$query->from($db->quoteName('#__mostwantedrealestate_help_document'));
		$query->order($db->quoteName('location') . ' ASC');

		// [Interpretation 9470] Reset the query using our newly populated query object.
		$db->setQuery($query);

		$results = $db->loadColumn();

		if ($results)
		{
			// [Interpretation 9478] get model
			$model = $this->getModel();
			$results = array_unique($results);
			$_filter = array();
			foreach ($results as $location)
			{
				// [Interpretation 9489] Translate the location selection
				$text = $model->selectionTranslation($location,'location');
				// [Interpretation 9491] Now add the location and its text to the options array
				$_filter[] = JHtml::_('select.option', $location, JText::_($text));
			}
			return $_filter;
		}
		return false;
	}
}
