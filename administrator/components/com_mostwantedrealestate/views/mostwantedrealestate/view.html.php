<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		2.0.0
	@build			22nd September, 2017
	@created		1st May, 2016
	@package		Most Wanted Real Estate
	@subpackage		view.html.php
	@author			Most Wanted Web Services, Inc. <http://mostwantedrealestatesites.com>	
	@copyright		Copyright (C) 2015-2017. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
	
	Most Wanted Real Estate Component
	
/------------------------------------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla view library
jimport('joomla.application.component.view');

/**
 * Mostwantedrealestate View class
 */
class MostwantedrealestateViewMostwantedrealestate extends JViewLegacy
{
	/**
	 * View display method
	 * @return void
	 */
	function display($tpl = null)
	{
		// Check for errors.
		if (count($errors = $this->get('Errors')))
                {
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		};
		// Assign data to the view
		$this->icons			= $this->get('Icons');
		$this->contributors		= MostwantedrealestateHelper::getContributors();
		$this->readme	= $this->get('Readme');
		
		// get the manifest details of the component
		$this->manifest = MostwantedrealestateHelper::manifest();
		
		// Set the toolbar
		$this->addToolBar();

		// Display the template
		parent::display($tpl);

		// Set the document
		$this->setDocument();
	}

	/**
	 * Setting the toolbar
	 */
	protected function addToolBar()
	{
		$canDo = MostwantedrealestateHelper::getActions('mostwantedrealestate');
		JToolBarHelper::title(JText::_('COM_MOSTWANTEDREALESTATE_DASHBOARD'), 'grid-2');

                // set help url for this view if found
                $help_url = MostwantedrealestateHelper::getHelpUrl('mostwantedrealestate');
                if (MostwantedrealestateHelper::checkString($help_url))
                {
			JToolbarHelper::help('COM_MOSTWANTEDREALESTATE_HELP_MANAGER', false, $help_url);
                }

		if ($canDo->get('core.admin') || $canDo->get('core.options'))
                {
			JToolBarHelper::preferences('com_mostwantedrealestate');
		}
	}

	/**
	 * Method to set up the document properties
	 *
	 * @return void
	 */
	protected function setDocument()
	{
		$document = JFactory::getDocument();
		
		// add dashboard style sheets
		$document->addStyleSheet(JURI::root() . "administrator/components/com_mostwantedrealestate/assets/css/dashboard.css");
		
		// set page title
		$document->setTitle(JText::_('COM_MOSTWANTEDREALESTATE_DASHBOARD'));
		
		// add manifest to page JavaScript
		$document->addScriptDeclaration("var manifest = jQuery.parseJSON('" . json_encode($this->manifest) . "');", "text/javascript");
	}
}
