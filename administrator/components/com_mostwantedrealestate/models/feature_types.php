<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		2.0.0
	@build			22nd September, 2017
	@created		1st May, 2016
	@package		Most Wanted Real Estate
	@subpackage		feature_types.php
	@author			Most Wanted Web Services, Inc. <http://mostwantedrealestatesites.com>	
	@copyright		Copyright (C) 2015-2017. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
	
	Most Wanted Real Estate Component
	
/------------------------------------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import the Joomla modellist library
jimport('joomla.application.component.modellist');

/**
 * Feature_types Model
 */
class MostwantedrealestateModelFeature_types extends JModelList
{
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
        {
			$config['filter_fields'] = array(
				'a.id','id',
				'a.published','published',
				'a.ordering','ordering',
				'a.created_by','created_by',
				'a.modified_by','modified_by',
				'a.featurename','featurename',
				'a.featuretype','featuretype'
			);
		}

		parent::__construct($config);
	}
	
	/**
	 * Method to auto-populate the model state.
	 *
	 * @return  void
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		$app = JFactory::getApplication();

		// Adjust the context to support modal layouts.
		if ($layout = $app->input->get('layout'))
		{
			$this->context .= '.' . $layout;
		}
		$featurename = $this->getUserStateFromRequest($this->context . '.filter.featurename', 'filter_featurename');
		$this->setState('filter.featurename', $featurename);

		$featuretype = $this->getUserStateFromRequest($this->context . '.filter.featuretype', 'filter_featuretype');
		$this->setState('filter.featuretype', $featuretype);
        
		$sorting = $this->getUserStateFromRequest($this->context . '.filter.sorting', 'filter_sorting', 0, 'int');
		$this->setState('filter.sorting', $sorting);
        
		$access = $this->getUserStateFromRequest($this->context . '.filter.access', 'filter_access', 0, 'int');
		$this->setState('filter.access', $access);
        
		$search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$published = $this->getUserStateFromRequest($this->context . '.filter.published', 'filter_published', '');
		$this->setState('filter.published', $published);
        
		$created_by = $this->getUserStateFromRequest($this->context . '.filter.created_by', 'filter_created_by', '');
		$this->setState('filter.created_by', $created_by);

		$created = $this->getUserStateFromRequest($this->context . '.filter.created', 'filter_created');
		$this->setState('filter.created', $created);

		// List state information.
		parent::populateState($ordering, $direction);
	}
	
	/**
	 * Method to get an array of data items.
	 *
	 * @return  mixed  An array of data items on success, false on failure.
	 */
	public function getItems()
	{ 
		// [Interpretation 11051] check in items
		$this->checkInNow();

		// load parent items
		$items = parent::getItems(); 

		// [Interpretation 11403] set selection value to a translatable value
		if (MostwantedrealestateHelper::checkArray($items))
		{
			foreach ($items as $nr => &$item)
			{
				// [Interpretation 11410] convert featuretype
				$item->featuretype = $this->selectionTranslation($item->featuretype, 'featuretype');
			}
		}
 
        
		// return items
		return $items;
	}

	/**
	* Method to convert selection values to translatable string.
	*
	* @return translatable string
	*/
	public function selectionTranslation($value,$name)
	{
		// [Interpretation 11436] Array of featuretype language strings
		if ($name === 'featuretype')
		{
			$featuretypeArray = array(
				0 => 'COM_MOSTWANTEDREALESTATE_FEATURE_TYPE_SELECT_AN_OPTION',
				1 => 'COM_MOSTWANTEDREALESTATE_FEATURE_TYPE_BASEMENT',
				2 => 'COM_MOSTWANTEDREALESTATE_FEATURE_TYPE_FENCED_YARD',
				3 => 'COM_MOSTWANTEDREALESTATE_FEATURE_TYPE_GARAGE_TYPE',
				4 => 'COM_MOSTWANTEDREALESTATE_FEATURE_TYPE_PATIO',
				5 => 'COM_MOSTWANTEDREALESTATE_FEATURE_TYPE_ROAD_FRONTAGE',
				6 => 'COM_MOSTWANTEDREALESTATE_FEATURE_TYPE_ROOF',
				7 => 'COM_MOSTWANTEDREALESTATE_FEATURE_TYPE_SIDING',
				8 => 'COM_MOSTWANTEDREALESTATE_FEATURE_TYPE_STYLE',
				9 => 'COM_MOSTWANTEDREALESTATE_FEATURE_TYPE_WATER',
				10 => 'COM_MOSTWANTEDREALESTATE_FEATURE_TYPE_HEATING',
				11 => 'COM_MOSTWANTEDREALESTATE_FEATURE_TYPE_COOLING',
				12 => 'COM_MOSTWANTEDREALESTATE_FEATURE_TYPE_SEWER',
				13 => 'COM_MOSTWANTEDREALESTATE_FEATURE_TYPE_ZONING',
				14 => 'COM_MOSTWANTEDREALESTATE_FEATURE_TYPE_TERMS'
			);
			// [Interpretation 11467] Now check if value is found in this array
			if (isset($featuretypeArray[$value]) && MostwantedrealestateHelper::checkString($featuretypeArray[$value]))
			{
				return $featuretypeArray[$value];
			}
		}
		return $value;
	}
	
	/**
	 * Method to build an SQL query to load the list data.
	 *
	 * @return	string	An SQL query
	 */
	protected function getListQuery()
	{
		// [Interpretation 7883] Get the user object.
		$user = JFactory::getUser();
		// [Interpretation 7885] Create a new query object.
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);

		// [Interpretation 7888] Select some fields
		$query->select('a.*');

		// [Interpretation 7895] From the mostwantedrealestate_item table
		$query->from($db->quoteName('#__mostwantedrealestate_feature_type', 'a'));

		// [Interpretation 7906] Filter by published state
		$published = $this->getState('filter.published');
		if (is_numeric($published))
		{
			$query->where('a.published = ' . (int) $published);
		}
		elseif ($published === '')
		{
			$query->where('(a.published = 0 OR a.published = 1)');
		}

		// [Interpretation 7918] Join over the asset groups.
		$query->select('ag.title AS access_level');
		$query->join('LEFT', '#__viewlevels AS ag ON ag.id = a.access');
		// [Interpretation 7921] Filter by access level.
		if ($access = $this->getState('filter.access'))
		{
			$query->where('a.access = ' . (int) $access);
		}
		// [Interpretation 7926] Implement View Level Access
		if (!$user->authorise('core.options', 'com_mostwantedrealestate'))
		{
			$groups = implode(',', $user->getAuthorisedViewLevels());
			$query->where('a.access IN (' . $groups . ')');
		}
		// [Interpretation 8003] Filter by search.
		$search = $this->getState('filter.search');
		if (!empty($search))
		{
			if (stripos($search, 'id:') === 0)
			{
				$query->where('a.id = ' . (int) substr($search, 3));
			}
			else
			{
				$search = $db->quote('%' . $db->escape($search) . '%');
				$query->where('(a.featurename LIKE '.$search.')');
			}
		}

		// [Interpretation 8198] Filter by Featuretype.
		if ($featuretype = $this->getState('filter.featuretype'))
		{
			$query->where('a.featuretype = ' . $db->quote($db->escape($featuretype)));
		}

		// [Interpretation 7962] Add the list ordering clause.
		$orderCol = $this->state->get('list.ordering', 'a.id');
		$orderDirn = $this->state->get('list.direction', 'asc');	
		if ($orderCol != '')
		{
			$query->order($db->escape($orderCol . ' ' . $orderDirn));
		}

		return $query;
	}

	/**
	* Method to get list export data.
	*
	* @return mixed  An array of data items on success, false on failure.
	*/
	public function getExportData($pks)
	{
		// [Interpretation 7635] setup the query
		if (MostwantedrealestateHelper::checkArray($pks))
		{
			// [Interpretation 7638] Set a value to know this is exporting method.
			$_export = true;
			// [Interpretation 7640] Get the user object.
			$user = JFactory::getUser();
			// [Interpretation 7642] Create a new query object.
			$db = JFactory::getDBO();
			$query = $db->getQuery(true);

			// [Interpretation 7645] Select some fields
			$query->select('a.*');

			// [Interpretation 7647] From the mostwantedrealestate_feature_type table
			$query->from($db->quoteName('#__mostwantedrealestate_feature_type', 'a'));
			$query->where('a.id IN (' . implode(',',$pks) . ')');
			// [Interpretation 7655] Implement View Level Access
			if (!$user->authorise('core.options', 'com_mostwantedrealestate'))
			{
				$groups = implode(',', $user->getAuthorisedViewLevels());
				$query->where('a.access IN (' . $groups . ')');
			}

			// [Interpretation 7662] Order the results by ordering
			$query->order('a.ordering  ASC');

			// [Interpretation 7664] Load the items
			$db->setQuery($query);
			$db->execute();
			if ($db->getNumRows())
			{
				$items = $db->loadObjectList();

				// [Interpretation 11341] set values to display correctly.
				if (MostwantedrealestateHelper::checkArray($items))
				{
					foreach ($items as $nr => &$item)
					{
						// [Interpretation 11353] unset the values we don't want exported.
						unset($item->asset_id);
						unset($item->checked_out);
						unset($item->checked_out_time);
					}
				}
				// [Interpretation 11362] Add headers to items array.
				$headers = $this->getExImPortHeaders();
				if (MostwantedrealestateHelper::checkObject($headers))
				{
					array_unshift($items,$headers);
				}
				return $items;
			}
		}
		return false;
	}

	/**
	* Method to get header.
	*
	* @return mixed  An array of data items on success, false on failure.
	*/
	public function getExImPortHeaders()
	{
		// [Interpretation 7686] Get a db connection.
		$db = JFactory::getDbo();
		// [Interpretation 7688] get the columns
		$columns = $db->getTableColumns("#__mostwantedrealestate_feature_type");
		if (MostwantedrealestateHelper::checkArray($columns))
		{
			// [Interpretation 7692] remove the headers you don't import/export.
			unset($columns['asset_id']);
			unset($columns['checked_out']);
			unset($columns['checked_out_time']);
			$headers = new stdClass();
			foreach ($columns as $column => $type)
			{
				$headers->{$column} = $column;
			}
			return $headers;
		}
		return false;
	} 
	
	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * @return  string  A store id.
	 *
	 */
	protected function getStoreId($id = '')
	{
		// [Interpretation 10653] Compile the store id.
		$id .= ':' . $this->getState('filter.id');
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.published');
		$id .= ':' . $this->getState('filter.ordering');
		$id .= ':' . $this->getState('filter.created_by');
		$id .= ':' . $this->getState('filter.modified_by');
		$id .= ':' . $this->getState('filter.featurename');
		$id .= ':' . $this->getState('filter.featuretype');

		return parent::getStoreId($id);
	}

	/**
	* Build an SQL query to checkin all items left checked out longer then a set time.
	*
	* @return  a bool
	*
	*/
	protected function checkInNow()
	{
		// [Interpretation 11067] Get set check in time
		$time = JComponentHelper::getParams('com_mostwantedrealestate')->get('check_in');
		
		if ($time)
		{

			// [Interpretation 11072] Get a db connection.
			$db = JFactory::getDbo();
			// [Interpretation 11074] reset query
			$query = $db->getQuery(true);
			$query->select('*');
			$query->from($db->quoteName('#__mostwantedrealestate_feature_type'));
			$db->setQuery($query);
			$db->execute();
			if ($db->getNumRows())
			{
				// [Interpretation 11082] Get Yesterdays date
				$date = JFactory::getDate()->modify($time)->toSql();
				// [Interpretation 11084] reset query
				$query = $db->getQuery(true);

				// [Interpretation 11086] Fields to update.
				$fields = array(
					$db->quoteName('checked_out_time') . '=\'0000-00-00 00:00:00\'',
					$db->quoteName('checked_out') . '=0'
				);

				// [Interpretation 11091] Conditions for which records should be updated.
				$conditions = array(
					$db->quoteName('checked_out') . '!=0', 
					$db->quoteName('checked_out_time') . '<\''.$date.'\''
				);

				// [Interpretation 11096] Check table
				$query->update($db->quoteName('#__mostwantedrealestate_feature_type'))->set($fields)->where($conditions); 

				$db->setQuery($query);

				$db->execute();
			}
		}

		return false;
	}
}
