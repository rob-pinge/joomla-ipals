<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		2.0.0
	@build			22nd September, 2017
	@created		1st May, 2016
	@package		Most Wanted Real Estate
	@subpackage		property.php
	@author			Most Wanted Web Services, Inc. <http://mostwantedrealestatesites.com>	
	@copyright		Copyright (C) 2015-2017. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
	
	Most Wanted Real Estate Component
	
/------------------------------------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

use Joomla\Registry\Registry;

// import Joomla modelform library
jimport('joomla.application.component.modeladmin');

/**
 * Mostwantedrealestate Property Model
 */
class MostwantedrealestateModelProperty extends JModelAdmin
{    
	/**
	 * @var        string    The prefix to use with controller messages.
	 * @since   1.6
	 */
	protected $text_prefix = 'COM_MOSTWANTEDREALESTATE';
    
	/**
	 * The type alias for this content type.
	 *
	 * @var      string
	 * @since    3.2
	 */
	public $typeAlias = 'com_mostwantedrealestate.property';

	/**
	 * Returns a Table object, always creating it
	 *
	 * @param   type    $type    The table type to instantiate
	 * @param   string  $prefix  A prefix for the table class name. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  JTable  A database object
	 *
	 * @since   1.6
	 */
	public function getTable($type = 'property', $prefix = 'MostwantedrealestateTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}
    
	/**
	 * Method to get a single record.
	 *
	 * @param   integer  $pk  The id of the primary key.
	 *
	 * @return  mixed  Object on success, false on failure.
	 *
	 * @since   1.6
	 */
	public function getItem($pk = null)
	{
		if ($item = parent::getItem($pk))
		{
			if (!empty($item->params))
			{
				// Convert the params field to an array.
				$registry = new Registry;
				$registry->loadString($item->params);
				$item->params = $registry->toArray();
			}

			if (!empty($item->metadata))
			{
				// Convert the metadata field to an array.
				$registry = new Registry;
				$registry->loadString($item->metadata);
				$item->metadata = $registry->toArray();
			}

			if (!empty($item->commonareas))
			{
				// [Interpretation 3746] Convert the commonareas field to an array.
				$commonareas = new Registry;
				$commonareas->loadString($item->commonareas);
				$item->commonareas = $commonareas->toArray();
			}

			if (!empty($item->indoorfeatures))
			{
				// [Interpretation 3746] Convert the indoorfeatures field to an array.
				$indoorfeatures = new Registry;
				$indoorfeatures->loadString($item->indoorfeatures);
				$item->indoorfeatures = $indoorfeatures->toArray();
			}

			if (!empty($item->buildingfeatures))
			{
				// [Interpretation 3746] Convert the buildingfeatures field to an array.
				$buildingfeatures = new Registry;
				$buildingfeatures->loadString($item->buildingfeatures);
				$item->buildingfeatures = $buildingfeatures->toArray();
			}

			if (!empty($item->otherfeatures))
			{
				// [Interpretation 3746] Convert the otherfeatures field to an array.
				$otherfeatures = new Registry;
				$otherfeatures->loadString($item->otherfeatures);
				$item->otherfeatures = $otherfeatures->toArray();
			}

			if (!empty($item->colistagent))
			{
				// [Interpretation 3746] Convert the colistagent field to an array.
				$colistagent = new Registry;
				$colistagent->loadString($item->colistagent);
				$item->colistagent = $colistagent->toArray();
			}

			if (!empty($item->appliances))
			{
				// [Interpretation 3746] Convert the appliances field to an array.
				$appliances = new Registry;
				$appliances->loadString($item->appliances);
				$item->appliances = $appliances->toArray();
			}

			if (!empty($item->outdoorfeatures))
			{
				// [Interpretation 3746] Convert the outdoorfeatures field to an array.
				$outdoorfeatures = new Registry;
				$outdoorfeatures->loadString($item->outdoorfeatures);
				$item->outdoorfeatures = $outdoorfeatures->toArray();
			}

			if (!empty($item->communityfeatures))
			{
				// [Interpretation 3746] Convert the communityfeatures field to an array.
				$communityfeatures = new Registry;
				$communityfeatures->loadString($item->communityfeatures);
				$item->communityfeatures = $communityfeatures->toArray();
			}

			if (!empty($item->unitdetails))
			{
				// [Interpretation 3746] Convert the unitdetails field to an array.
				$unitdetails = new Registry;
				$unitdetails->loadString($item->unitdetails);
				$item->unitdetails = $unitdetails->toArray();
			}

			if (!empty($item->openhouseinfo))
			{
				// [Interpretation 3746] Convert the openhouseinfo field to an array.
				$openhouseinfo = new Registry;
				$openhouseinfo->loadString($item->openhouseinfo);
				$item->openhouseinfo = $openhouseinfo->toArray();
			}

			if (!empty($item->terms))
			{
				// [Interpretation 3761] JSON Decode terms.
				$item->terms = json_decode($item->terms);
			}

			if (!empty($item->waterresources))
			{
				// [Interpretation 3761] JSON Decode waterresources.
				$item->waterresources = json_decode($item->waterresources);
			}

			if (!empty($item->porchpatio))
			{
				// [Interpretation 3761] JSON Decode porchpatio.
				$item->porchpatio = json_decode($item->porchpatio);
			}

			if (!empty($item->garagetype))
			{
				// [Interpretation 3761] JSON Decode garagetype.
				$item->garagetype = json_decode($item->garagetype);
			}

			if (!empty($item->heating))
			{
				// [Interpretation 3761] JSON Decode heating.
				$item->heating = json_decode($item->heating);
			}

			if (!empty($item->cooling))
			{
				// [Interpretation 3761] JSON Decode cooling.
				$item->cooling = json_decode($item->cooling);
			}

            $JDbQuery = $this->_db->getQuery(true);
			
            $JDbQuery->select($this->_db->quoteName(
                array('j.id','j.path','j.title','j.description'),
                array('id','path','title','description')))
            ->select($JDbQuery->concatenate(['j.filename','j.type'],'_th.').' AS '. $this->_db->quoteName('filename') )
            ->select($JDbQuery->concatenate(['j.filename','j.type'],'.').' AS '. $this->_db->quoteName('original_filename') )
            ->order("ordering desc");
			
            $JDbQuery->from($this->_db->quoteName('#__mostwantedrealestate_image', 'j'));
            $JDbQuery->where('j.propid = ' . $this->_db->quote($item->id));
            
            $this->_db->setQuery($JDbQuery);
            $item->images = $this->_db->loadObjectList();

			
			if (!empty($item->id))
			{
				$item->tags = new JHelperTags;
				$item->tags->getTagIds($item->id, 'com_mostwantedrealestate.property');
			}
		}

		return $item;
	} 

	/**
	 * Method to get the record form.
	 *
	 * @param   array    $data      Data for the form.
	 * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
	 *
	 * @return  mixed  A JForm object on success, false on failure
	 *
	 * @since   1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// [Interpretation 10035] Get the form.
		$form = $this->loadForm('com_mostwantedrealestate.property', 'property', array('control' => 'jform', 'load_data' => $loadData));

		if (empty($form))
		{
			return false;
		}

		$jinput = JFactory::getApplication()->input;

		// [Interpretation 10126] The front end calls this model and uses a_id to avoid id clashes so we need to check for that first.
		if ($jinput->get('a_id'))
		{
			$id = $jinput->get('a_id', 0, 'INT');
		}
		// [Interpretation 10131] The back end uses id so we use that the rest of the time and set it to 0 by default.
		else
		{
			$id = $jinput->get('id', 0, 'INT');
		}

		$user = JFactory::getUser();

		// [Interpretation 10137] Check for existing item.
		// [Interpretation 10138] Modify the form based on Edit State access controls.
		if ($id != 0 && (!$user->authorise('core.edit.state', 'com_mostwantedrealestate.property.' . (int) $id))
			|| ($id == 0 && !$user->authorise('core.edit.state', 'com_mostwantedrealestate')))
		{
			// [Interpretation 10151] Disable fields for display.
			$form->setFieldAttribute('ordering', 'disabled', 'true');
			$form->setFieldAttribute('published', 'disabled', 'true');
			// [Interpretation 10154] Disable fields while saving.
			$form->setFieldAttribute('ordering', 'filter', 'unset');
			$form->setFieldAttribute('published', 'filter', 'unset');
		}
		// [Interpretation 10159] If this is a new item insure the greated by is set.
		if (0 == $id)
		{
			// [Interpretation 10162] Set the created_by to this user
			$form->setValue('created_by', null, $user->id);
		}
		// [Interpretation 10165] Modify the form based on Edit Creaded By access controls.
		if ($id != 0 && (!$user->authorise('core.edit.created_by', 'com_mostwantedrealestate.property.' . (int) $id))
			|| ($id == 0 && !$user->authorise('core.edit.created_by', 'com_mostwantedrealestate')))
		{
			// [Interpretation 10177] Disable fields for display.
			$form->setFieldAttribute('created_by', 'disabled', 'true');
			// [Interpretation 10179] Disable fields for display.
			$form->setFieldAttribute('created_by', 'readonly', 'true');
			// [Interpretation 10181] Disable fields while saving.
			$form->setFieldAttribute('created_by', 'filter', 'unset');
		}
		// [Interpretation 10184] Modify the form based on Edit Creaded Date access controls.
		if ($id != 0 && (!$user->authorise('core.edit.created', 'com_mostwantedrealestate.property.' . (int) $id))
			|| ($id == 0 && !$user->authorise('core.edit.created', 'com_mostwantedrealestate')))
		{
			// [Interpretation 10196] Disable fields for display.
			$form->setFieldAttribute('created', 'disabled', 'true');
			// [Interpretation 10198] Disable fields while saving.
			$form->setFieldAttribute('created', 'filter', 'unset');
		}
		// [Interpretation 10231] Only load these values if no id is found
		if (0 == $id)
		{
			// [Interpretation 10234] Set redirected field name
			$redirectedField = $jinput->get('ref', null, 'STRING');
			// [Interpretation 10236] Set redirected field value
			$redirectedValue = $jinput->get('refid', 0, 'INT');
			if (0 != $redirectedValue && $redirectedField)
			{
				// [Interpretation 10240] Now set the local-redirected field default value
				$form->setValue($redirectedField, null, $redirectedValue);
			}
		}

		return $form;
	}

	/**
	 * Method to get the script that have to be included on the form
	 *
	 * @return string	script files
	 */
	public function getScript()
	{
		return 'administrator/components/com_mostwantedrealestate/models/forms/property.js';
	}
    
	/**
	 * Method to test whether a record can be deleted.
	 *
	 * @param   object  $record  A record object.
	 *
	 * @return  boolean  True if allowed to delete the record. Defaults to the permission set in the component.
	 *
	 * @since   1.6
	 */
	protected function canDelete($record)
	{
		if (!empty($record->id))
		{
			if ($record->published != -2)
			{
				return;
			}

			$user = JFactory::getUser();
			// [Interpretation 10356] The record has been set. Check the record permissions.
			return $user->authorise('core.delete', 'com_mostwantedrealestate.property.' . (int) $record->id);
		}
		return false;
	}

	/**
	 * Method to test whether a record can have its state edited.
	 *
	 * @param   object  $record  A record object.
	 *
	 * @return  boolean  True if allowed to change the state of the record. Defaults to the permission set in the component.
	 *
	 * @since   1.6
	 */
	protected function canEditState($record)
	{
		$user = JFactory::getUser();
		$recordId	= (!empty($record->id)) ? $record->id : 0;

		if ($recordId)
		{
			// [Interpretation 10444] The record has been set. Check the record permissions.
			$permission = $user->authorise('core.edit.state', 'com_mostwantedrealestate.property.' . (int) $recordId);
			if (!$permission && !is_null($permission))
			{
				return false;
			}
		}
		// [Interpretation 10466] In the absense of better information, revert to the component permissions.
		return parent::canEditState($record);
	}
    
	/**
	 * Method override to check if you can edit an existing record.
	 *
	 * @param	array	$data	An array of input data.
	 * @param	string	$key	The name of the key for the primary key.
	 *
	 * @return	boolean
	 * @since	2.5
	 */
	protected function allowEdit($data = array(), $key = 'id')
	{
		// [Interpretation 10267] Check specific edit permission then general edit permission.
		$user = JFactory::getUser();

		return $user->authorise('core.edit', 'com_mostwantedrealestate.property.'. ((int) isset($data[$key]) ? $data[$key] : 0)) or $user->authorise('core.edit',  'com_mostwantedrealestate');
	}
    
	/**
	 * Prepare and sanitise the table data prior to saving.
	 *
	 * @param   JTable  $table  A JTable object.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	protected function prepareTable($table)
	{
		$date = JFactory::getDate();
		$user = JFactory::getUser();
		
		if (isset($table->name))
		{
			$table->name = htmlspecialchars_decode($table->name, ENT_QUOTES);
		}
		
		if (isset($table->alias) && empty($table->alias))
		{
			$table->generateAlias();
		}
		
		if (empty($table->id))
		{
			$table->created = $date->toSql();
			// set the user
			if ($table->created_by == 0 || empty($table->created_by))
			{
				$table->created_by = $user->id;
			}
			// Set ordering to the last item if not set
			if (empty($table->ordering))
			{
				$db = JFactory::getDbo();
				$query = $db->getQuery(true)
					->select('MAX(ordering)')
					->from($db->quoteName('#__mostwantedrealestate_property'));
				$db->setQuery($query);
				$max = $db->loadResult();

				$table->ordering = $max + 1;
			}
		}
		else
		{
			$table->modified = $date->toSql();
			$table->modified_by = $user->id;
		}
        
		if (!empty($table->id))
		{
			// Increment the items version number.
			$table->version++;
		}
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return  mixed  The data for the form.
	 *
	 * @since   1.6
	 */
	protected function loadFormData() 
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_mostwantedrealestate.edit.property.data', array());

		if (empty($data))
		{
			$data = $this->getItem();
		}

		return $data;
	}

	/**
	* Method to validate the form data.
	*
	* @param   JForm   $form   The form to validate against.
	* @param   array   $data   The data to validate.
	* @param   string  $group  The name of the field group to validate.
	*
	* @return  mixed  Array of filtered data if valid, false otherwise.
	*
	* @see     JFormRule
	* @see     JFilterInput
	* @since   12.2
	*/
	public function validate($form, $data, $group = null)
	{
		// [Interpretation 9210] check if the not_required field is set
		if (MostwantedrealestateHelper::checkString($data['not_required']))
		{
			$requiredFields = (array) explode(',',(string) $data['not_required']);
			$requiredFields = array_unique($requiredFields);
			// [Interpretation 9215] now change the required field attributes value
			foreach ($requiredFields as $requiredField)
			{
				// [Interpretation 9218] make sure there is a string value
				if (MostwantedrealestateHelper::checkString($requiredField))
				{
					// [Interpretation 9221] change to false
					$form->setFieldAttribute($requiredField, 'required', 'false');
					// [Interpretation 9223] also clear the data set
					$data[$requiredField] = '';
				}
			}
		}
		return parent::validate($form, $data, $group);
	} 

	/**
	 * Method to get the unique fields of this table.
	 *
	 * @return  mixed  An array of field names, boolean false if none is set.
	 *
	 * @since   3.0
	 */
	protected function getUniqeFields()
	{
		return false;
	}
	
	/**
	 * Method to delete one or more records.
	 *
	 * @param   array  &$pks  An array of record primary keys.
	 *
	 * @return  boolean  True if successful, false if an error occurs.
	 *
	 * @since   12.2
	 */
	public function delete(&$pks)
	{
		if (!parent::delete($pks))
		{
			return false;
		}
		
		return true;
	}

	/**
	 * Method to change the published state of one or more records.
	 *
	 * @param   array    &$pks   A list of the primary keys to change.
	 * @param   integer  $value  The value of the published state.
	 *
	 * @return  boolean  True on success.
	 *
	 * @since   12.2
	 */
	public function publish(&$pks, $value = 1)
	{
		if (!parent::publish($pks, $value))
		{
			return false;
		}
		
		return true;
        }
    
	/**
	 * Method to perform batch operations on an item or a set of items.
	 *
	 * @param   array  $commands  An array of commands to perform.
	 * @param   array  $pks       An array of item ids.
	 * @param   array  $contexts  An array of item contexts.
	 *
	 * @return  boolean  Returns true on success, false on failure.
	 *
	 * @since   12.2
	 */
	public function batch($commands, $pks, $contexts)
	{
		// Sanitize ids.
		$pks = array_unique($pks);
		JArrayHelper::toInteger($pks);

		// Remove any values of zero.
		if (array_search(0, $pks, true))
		{
			unset($pks[array_search(0, $pks, true)]);
		}

		if (empty($pks))
		{
			$this->setError(JText::_('JGLOBAL_NO_ITEM_SELECTED'));
			return false;
		}

		$done = false;

		// Set some needed variables.
		$this->user			= JFactory::getUser();
		$this->table			= $this->getTable();
		$this->tableClassName		= get_class($this->table);
		$this->contentType		= new JUcmType;
		$this->type			= $this->contentType->getTypeByTable($this->tableClassName);
		$this->canDo			= MostwantedrealestateHelper::getActions('property');
		$this->batchSet			= true;

		if (!$this->canDo->get('core.batch'))
		{
			$this->setError(JText::_('JLIB_APPLICATION_ERROR_INSUFFICIENT_BATCH_INFORMATION'));
			return false;
		}
        
		if ($this->type == false)
		{
			$type = new JUcmType;
			$this->type = $type->getTypeByAlias($this->typeAlias);
		}

		$this->tagsObserver = $this->table->getObserverOfClass('JTableObserverTags');

		if (!empty($commands['move_copy']))
		{
			$cmd = JArrayHelper::getValue($commands, 'move_copy', 'c');

			if ($cmd == 'c')
			{
				$result = $this->batchCopy($commands, $pks, $contexts);

				if (is_array($result))
				{
					foreach ($result as $old => $new)
					{
						$contexts[$new] = $contexts[$old];
					}
					$pks = array_values($result);
				}
				else
				{
					return false;
				}
			}
			elseif ($cmd == 'm' && !$this->batchMove($commands, $pks, $contexts))
			{
				return false;
			}

			$done = true;
		}

		if (!$done)
		{
			$this->setError(JText::_('JLIB_APPLICATION_ERROR_INSUFFICIENT_BATCH_INFORMATION'));

			return false;
		}

		// Clear the cache
		$this->cleanCache();

		return true;
	}

	/**
	 * Batch copy items to a new category or current.
	 *
	 * @param   integer  $values    The new values.
	 * @param   array    $pks       An array of row IDs.
	 * @param   array    $contexts  An array of item contexts.
	 *
	 * @return  mixed  An array of new IDs on success, boolean false on failure.
	 *
	 * @since	12.2
	 */
	protected function batchCopy($values, $pks, $contexts)
	{
		if (empty($this->batchSet))
		{
			// [Interpretation 4801] Set some needed variables.
			$this->user 		= JFactory::getUser();
			$this->table 		= $this->getTable();
			$this->tableClassName	= get_class($this->table);
			$this->contentType	= new JUcmType;
			$this->type		= $this->contentType->getTypeByTable($this->tableClassName);
			$this->canDo		= MostwantedrealestateHelper::getActions('property');
		}

		if (!$this->canDo->get('core.create') || !$this->canDo->get('core.batch'))
		{
			return false;
		}

		// [Interpretation 4821] get list of uniqe fields
		$uniqeFields = $this->getUniqeFields();
		// [Interpretation 4823] remove move_copy from array
		unset($values['move_copy']);

		// [Interpretation 4826] make sure published is set
		if (!isset($values['published']))
		{
			$values['published'] = 0;
		}
		elseif (isset($values['published']) && !$this->canDo->get('core.edit.state'))
		{
				$values['published'] = 0;
		}

		if (isset($values['category']) && (int) $values['category'] > 0 && !static::checkCategoryId($values['category']))
		{
			return false;
		}
		elseif (isset($values['category']) && (int) $values['category'] > 0)
		{
			// [Interpretation 4851] move the category value to correct field name
			$values['catid'] = $values['category'];
			unset($values['category']);
		}
		elseif (isset($values['category']))
		{
			unset($values['category']);
		}

		$newIds = array();

		// [Interpretation 4863] Parent exists so let's proceed
		while (!empty($pks))
		{
			// [Interpretation 4866] Pop the first ID off the stack
			$pk = array_shift($pks);

			$this->table->reset();

			// [Interpretation 4871] only allow copy if user may edit this item.

			if (!$this->user->authorise('core.edit', $contexts[$pk]))

			{

				// [Interpretation 4881] Not fatal error

				$this->setError(JText::sprintf('JLIB_APPLICATION_ERROR_BATCH_MOVE_ROW_NOT_FOUND', $pk));

				continue;

			}

			// [Interpretation 4886] Check that the row actually exists
			if (!$this->table->load($pk))
			{
				if ($error = $this->table->getError())
				{
					// [Interpretation 4891] Fatal error
					$this->setError($error);

					return false;
				}
				else
				{
					// [Interpretation 4898] Not fatal error
					$this->setError(JText::sprintf('JLIB_APPLICATION_ERROR_BATCH_MOVE_ROW_NOT_FOUND', $pk));
					continue;
				}
			}

			// [Interpretation 4934] insert all set values
			if (MostwantedrealestateHelper::checkArray($values))
			{
				foreach ($values as $key => $value)
				{
					if (strlen($value) > 0 && isset($this->table->$key))
					{
						$this->table->$key = $value;
					}
				}
			}

			// [Interpretation 4946] update all uniqe fields
			if (MostwantedrealestateHelper::checkArray($uniqeFields))
			{
				foreach ($uniqeFields as $uniqeField)
				{
					$this->table->$uniqeField = $this->generateUniqe($uniqeField,$this->table->$uniqeField);
				}
			}

			// [Interpretation 4955] Reset the ID because we are making a copy
			$this->table->id = 0;

			// [Interpretation 4958] TODO: Deal with ordering?
			// [Interpretation 4959] $this->table->ordering	= 1;

			// [Interpretation 4961] Check the row.
			if (!$this->table->check())
			{
				$this->setError($this->table->getError());

				return false;
			}

			if (!empty($this->type))
			{
				$this->createTagsHelper($this->tagsObserver, $this->type, $pk, $this->typeAlias, $this->table);
			}

			// [Interpretation 4974] Store the row.
			if (!$this->table->store())
			{
				$this->setError($this->table->getError());

				return false;
			}

			// [Interpretation 4982] Get the new item ID
			$newId = $this->table->get('id');

			// [Interpretation 4985] Add the new ID to the array
			$newIds[$pk] = $newId;
		}

		// [Interpretation 4989] Clean the cache
		$this->cleanCache();

		return $newIds;
	} 

	/**
	 * Batch move items to a new category
	 *
	 * @param   integer  $value     The new category ID.
	 * @param   array    $pks       An array of row IDs.
	 * @param   array    $contexts  An array of item contexts.
	 *
	 * @return  boolean  True if successful, false otherwise and internal error is set.
	 *
	 * @since	12.2
	 */
	protected function batchMove($values, $pks, $contexts)
	{
		if (empty($this->batchSet))
		{
			// [Interpretation 4610] Set some needed variables.
			$this->user		= JFactory::getUser();
			$this->table		= $this->getTable();
			$this->tableClassName	= get_class($this->table);
			$this->contentType	= new JUcmType;
			$this->type		= $this->contentType->getTypeByTable($this->tableClassName);
			$this->canDo		= MostwantedrealestateHelper::getActions('property');
		}

		if (!$this->canDo->get('core.edit') && !$this->canDo->get('core.batch'))
		{
			$this->setError(JText::_('JLIB_APPLICATION_ERROR_BATCH_CANNOT_EDIT'));
			return false;
		}

		// [Interpretation 4632] make sure published only updates if user has the permission.
		if (isset($values['published']) && !$this->canDo->get('core.edit.state'))
		{
			unset($values['published']);
		}
		// [Interpretation 4645] remove move_copy from array
		unset($values['move_copy']);

		if (isset($values['category']) && (int) $values['category'] > 0 && !static::checkCategoryId($values['category']))
		{
			return false;
		}
		elseif (isset($values['category']) && (int) $values['category'] > 0)
		{
			// [Interpretation 4656] move the category value to correct field name
			$values['catid'] = $values['category'];
			unset($values['category']);
		}
		elseif (isset($values['category']))
		{
			unset($values['category']);
		}


		// [Interpretation 4666] Parent exists so we proceed
		foreach ($pks as $pk)
		{
			if (!$this->user->authorise('core.edit', $contexts[$pk]))
			{
				$this->setError(JText::_('JLIB_APPLICATION_ERROR_BATCH_CANNOT_EDIT'));

				return false;
			}

			// [Interpretation 4683] Check that the row actually exists
			if (!$this->table->load($pk))
			{
				if ($error = $this->table->getError())
				{
					// [Interpretation 4688] Fatal error
					$this->setError($error);

					return false;
				}
				else
				{
					// [Interpretation 4695] Not fatal error
					$this->setError(JText::sprintf('JLIB_APPLICATION_ERROR_BATCH_MOVE_ROW_NOT_FOUND', $pk));
					continue;
				}
			}

			// [Interpretation 4701] insert all set values.
			if (MostwantedrealestateHelper::checkArray($values))
			{
				foreach ($values as $key => $value)
				{
					// [Interpretation 4706] Do special action for access.
					if ('access' === $key && strlen($value) > 0)
					{
						$this->table->$key = $value;
					}
					elseif (strlen($value) > 0 && isset($this->table->$key))
					{
						$this->table->$key = $value;
					}
				}
			}


			// [Interpretation 4718] Check the row.
			if (!$this->table->check())
			{
				$this->setError($this->table->getError());

				return false;
			}

			if (!empty($this->type))
			{
				$this->createTagsHelper($this->tagsObserver, $this->type, $pk, $this->typeAlias, $this->table);
			}

			// [Interpretation 4731] Store the row.
			if (!$this->table->store())
			{
				$this->setError($this->table->getError());

				return false;
			}
		}

		// [Interpretation 4740] Clean the cache
		$this->cleanCache();

		return true;
	}
	
	/**
	 * Method to save the form data.
	 *
	 * @param   array  $data  The form data.
	 *
	 * @return  boolean  True on success.
	 *
	 * @since   1.6
	 */
	public function save($data)
	{
		$input	= JFactory::getApplication()->input;
		$filter	= JFilterInput::getInstance();
        
		// set the metadata to the Item Data
		if (isset($data['metadata']) && isset($data['metadata']['author']))
		{
			$data['metadata']['author'] = $filter->clean($data['metadata']['author'], 'TRIM');
            
			$metadata = new JRegistry;
			$metadata->loadArray($data['metadata']);
			$data['metadata'] = (string) $metadata;
		} 

		// [Interpretation 3857] Set the commonareas items to data.
		if (isset($data['commonareas']) && is_array($data['commonareas']))
		{
			$commonareas = new JRegistry;
			$commonareas->loadArray($data['commonareas']);
			$data['commonareas'] = (string) $commonareas;
		}
		elseif (!isset($data['commonareas']))
		{
			// [Interpretation 3866] Set the empty commonareas to data
			$data['commonareas'] = '';
		}

		// [Interpretation 3857] Set the indoorfeatures items to data.
		if (isset($data['indoorfeatures']) && is_array($data['indoorfeatures']))
		{
			$indoorfeatures = new JRegistry;
			$indoorfeatures->loadArray($data['indoorfeatures']);
			$data['indoorfeatures'] = (string) $indoorfeatures;
		}
		elseif (!isset($data['indoorfeatures']))
		{
			// [Interpretation 3866] Set the empty indoorfeatures to data
			$data['indoorfeatures'] = '';
		}

		// [Interpretation 3857] Set the buildingfeatures items to data.
		if (isset($data['buildingfeatures']) && is_array($data['buildingfeatures']))
		{
			$buildingfeatures = new JRegistry;
			$buildingfeatures->loadArray($data['buildingfeatures']);
			$data['buildingfeatures'] = (string) $buildingfeatures;
		}
		elseif (!isset($data['buildingfeatures']))
		{
			// [Interpretation 3866] Set the empty buildingfeatures to data
			$data['buildingfeatures'] = '';
		}

		// [Interpretation 3857] Set the otherfeatures items to data.
		if (isset($data['otherfeatures']) && is_array($data['otherfeatures']))
		{
			$otherfeatures = new JRegistry;
			$otherfeatures->loadArray($data['otherfeatures']);
			$data['otherfeatures'] = (string) $otherfeatures;
		}
		elseif (!isset($data['otherfeatures']))
		{
			// [Interpretation 3866] Set the empty otherfeatures to data
			$data['otherfeatures'] = '';
		}

		// [Interpretation 3857] Set the colistagent items to data.
		if (isset($data['colistagent']) && is_array($data['colistagent']))
		{
			$colistagent = new JRegistry;
			$colistagent->loadArray($data['colistagent']);
			$data['colistagent'] = (string) $colistagent;
		}
		elseif (!isset($data['colistagent']))
		{
			// [Interpretation 3866] Set the empty colistagent to data
			$data['colistagent'] = '';
		}

		// [Interpretation 3857] Set the appliances items to data.
		if (isset($data['appliances']) && is_array($data['appliances']))
		{
			$appliances = new JRegistry;
			$appliances->loadArray($data['appliances']);
			$data['appliances'] = (string) $appliances;
		}
		elseif (!isset($data['appliances']))
		{
			// [Interpretation 3866] Set the empty appliances to data
			$data['appliances'] = '';
		}

		// [Interpretation 3857] Set the outdoorfeatures items to data.
		if (isset($data['outdoorfeatures']) && is_array($data['outdoorfeatures']))
		{
			$outdoorfeatures = new JRegistry;
			$outdoorfeatures->loadArray($data['outdoorfeatures']);
			$data['outdoorfeatures'] = (string) $outdoorfeatures;
		}
		elseif (!isset($data['outdoorfeatures']))
		{
			// [Interpretation 3866] Set the empty outdoorfeatures to data
			$data['outdoorfeatures'] = '';
		}

		// [Interpretation 3857] Set the communityfeatures items to data.
		if (isset($data['communityfeatures']) && is_array($data['communityfeatures']))
		{
			$communityfeatures = new JRegistry;
			$communityfeatures->loadArray($data['communityfeatures']);
			$data['communityfeatures'] = (string) $communityfeatures;
		}
		elseif (!isset($data['communityfeatures']))
		{
			// [Interpretation 3866] Set the empty communityfeatures to data
			$data['communityfeatures'] = '';
		}

		// [Interpretation 3857] Set the unitdetails items to data.
		if (isset($data['unitdetails']) && is_array($data['unitdetails']))
		{
			$unitdetails = new JRegistry;
			$unitdetails->loadArray($data['unitdetails']);
			$data['unitdetails'] = (string) $unitdetails;
		}
		elseif (!isset($data['unitdetails']))
		{
			// [Interpretation 3866] Set the empty unitdetails to data
			$data['unitdetails'] = '';
		}

		// [Interpretation 3857] Set the openhouseinfo items to data.
		if (isset($data['openhouseinfo']) && is_array($data['openhouseinfo']))
		{
			$openhouseinfo = new JRegistry;
			$openhouseinfo->loadArray($data['openhouseinfo']);
			$data['openhouseinfo'] = (string) $openhouseinfo;
		}
		elseif (!isset($data['openhouseinfo']))
		{
			// [Interpretation 3866] Set the empty openhouseinfo to data
			$data['openhouseinfo'] = '';
		}

		// [Interpretation 3876] Set the terms string to JSON string.
		if (isset($data['terms']))
		{
			$data['terms'] = (string) json_encode($data['terms']);
		}

		// [Interpretation 3876] Set the waterresources string to JSON string.
		if (isset($data['waterresources']))
		{
			$data['waterresources'] = (string) json_encode($data['waterresources']);
		}

		// [Interpretation 3876] Set the porchpatio string to JSON string.
		if (isset($data['porchpatio']))
		{
			$data['porchpatio'] = (string) json_encode($data['porchpatio']);
		}

		// [Interpretation 3876] Set the garagetype string to JSON string.
		if (isset($data['garagetype']))
		{
			$data['garagetype'] = (string) json_encode($data['garagetype']);
		}

		// [Interpretation 3876] Set the heating string to JSON string.
		if (isset($data['heating']))
		{
			$data['heating'] = (string) json_encode($data['heating']);
		}

		// [Interpretation 3876] Set the cooling string to JSON string.
		if (isset($data['cooling']))
		{
			$data['cooling'] = (string) json_encode($data['cooling']);
		}

//Custom PHP Save Method - Alias modifications
				$mlsid = $data['mls_id'];
				$aliasStr = $mlsid;
			if($data['street'] != ''){
				$street = $data['street'];
				$aliasStr = $mlsid. ' ' .$street;
			}
			if($data['cityid']){
				$cityid = $data['cityid'];
				$db = JFactory::getDbo();

				$query = $db->getQuery(true)
					->select('alias')
					->from($db->quoteName('#__mostwantedrealestate_city'))
					->where($db->quoteName('id') . ' = '. $db->quote($cityid));

				$db->setQuery($query);

				$city = $db->loadResult();
				$aliasStr = $aliasStr.' '.$city;

			}
			if($data['stateid']){
				$stateid = $data['stateid'];
				$db = JFactory::getDbo();

				$query = $db->getQuery(true)
					->select('alias')
					->from($db->quoteName('#__mostwantedrealestate_state'))
					->where($db->quoteName('id') . ' = '. $db->quote($stateid));

				$db->setQuery($query);

				$state = $db->loadResult();
				$aliasStr = $aliasStr.' '.$state;

			}
			if($data['postcode'] != ''){
				$postcode = $data['postcode'];
				$aliasStr = $aliasStr.' '.$postcode;
			}
			
			$data['alias'] = JFilterOutput::stringURLSafe($aliasStr);
                        

        
		// Set the Params Items to data
		if (isset($data['params']) && is_array($data['params']))
		{
			$params = new JRegistry;
			$params->loadArray($data['params']);
			$data['params'] = (string) $params;
		}

		// [Interpretation 5081] Alter the uniqe field for save as copy
		if ($input->get('task') === 'save2copy')
		{
			// [Interpretation 5084] Automatic handling of other uniqe fields
			$uniqeFields = $this->getUniqeFields();
			if (MostwantedrealestateHelper::checkArray($uniqeFields))
			{
				foreach ($uniqeFields as $uniqeField)
				{
					$data[$uniqeField] = $this->generateUniqe($uniqeField,$data[$uniqeField]);
				}
			}
		}
		
		if (parent::save($data))
		{
			return true;
		}
		return false;
	}
	
	/**
	 * Method to generate a uniqe value.
	 *
	 * @param   string  $field name.
	 * @param   string  $value data.
	 *
	 * @return  string  New value.
	 *
	 * @since   3.0
	 */
	protected function generateUniqe($field,$value)
	{

		// set field value uniqe 
		$table = $this->getTable();

		while ($table->load(array($field => $value)))
		{
			$value = JString::increment($value);
		}

		return $value;
	}

	/**
	* Method to change the title & alias.
	*
	* @param   string   $title        The title.
	*
	* @return	array  Contains the modified title and alias.
	*
	*/
	protected function _generateNewTitle($title)
	{

		// [Interpretation 5139] Alter the title
		$table = $this->getTable();

		while ($table->load(array('title' => $title)))
		{
			$title = JString::increment($title);
		}

		return $title;
	}
}
