<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		2.0.0
	@build			22nd September, 2017
	@created		1st May, 2016
	@package		Most Wanted Real Estate
	@subpackage		agents.php
	@author			Most Wanted Web Services, Inc. <http://mostwantedrealestatesites.com>	
	@copyright		Copyright (C) 2015-2017. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
	
	Most Wanted Real Estate Component
	
/------------------------------------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import the Joomla modellist library
jimport('joomla.application.component.modellist');

/**
 * Agents Model
 */
class MostwantedrealestateModelAgents extends JModelList
{
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
        {
			$config['filter_fields'] = array(
				'a.id','id',
				'a.published','published',
				'a.ordering','ordering',
				'a.created_by','created_by',
				'a.modified_by','modified_by'
			);
		}

		parent::__construct($config);
	}
	
	/**
	 * Method to auto-populate the model state.
	 *
	 * @return  void
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		$app = JFactory::getApplication();

		// Adjust the context to support modal layouts.
		if ($layout = $app->input->get('layout'))
		{
			$this->context .= '.' . $layout;
		}
		
        
		$sorting = $this->getUserStateFromRequest($this->context . '.filter.sorting', 'filter_sorting', 0, 'int');
		$this->setState('filter.sorting', $sorting);
        
		$access = $this->getUserStateFromRequest($this->context . '.filter.access', 'filter_access', 0, 'int');
		$this->setState('filter.access', $access);
        
		$search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$published = $this->getUserStateFromRequest($this->context . '.filter.published', 'filter_published', '');
		$this->setState('filter.published', $published);
        
		$created_by = $this->getUserStateFromRequest($this->context . '.filter.created_by', 'filter_created_by', '');
		$this->setState('filter.created_by', $created_by);

		$created = $this->getUserStateFromRequest($this->context . '.filter.created', 'filter_created');
		$this->setState('filter.created', $created);

		// List state information.
		parent::populateState($ordering, $direction);
	}
	
	/**
	 * Method to get an array of data items.
	 *
	 * @return  mixed  An array of data items on success, false on failure.
	 */
	public function getItems()
	{ 
		// [Interpretation 11051] check in items
		$this->checkInNow();

		// load parent items
		$items = parent::getItems();

		// [Interpretation 11126] set values to display correctly.
		if (MostwantedrealestateHelper::checkArray($items))
		{
			// [Interpretation 11129] get user object.
			$user = JFactory::getUser();
			foreach ($items as $nr => &$item)
			{
				$access = ($user->authorise('agent.access', 'com_mostwantedrealestate.agent.' . (int) $item->id) && $user->authorise('agent.access', 'com_mostwantedrealestate'));
				if (!$access)
				{
					unset($items[$nr]);
					continue;
				}

			}
		}  
        
		// return items
		return $items;
	}
	
	/**
	 * Method to build an SQL query to load the list data.
	 *
	 * @return	string	An SQL query
	 */
	protected function getListQuery()
	{
		// [Interpretation 7883] Get the user object.
		$user = JFactory::getUser();
		// [Interpretation 7885] Create a new query object.
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);

		// [Interpretation 7888] Select some fields
		$query->select('a.*');
		$query->select($db->quoteName('c.title','category_title'));

		// [Interpretation 7895] From the mostwantedrealestate_item table
		$query->from($db->quoteName('#__mostwantedrealestate_agent', 'a'));
		$query->join('LEFT', $db->quoteName('#__categories', 'c') . ' ON (' . $db->quoteName('a.catid') . ' = ' . $db->quoteName('c.id') . ')');

		// [Interpretation 8039] From the mostwantedrealestate_city table.
		$query->select($db->quoteName('g.name','cityid_name'));
		$query->join('LEFT', $db->quoteName('#__mostwantedrealestate_city', 'g') . ' ON (' . $db->quoteName('a.cityid') . ' = ' . $db->quoteName('g.id') . ')');

		// [Interpretation 8039] From the mostwantedrealestate_state table.
		$query->select($db->quoteName('h.name','stateid_name'));
		$query->join('LEFT', $db->quoteName('#__mostwantedrealestate_state', 'h') . ' ON (' . $db->quoteName('a.stateid') . ' = ' . $db->quoteName('h.id') . ')');

		// [Interpretation 7906] Filter by published state
		$published = $this->getState('filter.published');
		if (is_numeric($published))
		{
			$query->where('a.published = ' . (int) $published);
		}
		elseif ($published === '')
		{
			$query->where('(a.published = 0 OR a.published = 1)');
		}

		// [Interpretation 7918] Join over the asset groups.
		$query->select('ag.title AS access_level');
		$query->join('LEFT', '#__viewlevels AS ag ON ag.id = a.access');
		// [Interpretation 7921] Filter by access level.
		if ($access = $this->getState('filter.access'))
		{
			$query->where('a.access = ' . (int) $access);
		}
		// [Interpretation 7926] Implement View Level Access
		if (!$user->authorise('core.options', 'com_mostwantedrealestate'))
		{
			$groups = implode(',', $user->getAuthorisedViewLevels());
			$query->where('a.access IN (' . $groups . ')');
		}

		// [Interpretation 7940] Filter by a single or group of categories.
		$baselevel = 1;
		$categoryId = $this->getState('filter.category_id');

		if (is_numeric($categoryId))
		{
			$cat_tbl = JTable::getInstance('Category', 'JTable');
			$cat_tbl->load($categoryId);
			$rgt = $cat_tbl->rgt;
			$lft = $cat_tbl->lft;
			$baselevel = (int) $cat_tbl->level;
			$query->where('c.lft >= ' . (int) $lft)
				->where('c.rgt <= ' . (int) $rgt);
		}
		elseif (is_array($categoryId))
		{
			JArrayHelper::toInteger($categoryId);
			$categoryId = implode(',', $categoryId);
			$query->where('a.category IN (' . $categoryId . ')');
		}


		// [Interpretation 7962] Add the list ordering clause.
		$orderCol = $this->state->get('list.ordering', 'a.id');
		$orderDirn = $this->state->get('list.direction', 'asc');	
		if ($orderCol != '')
		{
			$query->order($db->escape($orderCol . ' ' . $orderDirn));
		}

		return $query;
	}

	/**
	* Method to get list export data.
	*
	* @return mixed  An array of data items on success, false on failure.
	*/
	public function getExportData($pks)
	{
		// [Interpretation 7635] setup the query
		if (MostwantedrealestateHelper::checkArray($pks))
		{
			// [Interpretation 7638] Set a value to know this is exporting method.
			$_export = true;
			// [Interpretation 7640] Get the user object.
			$user = JFactory::getUser();
			// [Interpretation 7642] Create a new query object.
			$db = JFactory::getDBO();
			$query = $db->getQuery(true);

			// [Interpretation 7645] Select some fields
			$query->select('a.*');

			// [Interpretation 7647] From the mostwantedrealestate_agent table
			$query->from($db->quoteName('#__mostwantedrealestate_agent', 'a'));
			$query->where('a.id IN (' . implode(',',$pks) . ')');
			// [Interpretation 7655] Implement View Level Access
			if (!$user->authorise('core.options', 'com_mostwantedrealestate'))
			{
				$groups = implode(',', $user->getAuthorisedViewLevels());
				$query->where('a.access IN (' . $groups . ')');
			}

			// [Interpretation 7662] Order the results by ordering
			$query->order('a.ordering  ASC');

			// [Interpretation 7664] Load the items
			$db->setQuery($query);
			$db->execute();
			if ($db->getNumRows())
			{
				$items = $db->loadObjectList();

				// [Interpretation 11126] set values to display correctly.
				if (MostwantedrealestateHelper::checkArray($items))
				{
					// [Interpretation 11129] get user object.
					$user = JFactory::getUser();
					foreach ($items as $nr => &$item)
					{
						$access = ($user->authorise('agent.access', 'com_mostwantedrealestate.agent.' . (int) $item->id) && $user->authorise('agent.access', 'com_mostwantedrealestate'));
						if (!$access)
						{
							unset($items[$nr]);
							continue;
						}

						// [Interpretation 11353] unset the values we don't want exported.
						unset($item->asset_id);
						unset($item->checked_out);
						unset($item->checked_out_time);
					}
				}
				// [Interpretation 11362] Add headers to items array.
				$headers = $this->getExImPortHeaders();
				if (MostwantedrealestateHelper::checkObject($headers))
				{
					array_unshift($items,$headers);
				}
				return $items;
			}
		}
		return false;
	}

	/**
	* Method to get header.
	*
	* @return mixed  An array of data items on success, false on failure.
	*/
	public function getExImPortHeaders()
	{
		// [Interpretation 7686] Get a db connection.
		$db = JFactory::getDbo();
		// [Interpretation 7688] get the columns
		$columns = $db->getTableColumns("#__mostwantedrealestate_agent");
		if (MostwantedrealestateHelper::checkArray($columns))
		{
			// [Interpretation 7692] remove the headers you don't import/export.
			unset($columns['asset_id']);
			unset($columns['checked_out']);
			unset($columns['checked_out_time']);
			$headers = new stdClass();
			foreach ($columns as $column => $type)
			{
				$headers->{$column} = $column;
			}
			return $headers;
		}
		return false;
	} 
	
	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * @return  string  A store id.
	 *
	 */
	protected function getStoreId($id = '')
	{
		// [Interpretation 10653] Compile the store id.
		$id .= ':' . $this->getState('filter.id');
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.published');
		$id .= ':' . $this->getState('filter.ordering');
		$id .= ':' . $this->getState('filter.created_by');
		$id .= ':' . $this->getState('filter.modified_by');

		return parent::getStoreId($id);
	}

	/**
	* Build an SQL query to checkin all items left checked out longer then a set time.
	*
	* @return  a bool
	*
	*/
	protected function checkInNow()
	{
		// [Interpretation 11067] Get set check in time
		$time = JComponentHelper::getParams('com_mostwantedrealestate')->get('check_in');
		
		if ($time)
		{

			// [Interpretation 11072] Get a db connection.
			$db = JFactory::getDbo();
			// [Interpretation 11074] reset query
			$query = $db->getQuery(true);
			$query->select('*');
			$query->from($db->quoteName('#__mostwantedrealestate_agent'));
			$db->setQuery($query);
			$db->execute();
			if ($db->getNumRows())
			{
				// [Interpretation 11082] Get Yesterdays date
				$date = JFactory::getDate()->modify($time)->toSql();
				// [Interpretation 11084] reset query
				$query = $db->getQuery(true);

				// [Interpretation 11086] Fields to update.
				$fields = array(
					$db->quoteName('checked_out_time') . '=\'0000-00-00 00:00:00\'',
					$db->quoteName('checked_out') . '=0'
				);

				// [Interpretation 11091] Conditions for which records should be updated.
				$conditions = array(
					$db->quoteName('checked_out') . '!=0', 
					$db->quoteName('checked_out_time') . '<\''.$date.'\''
				);

				// [Interpretation 11096] Check table
				$query->update($db->quoteName('#__mostwantedrealestate_agent'))->set($fields)->where($conditions); 

				$db->setQuery($query);

				$db->execute();
			}
		}

		return false;
	}
}
