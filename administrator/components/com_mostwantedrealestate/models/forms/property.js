/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		2.0.0
	@build			22nd September, 2017
	@created		1st May, 2016
	@package		Most Wanted Real Estate
	@subpackage		property.js
	@author			Most Wanted Web Services, Inc. <http://mostwantedrealestatesites.com>	
	@copyright		Copyright (C) 2015-2017. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
	
	Most Wanted Real Estate Component
	
/------------------------------------------------------------------------------------------------------*/

// Some Global Values
jform_vvvvvwavwk_required = false;
jform_vvvvvwavwl_required = false;
jform_vvvvvwbvwm_required = false;
jform_vvvvvwdvwn_required = false;
jform_vvvvvwdvwo_required = false;
jform_vvvvvwevwp_required = false;
jform_vvvvvwevwq_required = false;
jform_vvvvvwgvwr_required = false;

// Initial Script
jQuery(document).ready(function()
{
	var showprice_vvvvvwa = jQuery("#jform_showprice input[type='radio']:checked").val();
	vvvvvwa(showprice_vvvvvwa);

	var showprice_vvvvvwb = jQuery("#jform_showprice input[type='radio']:checked").val();
	vvvvvwb(showprice_vvvvvwb);

	var openhouse_vvvvvwc = jQuery("#jform_openhouse input[type='radio']:checked").val();
	vvvvvwc(openhouse_vvvvvwc);

	var pm_price_override_vvvvvwd = jQuery("#jform_pm_price_override input[type='radio']:checked").val();
	vvvvvwd(pm_price_override_vvvvvwd);

	var owncoords_vvvvvwe = jQuery("#jform_owncoords input[type='radio']:checked").val();
	vvvvvwe(owncoords_vvvvvwe);

	var viewbooking_vvvvvwf = jQuery("#jform_viewbooking input[type='radio']:checked").val();
	vvvvvwf(viewbooking_vvvvvwf);

	var waterfront_vvvvvwg = jQuery("#jform_waterfront input[type='radio']:checked").val();
	vvvvvwg(waterfront_vvvvvwg);
});

// the vvvvvwa function
function vvvvvwa(showprice_vvvvvwa)
{
	// [Interpretation 8443] set the function logic
	if (showprice_vvvvvwa == 1)
	{
		jQuery('#jform_closedate').closest('.control-group').show();
		jQuery('#jform_closeprice').closest('.control-group').show();
		if (jform_vvvvvwavwk_required)
		{
			updateFieldRequired('closeprice',0);
			jQuery('#jform_closeprice').prop('required','required');
			jQuery('#jform_closeprice').attr('aria-required',true);
			jQuery('#jform_closeprice').addClass('required');
			jform_vvvvvwavwk_required = false;
		}

		jQuery('#jform_price').closest('.control-group').show();
		if (jform_vvvvvwavwl_required)
		{
			updateFieldRequired('price',0);
			jQuery('#jform_price').prop('required','required');
			jQuery('#jform_price').attr('aria-required',true);
			jQuery('#jform_price').addClass('required');
			jform_vvvvvwavwl_required = false;
		}

	}
	else
	{
		jQuery('#jform_closedate').closest('.control-group').hide();
		jQuery('#jform_closeprice').closest('.control-group').hide();
		if (!jform_vvvvvwavwk_required)
		{
			updateFieldRequired('closeprice',1);
			jQuery('#jform_closeprice').removeAttr('required');
			jQuery('#jform_closeprice').removeAttr('aria-required');
			jQuery('#jform_closeprice').removeClass('required');
			jform_vvvvvwavwk_required = true;
		}
		jQuery('#jform_price').closest('.control-group').hide();
		if (!jform_vvvvvwavwl_required)
		{
			updateFieldRequired('price',1);
			jQuery('#jform_price').removeAttr('required');
			jQuery('#jform_price').removeAttr('aria-required');
			jQuery('#jform_price').removeClass('required');
			jform_vvvvvwavwl_required = true;
		}
	}
}

// the vvvvvwb function
function vvvvvwb(showprice_vvvvvwb)
{
	// [Interpretation 8443] set the function logic
	if (showprice_vvvvvwb == 1)
	{
		jQuery('#jform_priceview').closest('.control-group').hide();
		if (!jform_vvvvvwbvwm_required)
		{
			updateFieldRequired('priceview',1);
			jQuery('#jform_priceview').removeAttr('required');
			jQuery('#jform_priceview').removeAttr('aria-required');
			jQuery('#jform_priceview').removeClass('required');
			jform_vvvvvwbvwm_required = true;
		}
	}
	else
	{
		jQuery('#jform_priceview').closest('.control-group').show();
		if (jform_vvvvvwbvwm_required)
		{
			updateFieldRequired('priceview',0);
			jQuery('#jform_priceview').prop('required','required');
			jQuery('#jform_priceview').attr('aria-required',true);
			jQuery('#jform_priceview').addClass('required');
			jform_vvvvvwbvwm_required = false;
		}

	}
}

// the vvvvvwc function
function vvvvvwc(openhouse_vvvvvwc)
{
	// [Interpretation 8443] set the function logic
	if (openhouse_vvvvvwc == 1)
	{
		jQuery('#jform_openhouseinfo-lbl').closest('.control-group').show();
	}
	else
	{
		jQuery('#jform_openhouseinfo-lbl').closest('.control-group').hide();
	}
}

// the vvvvvwd function
function vvvvvwd(pm_price_override_vvvvvwd)
{
	// [Interpretation 8443] set the function logic
	if (pm_price_override_vvvvvwd == 1)
	{
		jQuery('#jform_pmenddate').closest('.control-group').show();
		jQuery('#jform_propmgt_price').closest('.control-group').show();
		if (jform_vvvvvwdvwn_required)
		{
			updateFieldRequired('propmgt_price',0);
			jQuery('#jform_propmgt_price').prop('required','required');
			jQuery('#jform_propmgt_price').attr('aria-required',true);
			jQuery('#jform_propmgt_price').addClass('required');
			jform_vvvvvwdvwn_required = false;
		}

		jQuery('#jform_pmstartdate').closest('.control-group').show();
		jQuery('#jform_propmgt_description').closest('.control-group').show();
		if (jform_vvvvvwdvwo_required)
		{
			updateFieldRequired('propmgt_description',0);
			jQuery('#jform_propmgt_description').prop('required','required');
			jQuery('#jform_propmgt_description').attr('aria-required',true);
			jQuery('#jform_propmgt_description').addClass('required');
			jform_vvvvvwdvwo_required = false;
		}

	}
	else
	{
		jQuery('#jform_pmenddate').closest('.control-group').hide();
		jQuery('#jform_propmgt_price').closest('.control-group').hide();
		if (!jform_vvvvvwdvwn_required)
		{
			updateFieldRequired('propmgt_price',1);
			jQuery('#jform_propmgt_price').removeAttr('required');
			jQuery('#jform_propmgt_price').removeAttr('aria-required');
			jQuery('#jform_propmgt_price').removeClass('required');
			jform_vvvvvwdvwn_required = true;
		}
		jQuery('#jform_pmstartdate').closest('.control-group').hide();
		jQuery('#jform_propmgt_description').closest('.control-group').hide();
		if (!jform_vvvvvwdvwo_required)
		{
			updateFieldRequired('propmgt_description',1);
			jQuery('#jform_propmgt_description').removeAttr('required');
			jQuery('#jform_propmgt_description').removeAttr('aria-required');
			jQuery('#jform_propmgt_description').removeClass('required');
			jform_vvvvvwdvwo_required = true;
		}
	}
}

// the vvvvvwe function
function vvvvvwe(owncoords_vvvvvwe)
{
	// [Interpretation 8443] set the function logic
	if (owncoords_vvvvvwe == 1)
	{
		jQuery('#jform_latitude').closest('.control-group').show();
		if (jform_vvvvvwevwp_required)
		{
			updateFieldRequired('latitude',0);
			jQuery('#jform_latitude').prop('required','required');
			jQuery('#jform_latitude').attr('aria-required',true);
			jQuery('#jform_latitude').addClass('required');
			jform_vvvvvwevwp_required = false;
		}

		jQuery('#jform_longitude').closest('.control-group').show();
		if (jform_vvvvvwevwq_required)
		{
			updateFieldRequired('longitude',0);
			jQuery('#jform_longitude').prop('required','required');
			jQuery('#jform_longitude').attr('aria-required',true);
			jQuery('#jform_longitude').addClass('required');
			jform_vvvvvwevwq_required = false;
		}

	}
	else
	{
		jQuery('#jform_latitude').closest('.control-group').hide();
		if (!jform_vvvvvwevwp_required)
		{
			updateFieldRequired('latitude',1);
			jQuery('#jform_latitude').removeAttr('required');
			jQuery('#jform_latitude').removeAttr('aria-required');
			jQuery('#jform_latitude').removeClass('required');
			jform_vvvvvwevwp_required = true;
		}
		jQuery('#jform_longitude').closest('.control-group').hide();
		if (!jform_vvvvvwevwq_required)
		{
			updateFieldRequired('longitude',1);
			jQuery('#jform_longitude').removeAttr('required');
			jQuery('#jform_longitude').removeAttr('aria-required');
			jQuery('#jform_longitude').removeClass('required');
			jform_vvvvvwevwq_required = true;
		}
	}
}

// the vvvvvwf function
function vvvvvwf(viewbooking_vvvvvwf)
{
	// [Interpretation 8443] set the function logic
	if (viewbooking_vvvvvwf == 1)
	{
		jQuery('#jform_availdate').closest('.control-group').show();
	}
	else
	{
		jQuery('#jform_availdate').closest('.control-group').hide();
	}
}

// the vvvvvwg function
function vvvvvwg(waterfront_vvvvvwg)
{
	// [Interpretation 8443] set the function logic
	if (waterfront_vvvvvwg == 1)
	{
		jQuery('#jform_waterfronttype').closest('.control-group').show();
		if (jform_vvvvvwgvwr_required)
		{
			updateFieldRequired('waterfronttype',0);
			jQuery('#jform_waterfronttype').prop('required','required');
			jQuery('#jform_waterfronttype').attr('aria-required',true);
			jQuery('#jform_waterfronttype').addClass('required');
			jform_vvvvvwgvwr_required = false;
		}

	}
	else
	{
		jQuery('#jform_waterfronttype').closest('.control-group').hide();
		if (!jform_vvvvvwgvwr_required)
		{
			updateFieldRequired('waterfronttype',1);
			jQuery('#jform_waterfronttype').removeAttr('required');
			jQuery('#jform_waterfronttype').removeAttr('aria-required');
			jQuery('#jform_waterfronttype').removeClass('required');
			jform_vvvvvwgvwr_required = true;
		}
	}
}

// update required fields
function updateFieldRequired(name,status)
{
	var not_required = jQuery('#jform_not_required').val();

	if(status == 1)
	{
		if (isSet(not_required) && not_required != 0)
		{
			not_required = not_required+','+name;
		}
		else
		{
			not_required = ','+name;
		}
	}
	else
	{
		if (isSet(not_required) && not_required != 0)
		{
			not_required = not_required.replace(','+name,'');
		}
	}

	jQuery('#jform_not_required').val(not_required);
}

// the isSet function
function isSet(val)
{
	if ((val != undefined) && (val != null) && 0 !== val.length){
		return true;
	}
	return false;
} 
