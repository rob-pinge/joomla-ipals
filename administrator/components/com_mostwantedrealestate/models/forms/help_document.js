/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		2.0.0
	@build			16th September, 2017
	@created		1st May, 2016
	@package		Most Wanted Real Estate
	@subpackage		help_document.js
	@author			Most Wanted Web Services, Inc. <http://mostwantedrealestatesites.com>	
	@copyright		Copyright (C) 2015-2017. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
	
	Most Wanted Real Estate Component
	
/------------------------------------------------------------------------------------------------------*/

// Some Global Values
jform_vvvvvwhvws_required = false;
jform_vvvvvwivwt_required = false;
jform_vvvvvwjvwu_required = false;
jform_vvvvvwkvwv_required = false;
jform_vvvvvwlvww_required = false;
jform_vvvvvwmvwx_required = false;

// Initial Script
jQuery(document).ready(function()
{
	var location_vvvvvwh = jQuery("#jform_location input[type='radio']:checked").val();
	vvvvvwh(location_vvvvvwh);

	var location_vvvvvwi = jQuery("#jform_location input[type='radio']:checked").val();
	vvvvvwi(location_vvvvvwi);

	var type_vvvvvwj = jQuery("#jform_type").val();
	vvvvvwj(type_vvvvvwj);

	var type_vvvvvwk = jQuery("#jform_type").val();
	vvvvvwk(type_vvvvvwk);

	var type_vvvvvwl = jQuery("#jform_type").val();
	vvvvvwl(type_vvvvvwl);

	var target_vvvvvwm = jQuery("#jform_target input[type='radio']:checked").val();
	vvvvvwm(target_vvvvvwm);
});

// the vvvvvwh function
function vvvvvwh(location_vvvvvwh)
{
	// [Interpretation 8443] set the function logic
	if (location_vvvvvwh == 1)
	{
		jQuery('#jform_admin_view').closest('.control-group').show();
		if (jform_vvvvvwhvws_required)
		{
			updateFieldRequired('admin_view',0);
			jQuery('#jform_admin_view').prop('required','required');
			jQuery('#jform_admin_view').attr('aria-required',true);
			jQuery('#jform_admin_view').addClass('required');
			jform_vvvvvwhvws_required = false;
		}

	}
	else
	{
		jQuery('#jform_admin_view').closest('.control-group').hide();
		if (!jform_vvvvvwhvws_required)
		{
			updateFieldRequired('admin_view',1);
			jQuery('#jform_admin_view').removeAttr('required');
			jQuery('#jform_admin_view').removeAttr('aria-required');
			jQuery('#jform_admin_view').removeClass('required');
			jform_vvvvvwhvws_required = true;
		}
	}
}

// the vvvvvwi function
function vvvvvwi(location_vvvvvwi)
{
	// [Interpretation 8443] set the function logic
	if (location_vvvvvwi == 2)
	{
		jQuery('#jform_site_view').closest('.control-group').show();
		if (jform_vvvvvwivwt_required)
		{
			updateFieldRequired('site_view',0);
			jQuery('#jform_site_view').prop('required','required');
			jQuery('#jform_site_view').attr('aria-required',true);
			jQuery('#jform_site_view').addClass('required');
			jform_vvvvvwivwt_required = false;
		}

	}
	else
	{
		jQuery('#jform_site_view').closest('.control-group').hide();
		if (!jform_vvvvvwivwt_required)
		{
			updateFieldRequired('site_view',1);
			jQuery('#jform_site_view').removeAttr('required');
			jQuery('#jform_site_view').removeAttr('aria-required');
			jQuery('#jform_site_view').removeClass('required');
			jform_vvvvvwivwt_required = true;
		}
	}
}

// the vvvvvwj function
function vvvvvwj(type_vvvvvwj)
{
	if (isSet(type_vvvvvwj) && type_vvvvvwj.constructor !== Array)
	{
		var temp_vvvvvwj = type_vvvvvwj;
		var type_vvvvvwj = [];
		type_vvvvvwj.push(temp_vvvvvwj);
	}
	else if (!isSet(type_vvvvvwj))
	{
		var type_vvvvvwj = [];
	}
	var type = type_vvvvvwj.some(type_vvvvvwj_SomeFunc);


	// [Interpretation 8421] set this function logic
	if (type)
	{
		jQuery('#jform_url').closest('.control-group').show();
		if (jform_vvvvvwjvwu_required)
		{
			updateFieldRequired('url',0);
			jQuery('#jform_url').prop('required','required');
			jQuery('#jform_url').attr('aria-required',true);
			jQuery('#jform_url').addClass('required');
			jform_vvvvvwjvwu_required = false;
		}

	}
	else
	{
		jQuery('#jform_url').closest('.control-group').hide();
		if (!jform_vvvvvwjvwu_required)
		{
			updateFieldRequired('url',1);
			jQuery('#jform_url').removeAttr('required');
			jQuery('#jform_url').removeAttr('aria-required');
			jQuery('#jform_url').removeClass('required');
			jform_vvvvvwjvwu_required = true;
		}
	}
}

// the vvvvvwj Some function
function type_vvvvvwj_SomeFunc(type_vvvvvwj)
{
	// [Interpretation 8408] set the function logic
	if (type_vvvvvwj == 3)
	{
		return true;
	}
	return false;
}

// the vvvvvwk function
function vvvvvwk(type_vvvvvwk)
{
	if (isSet(type_vvvvvwk) && type_vvvvvwk.constructor !== Array)
	{
		var temp_vvvvvwk = type_vvvvvwk;
		var type_vvvvvwk = [];
		type_vvvvvwk.push(temp_vvvvvwk);
	}
	else if (!isSet(type_vvvvvwk))
	{
		var type_vvvvvwk = [];
	}
	var type = type_vvvvvwk.some(type_vvvvvwk_SomeFunc);


	// [Interpretation 8421] set this function logic
	if (type)
	{
		jQuery('#jform_article').closest('.control-group').show();
		if (jform_vvvvvwkvwv_required)
		{
			updateFieldRequired('article',0);
			jQuery('#jform_article').prop('required','required');
			jQuery('#jform_article').attr('aria-required',true);
			jQuery('#jform_article').addClass('required');
			jform_vvvvvwkvwv_required = false;
		}

	}
	else
	{
		jQuery('#jform_article').closest('.control-group').hide();
		if (!jform_vvvvvwkvwv_required)
		{
			updateFieldRequired('article',1);
			jQuery('#jform_article').removeAttr('required');
			jQuery('#jform_article').removeAttr('aria-required');
			jQuery('#jform_article').removeClass('required');
			jform_vvvvvwkvwv_required = true;
		}
	}
}

// the vvvvvwk Some function
function type_vvvvvwk_SomeFunc(type_vvvvvwk)
{
	// [Interpretation 8408] set the function logic
	if (type_vvvvvwk == 1)
	{
		return true;
	}
	return false;
}

// the vvvvvwl function
function vvvvvwl(type_vvvvvwl)
{
	if (isSet(type_vvvvvwl) && type_vvvvvwl.constructor !== Array)
	{
		var temp_vvvvvwl = type_vvvvvwl;
		var type_vvvvvwl = [];
		type_vvvvvwl.push(temp_vvvvvwl);
	}
	else if (!isSet(type_vvvvvwl))
	{
		var type_vvvvvwl = [];
	}
	var type = type_vvvvvwl.some(type_vvvvvwl_SomeFunc);


	// [Interpretation 8421] set this function logic
	if (type)
	{
		jQuery('#jform_content-lbl').closest('.control-group').show();
		if (jform_vvvvvwlvww_required)
		{
			updateFieldRequired('content',0);
			jQuery('#jform_content').prop('required','required');
			jQuery('#jform_content').attr('aria-required',true);
			jQuery('#jform_content').addClass('required');
			jform_vvvvvwlvww_required = false;
		}

	}
	else
	{
		jQuery('#jform_content-lbl').closest('.control-group').hide();
		if (!jform_vvvvvwlvww_required)
		{
			updateFieldRequired('content',1);
			jQuery('#jform_content').removeAttr('required');
			jQuery('#jform_content').removeAttr('aria-required');
			jQuery('#jform_content').removeClass('required');
			jform_vvvvvwlvww_required = true;
		}
	}
}

// the vvvvvwl Some function
function type_vvvvvwl_SomeFunc(type_vvvvvwl)
{
	// [Interpretation 8408] set the function logic
	if (type_vvvvvwl == 2)
	{
		return true;
	}
	return false;
}

// the vvvvvwm function
function vvvvvwm(target_vvvvvwm)
{
	// [Interpretation 8443] set the function logic
	if (target_vvvvvwm == 1)
	{
		jQuery('#jform_groups').closest('.control-group').show();
		if (jform_vvvvvwmvwx_required)
		{
			updateFieldRequired('groups',0);
			jQuery('#jform_groups').prop('required','required');
			jQuery('#jform_groups').attr('aria-required',true);
			jQuery('#jform_groups').addClass('required');
			jform_vvvvvwmvwx_required = false;
		}

	}
	else
	{
		jQuery('#jform_groups').closest('.control-group').hide();
		if (!jform_vvvvvwmvwx_required)
		{
			updateFieldRequired('groups',1);
			jQuery('#jform_groups').removeAttr('required');
			jQuery('#jform_groups').removeAttr('aria-required');
			jQuery('#jform_groups').removeClass('required');
			jform_vvvvvwmvwx_required = true;
		}
	}
}

// update required fields
function updateFieldRequired(name,status)
{
	var not_required = jQuery('#jform_not_required').val();

	if(status == 1)
	{
		if (isSet(not_required) && not_required != 0)
		{
			not_required = not_required+','+name;
		}
		else
		{
			not_required = ','+name;
		}
	}
	else
	{
		if (isSet(not_required) && not_required != 0)
		{
			not_required = not_required.replace(','+name,'');
		}
	}

	jQuery('#jform_not_required').val(not_required);
}

// the isSet function
function isSet(val)
{
	if ((val != undefined) && (val != null) && 0 !== val.length){
		return true;
	}
	return false;
} 
