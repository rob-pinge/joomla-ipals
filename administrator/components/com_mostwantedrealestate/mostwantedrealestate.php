<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		2.0.0
	@build			22nd September, 2017
	@created		1st May, 2016
	@package		Most Wanted Real Estate
	@subpackage		mostwantedrealestate.php
	@author			Most Wanted Web Services, Inc. <http://mostwantedrealestatesites.com>	
	@copyright		Copyright (C) 2015-2017. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
	
	Most Wanted Real Estate Component
	
/------------------------------------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_mostwantedrealestate'))
{
	return JError::raiseWaring(404, JText::_('JERROR_ALERTNOAUTHOR'));
};

// Load cms libraries
JLoader::registerPrefix('J', JPATH_PLATFORM . '/cms');
// Load joomla libraries without overwrite
JLoader::registerPrefix('J', JPATH_PLATFORM . '/joomla',false);

// Add CSS file for all pages
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_mostwantedrealestate/assets/css/admin.css');
$document->addScript('components/com_mostwantedrealestate/assets/js/admin.js');

// require helper files
JLoader::register('MostwantedrealestateHelper', dirname(__FILE__) . '/helpers/mostwantedrealestate.php'); 
JLoader::register('MostwantedrealestateEmail', JPATH_COMPONENT_ADMINISTRATOR . '/helpers/mostwantedrealestateemail.php'); 
JLoader::register('JHtmlBatch_', dirname(__FILE__) . '/helpers/html/batch_.php'); 

// import joomla controller library
jimport('joomla.application.component.controller');

// Get an instance of the controller prefixed by Mostwantedrealestate
$controller = JControllerLegacy::getInstance('Mostwantedrealestate');

// Perform the Request task
$controller->execute(JFactory::getApplication()->input->get('task'));

// Redirect if set by the controller
$controller->redirect();
