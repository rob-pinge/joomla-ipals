<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		2.0.0
	@build			22nd September, 2017
	@created		1st May, 2016
	@package		Most Wanted Real Estate
	@subpackage		agencies.php
	@author			Most Wanted Web Services, Inc. <http://mostwantedrealestatesites.com>	
	@copyright		Copyright (C) 2015-2017. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
	
	Most Wanted Real Estate Component
	
/------------------------------------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla controlleradmin library
jimport('joomla.application.component.controlleradmin');

/**
 * Agencies Controller
 */
class MostwantedrealestateControllerAgencies extends JControllerAdmin
{
	protected $text_prefix = 'COM_MOSTWANTEDREALESTATE_AGENCIES';
	/**
	 * Proxy for getModel.
	 * @since	2.5
	 */
	public function getModel($name = 'Agency', $prefix = 'MostwantedrealestateModel', $config = array())
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));
		
		return $model;
	}

	public function exportData()
	{
		// [Interpretation 7719] Check for request forgeries
		JSession::checkToken() or die(JText::_('JINVALID_TOKEN'));
		// [Interpretation 7721] check if export is allowed for this user.
		$user = JFactory::getUser();
		if ($user->authorise('agency.export', 'com_mostwantedrealestate') && $user->authorise('core.export', 'com_mostwantedrealestate'))
		{
			// [Interpretation 7725] Get the input
			$input = JFactory::getApplication()->input;
			$pks = $input->post->get('cid', array(), 'array');
			// [Interpretation 7728] Sanitize the input
			JArrayHelper::toInteger($pks);
			// [Interpretation 7730] Get the model
			$model = $this->getModel('Agencies');
			// [Interpretation 7732] get the data to export
			$data = $model->getExportData($pks);
			if (MostwantedrealestateHelper::checkArray($data))
			{
				// [Interpretation 7736] now set the data to the spreadsheet
				$date = JFactory::getDate();
				MostwantedrealestateHelper::xls($data,'Agencies_'.$date->format('jS_F_Y'),'Agencies exported ('.$date->format('jS F, Y').')','agencies');
			}
		}
		// [Interpretation 7741] Redirect to the list screen with error.
		$message = JText::_('COM_MOSTWANTEDREALESTATE_EXPORT_FAILED');
		$this->setRedirect(JRoute::_('index.php?option=com_mostwantedrealestate&view=agencies', false), $message, 'error');
		return;
	}


	public function importData()
	{
		// [Interpretation 7750] Check for request forgeries
		JSession::checkToken() or die(JText::_('JINVALID_TOKEN'));
		// [Interpretation 7752] check if import is allowed for this user.
		$user = JFactory::getUser();
		if ($user->authorise('agency.import', 'com_mostwantedrealestate') && $user->authorise('core.import', 'com_mostwantedrealestate'))
		{
			// [Interpretation 7756] Get the import model
			$model = $this->getModel('Agencies');
			// [Interpretation 7758] get the headers to import
			$headers = $model->getExImPortHeaders();
			if (MostwantedrealestateHelper::checkObject($headers))
			{
				// [Interpretation 7762] Load headers to session.
				$session = JFactory::getSession();
				$headers = json_encode($headers);
				$session->set('agency_VDM_IMPORTHEADERS', $headers);
				$session->set('backto_VDM_IMPORT', 'agencies');
				$session->set('dataType_VDM_IMPORTINTO', 'agency');
				// [Interpretation 7768] Redirect to import view.
				$message = JText::_('COM_MOSTWANTEDREALESTATE_IMPORT_SELECT_FILE_FOR_AGENCIES');
				$this->setRedirect(JRoute::_('index.php?option=com_mostwantedrealestate&view=import', false), $message);
				return;
			}
		}
		// [Interpretation 7788] Redirect to the list screen with error.
		$message = JText::_('COM_MOSTWANTEDREALESTATE_IMPORT_FAILED');
		$this->setRedirect(JRoute::_('index.php?option=com_mostwantedrealestate&view=agencies', false), $message, 'error');
		return;
	}  
}
