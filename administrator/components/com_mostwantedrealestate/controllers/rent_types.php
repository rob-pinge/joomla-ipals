<?php
/*----------------------------------------------------------------------------------|  www.vdm.io  |----/
				Most Wanted Web Services, Inc. 
/-------------------------------------------------------------------------------------------------------/

	@version		2.0.0
	@build			22nd September, 2017
	@created		1st May, 2016
	@package		Most Wanted Real Estate
	@subpackage		rent_types.php
	@author			Most Wanted Web Services, Inc. <http://mostwantedrealestatesites.com>	
	@copyright		Copyright (C) 2015-2017. All Rights Reserved
	@license		GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html
	
	Most Wanted Real Estate Component
	
/------------------------------------------------------------------------------------------------------*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla controlleradmin library
jimport('joomla.application.component.controlleradmin');

/**
 * Rent_types Controller
 */
class MostwantedrealestateControllerRent_types extends JControllerAdmin
{
	protected $text_prefix = 'COM_MOSTWANTEDREALESTATE_RENT_TYPES';
	/**
	 * Proxy for getModel.
	 * @since	2.5
	 */
	public function getModel($name = 'Rent_type', $prefix = 'MostwantedrealestateModel', $config = array())
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));
		
		return $model;
	}

	public function exportData()
	{
		// [Interpretation 7719] Check for request forgeries
		JSession::checkToken() or die(JText::_('JINVALID_TOKEN'));
		// [Interpretation 7721] check if export is allowed for this user.
		$user = JFactory::getUser();
		if ($user->authorise('rent_type.export', 'com_mostwantedrealestate') && $user->authorise('core.export', 'com_mostwantedrealestate'))
		{
			// [Interpretation 7725] Get the input
			$input = JFactory::getApplication()->input;
			$pks = $input->post->get('cid', array(), 'array');
			// [Interpretation 7728] Sanitize the input
			JArrayHelper::toInteger($pks);
			// [Interpretation 7730] Get the model
			$model = $this->getModel('Rent_types');
			// [Interpretation 7732] get the data to export
			$data = $model->getExportData($pks);
			if (MostwantedrealestateHelper::checkArray($data))
			{
				// [Interpretation 7736] now set the data to the spreadsheet
				$date = JFactory::getDate();
				MostwantedrealestateHelper::xls($data,'Rent_types_'.$date->format('jS_F_Y'),'Rent types exported ('.$date->format('jS F, Y').')','rent types');
			}
		}
		// [Interpretation 7741] Redirect to the list screen with error.
		$message = JText::_('COM_MOSTWANTEDREALESTATE_EXPORT_FAILED');
		$this->setRedirect(JRoute::_('index.php?option=com_mostwantedrealestate&view=rent_types', false), $message, 'error');
		return;
	}


	public function importData()
	{
		// [Interpretation 7750] Check for request forgeries
		JSession::checkToken() or die(JText::_('JINVALID_TOKEN'));
		// [Interpretation 7752] check if import is allowed for this user.
		$user = JFactory::getUser();
		if ($user->authorise('rent_type.import', 'com_mostwantedrealestate') && $user->authorise('core.import', 'com_mostwantedrealestate'))
		{
			// [Interpretation 7756] Get the import model
			$model = $this->getModel('Rent_types');
			// [Interpretation 7758] get the headers to import
			$headers = $model->getExImPortHeaders();
			if (MostwantedrealestateHelper::checkObject($headers))
			{
				// [Interpretation 7762] Load headers to session.
				$session = JFactory::getSession();
				$headers = json_encode($headers);
				$session->set('rent_type_VDM_IMPORTHEADERS', $headers);
				$session->set('backto_VDM_IMPORT', 'rent_types');
				$session->set('dataType_VDM_IMPORTINTO', 'rent_type');
				// [Interpretation 7768] Redirect to import view.
				$message = JText::_('COM_MOSTWANTEDREALESTATE_IMPORT_SELECT_FILE_FOR_RENT_TYPES');
				$this->setRedirect(JRoute::_('index.php?option=com_mostwantedrealestate&view=import', false), $message);
				return;
			}
		}
		// [Interpretation 7788] Redirect to the list screen with error.
		$message = JText::_('COM_MOSTWANTEDREALESTATE_IMPORT_FAILED');
		$this->setRedirect(JRoute::_('index.php?option=com_mostwantedrealestate&view=rent_types', false), $message, 'error');
		return;
	}  
}
