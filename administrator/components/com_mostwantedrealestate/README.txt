# Most Wanted Real Estate (2.0.0)

 ![Most Wanted Real Estate image] (https://mostwantedrealestatesites.com/images/MWR_3D_Box-right.png "The Most Wanted Real Estate")

Simple yet beautiful.  Our Real Estate component is multi-functional and will work with RETS or as a stand-alone.  The user-friendly interface allows for multi-choice features, multiple image galleries, drag and drop image uploads, open houses, featured listings, a watchlist for favorites, multiple categories, agencies, and so much more.  And of course it’s designed for Joomla! only and is responsive.  Choose from Bing or Google maps.  The front-end management makes it easy for agents to add and update their profiles and their listings easily and efficiently.

The best way to see all your options is to install this component on you Joomla website and test all features yourself.

# Build Details

+ *Company*: [Most Wanted Web Services, Inc.] (http://mostwantedrealestatesites.com)
+ *Author*: [Most Wanted Web Services, Inc.] (mailto:sales@mwweb.host)
+ *Name*: [Most Wanted Real Estate] (http://mostwantedrealestatesites.com)
+ *First Build*: 1st May, 2016
+ *Last Build*: 22nd September, 2017
+ *Version*: 2.0.0
+ *Copyright*: Copyright (C) 2015-2017. All Rights Reserved
+ *License*: GNU/GPL Version 2 or later - http://www.gnu.org/licenses/gpl-2.0.html

## Build Time :hourglass:

**322 Hours** or **40 Eight Hour Days** (actual time the author saved -
due to [Automated Component Builder] (https://www.vdm.io/joomla-component-builder))

> (if creating a folder and file took **5 seconds** and writing one line of code took **10 seconds**,
> never making one mistake or taking any coffee break.)

+ *Line count*: **115524**
+ *File count*: **818**
+ *Folder count*: **140**

**213 Hours** or **27 Eight Hour Days** (the actual time the author spent)

> (with the following break down:
> **debugging @81hours** = codingtime / 4;
> **planning @46hours** = codingtime / 7;
> **mapping @32hours** = codingtime / 10;
> **office @54hours** = codingtime / 6;)

**535 Hours** or **67 Eight Hour Days**
(a total of the realistic time frame for this project)

> (if creating a folder and file took **5 seconds** and writing one line of code took **10 seconds**,
> with the normal everyday realities at the office, that includes the component planning, mapping & debugging.)

Project duration: **13.4 weeks** or **2.8 months**

> This **component** was built with a Joomla [Automated Component Builder] (https://www.vdm.io/joomla-component-builder).
> Developed by [Llewellyn van der Merwe] (mailto:joomla@vdm.io)