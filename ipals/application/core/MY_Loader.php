<?php
class MY_Loader extends CI_Loader {
	
	public function plugin_library($library, $plugin, $params = NULL, $object_name = NULL)
	{
		if (is_array($library))
		{
			foreach ($library as $class)
			{
				$this->plugin_library($class, $plugin, $params);
			}

			return;
		}
		$library = str_replace('.php', '', strtolower($library));
		
		// always need the core class
		$this->library('core/' . $library, $params, $object_name);
		
		// if it ends in a number, then its a minor variant that builds onto original
		if(preg_match('/[0-9]$/', $plugin))
		{
			$basePlugin = preg_replace('/[0-9]+$/', '', $plugin);
			if(file_exists(APPPATH . 'libraries/' . $basePlugin . '/' . $basePlugin . '_' .  $library . '.php'))
			{
				$this->library($basePlugin . '/' . $basePlugin . '_' .  $library, $params, $library);
			}
		}
		
		// lastly, load the most specific class if it exists
		if(file_exists(APPPATH . 'libraries/' . $plugin . '/' . $plugin . '_' . $library . '.php'))
		{
			$this->library($plugin . '/' . $plugin . '_' .  $library, $params, $library);
		}		
	}
}
