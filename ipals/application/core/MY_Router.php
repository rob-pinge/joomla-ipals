<?php
class MY_Router extends CI_Router {

	function _set_request ($seg = array())
	{
		// The str_replace() below goes through all our segments
		// and replaces the hyphens with underscores making it
		// possible to use hyphens in controllers, folder names and
		// function names
		if(isset($seg[0])) $seg[0] = str_replace('-', '_', $seg[0]);
		if(isset($seg[1])) $seg[1] = str_replace('-', '_', $seg[1]);
		parent::_set_request($seg);
	}

}
