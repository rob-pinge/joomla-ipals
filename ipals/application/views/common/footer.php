            <span>Copyright 2011-<?php echo date('Y') ?> <a href="http://retsforealestate.com" target="_blank">Most Wanted Web Services, Inc.</a></span>
            <p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds</p>
        </div> <!-- .container -->
    </div> <!-- .wrapper -->
    
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="<?=base_url()?>js/libs/jquery-1.7.1.min.js"><\/script>')</script>
    <script src="<?=base_url()?>js/libs/bootstrap.min.js"></script>

    <script src="<?=base_url()?>js/plugins.js"></script>
    <script src="<?=base_url()?>js/script.js"></script>
    
</body>
</html>