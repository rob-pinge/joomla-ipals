<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	
	<title>iPALS - Version 2.3.1</title>
	
	<link href="<?=base_url()?>css/bootstrap.min.css" rel="stylesheet">
	<link href="<?=base_url()?>css/style.css" rel="stylesheet">
	
    <script src="<?=base_url()?>js/libs/modernizr-2.5.3.min.js"></script>
</head>
<body>
	<!-- base url populated by PHP, used in javascript -->
	<div id="base-url" class="hide"><?=base_url()?></div>
	<div class="navbar navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<span class="brand">iPALS 2.3.1</span>
				<div class="nav-collapse">
					<ul class="nav">
						<li class="<?= $this->uri->rsegment(2) == 'map' ? 'active' : '' ?>">
							<a href="<?=site_url('admin/map')?>">Mapping</a>
						</li>
						<li class="<?= $this->uri->rsegment(2) == 'settings' ? 'active' : '' ?>">
							<a href="<?=site_url('admin/settings')?>">Settings</a>
						</li>
						<li class="<?= $this->uri->rsegment(2) == 'query_fields' ? 'active' : '' ?>">
							<a href="<?=site_url('admin/query-fields')?>">Query Fields</a>
						</li>
						<li class="divider-vertical"></li>
						<li>
							<a href="<?=site_url('/')?>" target="_blank">Run Manually</a>
						</li>
						<li>
							<a href="<?=site_url('cleanup')?>" target="_blank">Clean Up</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="wrapper">
		<div class="container body">
