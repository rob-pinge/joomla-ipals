<?php $this->load->view('common/header') ?>
			<form id="settings-form" method="POST">
				<fieldset>
					<legend><h2>Settings <a href="#" id="show-all-settings" class="btn btn-mini">Show All</a> <a href="#" id="hide-all-settings" class="btn btn-mini">Hide All</a> </h2></legend><br>
					<?php foreach($settings as $category => $s): ?>
						<h1 class="settings-toggle"><?=$category?></h1>
						<div class="category-group <?=str_replace(' ', '-', strtolower($category))?> hide">
							<div class="row">
								<?php foreach ($s as $i => $setting): ?>
									<?php //if($i % 3 == 0) echo '<div class="row">'; ?>
								<div class="span4">
									<div class="control-group">
										<label class="control-label" for="<?=$setting['key']?>"><?=$setting['display']?> <?=$setting['description']?></label>
										<div class="controls">
											<?=$setting['input']?>
										</div>
									</div>
								</div>
								<?php //if($i % 3 == 2) echo '</div>'; ?>
							
							<?php endforeach ?>
						
							<?php //if($i % 3 != 2) echo '</div>'; ?>
							</div>
						</div>
					<?php endforeach ?>
					<div class="form-actions">
						<button type="submit" class="btn btn-primary">Save</button>
					</div>

				</fieldset>
			</form>
<?php $this->load->view('common/footer') ?>