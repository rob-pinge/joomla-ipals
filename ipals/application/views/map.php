<?php $this->load->view('common/header') ?>			
	<p class="pull-right">
		<button class="edit-all btn btn-infoNO">Edit All</button> 
		<button class="save-all btn btn-successNO">Save All</button> 
		<button data-toggle="modal" data-target="#add-new-modal" class="add-new-field btn-warning btn">Add New Field</button>
		
		<div class="form-inline">
			<label>Filter by: </label>
			<select id="map-filter">
				<option value="none">none</option>
				<?=$type_options?>
			</select>
		</div>
	</p>

	<p id="fields-table">
		<?=$table?>	
	</p>
	
	<p>
		<?php if ($no_rows): ?>
			<div class="hero-unit">
				<h1>No Rows?</h1>
				<p>Click the button to get help you get started!</p>
				<p>
					<form method="POST">
						<button type="submit" name="submit" value="get_rows" class="btn btn-primary btn-large">
							Click Me!
						</button>
					</form>
				</p>
			</div>
		<?php endif ?>
	</p>

	<div class="group">
		<div class="pull-right group">
			<button class="edit-all btn btn-infoNO">Edit All</button> 
			<button class="save-all btn btn-successNO">Save All</button> 
			<button data-toggle="modal" data-target="#add-new-modal" class="add-new-field btn-warning btn">Add New Field</button>
		</div>		
	</div>

	<div class="modal hide fade in" id="add-new-modal">
		<div class="modal-header">
			<a class="close" data-dismiss="modal">&times;</a>
			<h3>Use this form to add another field</h3>
		</div>

		<form class="form-horizontal nm" id="add-new-form" method="POST">
			<div class="modal-body">

				<div class="alert alert-error hide" id="form-error">
					<a class="close">&times;</a>
					<strong>Error!</strong> <span class="text"></span>
				</div>

				<div class="control-group">
					<label class="control-label" for="type-options">
						Type 
						<i rel="tooltip" data-title="Resource type that field applies to" class="icon-info-sign"></i>
					</label>
					<div class="controls">
						<select name="type" id="type-options">
							<?php echo $type_options ?>
						</select>
					</div>
				</div>

				<div class="control-group">
					<label class="control-label" for="action-options">
						Action 
						<i rel="tooltip" data-title="Action to perform before assigning" class="icon-info-sign"></i>
					</label>
					<div class="controls">
						<select name="action" class="add-action" id="action-options">
							<?php echo $action_options ?>
						</select>
					</div>
				</div>

				<div class="control-group">
					<label class="control-label" for="field">
						Field 
						<i rel="tooltip" data-title="Field name in database" class="icon-info-sign"></i>
					</label>
					<div class="controls" class="key">
						<input type="text" class="" name="field" id="field" value="">
					</div>
				</div>

				<div class="control-group">
					<label class="control-label" for="key">
						Key 
						<i rel="tooltip" data-title="Key in RETS data array" class="icon-info-sign"></i>
					</label>
					<div class="controls">
						<input type="text" class="" name="key" id="key" value=""> <a class="btn hide edit-special-field">Edit</a>
					</div>
				</div>

				<div class="control-group">
					<label class="control-label" for="description">
						Description 
						<i rel="tooltip" data-title="Reminder of what this field means" class="icon-info-sign"></i>
					</label>
					<div class="controls">
						<input type="text" class="" name="description" id="description" value="">
					</div>
				</div>

			</div>

			<div class="modal-footer">
				<button data-dismiss="modal" data-loading-text="Cancel" class="cancel-new btn">Cancel</button>
				<button type="submit" class="btn btn-primary">Save</button>
			</div>

		</form>
	</div>


	<div class="modal hide fade in" id="cross-modal">

		<div class="modal-header">
			<h3>Use this form to set up a cross-table populated field</h3>
		</div>

		<form class="form-horizontal nm" id="add-new-cross" method="POST">

			<div class="modal-body">
				<div class="alert alert-error hide" id="cross-form-error">
					<a class="close">&times;</a>
					<strong>Error!</strong> <span class="text"></span>
				</div>

				<div class="control-group">
					<label class="control-label" for="key2">
						Key 
						<i rel="tooltip" data-title="Key in RETS data array" class="icon-info-sign"></i>
					</label>
					<div class="controls">
						<input type="text" class="" name="key" id="key2" value="">
					</div>
				</div>

				<div class="control-group">
					<label class="control-label" for="table2">
						Table 
						<i rel="tooltip" data-title="Table to get data from" class="icon-info-sign"></i>
					</label>
					<div class="controls">
						<input type="text" class="" name="table" id="table2" value="">
					</div>
				</div>

				<div class="control-group">
					<label class="control-label" for="source2">
						Source 
						<i rel="tooltip" data-title="Table field that matches RETS data" class="icon-info-sign"></i>
					</label>
					<div class="controls">
						<input type="text" class="" name="source" id="source2" value="">
					</div>
				</div>

				<div class="control-group">
					<label class="control-label" for="destination2">
						Destination 
						<i rel="tooltip" data-title="Table field that we want (usually `id`)" class="icon-info-sign"></i>
					</label>
					<div class="controls">
						<input type="text" class="" name="destination" id="destination2" value="">
					</div>
				</div>

				<div class="control-group">
					<label class="control-label" for="add_if_not_exists2">
						Add if not exist? 
						<i rel="tooltip" data-title="Add the field in the database if it doesn't exist" class="icon-info-sign"></i>
					</label>
					<div class="controls">
						<input type="checkbox" class="" name="add_if_not_exists" id="add_if_not_exists2" value="1">
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<input type="hidden" name="id" value="">
				<!-- <button data-dismiss="modal" data-loading-text="Cancel" class="cancel-modal btn">Cancel</button> -->
				<button data-loading-text="Saving..." id="cross-save" class="btn btn-primary">Save</button>
			</div>

		</form>
	</div>


	<div class="modal hide fade in" id="conditional-modal">

		<div class="modal-header">
			<h3>Use this form to set up a conditonal passthru</h3>
		</div>

		<form class="form-horizontal nm" id="add-new-conditional" method="POST">

			<div class="modal-body">
				<div class="alert alert-error hide" id="conditional-form-error">
					<a class="close">&times;</a>
					<strong>Error!</strong> <span class="text"></span>
				</div>

				<div class="alert">
					<a class="close">&times;</a>
					<strong>Please note:</strong> 
					<span class="text">You can use asterisk (*) in the left column to have it set that as default.<br>
						Also, blank rows will automatically be disgarded.
					</span>
				</div>


				<div class="control-group">
					<label class="control-label" for="key2">
						Key 
						<i rel="tooltip" data-title="Key in RETS data array" class="icon-info-sign"></i>
					</label>
					<div class="controls">
						<input type="text" class="" name="key" id="key2" value="">
					</div>
				</div>

				<div class="row-fluid">
					<div class="span6">
						If RETS key has this value...
					</div>
					<div class="span6">
						...set field to this value.
					</div>
				</div>

				<div id="conditional-row">
					<div class="conditonal-clone row-fluid mb10">
						<div class="span6">
							<input type="text" class="conditional-key" name="ckeys[]">
						</div>
						<div class="span6">
							<input type="text" class="conditional-value" name="cvalues[]">
						</div>
					</div>
				</div>


			</div>

			<div class="modal-footer">
				<input type="hidden" name="id" value="">
				<!-- <button data-dismiss="modal" data-loading-text="Cancel" class="cancel-modal btn">Cancel</button> -->
				<button data-loading-text="Saving..." id="conditional-save" class="btn btn-primary">Save</button>
			</div>

		</form>
	</div>

	<div class="modal hide fade in" id="sum-modal">
		<div class="modal-header">
			<a class="close" data-dismiss="modal">&times;</a>
			<h3>Are you sure you want to delete this?</h3>
		</div>
		<div class="modal-body">
			<p>There is no undoing this action. Once you hit delete, it will be gone forever.</p>
		</div>
		<div class="modal-footer">
			<button data-dismiss="modal" data-loading-text="Cancel" class="cancel-modal btn">Cancel</button>
			<button data-loading-text="Deleting..." class="btn confirm-delete btn-danger">Delete</button>
		</div>
	</div>



	<div class="modal hide fade in" id="delete-modal">
		<div class="modal-header">
			<a class="close" data-dismiss="modal">&times;</a>
			<h3>Are you sure you want to delete this?</h3>
		</div>
		<div class="modal-body">
			<p>There is no undoing this action. Once you hit delete, it will be gone forever.</p>
		</div>
		<div class="modal-footer">
			<button data-dismiss="modal" data-loading-text="Cancel" class="cancel-modal btn">Cancel</button>
			<button data-loading-text="Deleting..." class="btn confirm-delete btn-danger">Delete</button>
		</div>
	</div>


<!--	<div class="hide">
		<select name="type" id="type-options">
			<?php echo $type_options ?>
		</select>

		<select name="action" id="action-options">
			<?php echo $action_options ?>
		</select>
	</div> -->
<?php $this->load->view('common/footer') ?>