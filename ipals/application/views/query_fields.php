<?php $this->load->view('common/header') ?>
			<fieldset>
				<form method="POST">
					<legend>Setup Fields</legend>
					<?php /*
					<div class="row">
						<div class="span12">
							<div class="control-group">
								<label class="control-label" for="fields">Fields to Map</label>
								<div class="controls">
									<textarea class="" rows="8" name="fields" id="fields"></textarea>
									<p class="help-block">One field per line</p>
								</div>
							</div>

							<div class="control-group well form-inline">
								<label class="control-label" for="table_name">Import Fields</label>
								<div class="controls">
									<input type="text" class="" name="table_name" id="table_name"> 
									<button id="import_fields" class="btn btn-info">Import</button>
									<p class="help-block">Provide a table name to import fields from.</p>
								</div>
							</div>
						</div>
					</div> 
					*/ ?>
				
					<?php if(!empty($queryFields['manual'])): ?>
					<p><strong>Required Fields</strong></p>
					<div class="row">
						<div class="span12">
							<?php foreach($queryFields['manual'] as $qf): extract($qf); ?>
							<div class="control-group">
								<div class="control-group form-horizontal">
									<label class="control-label" for="<?=$key?>"><?=$name?></label>
									<div class="controls">
										<input type="text" class="" name="<?=$key?>" id="<?=$key?>" value="<?=$value?>"> 
										<?=$description?>
									</div>
								</div>
							</div>
							<?php endforeach ?>
						</div>
					</div> <!-- .row -->
					<?php endif ?>
				
					<div class="row">
						<div class="span12">
							<div class="control-group">
								<label class="control-label"><strong>Add Parameters</strong></label>
								<?php foreach($queryFields['auto'] as $qf): extract($qf); ?>
								<div class="controls bb add-parameter form-inline mb10">
									<input type="text" class="add-parameter-key" placeholder="key" name="param[key][]" value="<?=$key?>">
									<input type="text" class="add-parameter-value" placeholder="value" name="param[value][]" value="<?=$value?>">
									<input type="text" class="add-parameter-group" placeholder="group (optional)" name="param[group][]" value="<?=$group?>">
									<select class="add-parameter-type" name="param[type][]">
										<option value="property" <?=$type == 'property' ? 'selected="selected"' : ''?>>property</option>
										<option value="office" <?=$type == 'office' ? 'selected="selected"' : ''?>>office</option>
										<option value="agent" <?=$type == 'agent' ? 'selected="selected"' : ''?>>agent</option>
									</select> 
									<a href="#" class="btn query-field-remove-row btn-danger"><i class="icon-remove icon-white"></i> Remove</a>
								</div>
								<?php endforeach; ?>
							
								<!-- always have one blank row at the end -->
								<div class="controls bb add-parameter">
									<input type="text" class="add-parameter-key" placeholder="key" name="param[key][]">
									<input type="text" class="add-parameter-value" placeholder="value" name="param[value][]">
									<input type="text" class="add-parameter-group" placeholder="group (optional)" name="param[group][]" value="<?=$group?>">
									<select class="add-parameter-type" name="param[type][]">
										<option value="property">property</option>
										<option value="office">office</option>
										<option value="agent">agent</option>
									</select>
								</div>
							</div>
						</div>
					</div> <!-- .row -->
					
					<div class="form-actions">
						<button type="submit" class="btn btn-primary">Save</button>
					</div>
				</form>
			</fieldset>
<?php $this->load->view('common/footer') ?>