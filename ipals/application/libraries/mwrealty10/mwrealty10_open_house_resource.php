<?php 
class Mwrealty10_Open_house_resource extends Mwrealty1_Open_house_resource
{	
	public function preInsertProcess(&$row)
	{
		parent::preInsertProcess($row);

		// convert date to unix timestamp, a breaking change introduced in EZ 7.2
		$this->data['lastupdate'] = strtotime($this->data['lastupdate']);
	}
}