<?php 
class Mwrealty10_agent_resource extends Mwrealty1_agent_resource
{	
	public function preInsertProcess(&$row)
	{
		parent::preInsertProcess($row);

		// convert date to unix timestamp, a breaking change introduced in EZ 7.2
		$this->data['lastupdate'] = strtotime($this->data['lastupdate']);
	}
}