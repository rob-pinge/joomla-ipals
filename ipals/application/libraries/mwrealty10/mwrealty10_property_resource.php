<?php 
/*
 * Plugin for Most Wanted Real Estate 1.0
 */

 class Mwrealty10_property_resource extends Mwrealty1_property_resource
{	
	public function preInsertProcess(&$row)
	{
		parent::preInsertProcess($row);

		// convert date to unix timestamp, a breaking change introduced in MW 1.0
		$this->data['lastupdate'] = strtotime($this->data['lastupdate']);
	}
}