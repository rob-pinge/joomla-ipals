<?php 
class Iproperty_property_resource extends Property_resource
{	
	protected $data;
	protected $retsQuery = array();
	
	protected $type = 'property';
	
	protected function delete($id)
	{
		$deletions = array(
			$this->settings->agentMidTable    => 'prop_id',
			$this->settings->propertyTable    => 'id',
			$this->settings->propertyMidTable => 'prop_id',
			$this->settings->imageTable       => 'propid'
		);
		
		$imgResult = $this->db->where('propid', $id)->get($this->settings->imageTable)->result();
		
		foreach($imgResult as $imgRow) 
		{
			$image = $imgRow->fname . '.jpg';
			if($this->settings->amazonS3)
			{
				$this->S3->deleteObject($this->settings->amazonBucket, $image);
			}
			else
			{
				@unlink($this->settings->imagePath . '/' . $image);
			}
		}
		
		foreach($deletions as $table => $field) 
		{
			$this->db->where($field, $id)->delete($table);
		}
	}
	
	public function preInsertProcess(&$row)
	{
		if($this->class == @$this->settings->propertyClasses['lease']) $this->data['stype'] = 4;
		if($this->class == @$this->settings->propertyClasses['rental']) $this->data['stype'] = 2;
		else $this->data['stype'] = 1; // else residential
		
		parent::preInsertProcess($row);
	}
	
	protected function prepFields($data)
	{
		// already formatted for iProperty
		return $data;
	}
		
	public function postInsertProcess(&$row)
	{		
		$this->db->where('prop_id', $this->insertID)->delete($this->settings->propertyMidTable);
		$this->db->where('prop_id', $this->insertID)->delete($this->settings->agentMidTable);
		
		// TODO: unify categories with types
		switch($this->subType)
		{
			case 'residential': $propertyType = 1; break;
			case 'land': $propertyType = 2; break;
			case 'commercial': $propertyType = 3; break;
			case 'multi-family': $propertyType = 4; break;
			case 'custom-1': $propertyType = 5; break;
			case 'custom-2': $propertyType = 6; break;
			case 'custom-3': $propertyType = 7; break;
			case 'custom-4': $propertyType = 8; break;
			case 'custom-5': $propertyType = 9; break;
			case 'custom-6': $propertyType = 10; break;
			default: $propertyType = 0; break;
		}
		
		$data = array(
			'prop_id'	=> $this->insertID,
			'cat_id'	=> $propertyType,
			$this->settings->retsID	=> $this->data[$this->settings->retsID],
		);
		$this->db->insert($this->settings->propertyMidTable, $data);		
		
		if(!@$this->settings->idx)
		{
			$agentID = isset($this->agent_resource->existing[$row[$this->settings->agentID]]) 
				? $this->agent_resource->existing[$row[$this->settings->agentID]] 
				: $this->settings->defaultAgent;
				
			$data = array(
				'prop_id'	=> $this->insertID,
				'agent_id'	=> $agentID,
				$this->settings->retsID	=> $this->data[$this->settings->retsID],
			);
			$this->db->insert($this->settings->agentMidTable, $data);
		}
		
		if(empty($this->data['listing_office']) && !empty($this->settings->defaultAgent))
		{
			$this->data['listing_office'] = $this->settings->defaultAgent;
		}
		
		parent::postInsertProcess($row);
	}
}