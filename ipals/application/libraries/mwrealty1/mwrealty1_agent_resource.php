<?php 
class Mwrealty1_agent_resource extends Agent_resource
{	
	public function preInsertProcess(&$row)
	{		
		$this->data['alias'] = url_title($this->data['seller_name'], 'dash', true);
		$this->data['alias'] = str_replace('.', '', $this->data['alias']);
		parent::postInsertProcess($row);
	}

	public function postInsertProcess()
	{
		$this->getGeocode();
	}

	protected function getGeocode()
	{
		if(!$this->settings->processGeocode) return;
		$address = $this->parser->parse_string($this->settings->agentGeocodeAddress, $this->data, true);
		if($this->settings->proGeocode) $success = $this->proGeocode($address);
		else $success = $this->basicGeocode($address);

		if($this->settings->verboseLevel >= 2 && $success) echo "[{$this->insertID}] Saved Geocode information<br>"; 
	}

	protected function prepFields($data)
	{		
		$return = array(
			'seller_declat'  => $data['latitude'],
			'seller_declong' => $data['longitude']
		);

		return $return;
	}
}