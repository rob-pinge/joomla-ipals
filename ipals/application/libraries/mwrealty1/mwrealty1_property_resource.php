<?php 
/*
 * Plugin for Most Wanted Real Estate 1
 */

 class Mwrealty1_property_resource extends Mwrealty_property_resource
{	
	protected $data;
	protected $retsQuery = array();
	
	protected $type = 'property';
		
	protected $agent_info_key = 'mls_agent';
	public function preInsertProcess(&$row)
	{
		parent::preInsertProcess($row);

		// convert commas to semi-colons for mw1
		$features = array('indoorfeatures', 'outdoorfeatures', 'buildingfeatures', 'communityfeatures', 'otherfeatures');
		foreach($features as $feature)
		{
			if($this->data[$feature]) $this->data[$feature] = str_replace(',', ';', $this->data[$feature]);			
		}
	}
		
	public function postInsertProcess(&$row)
	{		
		parent::postInsertProcess($row);

		$data = array(
			'property_id' => $this->insertID,
			'category_id' => $this->data['cid']
		);

		// yes, this is how I'm chosing to do this... I don't want to delete user-set values and I don't want duplicates
		if($data['category_id']) 
		{
			$this->db->where($data)->delete($this->settings->propertyMidTable);
			$this->db->insert($this->settings->propertyMidTable, $data);			
		}

		foreach ($this->categories as $category) 
		{
			$searches = explode('|', $category['search']);
			foreach ($searches as $search) 
			{
				if (stristr($category['field'], $search))
				{
					$data = array(
						'property_id' => $this->insertID,
						'category_id' => $category['cat']
					);
					$this->db->where($data)->delete($this->settings->propertyMidTable);
					$this->db->insert($this->settings->propertyMidTable, $data);			
					break;
				}

			}
		}
		// reset categories
		$this->categories = array();
	}
		
	// locality maintenance functions
	protected function _update_locality($locality, $stid)
	{
		$table = $this->db->dbprefix('mostwantedrealestate_locality');
		
		// determine max sort order
		$result = $this->db->query("SELECT MAX(ordering) as max FROM {$table}");
		$row = $result->row_array();
		$max = $row['max'];

		$coords = $this->getGeocode($locality, true);
		
		$fields = array(
			'stateid'   => $stid,
			'mwcity'    => $locality,
			'alias'     => url_title($locality, 'dash', true),
			'published' => 1,
			'declat'    => @$coords['latitude']  ? $coords['latitude']  : '',
			'declong'   => @$coords['longitude'] ? $coords['longitude'] : '',
			'ordering'  => ($max + 1),
		);
		$this->db->insert($table, $fields);
		
		// make sure our data is up to date
		$this->_localities = $this->_get_localities();
	}
	
	protected function _update_state($state, $cnid)
	{
		$table = $this->db->dbprefix('mostwantedrealestate_state');
		
		// determine max sort order
		$result = $this->db->query("SELECT MAX(ordering) as max FROM {$table}");
		$row = $result->row_array();
		$max = $row['max'];

		$coords = $this->getGeocode($state, true);
		
		$fields = array(
			'countid'   => $cnid,
			'name'      => $state,
			'alias'     => url_title($state, 'dash', true),
			'published' => 1,
			'declat'    => @$coords['latitude']  ? $coords['latitude']  : '',
			'declong'   => @$coords['longitude'] ? $coords['longitude'] : '',
			'ordering'  => ($max + 1),
		);
		$this->db->insert($table, $fields);
	
		// make sure our data is up to date
		$this->_states = $this->_get_states();
	}
	
	protected function _update_country($country)
	{
		$table = $this->db->dbprefix('mostwantedrealestate_country');
		
		// determine max sort order
		$result = $this->db->query("SELECT MAX(ordering) as max FROM {$table}");
		$row = $result->row_array();
		$max = $row['max'];
		
		$fields = array(
			'name'    => $country,
			'alias'    => url_title($country, 'dash', true),
			'published' => 1,
			'ordering'  => ($max + 1),
		);
		$this->db->insert($table, $fields);
		
		// make sure our data is up to date
		$this->_countries = $this->_get_countries();
	}
}