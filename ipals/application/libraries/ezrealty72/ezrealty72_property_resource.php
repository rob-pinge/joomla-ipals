<?php 
/*
 * Plugin for EZ Realty 7.2
 */

 class Ezrealty72_property_resource extends Ezrealty7_property_resource
{	
	public function preInsertProcess(&$row)
	{
		parent::preInsertProcess($row);

		// convert date to unix timestamp, a breaking change introduced in EZ 7.2
		$this->data['lastupdate'] = strtotime($this->data['lastupdate']);
	}
}