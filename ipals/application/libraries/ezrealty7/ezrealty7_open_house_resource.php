<?php 
class Ezrealty7_Open_house_resource extends Open_house_resource
{	
	public function insertData()
	{
		// remove everything except fields that start with oh and the retsID
		foreach($this->data as $key => $value)
		{
			if($key == $this->settings->retsID) continue;
			if($key[0] == 'o' && $key[1] == 'h') continue;
			unset($this->data[$key]);
		}
		parent::insertData();
	}
}