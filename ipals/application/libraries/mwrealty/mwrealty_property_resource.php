<?php 
class Mwrealty_property_resource extends Property_resource
{	
	protected $data;
	protected $retsQuery = array();
	
	protected $type = 'property';
	
	protected function delete($id)
	{
		$deletions = array(
			$this->settings->propertyTable    => 'id',
			$this->settings->imageTable       => 'propid'
		);
		
		$imgResult = $this->db->where('propid', $id)->get($this->settings->imageTable)->result();
		
		foreach($imgResult as $imgRow) 
		{
			$image = $imgRow->fname . '.jpg';
			if($this->settings->amazonS3)
			{
				$this->S3->deleteObject($this->settings->amazonBucket, $image);
			}
			else
			{
				@unlink($this->settings->imagePath . '/' . $image);
			}
		}
		
		foreach($deletions as $table => $field) 
		{
			$this->db->where($field, $id)->delete($table);
		}
	}
	
	protected $agent_info_key = 'mls_agent';
	public function preInsertProcess(&$row)
	{
		$this->data['sold'] = @$this->settings->statusAcceptValues[$row[$this->settings->statusAccept]];
		// fallback
		if($this->data['sold'] === null) $this->data['sold'] = 1;

		// Most Wanted Real Estate FIELDS
		if(!$this->_check_country($this->data['country']))
		{
			$this->_update_country($this->data['country']);
		}
		$this->data['cnid'] = $this->_countries[$this->data['country']];
						
		if(!$this->_check_state($this->data['state']))
		{
			$this->_update_state($this->data['state'], $this->data['cnid']);
		}
		$this->data['stid'] = $this->_states[$this->data['state']];
						
		if(!$this->_check_locality($this->data['locality']))
		{
			$this->_update_locality($this->data['locality'], $this->data['stid']);
		}
		$this->data['locid'] = $this->_localities[strtolower($this->data['locality'])];
        
        $mlsID = $this->data[$this->settings->retsID];
        $address = $this->data['street_num'] . ' ' . 
                   $this->data['address2'] . ' ' . 
                   $this->data['streettype'] . ', ' . 
                   $this->data['locality'] . ' ' . 
                   $this->data['state'] . ' ' . 
                   $this->data['postcode'];
        
		$this->data['alias'] = preg_replace('/[^0-9a-z]+/', '-', strtolower("{$mlsID} {$address}"));
		
		parent::preInsertProcess($row);
	}
		
	public function postInsertProcess(&$row)
	{		
		if(empty($this->data['owner']) && !empty($this->settings->defaultAgent))
		{
			$this->data['owner'] = $this->settings->defaultAgent;
		}
				
		parent::postInsertProcess($row);
	}
	
	protected function addToPhotoArray($p)
	{
		$this->photoArray[] = array(
			'propid'      => $p['property_id'],
			'description' => $p['description'],
			'fname'       => $p['file_name'] . $p['type'],
			'path'        => rtrim($p['path'], '/'),
			'ordering'    => $p['order'],
			$this->settings->retsID => $p[$this->settings->retsID],
		);
	}
	
	protected function prepFields($data)
	{		
		$return = array(
			'declat'  => $data['latitude'],
			'declong' => $data['longitude']
		);

		return $return;
	}

	// locality maintenance functions
	protected function _get_localities()
	{
		$table = $this->db->dbprefix('mostwantedrealestate_locality');
		$result = $this->db->query("SELECT mwcity, id FROM {$table}");
		foreach($result->result_array() as $row) $localities[strtolower($row['mwcity'])] = $row['id'];
		return isset($localities) ? $localities : array();
	}
	protected function _check_locality($locality)
	{
		if(!isset($this->_localities)) $this->_localities = $this->_get_localities();
		return isset($this->_localities[strtolower($locality)]);
	}
	protected function _update_locality($locality, $stid)
	{
		$table = $this->db->dbprefix('mostwantedrealestate_locality');
		
		// determine max sort order
		$result = $this->db->query("SELECT MAX(ordering) as max FROM {$table}");
		$row = $result->row_array();
		$max = $row['max'];
		
		$fields = array(
			'stateid'   => $stid,
			'mwcity'    => $locality,
			'published' => 1,
			'ordering'  => ($max + 1),
		);
		$this->db->insert($table, $fields);
		
		// make sure our data is up to date
		$this->_localities = $this->_get_localities();
	}
	
	protected function _get_states()
	{
		$table = $this->db->dbprefix('mostwantedrealestate_state');
		$result = $this->db->query("SELECT name, id FROM {$table}");
		foreach($result->result_array() as $row) $states[$row['name']] = $row['id'];
		return isset($states) ? $states : array();
	}
	protected function _check_state($state)
	{
		if(!isset($this->_states)) $this->_states = $this->_get_states();
		return isset($this->_states[$state]);
	}
	protected function _update_state($state, $cnid)
	{
		$table = $this->db->dbprefix('mostwantedrealestate_state');
		
		// determine max sort order
		$result = $this->db->query("SELECT MAX(ordering) as max FROM {$table}");
		$row = $result->row_array();
		$max = $row['max'];
		
		$fields = array(
			'countid'   => $cnid,
			'name'      => $state,
			'published' => 1,
			'ordering'  => ($max + 1),
		);
		$this->db->insert($table, $fields);
	
		// make sure our data is up to date
		$this->_states = $this->_get_states();
	}
	
	protected function _get_countries()
	{
		$table = $this->db->dbprefix('mostwantedrealestate_country');
		$result = $this->db->query("SELECT name, id FROM {$table}");
		foreach($result->result_array() as $row) $countries[$row['name']] = $row['id'];
		return isset($countries) ? $countries : array();
	}
	protected function _check_country($country)
	{
		if(!isset($this->_countries)) $this->_countries = $this->_get_countries();
		return isset($this->_countries[$country]);
	}
	protected function _update_country($country)
	{
		$table = $this->db->dbprefix('mostwantedrealestate_country');
		
		// determine max sort order
		$result = $this->db->query("SELECT MAX(ordering) as max FROM {$table}");
		$row = $result->row_array();
		$max = $row['max'];
		
		$fields = array(
			'name'    => $country,
			'published' => 1,
			'ordering'  => ($max + 1),
		);
		$this->db->insert($table, $fields);
		
		// make sure our data is up to date
		$this->_countries = $this->_get_countries();
	}
}