<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * Image Manipulation class
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Image_lib
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/libraries/image_lib.html
 */
class MY_Image_lib extends CI_Image_lib 
{
	function initialize($props = array())
	{
		$this->config = $props;
		return parent::initialize($props);
	}
	
	function clear()
	{
		$this->config = array();
		parent::clear();
	}
	
	// --------------------------------------------------------------------

	/**
	 * Image Resize and Crop
	 *
	 * This function resizes the images, and then crops the edges
	 * so that the final image is the exact dimensions passed in
	 *
	 * @access	public
	 * @return	bool
	 */
	function resize_and_crop()
	{
		try
		{
			$ratio = (($this->orig_height/$this->orig_width) - ($this->config['height']/$this->config['width']));
			$this->config['maintain_ratio'] = true;
			$this->config['master_dim'] = $ratio < 0 ? 'height' : 'width';
			$this->initialize($this->config);

			$resize = $this->resize();
			
			$this->config['maintain_ratio'] = false;
			$this->config['source_image'] = isset($this->config['new_image']) ? $this->config['new_image'] : $this->config['source_image'];
			$this->initialize($this->config);
			
			if($ratio > 0) // taller
			{
				$this->y_axis = intval(($this->orig_height - $this->config['height']) / 2);
			}
			else // wider
			{
				$this->x_axis = intval(($this->orig_width - $this->config['width']) / 2);
			}
			
			return $this->crop() && $resize;
		}
		catch (Exception $e) 
		{
			return false;
		}
	}
}
// END Image_lib Class

/* End of file Image_lib.php */
/* Location: ./system/libraries/Image_lib.php */
