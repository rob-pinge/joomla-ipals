<?php 
class Office_resource extends Resource
{	
	protected $data;
	protected $retsQuery = array();
	
	protected $type = 'office';
	protected $subType = 'office';
	
	public function init($resourceName, $className, $additionalOptions = array())
	{
		parent::init($resourceName, $className, $additionalOptions);
		$this->table = $this->settings->officeTable;
	}			
}