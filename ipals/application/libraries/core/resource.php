<?php 

// abstract class Resource
class Resource
{
	protected $resource;
	protected $class;
	
	protected $data;
	protected $ci;
	protected $isUpdate = false;
	
	public $settings = null;
	protected $retsQuery = array();
	protected $queryFields = null;

	protected $categories = array();
	
	public function __construct()
	{
		$this->ci =& get_instance();
				
		$this->load->library(array('phrets', 'parser', 'image_lib'));
	}
	
	public function init($resourceName, $className, $additionalOptions = array())
	{
		$this->reset();
		
		global $settings, $queryFields;
		$this->settings = $settings;
		$this->queryFields = $queryFields;
		
		$this->resource = $resourceName;
		$this->class = $className;
		
		foreach($additionalOptions as $optKey => $optVal)
		{
			$this->$optKey = $optVal;
		}		
	}
	
	public function reset()
	{
		$this->data = array();
		$this->settings = null;
		$this->retsQueryFields = null;
		$this->retsQuery = array();	
		$this->existing = array();		
	}
		
	public function getFields()
	{
		return $this->db->where('type', $this->type)->or_where('type', $this->subType)->get('ipals_fields')->result();
	}
	
	public function getExisting()
	{
		$existing = $this->db->select('id, ' . $this->settings->retsID)->get($this->table)->result();
		$return = array();
		foreach($existing as $row)
		{
			$id = $this->settings->retsID;
			$return[$row->$id] = $row->id;
		}
		
		return $this->existing = $return;
	}
	
	public function process()
	{
		$this->existing = $this->getExisting();
		$this->fields = $this->getFields();
		
		$this->settings->dbRefreshCounter = !empty($this->settings->dbRefreshRate) ? $this->settings->dbRefreshRate : -1;
				
		$this->data = array();
				
		$extra = array(
			'Count'               => 1,
			'Limit'               => $this->settings->limit,
			'RestrictedIndicator' => '',
		);
		
		$this->getSourceResult($extra);

		while($row = $this->getSourceRow())
		{
			if($this->shouldContinue($row))
			{
				$this->autoProcessRow($row);
				$this->preInsertProcess($row);
				$this->insertData();
				$this->postInsertProcess($row);
				$this->processImages($row);
				
				// prep array for next row (don't want lingering data for preInsert)
				$this->data = array();
			}

			if($this->settings->verboseLevel) { flush(); ob_flush(); }
		}
		
		$this->freeSourceResult();
	}
	
	public function processImages($row)     { /* override in child classes if necessary */ }
	public function preInsertProcess($row)  { /* override in child classes if necessary */ }
	public function postInsertProcess($row) { /* override in child classes if necessary */ }

	protected function shouldContinue($row)
	{
		$this->isUpdate = isset($this->existing[$row[$this->fetchFieldKey($this->settings->retsID)]]);
		return true;
	}

	protected function delete($id)
	{
		$this->db->where('id', $id)->delete($this->table);
	}
	
	public function insertData()
	{
		if(!$this->isUpdate)
		{
			$this->db->insert($this->table, $this->data);
			$this->insertID = $this->db->insert_id();
			$this->existing[$this->data[$this->settings->retsID]] = $this->insertID;
			if($this->settings->verboseLevel >= 1) echo "[{$this->insertID}] Inserted " , ucwords($this->type) , "<br>";
		}
		else
		{
			$this->db->where($this->settings->retsID, $this->data[$this->settings->retsID])->update($this->table, $this->data);
			$this->insertID = $this->existing[$this->data[$this->settings->retsID]];
			if($this->settings->verboseLevel >= 1) echo "[{$this->insertID}] Updated " , ucwords($this->type) , "<br>";
		}
		
		// refresh database if we need to
		if(--$this->settings->dbRefreshCounter == 0)
		{
			if($this->settings->verboseLevel >= 2) echo "Refreshing database connection<br>";
			$this->db->reconnect();
			$this->settings->dbRefreshCounter = $this->settings->dbRefreshRate;
		}
	}
		
	public function fetchFieldKey($field)
	{
		foreach($this->fields as $f)
		{
			if($field == $f->field) return $f->key;
		}
	}
	
	// abstract public function addParams();
	// dateFormat
	// timeFormat
	
	public function addParam($key, $value, $group = '')
	{
		if(empty($group)) $group = $key;
		$this->retsQuery[$group][$key][] = $value;
	}
	
	public function getParams()
	{
		$params = $this->db->where('type', $this->type)->where('auto', true)->get('ipals_query_fields')->result();
		
		foreach($params as $param)
		{
			// group is optional, and it may not exist, so we don't want an error
			$this->addParam($param->key, $param->value, @$param->group);
		}
	}
		
	public function setTimeRange($start, $length = null)
	{
		if($length == null) $length = $this->settings->processLength;
		
		$startTime = is_numeric($start) ? $start : strtotime($start);
		
		$this->startDate = date($this->settings->dateFormat, $startTime);
		$this->endDate = date($this->settings->dateFormat, strtotime($length, $startTime));
		
		$this->addParam($this->queryFields->queryField, "{$this->startDate}-{$this->endDate}");
	}
	
	protected function fetchSourceQueryString()
	{
		if(@$this->settings->idx)
		{
			return $this->retsQueryString = '';
		}

		if(empty($this->retsQuery)) return '';
		
		$retsQuery = array();
		foreach($this->retsQuery as $groupName => $groupValues)
		{
			$group = array();
			foreach($groupValues as $key => $values)
			{
				foreach ($values as $value) 
				{
					$group[] = "({$key}={$value})";
				}
			}
			$queryPart = implode('|', $group);
			$retsQuery[] = count($group) > 1 ? "({$queryPart})" : $queryPart;
		}

		$retsQueryString = implode(',', $retsQuery);

		return $this->retsQueryString = $retsQueryString;
	}
	
	public function getSourceResult($optionalParams = array())
	{		
		if(@$this->settings->idx)
		{
			echo $this->subType;
			$this->sourceResult = fopen($this->settings->idxFiles . '/' . $this->subType . '.txt', 'r');
			if(!$this->sourceResult) die("There was an error opening {$this->subType}.txt");
		}
		else
		{
			$this->getParams();
			$this->sourceResult = $this->phrets->SearchQuery($this->resource, $this->class, $this->fetchSourceQueryString(), $optionalParams);
		
			$totalRecords = $this->phrets->TotalRecordsFound();
			if($this->settings->verboseLevel >= 1) { echo "Total Records: {$totalRecords} - {$this->resource} - {$this->class} | "; if($this->type == 'property') echo "Dates: {$this->startDate} - {$this->endDate} | "; echo"Query: {$this->retsQueryString}<br>"; }
		
			if($error_info = $this->phrets->Error())
			{
				// code 20201 is "No Records Found"
				if($error_info['code'] != '20201')
				{
					d($error_info);
				}
			}
		}
		
		// refresh database at this point, because phrets could have taken
		// longer than MySQL will stay open for to run.
		$this->db->reconnect();
		
		return $this->sourceResult;
	}
		
	public function getSourceRow()
	{
		if(@$this->settings->idx)
		{
			if(empty($this->headingRows)) $this->headingRows = fgetcsv($this->sourceResult, 0, $this->settings->idxDelimiter);
		
			$values = fgetcsv($this->sourceResult, 0, $this->settings->idxDelimiter);
			
			// prevent an infinite loop!
			if(!$values) return false;
			
			$headingRowsCount = count($this->headingRows);
			$valuesCount = count($values);

			if($headingRowsCount != $valuesCount)
			{
				echo "arrays were not the same size ({$headingRowsCount} vs {$valuesCount})<br>";
				$row = array($this->fetchFieldKey('status') => 'skipme');
			}
			else
			{
				$row = array_combine($this->headingRows, $values);
			}
			
			// dd($row);
		}
		else
		{
			if(empty($this->sourceResult)) return; // zero results
		
			$row = $this->phrets->FetchRow($this->sourceResult);
		}
		return $row;
	}
	
	protected function freeSourceResult()
	{
		if(@$this->settings->idx)
		{
			fclose($this->sourceResult);
		}
		else
		{
			$this->phrets->FreeResult($this->sourceResult);
		}
	}
	
	public function autoProcessRow($retsRow)
	{
		foreach($this->fields as $field)
		{
			$skipNullCheck = false;
			if($field->key === false || $field->key == '') continue;
			if(isset($this->data[$field->field])) continue;
			switch($field->action):
				case 'concat':      $this->data[$field->field] = $this->parser->parse_string($field->key, $retsRow, true); break;
				case 'sum':         $this->data[$field->field] = $this->sumData($field->key, $retsRow); break;
				case 'conditional': $this->data[$field->field] = $this->pickConditional($field->key, $retsRow); break;
				case 'passthru':    $this->data[$field->field] = $field->key; break;
				case 'cross':       $this->data[$field->field] = $this->getCross($field->key, $retsRow); break;
				case 'category':
					$skipNullCheck = true;
					if (!isset($retsRow[$field->field])) break;
					$this->categories[] = array(
						'search' => $field->description, 
						'field'  => $retsRow[$field->field], 
						'cat'    => $field->key,
					);
				break;
				
				case 'none': 
				default: $this->data[$field->field] = $retsRow[$field->key]; break;
			endswitch;	
			
			if (!$skipNullCheck && $this->data[$field->field] === null)
				$this->data[$field->field] = '';		
		}
	}
	
	protected function getCross($crossID, $retsRow)
	{
		$crossField = $this->db->where('id', $crossID)->get('ipals_cross_fields')->row();

		// TODO: how to handle this special case?
		// if($crossField->key == 'STATE')
		
		// $crossField->destination should be the auto_increment field of table (id)
		$result = $this->db->select($crossField->destination) 
			->where($crossField->source, $retsRow[$crossField->key])
			->get($crossField->table)->row();
		
		if(!isset($result->{$crossField->destination}) && $crossField->add_if_not_exists)
		{
			$insert = array(
				$crossField->source => $retsRow[$crossField->key]
			);

			$this->db->insert($crossField->table, $insert);
			return $this->db->insert_id();
		}

		return @$result->{$crossField->destination};
	}
	
	protected function pickConditional($conditionalID, $retsRow)
	{
		$conditionalField = $this->db->where('id', $conditionalID)->get('ipals_conditional_fields')->row();
		
		$conditional = json_decode($conditionalField->conditional, true);
		
		if(isset($conditional[$retsRow[$conditionalField->key]])) return $conditional[$retsRow[$conditionalField->key]];
		if(isset($conditional['*'])) return $conditional['*'];
		return '';
	}
	
	protected function sumData($fieldsStr, $retsRow)
	{
		$matches = explode(',', $fieldsStr);
		
		$sum = array();
		foreach($matches as $match)
		{
			$sum[] = @$retsRow[$match];
		}
		
		return array_sum($sum);
	}
	
	protected function fetchFieldProperties($field)
	{
		return $this->db->get_where('ipals_fields', $field)->result();
	}

	// so everyone can Geocode!
	protected function getGeocode($address = false, $return = false)
	{
		if(!$this->settings->processGeocode) return;
		if(!$address) $address = $this->parser->parse_string($this->settings->geocodeAddress, $this->data, true);
		if($this->settings->proGeocode) $result = $this->proGeocode($address, $return);
		else $result = $this->basicGeocode($address, $return);

		if($this->settings->verboseLevel >= 2 && $result) echo "[{$this->insertID}] Saved Geocode information<br>"; 

		return $result;
	}
	
	private $processGeocode = true;

	// Google Maps API
	protected function basicGeocode($address, $return = false)
	{
		$id = $this->insertID;
		if(!$this->processGeocode) return; // we've already hit limit, don't keep querying Google Maps this batch
		
		$url = 'http://maps.googleapis.com/maps/api/geocode/json?address=' . str_replace('++', '+', urlencode($address)) . '&sensor=false';
		$json = json_decode(file_get_contents($url));

		if (!$json)
		{
			if($this->settings->verboseLevel >= 1) echo "[Basic] Property {$id} had an unknown issue (address: {$address}). We'll try again next time.<br />";
			return false;
		}
				
		if ((string) $json->status == 'OK')
		{
			$data = array(
				'gbase_address'	=> (string) $json->results[0]->formatted_address,
				'latitude'		=> (string) $json->results[0]->geometry->location->lat,
				'longitude'		=> (string) $json->results[0]->geometry->location->lng,
			);
			
			$data = $this->prepFields($data);

			if($return) return $data;
			
			$this->db->where('id', $id)->update($this->table, $data);

			return true;
		}
		else if (strval($json->status) === 'OVER_QUERY_LIMIT')
		{
			$this->processGeocode = false;
			if($this->settings->verboseLevel >= 1) echo "[{$this->insertID}] [Basic] Too many queries to the Geocoder (limit: 2500 per day). Stopping geocode requests.<br />";
			return false;
		}

		if($this->settings->verboseLevel >= 1) echo "[{$this->insertID}] [Basic] Property had an unknown issue (address: {$address}). We'll try again next time.<br />";
		return false;
	}
	
	// Pro Geocode for MapQuest. Next step is to break up the URL and populate the key from the database.
	protected function proGeocode($address, $return = false)
	{
		$id = $this->insertID;
		if(!$this->processGeocode) return; // we've already hit limit, don't keep querying this batch
		
		$url = 'http://www.mapquestapi.com/geocoding/v1/address?key=2Ioxl8JZlh7hCebPP6unhVZJTjAIGRjL&location=' . urlencode($address);
		$json = json_decode(file_get_contents($url));

		if (!$json)
		{
			if($this->settings->verboseLevel >= 1) echo "[Pro] Property {$id} had an unknown issue (address: {$address}). We'll try again next time.<br />";
			return false;
		}
				
		if ((string) $json->info->statuscode === '0')
		{
			$data = array(
				'gbase_address'	=> (string) $json->results[0]->providedLocation->location,
				'latitude'		=> (string) $json->results[0]->locations[0]->latLng->lat,
				'longitude'		=> (string) $json->results[0]->locations[0]->latLng->lng,
			);

			if($return) return $data;
			
			$data = $this->prepFields($data);
			
			$this->db->where('id', $id)->update($this->table, $data);

			return true;
		}
		else if (strval($json->status) === 'OVER_QUERY_LIMIT')
		{
			$this->processGeocode = false;
			if($this->settings->verboseLevel >= 1) echo "[{$this->insertID}] [Pro] Too many queries to the Geocoder. Stopping geocode requests.<br />";
			return false;
		}

		if($this->settings->verboseLevel >= 1) echo "[{$this->insertID}] [Pro] Property had an unknown issue (address: {$address}). We'll try again next time.<br />";
		return false;
	}
		
	public function __get($name)
	{
		return isset($this->ci->$name) ? $this->ci->$name : new stdClass;
	}
}

class ResourceException extends Exception {}