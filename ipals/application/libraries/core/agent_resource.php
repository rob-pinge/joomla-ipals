<?php 
class Agent_resource extends Resource
{	
	protected $data;
	protected $retsQuery = array();
	
	protected $type = 'agent';
	protected $subType = 'agent';
	
	public function init($resourceName, $className, $additionalOptions = array())
	{
		parent::init($resourceName, $className, $additionalOptions);
		$this->table = $this->settings->agentTable;
	}			
}