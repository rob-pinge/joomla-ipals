<?php 
class Open_house_resource extends Resource
{	
	protected $data;
	protected $retsQuery = array();
	
	// yeah... these are lowercase
	protected $type = 'openhouse';
	protected $subType = 'openhouse';
	
	public function init($resourceName, $className, $additionalOptions = array())
	{
		parent::init($resourceName, $className, $additionalOptions);
		$this->table = $this->settings->openHouseTable;
	}

	// Open houses have similar params to properties
	public function getParams()
	{
		$this->setTimeRange($this->settings->lastRun);
		parent::getParams();
	}

	public function insertData()
	{
		// make sure they know we need the retsID in open house mapping (i.e. rets_source from EZ)
		if(empty($this->data[$this->settings->retsID]))
		{
			echo 'ERROR: The "Rets ID" field from the settings is a required "field" in the Open House mapping<br>';
			return;
		}

		// open house should only do an update, only editing existing property entries
		$this->db->where($this->settings->retsID, $this->data[$this->settings->retsID])->update($this->table, $this->data);
		$this->insertID = $this->existing[$this->data[$this->settings->retsID]];
		if($this->settings->verboseLevel >= 1) echo "[{$this->insertID}] Updated Property's Open House Info<br>";
		
		// refresh database if we need to
		if(--$this->settings->dbRefreshCounter == 0)
		{
			if($this->settings->verboseLevel >= 2) echo "Refreshing database connection<br>";
			$this->db->reconnect();
			$this->settings->dbRefreshCounter = $this->settings->dbRefreshRate;
		}
	}

	public function setTimeRange($start, $length = null)
	{
		if($length == null) $length = $this->settings->processLength;
		
		$startTime = is_numeric($start) ? $start : strtotime($start);
		
		$this->startDate = date($this->settings->dateFormat, $startTime);
		$this->endDate = date($this->settings->dateFormat, strtotime($length, $startTime));
		
		$this->addParam($this->queryFields->openHouseQueryField, "{$this->startDate}-{$this->endDate}");
	}
}