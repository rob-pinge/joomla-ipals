<?php 
class Property_resource extends Resource
{	
	protected $data;
	protected $retsQuery = array();
	
	protected $type = 'property';
	
	public function init($resourceName, $className, $additionalOptions = array())
	{
		parent::init($resourceName, $className, $additionalOptions);
		$this->table = $this->settings->propertyTable;
	}
	
	public function getParams()
	{
		$this->setTimeRange($this->settings->lastRun);
		parent::getParams();
	}

	protected function shouldContinue($row)
	{
		$this->isUpdate = isset($this->existing[$row[$this->fetchFieldKey($this->settings->retsID)]]) ? true : false;
		$id = $this->existing[$row[$this->fetchFieldKey($this->settings->retsID)]];

		if($this->isUpdate)
		{
			if(array_key_exists($status = $row[$this->settings->statusAccept], $this->settings->statusAcceptValues))
			{
				return true;
			}
			else if(in_array($status = $row[$this->settings->statusDelete], $this->settings->statusDeleteValues))
			{
				$this->delete($id);
				if($this->settings->verboseLevel >= 1) echo "[{$id}] Deleted for status {$status}<br>";
				return false;
			}
			else
			{
				if($this->settings->verboseLevel >= 1) echo "[{$id}] Unknown status: {$status}<br>";
			}
		}
		else
		{
			if(!array_key_exists($status = $row[$this->settings->statusAccept], $this->settings->statusAcceptValues))
			{
				if($this->settings->verboseLevel >= 1) echo "[] Skipped for status {$status}<br>";
				return false;
			}
		}

		return true;
	}

	protected function delete($id)
	{
		$deletions = array(
			$this->settings->agentMidTable    => 'prop_id',
			$this->settings->propertyTable    => 'id',
			$this->settings->propertyMidTable => 'prop_id',
			$this->settings->imageTable       => 'propid'
		);
		
		$imgResult = $this->db->where('propid', $id)->get($this->settings->imageTable)->result();
		
		foreach($imgResult as $imgRow) 
		{
			$image = $imgRow->fname . '.jpg';
			if($this->settings->amazonS3)
			{
				$this->S3->deleteObject($this->settings->amazonBucket, $image);
			}
			else
			{
				@unlink($this->settings->imagePath . '/' . $image);
			}
		}
		
		foreach($deletions as $table => $field) 
		{
			$this->db->where($field, $id)->delete($table);
		}
	}
	
	protected $zipOnce = true;
	public function idxPhotos($row)
	{
		$extractionPoint = "{$this->settings->siteRoot}{$this->settings->tempPath}/{$this->resource}";
		if($this->zipOnce)
		{
			$zip = new ZipArchive;
			$opened = $zip->open($this->settings->idxFiles . '/photos.zip');
			if ($opened === TRUE) 
			{
				if(!is_dir($extractionPoint))
				{
					mkdir($extractionPoint, 0777, true);
				}

			    $zip->extractTo($extractionPoint);
			    $zip->close();

			    $this->zipOnce = false;

			    echo 'Extracting photos to temp dir<br>';
			}
			else
			{
				echo 'Could not extract photos<br>';
				return;
			} 
		}

		$retsID = $this->data[$this->settings->retsID];
		$pattern = "{$extractionPoint}/*{$retsID}*.jpg";
		$photos = glob($pattern);

		$return = array();
		foreach($photos as $i => $photo)
		{
			$number = $i + 1;

			// format name as the processor will recognize it (i.e. same as RETS format)
			rename($photo, "{$extractionPoint}/{$retsID}-{$number}.jpg");

			// make sure the photo deletes
			@unlink($photo);

			// format it to look like the RETS Photos Array
			$return[] = array(
			    'Content-ID' => $retsID,
			    'Object-ID'  => $number,
			);
		}

		return $return;
	}

	public function processImages($row)
	{
		// if($_SERVER['HTTP_HOST'] == 'ipals.cam') return;

		if(@$this->settings->idx)
		{
			$photos = $this->idxPhotos($row);
		}
		else
		{
			$locationFlag = intval(!$this->settings->getPhotos);
			if ($this->settings->mediaResource)
			{
				$photoQuery = "({$this->settings->extKey}={$this->data[$this->settings->retsID]})";
				$photosResult = $this->phrets->SearchQuery($this->settings->mediaResource, $this->settings->mediaClass, $photoQuery);

				$photos = array();
				while($photo = $this->phrets->FetchRow($photosResult))
				{					
					if($errorInfo = $this->phrets->Error())
					{
						d($errorInfo);
					}
					$photos[] = $photo;
				}
				$this->phrets->FreeResult($photosResult);
				$isImageClass = true;
			}
			else
			{
				$photos = $this->phrets->GetObject($this->resource, $this->settings->retsPhotoObjectName, $this->data[$this->settings->retsID], '*', $locationFlag);
				$isImageClass = false;
			}
		}
		
		// don't delete photo data from RETS if we're not processing new photos!
		// if it's an update and either IDX and non-empty photos, or isn't IDX, delete
		if($this->isUpdate && (($this->settings->idx && !empty($photos)) || (!$this->settings->idx)))
		{
			$this->db->where('propid', $this->insertID)->delete($this->settings->imageTable);
		}

		$this->photoArray = array();
		foreach ($photos as $idxIndex => $photo) 
		{
			flush(); ob_flush();
			
			if ($isImageClass)
			{
				$number      = $idxIndex;
				$description = '';
				$remotePath  = $photo[$this->settings->imgURL];
				$data        = $this->settings->getPhotos ? file_get_contents($remotePath) : false;
								
				$filename = basename($remotePath) . '/';
				$fileBasename = basename($filename, '.jpg');
				$ext = '.jpg';
			}
			else
			{
				$listing     = isset($photo['Content-ID']) ? $photo['Content-ID'] : '';
				$number      = isset($photo['Object-ID']) ? $photo['Object-ID'] : $idxIndex;
				$description = isset($photo['Content-Description']) ? $photo['Content-Description'] : '';
				$remotePath  = isset($photo['Location']) ? $photo['Location'] : '';
				$data        = isset($photo['Data']) ? $photo['Data'] : '';
				
				if($number == '' || (isset($photo['Success']) && $photo['Success'] === false)) continue;
				
				$filename = $listing . '-' . $number . '.jpg';
				$fileBasename = basename($filename, '.jpg');
				$ext = '.jpg';
			}
			
			if ($this->settings->amazonS3)
			{
				$path = "http://{$this->settings->amazonEndPoint}/{$this->settings->amazonBucket}/";
				$remote = 1;
			}
			else
			{
				$path = $this->settings->imagePath;
				$remote = 0;
			}
			
			if($this->settings->getPhotos)
			{
				if(!is_dir("{$this->settings->siteRoot}{$this->settings->tempPath}/{$this->resource}"))
				{
					mkdir("{$this->settings->siteRoot}{$this->settings->tempPath}/{$this->resource}", 0777, true);
				}
				
				if(!$this->settings->idx)
				{
					file_put_contents("{$this->settings->siteRoot}{$this->settings->tempPath}/{$this->resource}/{$filename}", $data);
				}
				// else photos are already in place!
			}
			else
			{
				$path = dirname($remotePath) .'/';
				$fileBasename = basename($remotePath, '.jpg');
				if (end(explode('.', $remotePath)) != 'jpg') $ext = '';
				$remote = 1;
			}
			
			if($this->settings->quickPhoto && $this->settings->getPhotos)
			{
				if($this->settings->imageLibrary != 'off')
				{
					// make sure our dirs create
					if(!is_dir("{$this->settings->siteRoot}{$this->settings->imagePath}"))
					{
						mkdir("{$this->settings->siteRoot}{$this->settings->imagePath}" , 0777, true);
					}
					
					if(!is_dir("{$this->settings->siteRoot}{$this->settings->thumbPath}"))
					{
						mkdir("{$this->settings->siteRoot}{$this->settings->thumbPath}" , 0777, true);
					}
					
					$this->image_lib->clear();
			
					$config['image_library']  = $this->settings->imageLibrary;
					$config['source_image']   = "{$this->settings->siteRoot}{$this->settings->tempPath}/{$this->resource}/{$filename}";
					$config['new_image']      = "{$this->settings->siteRoot}{$this->settings->imagePath}/{$filename}";
					$config['width']          = $this->settings->fullWidth;
					$config['height']         = $this->settings->fullHeight;
		
					$this->image_lib->initialize($config); 
					if(!$this->image_lib->resize()) if($this->settings->verboseLevel >= 1) echo $this->image_lib->display_errors();
		
					$this->image_lib->clear();
		
					$config['source_image'] = "{$this->settings->siteRoot}{$this->settings->imagePath}/{$filename}";
					$config['new_image']    = "{$this->settings->siteRoot}{$this->settings->thumbPath}/{$this->settings->thumbPrefix}{$fileBasename}{$this->settings->thumbPostfix}.jpg";
					$config['width']        = $this->settings->thumbWidth;
					$config['height']       = $this->settings->thumbHeight;
		
					$this->image_lib->initialize($config); 
					if(!$this->image_lib->resize()) if($this->settings->verboseLevel >= 1) echo $this->image_lib->display_errors();
					
					unlink("{$this->settings->siteRoot}{$this->settings->tempPath}/{$this->resource}/{$filename}");
					
					if($this->settings->verboseLevel >= 2) echo "[{$this->insertID}] {$this->settings->imageLibrary}: Resized and moved {$filename}<br />";					
				}
				else if($this->settings->amazonS3) // just in case
				{
					if($this->S3->putObjectFile("{$this->settings->tempPath}/{$this->resource}/{$filename}", $this->settings->amazonBucket, $filename, S3::ACL_PUBLIC_READ)) 
					{
						if($this->settings->verboseLevel >= 2) echo "[{$this->insertID}] S3: File uploaded to http://{$this->settings->amazonEndPoint}/{$this->settings->amazonBucket}/{$filename}<br />";
						unlink("{$this->settings->tempPath}/{$this->resource}/{$filename}");
					}
					else
					{
						if($this->settings->verboseLevel >= 1) echo "[{$this->insertID}] S3: Failed to upload {$directory}{$file}<br  />";
					}
				}
			}

			// taking a different direction, but don't want to remove this yet
			
			// // the idea here is to turn ../photos into something like
			// // /Users/cameron/Sites/vhosts/ipals.cam/photos into
			// // photos... it works in testing. We'll see!
			// if(strpos($path, '..') !== false)
			// {
			// 	// make sure we have exactly one slash at the end
			// 	$siteRoot = rtrim($this->settings->siteRoot, '/') . '/';
			// 	
			// 	// I hate not using the slash as the delimiter, but it's 100x easier
			// 	// since $siteRoot is all but guarenteed to have slashes in it.
			// 	$path = str_replace($siteRoot, '', realpath($path));
			// }
						
			$p = array(
				'property_id' => $this->insertID,
				'description' => $description,
				'file_name'   => $fileBasename,
				'path'        => $path,
				'order'       => $number,
				'type'        => $ext,
				$this->settings->retsID   => $this->data[$this->settings->retsID],
				'remote'      => $remote,
				'published'   => 1,
			);
			
			$this->addToPhotoArray($p);
		}
		
		if(!empty($this->photoArray))
		{
			$this->db->insert_batch($this->settings->imageTable, $this->photoArray);
		}
				
		// if ($mresource['photo_type_small'])
		// {
		// 	if ($mresource['ip_type'] == $this->_map['fields']['property'])
		// 	{
		// 		$this->_get_photos($resource, $listing[$id_key], $mresource['photo_type_small'], $new_pid, true);
		// 	}
		// }
	}
	
	protected function addToPhotoArray($p)
	{
		$this->photoArray[] = array(
			'propid'      => $p['property_id'],
			'description' => $p['description'],
			'fname'       => $p['file_name'],
			'path'        => $p['path'],
			'ordering'    => $p['order'],
			$this->settings->retsID => $p[$this->settings->retsID],
			'remote'      => $p['remote'],
			'state'       => $p['published'],
		);
	}
	
	protected $agent_info_key = 'listing_info';
	public function preInsertProcess(&$row)
	{		
		$listing_info = @$this->fetchFieldKey($this->agent_info_key);

		// do it twice in case there's a first name and last name
		for($i = 0; $i < 2; $i++)
		{
			if($listing_info && preg_match('/{RETSAGENT\|(.*?)\|(.*?)}/', $listing_info, $matches))
			{
				$agent = $this->getFromRETS($row[$matches[1]], $matches[2], $this->agent_resource);
				$listing_info = preg_replace('/{RETSAGENT.*?}/', $agent, $listing_info, 1);
			}
			else
			{
				// if it wasn't there the first time, it won't be the 2nd time
				break;
			}
		}
		
		if($listing_info && preg_match('/{RETSOFFICE\|(.*?)\|(.*?)}/', $listing_info, $matches))
		{
			$office = $this->getFromRETS($row[$matches[1]], $matches[2], $this->office_resource);
			$listing_info = preg_replace('/{RETSOFFICE.*?}/', $office, $listing_info);
		}
		
		// just in case there are other fields that need to be parsed
		if($listing_info) $this->data[$this->agent_info_key] = $this->parser->parse_string($listing_info, $row, true);
		

		// do the same thing for office_id, but just check for office
		$office_id = @$this->fetchFieldKey('office');

		if($office_id && preg_match('/{RETSOFFICE\|(.*?)\|(.*?)}/', $office_id, $matches))
		{
			$office = $this->getFromRETS($row[$matches[1]], $matches[2], $this->office_resource);
			$office_id = preg_replace('/{RETSOFFICE.*?}/', $office, $office_id);
		}
		
		// just in case there are other fields that need to be parsed
		if($office_id) $this->data['office_id'] = $this->parser->parse_string($office_id, $row, true);

		
		// make sure our possibly formated price is a valid double.
		$price = @$this->data['price'];
		if($price) $this->data['price'] = str_replace(array('$', ','), '', $price);
	}
	
	public function postInsertProcess(&$row)
	{	
		$this->getGeocode();
	}
	
	// right now, this is a very isolated specific exception. may one day
	// use other techniques, or add another action and manage through the map
	// TODO: use addParam()? this is simple enough to not need it for now
	private function getFromRETS($agentID, $fetch, $resource)
	{								
		// $agentID='4579880';
		$query = $resource->fetchFieldKey($this->settings->retsID) . '=' . $agentID;
					
		$extra = array(
			'Count' => 1,
			'Limit' => 1,
			'RestrictedIndicator' => '',
		);
																
		$result = $this->phrets->SearchQuery($resource->resource, $resource->class, $query, $extra);
		$row = $this->phrets->FetchRow($result);
		$this->phrets->FreeResult($result);
		
		if($error_info = $this->phrets->Error())
		{
			d($error_info);
		}
		
		return $row[$fetch];
	}
	
	// currently not used
	private function uploadS3()
	{						
		// we're only going to worry about property images, not agent or company
		$directories =  array(
			$this->$this->settings->tempPath . '/Property_thumbs/',
			$this->$this->settings->tempPath . '/Property/',
		);
		
		foreach($directories as $directory)
		{
			if ($handle = opendir($directory))
			{
				while (($file = readdir($handle)) !== false)
				{
					if ($file == '.' || $file == '..' || is_dir($file))
					{
						continue;
					}
				
					if($this->_s3->putObjectFile($directory . $file, $this->bucket_name, baseName($file), S3::ACL_PUBLIC_READ)) 
					{
						if($this->settings->verboseLevel >= 2) echo "S3: File uploaded to http://{$this->endpoint}/{$this->bucket_name}/" . baseName($file) . '<br />';
						unlink($directory . $file);
					}
					else
					{
						if($this->settings->verboseLevel >= 1) echo "S3: Failed to upload {$directory}{$file}<br  />";
					}
				}

				closedir($handle);
			}
		}

		return true;
	}

	private function curl_contents($url)
	{
	    // is cURL installed yet?
	    if (!function_exists('curl_init'))
	        die('Sorry cURL is not installed!');
 
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_HEADER, 0);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
 
	    $output = curl_exec($ch);
 
	    curl_close($ch);
 
	    return $output;
	}

}