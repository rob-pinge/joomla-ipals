<?php 
class Iproperty15_property_resource extends Iproperty_property_resource
{	
	protected $data;
	protected $retsQuery = array();
	
	protected $type = 'property';
	
	public function postInsertProcess(&$row)
	{		
		$this->db->where('prop_id', $this->insertID)->delete($this->settings->propertyMidTable);
		$this->db->where('prop_id', $this->insertID)->delete($this->settings->agentMidTable);
		
		// TODO: unify categories with types
		switch($this->subType)
		{
			case 'residential': $propertyType = 1; break;
			case 'land': $propertyType = 2; break;
			case 'commercial': $propertyType = 3; break;
			case 'multi-family': $propertyType = 4; break;
			case 'custom-1': $propertyType = 5; break;
			case 'custom-2': $propertyType = 6; break;
			case 'custom-3': $propertyType = 7; break;
			case 'custom-4': $propertyType = 8; break;
			case 'custom-5': $propertyType = 9; break;
			case 'custom-6': $propertyType = 10; break;
			default: $propertyType = 0; break;
		}

		$data = array(
			'prop_id'	=> $this->insertID,
			'cat_id'	=> $propertyType,
			$this->settings->retsID	=> $this->data[$this->settings->retsID],
		);
		$this->db->insert($this->settings->propertyMidTable, $data);		
		
		if($agentID = $this->agent_resource->existing[$row[$this->settings->agentID]])
		{
			$data = array(
				'prop_id'	=> $this->insertID,
				'agent_id'	=> $agentID,
				$this->settings->retsID	=> $this->data[$this->settings->retsID],
			);
			$this->db->insert($this->settings->agentMidTable, $data);
		}
		
		// call this manually because we DON'T want to do what our direct parent does
		$this->getGeocode();
	}
	
	protected function addToPhotoArray($photo)
	{
		$this->photoArray[] = array(
			'propid'      => $photo['property_id'],
			'description' => $photo['description'],
			'fname'       => $photo['file_name'],
			'path'        => $photo['path'],
			'type'        => $photo['type'],
			'ordering'    => $photo['order'],
			$this->settings->retsID   => $photo[$this->settings->retsID],
			'remote'      => $photo['remote'],
			'published'   => $photo['published'],
		);
	}
}