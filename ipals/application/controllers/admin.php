<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller 
{	
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('form');
		$this->load->library('common');
	}
	
	public function index()
	{
		redirect('admin/map');
	}
	
	private function load_rows()
	{
		$settings = $this->common->loadSettings();
		
		$tables = array(
			'agent'    => $settings->agentTable,
			'office'   => $settings->officeTable,
			'property' => $settings->propertyTable,
		);
		
		foreach($tables as $type => $table)
		{
			if(empty($table)) continue;
			foreach($this->_fields($table) as $field)
			{
				$data = array(
					'field'       => $field,
					'key'         => '',
					'action'      => 'none',
					'type'        => $type,
					'description' => '',
				);
				$this->db->insert('ipals_fields', $data);
			}
		}
	}	

	public function map()
	{
		if($data = $this->input->post())
		{
			if(@$data['submit'] != 'get_rows')
				$this->save_field($data);
			else
				$this->load_rows();
		}

		$this->load->library('table');
		$table_opts = array('table_open' => '<table class="table table-bordered table-striped" border="0" cellpadding="0" cellspacing="0">',);
		$this->table->set_template($table_opts);

		$heading = array('Field', 'Key', 'Action', 'Type', 'Description', 'Manage');

		$this->table->set_heading($heading);
		$rows = $this->db->order_by('type, action, field')->get('ipals_fields')->result_array();

		// $data['resources'] = array_keys($this->resources);
		foreach($rows as &$row)
		{
			// foreach($row as &$r)
			// {
			// 	$r = "<span>{$r}</span>";
			// }

			$row['manage'] = '<span data-toggle="modal" data-target="#delete-modal" data-id="' . $row['id'] . '" class="label delete label-important">Delete</span>';
			
			if($row['action'] != 'cross' && $row['action'] != 'conditional') 
			{
				$row['manage'] = '<span data-id="' . $row['id'] . '" class="label edit label-info">Edit</span> ' . $row['manage'];
			}

			// $row['key'] .= ' <a class="btn hide edit-special-field-inline">Edit</a>';

			unset($row['id']);

			$cells = array();
			foreach($row as $k => $v)
			{
				$cells[] = array('data' => $v, 'class' => $k, 'data-name' => $k);
			}

			$this->table->add_row($cells);
		}

		$data['type_options'] = '';
		$data['action_options'] = '';

		$types = $this->_enum('type', 'ipals_fields');
		foreach($types as $type)
		{
			$data['type_options'] .= "<option value='{$type}'>{$type}</option>\n";
		}

		$actions = $this->_enum('action', 'ipals_fields');
		foreach($actions as $action)
		{
			$data['action_options'] .= "<option value='{$action}'>{$action}</option>\n";
		}

		$data['table'] = $this->table->generate();		
		$data['no_rows'] = !count($rows);
		
		$this->load->view('map', $data);		
	}
	
	private function _enum($column, $table)
	{
		$enums = $this->db->query("SHOW COLUMNS FROM `" . $this->db->dbprefix($table) ."` LIKE '{$column}'")->row();
		preg_match('/enum\((.*)\)$/', $enums->Type, $matches);
		$enums = explode(',', $matches[1]);
		foreach($enums as &$enum)
		{
			$enum = str_replace("''", "'", trim($enum, "'"));
		}
		
		return $enums;
	}

	public function settings()
	{
		if($post = $this->input->post())
		{
			foreach($post as $key => $value)
			{
				switch($key)
				{
					case 'statusAcceptValues':
						$v = array();
						foreach($value['key'] as $a => $b)
						{
							if(!empty($b)) $v[$value['key'][$a]] = $value['value'][$a];
						}
						$value = json_encode($v);
						break;
					case 'propertyClasses':
					case 'openHouseClasses':
						$value = json_encode($value);
						break;
					case 'statusDeleteValues':
					case 'cleanupClasses':
						$value = explode("\n", $value);
						foreach($value as $a => &$b)
						{
							$b = trim($b);
						}
						$value = json_encode($value);
						break;
				}
				$this->db->where('key', $key)->set('value', $value)->update('ipals_settings');	
			}
		}
				
		$booleans = array(
			'debugMode',
			'downloadRemote',
			'amazonS3',
			'getPhotos',
			'quickPhoto',
			'postProcessPhotos',
			'updateOnDuplicateKey',
			'proGeocode',
			'cleanupScript',
			'idx',
			'processGeocode',
		);
										
		$settings = $this->db->get('ipals_settings')->result_array();
				
		foreach($settings as $setting)
		{
			extract($setting);
			if($category == 'private') continue;
			
			if(strpos($description, '(not used?)') !== false) 
			{
				$description = "<i rel=\"tooltip\" data-title=\"{$description}\" class=\"icon-question-sign\"></i>";
			}
			else
			{
				$description = !empty($description) ? "<i rel=\"tooltip\" data-title=\"{$description}\" class=\"icon-info-sign\"></i>" : '';
			}
			
			$setting = array(
				'value'       => htmlentities($value),
				'key'         => $key,
				'display'     => $this->common->humanize($key),
				'description' => $description,
			);
						
			if(in_array($key, $booleans))
			{
				$checked1 = $checked0 = '';
				${'checked' . $value} = 'checked="checked"';
				$setting['input'] = 
				"<div class=\"form-inline\">
				    <input type=\"radio\" class=\"\" name=\"{$key}\" id=\"{$key}_true\" value=\"1\" {$checked1}><label class=\"boolean-label\" for=\"{$key}_true\"> True</label>
				    <input type=\"radio\" class=\"\" name=\"{$key}\" id=\"{$key}_false\" value=\"0\" {$checked0}><label class=\"boolean-label\" for=\"{$key}_false\"> False</label>
				</div>";
			}
			else if(method_exists(__CLASS__, $key))
			{
				$setting['input'] = $this->$key($value);
			}
			else
			{
				$value = htmlentities($value);
				$setting['input'] = "<input type=\"text\" class=\"\" name=\"{$key}\" id=\"{$key}\" value=\"{$value}\">";
			}

			$data['settings'][$category][] = $setting;			
		}

		$this->load->view('settings', $data);
	}
	
	public function query_fields()
	{
		if($post = $this->input->post())
		{
			$this->save_query_fields($post);
		}
		
		$required = array('queryField' => 'Primary Query Field to determine time range to query.');
		foreach($required as $r => $description)
		{
			$row = $this->db->where('key', $r)->get('ipals_query_fields')->row();
			
			if(empty($row))
			{
				$insert = array(
					'key' => $r,
					'value' => '',
					'auto' => 0,
					'type' => '',
					'description' => $description,
				);
				$this->db->insert('ipals_query_fields', $insert);
			}
		}
		
		$data = array();
		$queryFields = $this->db->order_by('type, id')->get('ipals_query_fields')->result_array();
		
		$data['queryFields']['auto'] = array();
		foreach($queryFields as &$queryField)
		{
			if(!$queryField['auto']) $queryField['name'] = $this->common->humanize($queryField['key']);
			$queryField['description'] = !empty($queryField['description']) ? "<p class=\"help-block\">{$queryField['description']}</p>" : '';
			
			$data['queryFields'][$queryField['auto'] ? 'auto' : 'manual'][] = $queryField;
		}
		
		$this->load->view('query_fields', $data);
	}
	
	private function save_query_fields($data)
	{
		$insert = array();
		foreach ($data['param']['key'] as $index => $key) 
		{
			if(trim($key) != '')
			{
				$insert[] = array(
					'key'   => $data['param']['key'][$index],
					'value' => $data['param']['value'][$index],
					'type'  => $data['param']['type'][$index],
					'group'  => $data['param']['group'][$index],
					'auto'  => true,
				);
			}
		}
		unset($data['param']);
		
		$this->db->where('auto', true)->delete('ipals_query_fields');
		if(!empty($insert))
		{
			$this->db->insert_batch('ipals_query_fields', $insert);
		}
		
		foreach($data as $key => $value)
		{
			$this->db->where('key', $key)->update('ipals_query_fields', array('value' => $value));
		}
	}
	
	private function _fields($table)
	{
		$columns = $this->db->query("DESCRIBE " . $this->db->dbprefix($table))->result();
		foreach($columns as $col)
		{
			$fields[] = $col->Field;
		}
		return isset($fields) ? $fields : array();
	}

	private function _comments($table)
	{
		$columns = $this->db->query("SHOW FULL COLUMNS FROM " . $this->db->dbprefix($table))->result();
		foreach($columns as $col)
		{
			$fields[$col->Field] = $col->Comment;
		}
		return isset($fields) ? $fields : array();
	}
	
	public function _humanize($str)
	{
		$j = 0;
		$return = array();
		for($i = 0; $i < strlen($str); $i++) 
		{ 
			$char = $str[$i]; 
			if(preg_match("/[A-Z]/", $char))
			{ 
				@$return[++$j] .= $char; 
			} 
			else 
			{ 
				@$return[$j] .= $char; 
			} 
		} 
		
		return implode(' ', array_map('ucwords', $return));
	}

	public function delete_field($id)
	{
		$field = $this->db->where('id', $id)->get('ipals_fields')->row();

		if($field->action == 'cross' || $field->action == 'conditional')
		{
			$this->db->where('id', $field->key)->delete("ipals_{$field->action}_fields");
		}

		$result = $this->db->where('id', $id)->delete('ipals_fields');

		$field->success = $result;
		die(json_encode($field));
	}

	private function save_field($data)
	{
		$this->db->insert('ipals_fields', $data);
	}

	public function update_field()
	{
		$data = $this->input->get();

		$this->db->where('id', $data['id'])->update('ipals_fields', $data);
		$data['success'] = 1;
		die(json_encode($data));
	}

	public function get_cross($id)
	{
		$data = $this->db->where('id', $id)->get('ipals_cross_fields')->row_array();
		die(json_encode($data));
	}

	public function get_conditional($id)
	{
		$data = $this->db->where('id', $id)->get('ipals_conditional_fields')->row_array();
		die(json_encode($data));
	}

	public function save_cross() 
	{
		$data = $this->input->get();

		if(empty($data['id']))
		{
			$this->db->insert('ipals_cross_fields', $data);
			$return['id'] = $this->db->insert_id();
		}
		else
		{
			$this->db->where('id', $data['id'])->update('ipals_cross_fields', $data);
			$return['id'] = $data['id'];
		}

		$return['success'] = 1;

		die(json_encode($return));
	}

	public function save_conditional() 
	{
		$get = $this->input->get();

		foreach($get['ckeys'] as $key => $value)
		{
			if($value == '') unset($get['ckeys'][$key], $get['cvalues'][$key]);
		}

		if(@count($get['ckeys']) <= 0)
		{
			$return = array('success' => false, 'code' => 2);
			die(json_encode($return));
		}

		$data['conditional'] = json_encode(array_combine($get['ckeys'], $get['cvalues']));
		$data['key'] = $get['key'];
		// $data['id'] = $data['id'];

		if(empty($data['id']))
		{
			$this->db->insert('ipals_conditional_fields', $data);
			$return['id'] = $this->db->insert_id();
		}
		else
		{
			$this->db->where('id', $data['id'])->update('ipals_conditional_fields', $data);
			$return['id'] = $data['id'];
		}

		$return['success'] = 1;

		die(json_encode($return));
	}
	
	
	// special cases for settings
	function imageLibrary($value)
	{
		$options = array(
			'off'         => 'off', 
			'GD2'         => 'GD2', 
			'ImageMagick' => 'ImageMagick', 
			'NetPBM'      => 'NetPBM'
		);
		return form_dropdown(__FUNCTION__, $options, $value);
	}
	
	function cleanupClasses($value) { return $this->statusDeleteValues($value, __FUNCTION__); }
	
	function statusDeleteValues($value, $function = null)
	{
		if($function === null) $function = __FUNCTION__;
		$array = json_decode($value, true);
		$value = implode("\n", $array);
		$data = array(
			'name'  => $function,
			'id'    => $function,
			'value' => $value,
			'rows'  => count($array) + 1,
		);
		return form_textarea($data);
	}
	
	function plugin($value)
	{
		$options = array(
			'mwrealty10'  => 'Most Wanted Real Estate 1.0+',
			'ezrealty72'  => 'EZ-Realty 7.2+',
			'ezrealty7'   => 'EZ-Realty 7',
			'ezrealty'    => 'EZ-Realty (Depreciated)',
			'iproperty3'  => 'iProperty 3',
			'iproperty'   => 'iProperty 2', 
			'iproperty15' => 'iProperty 1.5 (Depreciated_', 
		);
		return form_dropdown(__FUNCTION__, $options, $value);
	}	

	function openHouseClasses($value)
	{
		return $this->propertyClasses($value, 'openHouseClasses');
	}
	
	function propertyClasses($value, $base = 'propertyClasses')
	{
		$subtypes = $this->_enum('type', 'ipals_fields');
		// remove agent and office and property
		unset($subtypes[0], $subtypes[1], $subtypes[2]);

		// also unset Openhouse (which might be hard to find)
		$index = array_search('openhouse', $subtypes);
		if($index >= 0) unset($subtypes[$index]);
		
		$array = json_decode($value, true);
		
		foreach($subtypes as $subtype)
		{
			if(!isset($array[$subtype])) $array[$subtype] = '';
		}
		
		$return = '<table>';
		foreach($array as $name => $value)
		{
			$human = $this->common->humanize($name);
			$html = <<<HTML
			<tr>
				<td align="right" style="padding-right:10px">
					<label class="control-label" for="{$name}">{$human}</label>
				</td>
				<td>
					<input type="text" class="input-medium" name="{$base}[{$name}]" id="{$name}" value="{$value}">
				</td>
			</tr>
HTML;
			
			$return .= $html;
			
		}
		$return .= '</table>';
		
		return $return;
	}
	
	function statusAcceptValues($value)
	{
		$array = json_decode($value, true);
		
		$return = '<table id="statusAcceptValues-table">';
		foreach($array as $key => $value)
		{
			$return .= <<<HTML
			<tr class="statusAcceptValues-row">
				<td style="padding-right:10px">
					<input type="text" class="input-small" name="statusAcceptValues[key][]" value="{$key}">
				</td>
				<td>
					<input type="text" class="input-small" name="statusAcceptValues[value][]" value="{$value}"> <i class="statusAcceptValues-remove icon-remove"></i>
				</td>
			</tr>
HTML;
		}
		
		$return .= <<<HTML
		<tr class="statusAcceptValues-row">
			<td style="padding-right:10px">
				<input type="text" class="input-small" name="statusAcceptValues[key][]" value="">
			</td>
			<td>
				<input type="text" class="input-small" name="statusAcceptValues[value][]" value=""> <i class="statusAcceptValues-remove icon-remove"></i>
			</td>
		</tr>
HTML;
		$return .= '</table>';
		
		return $return;
	}

	function idxDelimiter($value)
	{
		$options = array(
			','  => 'Comma Separated', 
			"\t" => 'Tab Delimited', 
		);
		return form_dropdown(__FUNCTION__, $options, $value);
	}
}

/* End of file admin.php */
/* Location: ./application/controllers/admin.php */
