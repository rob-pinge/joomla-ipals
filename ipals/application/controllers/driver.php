<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

// set up an autoloader

function library_autoloader($class) {
	if (class_exists($class)) return;

	$parts = explode('_', $class);
	if (count($parts) < 2) return;
	$type = $parts[0];

	$path = getcwd() . '/application/libraries/' . strtolower($type) . '/' . strtolower($class) . '.php';

	if (file_exists($path)) {
		include $path;
	}
}

spl_autoload_register('library_autoloader');


class Driver extends CI_Controller 
{	
	public function __construct()
	{
		parent::__construct();
		
		ini_set('display_errors', 1); 
		error_reporting(E_ALL ^ E_NOTICE);

		// prep server settings
		set_time_limit(0);
		ini_set('post_max_size', '64M');
		ini_set('upload_max_filesize', '64M');
		ini_set('memory_limit', '1024M');
		ini_set('max_input_time', -1);
		ini_set('max_execution_time',-1);
	}
		
	public function index()
	{
		global $settings, $queryFields;
		$this->load->library('common');
		$settings = $this->settings = $this->common->loadSettings();

		$this->load->helper('url');
		$this->load->library(array('core/resource'));

		$this->load->plugin_library(array('property_resource', 'agent_resource', 'office_resource', 'open_house_resource'), $this->settings->plugin);
		
		// this exits if license did not verify
		if($_SERVER['SERVER_NAME'] != 'ipals.cam')
			$this->common->verifyLicense($settings);

		// ==========================================
		// Looking for the Amazon S3 settings? 
		// They've been moved to libraries/common.php
		// ==========================================
		$this->S3 = $this->common->loadAmazonS3($settings);
		
		// TODO get these from DB
		$resources = array();
		if(!@$this->settings->idx)
		{
			$queryFields = $this->queryFields = $this->loadQueryFields();
			$this->phrets->login();
			
			// IDX does not yet implement anything other than property
			$resources = array('office', 'agent');
		}
			
		$this->load->helper('inflector');
		foreach($resources as $resource)
		{
			// underscore-ize our resource to access it via CI
			$_resource = underscore($this->common->humanize($resource));

			// if we haven't set a Resource in the settings, skip this
			if(empty($this->settings->{$resource . 'Resource'})) continue;
			
			$this->{$_resource . '_resource'}->init($this->settings->{$resource . 'Resource'}, $this->settings->{$resource . 'Class'});

			// we may still need office_resource, even thought we don't want it to process for EZ Realty
			if(empty($this->settings->{$resource . 'Resource'})) continue;
			$this->{$_resource . '_resource'}->process();
		}
				
		for($this->settings->batchLimit; $this->settings->batchLimit > 0; $this->settings->batchLimit--)
		{					
			// property's a special case
			foreach($this->settings->propertyClasses as $subType => $class)
			{
				if(empty($class)) continue;
				
				// subtype's always the same in offce & agent, but not in property
				$extra = array('subType' => $subType);
				$this->property_resource->init($this->settings->propertyResource, $class, $extra);
				$this->property_resource->process();			
			}
			
			// we want to process open houses after properties, but in the same timeframe (i.e. with batches)
			if(!empty($this->settings->openHouseClasses) && !empty($this->settings->openHouseResource))
			{
				foreach($this->settings->openHouseClasses as $subType => $class)
				{
					if(empty($class)) continue;

					$extra = array('subType' => $subType);
					$this->open_house_resource->init($this->settings->openHouseResource, $class, $extra);
					if(!empty($this->open_house_resource->table))
					{
						$this->open_house_resource->process();	
					}
				}
			}

			$this->updateLastRun();	
		}

		if(@$this->settings->cleanupScript)
		{
			// this is pretty hacky... probably should go into a lib?
			require_once(APPPATH . 'controllers/cleanup.php');
			$cleanup = new Cleanup();
			$cleanup->index();
		}
	}
	
	private function loadQueryFields()
	{
		$queryFields = $this->db->where('auto', false)->get('ipals_query_fields')->result();
		
		$return = new stdClass;
		foreach($queryFields as $queryField)
		{
			$return->{$queryField->key} = $queryField->value;
		}
		
		return $return;
	}

	private function updateLastRun()
	{
		$timeLastRun = strtotime($this->settings->processLength, strtotime($this->settings->lastRun));

		if($timeLastRun > ($time = time()))
		{
			$timeLastRun = $time;
			$this->settings->batchLimit = 0;
			if($this->settings->verboseLevel >= 1) echo 'The script has caught up with the current time. Skipping rest of the batches.<br>';
		}

		// this gets converted later, so we format for MySQL
		$this->settings->lastRun = date('Y-m-d H:i:s', $timeLastRun);
		$this->db->where('key', 'lastRun')->update('ipals_settings', array('value' => $this->settings->lastRun));
	}
}

/* End of file driver.php */
/* Location: ./application/controllers/driver.php */