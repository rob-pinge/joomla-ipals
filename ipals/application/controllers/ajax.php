<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller 
{	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		
	}
		
	public function import_fields($table)
	{
		$columns = $this->db->query("DESCRIBE " . $this->db->dbprefix($table))->result();
		foreach($columns as $col)
		{
			echo "{$col->Field}\n";
		}
	}

	public function settings()
	{
		$this->load->view('settings');
	}
}

/* End of file admin.php */
/* Location: ./application/controllers/admin.php */
