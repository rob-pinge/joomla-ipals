<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Cleanup extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();

		ini_set('display_errors', 1); 
		error_reporting(E_ALL ^ E_NOTICE);

		// prep server settings
		set_time_limit(0);
		ini_set('post_max_size', '64M');
		ini_set('upload_max_filesize', '64M');
		ini_set('memory_limit', '1024M');
		ini_set('max_input_time', -1);
		ini_set('max_execution_time',-1);       
	}

	public function index()
	{
		global $settings;

		$this->load->library('common');
		if(!$this->settings)
		{
			$this->settings = $this->common->loadSettings();
		}

		// convenience?
		$settings = $this->settings;

		$this->load->helper('url');

		// this exits if license did not verify
		if($_SERVER['SERVER_NAME'] != 'ipals.cam')
			$this->common->verifyLicense($settings);

		// this function may be ran from another function, so we 
		// don't need to reload this stuff if it already exists

		if(!$this->phrets)
		{
			$this->load->library('phrets');
			$this->phrets->login();
		}

		if(!$this->S3) 
		{
			$this->S3 = $this->common->loadAmazonS3($settings);
		}

		$count = 0;

		$imageTable     = $settings->imageTable;
		$propertyTable  = $settings->propertyTable;
		$openHouseTable = $settings->openHouseTable;

		$deletionTables = array(
			$propertyTable => 'id',
			$imageTable    => 'propid',
		);

		$resource  = $settings->cleanupResource;
		$types     = $settings->cleanupClasses;
		$key       = $settings->cleanupKey;
		$retsQuery = $settings->cleanupRetsQuery;
		$imagePath = $settings->siteRoot . $settings->imagePath;

		if(!is_array($types)) $types = array($types);

		$result = $this->db->update($propertyTable, array('ghost' => 0));

		foreach($types as $type) 
		{
			// (from old cleanup): re-implement this if we need it, but as of now,
			// there is no setting related to "RETS_CONNECTION_REFRESH_RATE"

			// if(!isset($retsCount)) $retsCount = 0;

			// if(isset($settings['RETS_CONNECTION_REFRESH_RATE']) && $settings['RETS_CONNECTION_REFRESH_RATE'] > 0 && !(++$retsCount % $settings['RETS_CONNECTION_REFRESH_RATE']))
			// {
			// 	$this->phrets->Disconnect();
			// 	$this->phrets->login();
			// }

			$search = $this->phrets->SearchQuery($resource, $type, $retsQuery, array('Select' => $key));

			while($listing = $this->phrets->FetchRow($search))
			{
				$id = ($listing[$key]);

				// if this entries was found, mark it as such (ghost = 1)
				$result = $this->db->where('rets_source', $id)->update($propertyTable, array('ghost' => 1));

				// just so we know that there is progress
				$count++;
				if($count % 10 == 0) { echo "{$count} rows ghost checked.<br>\n"; }
			}

			// print out errors
			if ($errorInfo = $this->phrets->Error())
			{
				if ($errorInfo['code'] == 20201) continue;
				echo "Full Query: ({$retsQuery})<br>\n";
				print_r($errorInfo);
				flush();
				return;
			}
		}

		// just is case there is an error in the for loop, let's ONLY delete 
		// the entire table if there were less than 500 rows affected
		// (as there's unlikely to be 500 deleted since last run!)
		// if($count < 500)
		if($count > 1)
		{
			// if rets_source is empty, it was user-added and we don't want to mess with it
			$ghostResult = $this->db->select('id, mls_id')->from($propertyTable)->where(array('ghost' => 0, 'rets_source !=' => ''))->get()->result_array();

			$dels = 0;
			foreach($ghostResult as $row)
			{
				$dels++;
				$delId = intval($row['id']);
				$mlsId = $row['mls_id'];

				$imgResult = $this->db->select('fname')->from($imageTable)->where('propid', $delId)->get()->result_array();

				foreach($imgResult as $imgRow)
				{
					$image = $imgRow['fname'] . '.jpg';
					if($settings->amazonS3)
					{
						$this->s3->deleteObject($settings->amazonBucket, $image);
					}
					else
					{
						@unlink($image_path . '/' . $image);
					}
				}

				foreach($deletionTables as $table => $field) 
				{
					$this->db->where($field, $delId)->delete($table);
				}
			}

			echo "{$count} rows were checked and {$dels} entries were deleted.";
		}
		else
		{
			echo "{$count} rows were checked. Something probably went wrong.<br>\nNothing was deleted.";
		}

		// remove open houses that are in the past (EZ Realty)
		if($openHouseTable)
		{
			$rows = $this->db->select('id')->from($openHouseTable)->where('ohdate <', date('Y-m-d'))->where('ohdate !=', '0000-00-00')->get()->result_array();

			foreach($rows as $row)
			{
				$this->db->where('id', $row['id'])->update($openHouseTable, array(
					'ohouse_desc' => '',
					'ohdate' => '0000-00-00',
					'ohstarttime' => '00:00:00',
					'ohendtime' => '00:00:00',
					'ohdate2' => '0000-00-00',
					'ohstarttime2' => '00:00:00',
					'ohendtime2' => '00:00:00',
					'oh_id' => null,
				));
			}
		}
	}
}
