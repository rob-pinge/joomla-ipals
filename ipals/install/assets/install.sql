-- phpMyAdmin SQL Dump
-- version 4.1.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 02, 2014 at 11:04 AM
-- Server version: 5.5.36-cll
-- PHP Version: 5.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `retsfor_ipals-test`
--

-- --------------------------------------------------------

--
-- Table structure for table `jos_ipals_conditional_fields`
--

CREATE TABLE IF NOT EXISTS `jos_ipals_conditional_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(200) NOT NULL,
  `conditional` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jos_ipals_cross_fields`
--

CREATE TABLE IF NOT EXISTS `jos_ipals_cross_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(200) NOT NULL,
  `table` varchar(200) NOT NULL,
  `source` varchar(200) NOT NULL,
  `destination` varchar(200) NOT NULL,
  `add_if_not_exists` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `jos_ipals_fields`
--

CREATE TABLE IF NOT EXISTS `jos_ipals_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field` varchar(200) NOT NULL,
  `key` varchar(200) NOT NULL,
  `action` enum('none','concat','sum','passthru','conditional','cross') NOT NULL DEFAULT 'none',
  `type` enum('agent','office','property','residential','land','commercial','multi-family','openhouse','custom-1','custom-2','custom-3','custom-4','custom-5','custom-6') NOT NULL DEFAULT 'property' COMMENT 'Property applies to all property resources, residential only applies to residential class, etc',
  `description` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=249 ;

--
-- Dumping data for table `jos_ipals_fields`
--

INSERT INTO `jos_ipals_fields` (`id`, `field`, `key`, `action`, `type`, `description`) VALUES
(1, 'id', '', 'none', 'property', ''),
(2, 'type', '', 'none', 'property', ''),
(3, 'rent_type', '', 'none', 'property', ''),
(4, 'cid', '', 'none', 'property', ''),
(248, 'country', 'US', 'passthru', 'property', 'Set the country to US always.'),
(7, 'cnid', '', 'none', 'property', ''),
(8, 'locality', 'CITY', 'none', 'property', ''),
(9, 'state', 'STATE', 'none', 'property', ''),
(10, 'country', '', 'none', 'property', ''),
(11, 'office_id', 'OFFICELIST_OFFICENAM1', 'none', 'property', ''),
(12, 'mls_id', 'MLSNUM', 'none', 'property', ''),
(13, 'mls_agent', 'AGENTLIST_FULLNAME', 'none', 'property', ''),
(14, 'viewad', '', 'none', 'property', ''),
(15, 'viewbooking', '', 'none', 'property', ''),
(16, 'bookinglink', '', 'none', 'property', ''),
(17, 'unit_num', 'UNITNUM', 'none', 'property', ''),
(18, 'street_num', 'STREETNUM', 'none', 'property', ''),
(19, 'address2', 'STREETNAME', 'none', 'property', ''),
(20, 'postcode', 'ZIPCODE', 'none', 'property', ''),
(21, 'county', 'COUNTY', 'none', 'property', ''),
(22, 'price', 'LISTPRICE', 'none', 'residential', ''),
(23, 'showprice', '', 'none', 'property', ''),
(24, 'freq', '', 'none', 'property', ''),
(25, 'bond', '', 'none', 'property', ''),
(26, 'closeprice', '', 'none', 'residential', ''),
(27, 'priceview', '', 'none', 'residential', ''),
(28, 'age', 'YEARBUILT', 'none', 'residential', ''),
(29, 'landtype', '', 'none', 'residential', ''),
(30, 'frontage', '', 'none', 'residential', ''),
(31, 'depth', '', 'none', 'residential', ''),
(32, 'bedrooms', 'BEDS', 'none', 'residential', ''),
(33, 'totalrooms', '', 'none', 'residential', ''),
(34, 'livingarea', '', 'none', 'residential', ''),
(35, 'bathrooms', 'BATHSTOTAL', 'none', 'property', ''),
(36, 'ensuite', '', 'none', 'residential', ''),
(37, 'parking', 'GARAGECAP', 'none', 'residential', ''),
(38, 'stories', '', 'none', 'residential', ''),
(39, 'declat', '', 'none', 'property', ''),
(40, 'declong', '', 'none', 'property', ''),
(41, 'vtour', '', 'none', 'residential', ''),
(42, 'com_feature', '', 'none', 'residential', ''),
(43, 'adline', '', 'none', 'residential', ''),
(44, 'alias', '', 'none', 'property', ''),
(45, 'propdesc', 'REMARKS', 'none', 'residential', ''),
(46, 'smalldesc', '', 'none', 'residential', ''),
(247, 'rets_source', 'UID', 'none', 'agent', ''),
(95, 'panorama', '', 'none', 'residential', ''),
(96, 'ctown', '', 'none', 'residential', ''),
(97, 'ctport', '', 'none', 'residential', ''),
(98, 'schooldist', 'SCHOOLDISTRICT', 'none', 'residential', ''),
(99, 'preschool', '', 'none', 'residential', ''),
(100, 'primaryschool', '', 'none', 'residential', ''),
(101, 'highschool', '', 'none', 'residential', ''),
(102, 'university', '', 'none', 'residential', ''),
(103, 'hofees', '', 'none', 'residential', ''),
(104, 'custom1', '', 'none', 'residential', ''),
(105, 'custom2', '', 'none', 'residential', ''),
(106, 'custom3', '', 'none', 'residential', ''),
(107, 'custom4', '', 'none', 'residential', ''),
(108, 'custom5', '', 'none', 'residential', ''),
(109, 'custom6', '', 'none', 'residential', ''),
(110, 'custom7', '', 'none', 'residential', ''),
(111, 'custom8', '', 'none', 'residential', ''),
(112, 'pool', '', 'none', 'property', ''),
(113, 'fplace', '', 'none', 'property', ''),
(114, 'bbq', '', 'none', 'property', ''),
(115, 'gazebo', '', 'none', 'property', ''),
(116, 'lug', '', 'none', 'property', ''),
(117, 'bir', '', 'none', 'property', ''),
(118, 'heating', '', 'none', 'property', ''),
(119, 'airco', '', 'none', 'property', ''),
(120, 'shops', '', 'none', 'property', ''),
(121, 'schools', '', 'none', 'property', ''),
(122, 'elevator', '', 'none', 'property', ''),
(123, 'pets', '', 'none', 'property', ''),
(124, 'furnished', '', 'none', 'property', ''),
(125, 'extra1', '', 'none', 'property', ''),
(126, 'extra2', '', 'none', 'property', ''),
(127, 'extra3', '', 'none', 'property', ''),
(128, 'extra4', '', 'none', 'property', ''),
(129, 'extra5', '', 'none', 'property', ''),
(130, 'extra6', '', 'none', 'property', ''),
(131, 'extra7', '', 'none', 'property', ''),
(132, 'extra8', '', 'none', 'property', ''),
(133, 'openhouse', '', 'none', 'property', ''),
(134, 'ohouse_desc', '', 'none', 'property', ''),
(135, 'takings', '', 'none', 'residential', ''),
(136, 'returns', '', 'none', 'residential', ''),
(137, 'netprofit', '', 'none', 'residential', ''),
(138, 'bustype', '', 'none', 'residential', ''),
(139, 'bussubtype', '', 'none', 'residential', ''),
(140, 'stock', '', 'none', 'residential', ''),
(141, 'fixtures', '', 'none', 'residential', ''),
(142, 'fittings', '', 'none', 'residential', ''),
(143, 'squarefeet', 'SQFTTOTAL', 'none', 'residential', ''),
(144, 'percentoffice', '', 'none', 'residential', ''),
(145, 'percentwarehouse', '', 'none', 'residential', ''),
(146, 'loadingfac', '', 'none', 'property', ''),
(147, 'fencing', '', 'none', 'residential', ''),
(148, 'rainfall', '', 'none', 'residential', ''),
(149, 'soiltype', '', 'none', 'residential', ''),
(150, 'grazing', '', 'none', 'residential', ''),
(151, 'cropping', '', 'none', 'residential', ''),
(152, 'irrigation', '', 'none', 'residential', ''),
(153, 'waterresources', '', 'none', 'residential', ''),
(154, 'carryingcap', '', 'none', 'residential', ''),
(155, 'storage', '', 'none', 'residential', ''),
(156, 'services', '', 'none', 'residential', ''),
(157, 'listdate', '', 'none', 'residential', ''),
(158, 'lastupdate', '', 'none', 'residential', ''),
(159, 'expdate', '', 'none', 'residential', ''),
(160, 'closedate', '', 'none', 'residential', ''),
(161, 'contractdate', '', 'none', 'property', ''),
(162, 'hits', '', 'none', 'property', ''),
(163, 'sold', '', 'none', 'property', ''),
(164, 'published', '', 'none', 'property', ''),
(165, 'checked_out', '', 'none', 'property', ''),
(166, 'checked_out_time', '', 'none', 'property', ''),
(167, 'editor', '', 'none', 'property', ''),
(168, 'owner', '', 'none', 'property', ''),
(169, 'premium', '', 'none', 'property', ''),
(170, 'featured', '', 'none', 'property', ''),
(171, 'camtype', '', 'none', 'property', ''),
(172, 'metadesc', '', 'none', 'property', ''),
(173, 'metakey', '', 'none', 'property', ''),
(174, 'currency_position', '', 'none', 'property', ''),
(175, 'currency', '', 'none', 'property', ''),
(176, 'currency_format', '', 'none', 'property', ''),
(177, 'assoc_agent', '', 'none', 'property', ''),
(178, 'email_status', '', 'none', 'property', ''),
(179, 'ordering', '', 'none', 'property', ''),
(180, 'schoolprof', '', 'none', 'property', ''),
(181, 'hoodprof', '', 'none', 'property', ''),
(182, 'pdfinfo', '', 'none', 'property', ''),
(183, 'ohdate', '', 'none', 'property', ''),
(184, 'ohstarttime', '', 'none', 'property', ''),
(185, 'ohendtime', '', 'none', 'property', ''),
(186, 'ohdate2', '', 'none', 'property', ''),
(187, 'ohstarttime2', '', 'none', 'property', ''),
(188, 'ohendtime2', '', 'none', 'property', ''),
(189, 'offpeak', '', 'none', 'property', ''),
(190, 'owncoords', '', 'none', 'property', ''),
(191, 'vidtype', '', 'none', 'property', ''),
(192, 'availdate', '', 'none', 'property', ''),
(193, 'aucdate', '', 'none', 'property', ''),
(194, 'auctime', '', 'none', 'property', ''),
(195, 'aucdet', '', 'none', 'property', ''),
(196, 'language', '', 'none', 'property', ''),
(197, 'private', '', 'none', 'property', ''),
(198, 'oh_id', '', 'none', 'property', ''),
(200, 'id', '', 'none', 'agent', ''),
(201, 'mid', '', 'none', 'agent', ''),
(202, 'dealer_name', '{FIRSTNAME} {LASTNAME}', 'concat', 'agent', ''),
(203, 'dealer_company', 'OFFICENAME', 'none', 'agent', ''),
(204, 'dealer_type', '', 'none', 'agent', ''),
(205, 'dealer_info', '', 'none', 'agent', ''),
(206, 'dealer_bio', '', 'none', 'agent', ''),
(207, 'dealer_unitnum', '', 'none', 'agent', ''),
(208, 'dealer_address1', '', 'none', 'agent', ''),
(209, 'dealer_address2', '', 'none', 'agent', ''),
(210, 'dealer_locality', '', 'none', 'agent', ''),
(211, 'dealer_pcode', '', 'none', 'agent', ''),
(212, 'dealer_state', '', 'none', 'agent', ''),
(213, 'dealer_country', '', 'none', 'agent', ''),
(214, 'show_addy', '', 'none', 'agent', ''),
(215, 'dealer_declat', '', 'none', 'agent', ''),
(216, 'dealer_declong', '', 'none', 'agent', ''),
(217, 'dealer_email', '', 'none', 'agent', ''),
(218, 'dealer_phone', '', 'none', 'agent', ''),
(219, 'dealer_fax', '', 'none', 'agent', ''),
(220, 'dealer_mobile', '', 'none', 'agent', ''),
(221, 'dealer_sms', '', 'none', 'agent', ''),
(222, 'show_sms', '', 'none', 'agent', ''),
(223, 'dealer_exempt', '', 'none', 'agent', ''),
(224, 'feat_upgr', '', 'none', 'agent', ''),
(225, 'publish_own', '', 'none', 'agent', ''),
(226, 'reset_own', '', 'none', 'agent', ''),
(227, 'dealer_skype', '', 'none', 'agent', ''),
(228, 'dealer_ymsgr', '', 'none', 'agent', ''),
(229, 'dealer_icq', '', 'none', 'agent', ''),
(230, 'dealer_web', '', 'none', 'agent', ''),
(231, 'dealer_blog', '', 'none', 'agent', ''),
(232, 'dealer_image', '', 'none', 'agent', ''),
(233, 'logo_image', '', 'none', 'agent', ''),
(234, 'page_topper', '', 'none', 'agent', ''),
(235, 'pdf_promo', '', 'none', 'agent', ''),
(236, 'rappit', '', 'none', 'agent', ''),
(237, 'linkit', '', 'none', 'agent', ''),
(238, 'calcown', '', 'none', 'agent', ''),
(239, 'published', '', 'none', 'agent', ''),
(240, 'language', '', 'none', 'agent', ''),
(241, 'checked_out', '', 'none', 'agent', ''),
(242, 'checked_out_time', '', 'none', 'agent', ''),
(243, 'editor', '', 'none', 'agent', ''),
(244, 'rets_source', 'MLSNUM', 'none', 'residential', '');

-- --------------------------------------------------------

--
-- Table structure for table `jos_ipals_query_fields`
--

CREATE TABLE IF NOT EXISTS `jos_ipals_query_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(200) NOT NULL,
  `value` varchar(200) NOT NULL,
  `group` varchar(200) NOT NULL DEFAULT '',
  `auto` tinyint(1) NOT NULL DEFAULT '1',
  `type` enum('property','office','agent') NOT NULL,
  `description` varchar(500) NOT NULL COMMENT 'Just a description for manual fields to explain what they do',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=33 ;

--
-- Dumping data for table `jos_ipals_query_fields`
--

INSERT INTO `jos_ipals_query_fields` (`id`, `key`, `value`, `group`, `auto`, `type`, `description`) VALUES
(15, 'queryField', 'MODIFIED', '', 0, '', 'Primary Query Field to determine time range to query.'),
(16, 'openHouseQueryField', '', '', 0, '', 'Primary Query Field to determine time range to query for open houses.'),
(30, 'INTERNETLIST_ALL', '*OMWS*', '', 1, 'property', ''),
(29, 'INTERNETDISPLAYYN', 'Y', '', 1, 'property', ''),
(28, 'COUNTY', 'Tarrant', '', 1, 'property', ''),
(27, 'COUNTY', 'Rockwall', '', 1, 'property', ''),
(26, 'COUNTY', 'Denton', '', 1, 'property', ''),
(25, 'COUNTY', 'Dallas', '', 1, 'property', ''),
(24, 'COUNTY', 'Collin', '', 1, 'property', ''),
(31, 'UIDOFFICE', 'MCR01C', '', 1, 'agent', ''),
(32, 'UID', 'MCR01C', '', 1, 'office', '');

-- --------------------------------------------------------

--
-- Table structure for table `jos_ipals_settings`
--

CREATE TABLE IF NOT EXISTS `jos_ipals_settings` (
  `key` varchar(200) NOT NULL,
  `value` mediumtext NOT NULL,
  `category` varchar(100) NOT NULL,
  `description` varchar(350) NOT NULL,
  UNIQUE KEY `key` (`key`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jos_ipals_settings`
--

INSERT INTO `jos_ipals_settings` (`key`, `value`, `category`, `description`) VALUES
('licenseKey', 'Enter iPALSPro-KEY', 'License', 'iPALS license key'),
('retsServer', '', 'RETS Login', 'RETS server used to access MLS data'),
('retsUsername', '', 'RETS Login', ''),
('retsPassword', '', 'RETS Login', ''),
('retsVersion', '1.5', 'RETS Login', ''),
('retsHeaderVersion', 'RETS/1.5', 'RETS Login', ''),
('retsUserAgent', 'OMC-iPALS/2.0', 'RETS Login', ''),
('retsLogin', '', 'RETS Login', ''),
('retsUserAgentPassword', '', 'RETS Login', ''),
('agentMidTable', '', 'Database', ''),
('agentTable', 'ezrealty_profile', 'Database', ''),
('officeTable', '', 'Database', ''),
('imageTable', 'ezrealty_images', 'Database', ''),
('propertyMidTable', '', 'Database', ''),
('propertyTable', 'ezrealty', 'Database', ''),
('settingsTable', '', 'Database', ''),
('openHouseTable', '', 'Database', ''),
('amenitiesTable', '', 'Database', ''),
('batchLimit', '5', 'Process Length', 'Number of batches to run per script execution'),
('processLength', '+1 day', 'Process Length', 'A relative time period to proccess in a batch, acceptable by PHP''s strtotime ()http://www.php.net/manual/en/function.strtotime.php)'),
('dateFormat', 'Y-m-d\\TH:i:s', 'RETS Query Data', 'PHP date() valid date recognized by RETS'),
('retsDebugMode', '0', 'Debug', 'RETS Debug Mode'),
('limit', '50000', 'Process Length', 'Max number of items to process'),
('serverBreather', '5', 'Process Length', '(not used?)'),
('retsPhotoObjectName', 'Photo', 'Image', 'Used to get photos form RETS'),
('amazonS3', '0', 'Image', 'Upload to Amazon S3 if supported'),
('amazonBucket', 'ipals-test', 'Image', ''),
('amazonEndPoint', 's3.amazonaws.com', 'Image', ''),
('getPhotos', '1', 'Image', ''),
('imagePath', 'photos', 'Image', 'Image path as access by plugin'),
('imageLibrary', 'GD2', 'Image', 'Image Library to use to process images (only GD2 may work for now...)'),
('quickPhoto', '1', 'Image', 'Process photos as they are being downloaded.'),
('tempPath', 'photos/temp', 'Image', 'temp path for unprocessed photos'),
('thumbWidth', '150', 'Image', 'Width of thumbnail images'),
('thumbHeight', '112', 'Image', 'Height of thumbnail images'),
('fullWidth', '710', 'Image', 'Width of full size images'),
('fullHeight', '533', 'Image', 'Height of full size images'),
('processGeocode', '1', 'Geocode', 'Don''t even process geocode if this is false'),
('geocodeAddress', '{street} {city}, {postcode}', 'Geocode', 'Fields from this database used to process geocode address in "concat" format (i.e. "{street} {zipcode}")'),
('propertyResource', 'Property', 'RETS Query Data', 'RETS property resource'),
('propertyClasses', '{"residential":"RES","land":"","commercial":"","multi-family":"","custom-1":"","custom-2":"","custom-3":"","custom-4":"","custom-5":"","custom-6":""}', 'RETS Query Data', 'Mapping of property subtypes to classes'),
('openHouseResource', '', 'RETS Query Data', 'RETS open house resource'),
('openHouseClasses', '{"residential":"RES","land":"","commercial":"","multi-family":"","custom-1":"","custom-2":"","custom-3":"","custom-4":"","custom-5":"","custom-6":""}', 'RETS Query Data', 'RETS open house classes'),
('agentResource', 'AGENT', 'RETS Query Data', 'RETS agent resource'),
('agentClass', 'AGENT', 'RETS Query Data', 'RETS agent class'),
('officeResource', '', 'RETS Query Data', 'RETS office resource'),
('officeClass', '', 'RETS Query Data', 'RETS office class'),
('updateOnDuplicateKey', '1', 'Debug', 'For testing purposes, you can skip properties already in the database (not used?)'),
('statusDelete', 'LISTSTATUS', 'RETS Query Data', 'RETS key to check if we should delete'),
('statusDeleteValues', '["Cancelled","Expired","Temp Off Market","Withdrawn Sublisting","Withdrawn"]', 'RETS Query Data', 'List of values that would trigger a delete (one item per line)'),
('statusAccept', 'LISTSTATUS', 'RETS Query Data', 'RETS key to check if we should import'),
('statusAcceptValues', '{"Active":"1","Active Contingent":"1","Active Kick Out":"1","Active Option Contract":"1","Pending":"3","Sold":"5"}', 'RETS Query Data', 'List of values that would trigger a import (one item per line)'),
('proGeocode', '1', 'Geocode', ''),
('defaultAgent', '1', 'Misc', 'You can manually set a default agent (use ID of an agent in this database)'),
('defaultOffice', '1', 'Misc', 'You can manually set a default office (use ID of an office in this database)'),
('cleanupResource', 'Property', 'Housekeeping', 'Resource to cleanup (usually property resource)'),
('cleanupClasses', '["ResidentialProperty"]', 'Housekeeping', 'Property classes used to lean up (one per line)'),
('cleanupKey', 'ListingID', 'Housekeeping', 'key used to compare to ip_source'),
('cleanupRetsQuery', '(MLSNUM=0+)', 'Housekeeping', 'Query used to get ALL properties in RETS'),
('lastRun', '2012-04-27 19:31:51', 'Misc', 'Time script was last ran (only override if you know what you''re doing)'),
('agentID', 'ListAgentAgentID', 'RETS Query Data', 'RETS key used to look up agent for agentmid table'),
('local_key', '', 'private', 'This is for SPBAS'),
('plugin', 'ezrealty', 'Misc', 'Special value that corresponds to which code to load'),
('cleanupScript', '0', 'Housekeeping', 'Whether or not to use the cleanup script at the end of the run (NYI)'),
('verboseLevel', '2', 'Debug', '0, 1 or 2 amount of verbosity'),
('retsID', 'rets_source', 'Misc', 'Field name of Unique ID from RETS as it exists in this database.'),
('thumbPath', 'photos', 'Image', 'Path to the thumbs folder (may be same as image folder if thumb post- or prefix is non-blank)'),
('thumbPrefix', '', 'Image', 'Optional prefix for thumbnails'),
('thumbPostfix', '_thumb', 'Image', 'Optional thumb postfix'),
('dbRefreshRate', '10', 'Database', 'Number of records to process before refreshing DB connection. -1 to never refresh.'),
('siteRoot', '/Users/cameron/Sites/vhosts/ipals.cam/', 'Image', 'Absolute path from / (w/ ending slash)');
