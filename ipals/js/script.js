/* Author:
Cameron Spear
*/

$(document).ready(function() {
	var baseURL = $('#base-url').text();
	
	$('[rel="tooltip"]').tooltip();
	$('.close').click(function() {
		$(this).closest('.alert').slideUp();
	});

	$(document).on('keypress', '.add-parameter:last .add-parameter-value', function(event) {
		var $addParam = $('.add-parameter:last').clone();
		$addParam.children('input').val('');
		$(this).parent().append($('.query-field-remove-row').clone().eq(0));

		$addParam.appendTo($('.add-parameter').parent());
	});
	
	$(document).on('click', '.query-field-remove-row', function(event) {
		event.preventDefault();
		$(this).parent().remove();
	})
	
	$('#import_fields').click(function() {
		var url = baseURL + 'index.php/ajax/import_fields/' + $('#table_name').val();
		$('#fields').load(url);
	});

	$('.cancel-new').click(function() {
		$('#add-new-form')[0].reset();
	});

	$('#add-new-form').submit(function(event) {
		if($(this).find('#field').val() == '') {
			event.preventDefault();
			$('#form-error').slideDown().find('.text').text('Field \'field\' is required.');
		}
	});

	$('#delete-modal, #add-new-modal, #cross-modal, #conditional-modal').modal({show: false, backdrop: 'static', keyboard: false});

	$('.manage .delete').click(function() {
		$('.confirm-delete').data('id', $(this).data('id'));
	});

	$('.confirm-delete').click(function() {
		var $this = $(this);

		$this.button('loading');
		$('.cancel-modal').button('loading');

		var id = $this.data('id');
		$.getJSON(baseURL + 'index.php/admin/delete_field/' + id, function(response) {
			if(response.success) {
				$('#delete-modal').modal('hide');
				$('.cancel-modal').button('reset');
				$this.button('reset');
				$this.removeData('id');

				$('span[data-id="' + response.id + '"]').closest('tr').remove();
			} else {
				console.log(response);
			}
		});
	});

	$(document).on('click', '.manage .edit', function() {
		$tr = $(this).closest('tr');
		$tr.find('td').each(function() {
			if(!$(this).hasClass('manage')) {
				var w = $(this).width();
				var name = $(this).data('name');
				var value = $(this).text();
				// var value = $(this).find('span').text();

				if(name == 'action' || name == 'type') {
					$elem = $('#' + name + '-options').clone();
					$elem.val(value).css({width: w-10, margin:0});
					$(this).html($elem);
				} else {
					$(this).html('<input type="text" style="width:' + (w - 10) + 'px;margin:0" value="' + value + '" name="' + name + '">')
				}
			}
		});
		$(this).removeClass('label-info edit').addClass('label-success save').text('Save');
	});

	$(document).on('click', '.manage .save', function() {
		$tr = $(this).closest('tr');

		if($tr.find('input[name="field"]').eq(0).val() == '') { alert('Field name cannot be blank.'); return; }

		var data = {};
		$tr.find('input, select').each(function() {
			if(!$(this).hasClass('manage')) {
				data[$(this).prop('name')] = $(this).val();

				$(this).closest('td').html($(this).val());
				// $(this).closest('td').html('<span>' + $(this).val() + '</span>');
			}
		});
		$(this).addClass('label-info edit').removeClass('label-success save').text('Edit');

		data['id'] = $(this).data('id');

		$.getJSON(baseURL + 'index.php/admin/update_field/', data, function(response) {
			// console.log(response);
		});
	});

	$(document).on('click', '.edit-all', function() {
		$('.manage .edit').click();
		// $(this).addClass('btn-success save-all').removeClass('btn-infoNO edit-all').text('Save All');
	});

	$(document).on('click', '.save-all', function() {
		$('.manage .save').click();
		// $(this).addClass('btn-infoNO edit-all').removeClass('btn-success save-all').text('Edit All');
	});

	$('tr').dblclick(function() {
		$(this).find('.manage .edit').click();
	});

	$(document).on('keypress', 'input[type="text"]', function(event) {
		if(event.keyCode == 13) {
			$(this).closest('tr').find('.manage .save').click();
		}
	});

	$('#conditional-row').on('keypress', '.conditional-value:last', function() {
		var $clone = $('.conditonal-clone').eq(0).clone();
		$clone.find('input').val('');

		$('#conditional-row').append($clone);
	});

	// edit form stuff
	$('.add-action').change(function() {
		if($(this).val() == 'cross' || $(this).val() == 'conditional') {
			$(this).closest('form').find('input[name="key"]').addClass('input-medium').prop('readonly', true);
			$('.edit-special-field').show();
		} else {
			$(this).closest('form').find('input[name="key"]').removeClass('input-medium').prop('readonly', false);
			$('.edit-special-field').hide();
		}
	});

	// $(document).on('change', 'td.action select', function() {
	// 	var $tar = $(this).closest('tr').find('.key input');
	// 	console.log($tar);

	// 	if($(this).val() == 'cross' || $(this).val() == 'conditional') {
	// 		$tar.prop('readonly', true).width($tar.width('-=50'));
	// 		$('.edit-special-field-inline').show();
	// 	} else {
	// 		$tar.prop('readonly', false).width($tar.width('+=50'));
	// 		$('.edit-special-field-inline').show();
	// 	}
	// });


	$('.edit-special-field').click(function(event) {
		event.preventDefault;
		var action = $('.add-action').val();
		$('#add-new-modal').modal('hide');
		$('#' + action + '-modal').modal('show');

		var id = $(this).prev('input[name="key"]').val();

		if(id != '') {
			$.getJSON(baseURL + 'index.php/admin/get_' + action + '/' + id, function(response) {
				if(action == 'cross') {
					$.each(response, function(name, value) {
						if(name == 'add_if_not_exists') {
							$('#add-new-cross').find('input[name="' + name + '"]').prop('checked', value);							
						} else {
							$('#add-new-cross').find('input[name="' + name + '"]').val(value);							
						}
					});
				} else if(action == 'conditional') {
					$('#add-new-conditional').find('input[name="id"]').val(response.id);
				}
			});
		}
	});

	$('#add-new-cross').submit(function(event) {
		event.preventDefault();
		var $this = $(this);

		var required = {'key':'Key', 'table':'Table', 'source':'Source', 'destination':'Destination'};
		var message = '';
		$.each(required, function(name, value) {
			if($this.find('input[name="' + name + '"]').eq(0).val() == '') { 
				message += '<br>' + value + ' cannot be blank.'; 
			}
		});

		if(message != '') {
			$('#cross-form-error').slideDown().find('.text').html(message);
			return;
		}

		$.getJSON(baseURL + 'index.php/admin/save_cross', $(this).serialize(), function(response) {
			if(response.success) {
				$('#cross-modal').modal('hide');
				$('#add-new-modal').modal('show').find('input[name="key"]').val(response.id);
			} else {
				$('#cross-form-error').slideDown().find('.text').text('There was an unknown error.');
			}
		});
	});

	$('#add-new-conditional').submit(function(event) {
		event.preventDefault();
		var $this = $(this);

		var required = {'key':'Key'};
		var message = '';
		$.each(required, function(name, value) {
			if($this.find('input[name="' + name + '"]').eq(0).val() == '') { 
				message += '<br>' + value + ' cannot be blank.'; 
			}
		});

		if(message != '') {
			$('#conditional-form-error').slideDown().find('.text').html(message);
			return;
		}

		$.getJSON(baseURL + 'index.php/admin/save_conditional', $(this).serialize(), function(response) {
			if(response.success) {
				$('#conditional-modal').modal('hide');
				$('#add-new-modal').modal('show').find('input[name="key"]').val(response.id);
			} else {
				if(response.code == '2') {
					$('#conditional-form-error').slideDown().find('.text').text('You must provide at least one conditional pair.');
				} else {
					$('#conditional-form-error').slideDown().find('.text').text('There was an unknown error.');
				}
			}
		});
	});
	
	
	// Settings Page
	$('#statusAcceptValues-table').on('keypress', 'td input:last', function() {
		var $clone = $('#statusAcceptValues-table').find('.statusAcceptValues-row').eq(0).clone();
		$clone.find('input').val('');
		$('#statusAcceptValues-table').append($clone);
	});
	
	$('#statusAcceptValues-table').on('click', '.statusAcceptValues-remove', function() {
		$(this).closest('tr').remove();
	});
	
	$('.settings-toggle').click(function() {
		$(this).next().slideToggle();
	});
	
	$('#hide-all-settings').click(function() {
		$('.settings-toggle').next().slideUp();
	})
	
	$('#show-all-settings').click(function() {
		$('.settings-toggle').next().slideDown();
	})
	
	$('#map-filter').change(function() {
		var filter = $(this).val();
		if(filter == 'none') {
			$('table tbody tr').removeClass('hide');
		} else {
			$('table tbody tr').each(function() {
				var type = $(this).find('td.type select').val(); // in edit mode
				if(type === undefined) type = $(this).find('td.type').text();
				if(type != filter) {
					$(this).addClass('hide');
				} else {
					$(this).removeClass('hide');
				}
			});
		}
		
		window.localStorage.setItem('map-filter', filter);
	});
	
	if(window.localStorage.getItem('map-filter')) {
		$('#map-filter').val(window.localStorage.getItem('map-filter')).change();
	}
	$('.settings-toggle').next().show();
	$('#settings-form .row').masonry({
		itemSelector: '.span4',
	    columnWidth: function( containerWidth ) {
	      return containerWidth / 3;
	    }
		
	});
	
	// keep items open in IE9 for masonry to work
	if(!($.browser.msie && parseInt($.browser.version) == 9)) {
		$('.settings-toggle').next().hide();
	}
});

// localStroage fallback
if (!window.localStorage) {
  Object.defineProperty(window, "localStorage", new (function () {
    var aKeys = [], oStorage = {};
    Object.defineProperty(oStorage, "getItem", {
      value: function (sKey) { return sKey ? this[sKey] : null; },
      writable: false,
      configurable: false,
      enumerable: false
    });
    Object.defineProperty(oStorage, "key", {
      value: function (nKeyId) { return aKeys[nKeyId]; },
      writable: false,
      configurable: false,
      enumerable: false
    });
    Object.defineProperty(oStorage, "setItem", {
      value: function (sKey, sValue) {
        if(!sKey) { return; }
        document.cookie = escape(sKey) + "=" + escape(sValue) + "; path=/";
      },
      writable: false,
      configurable: false,
      enumerable: false
    });
    Object.defineProperty(oStorage, "length", {
      get: function () { return aKeys.length; },
      configurable: false,
      enumerable: false
    });
    Object.defineProperty(oStorage, "removeItem", {
      value: function (sKey) {
        if(!sKey) { return; }
        var sExpDate = new Date();
        sExpDate.setDate(sExpDate.getDate() - 1);
        document.cookie = escape(sKey) + "=; expires=" + sExpDate.toGMTString() + "; path=/";
      },
      writable: false,
      configurable: false,
      enumerable: false
    });
    this.get = function () {
      var iThisIndx;
      for (var sKey in oStorage) {
        iThisIndx = aKeys.indexOf(sKey);
        if (iThisIndx === -1) { oStorage.setItem(sKey, oStorage[sKey]); }
        else { aKeys.splice(iThisIndx, 1); }
        delete oStorage[sKey];
      }
      for (aKeys; aKeys.length > 0; aKeys.splice(0, 1)) { oStorage.removeItem(aKeys[0]); }
      for (var iCouple, iKey, iCouplId = 0, aCouples = document.cookie.split(/\s*;\s*/); iCouplId < aCouples.length; iCouplId++) {
        iCouple = aCouples[iCouplId].split(/\s*=\s*/);
        if (iCouple.length > 1) {
          oStorage[iKey = unescape(iCouple[0])] = unescape(iCouple[1]);
          aKeys.push(iKey);
        }
      }
      return oStorage;
    };
    this.configurable = false;
    this.enumerable = true;
  })());
}